\documentclass[dvipsnames]{beamer}

%% general
%% --------------------------------------------------------------------------------
\usepackage{xcolor}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{hyperref}
\usepackage{fontspec}
\usepackage{proof}
\usepackage{tikz-cd}

%% Beamer
%% --------------------------------------------------------------------------------
\usetheme{Boadilla}
\setbeamertemplate{navigation symbols}{}
\setbeamertemplate{footline}[frame number]
\setbeamerfont{itemize/enumerate subbody}{size=\normalsize}
\setbeamerfont{itemize/enumerate subsubbody}{size=\normalsize}


%% Bibliography
%% --------------------------------------------------------------------------------
\bibliographystyle{apalike}
\setbeamerfont{bibliography item}{size=\footnotesize}
\setbeamerfont{bibliography entry author}{size=\footnotesize}
\setbeamerfont{bibliography entry title}{size=\footnotesize}
\setbeamerfont{bibliography entry location}{size=\footnotesize}
\setbeamerfont{bibliography entry note}{size=\footnotesize}
\setbeamertemplate{bibliography item}{}

%% Monofont
%%--------------------------------------------------------------------------------
\setmonofont[Scale=0.7]{DejaVu Sans Mono}

%% square dot
\makeatletter
\DeclareRobustCommand{\sqcdot}{\mathbin{\mathpalette\morphic@sqcdot\relax}}
\newcommand{\morphic@sqcdot}[2]{%
  \sbox\z@{$\m@th#1\centerdot$}%
  \ht\z@=.33333\ht\z@
  \vcenter{\box\z@}%
}
\makeatother


%% --------------------------------------------------------------------------------
\input{abbrevs.tex}
\newcommand{\cR}[1]{{\color{Red}{#1}}}
\newcommand{\targetass}{\cR{\Gamma};}
\newcommand{\semicol}{\cR;\,}
\newcommand{\A}{\mathsf{A}}
\newcommand{\F}{\mathsf{F}}
\newcommand{\D}{\mathsf{D}}
\renewcommand{\S}{\mathsf{S}}


%% --------------------------------------------------------------------------------

%% TODO: adjust beginning (what are HIITs, what is the content of the talk)

%% --------------------------------------------------------------------------------

\title{A Syntax for Higher Inductive-Inductive Types\thanks{This work was supported by the European Union, co-financed by the European
    Social Fund (EFOP-3.6.3-VEKOP-16-2017-00002).}}

\author{Ambrus Kaposi, \textbf{András Kovács}}
\institute{Eötvös Loránd University, Budapest}
\date{FSCD, Oxford, 10 July 2018}

%% --------------------------------------------------------------------------------

\AtBeginSection[]
{
  \begin{frame}
    \frametitle{Contents}
    \tableofcontents[currentsection]
  \end{frame}
}


%% --------------------------------------------------------------------------------

\begin{document}

\frame{\titlepage}

\begin{frame}{Motivation, overview}

Higher inductive types (HITs) allow inductive equality (path)
constructors.

\pause
\vspace{1em}
Numerous specific HITs are used in homotopy type theory.

\pause
\vspace{1em}
We would like a definition for what well-formed HITs are, and what the
corresponding induction principles are, for a class of HITs that
covers most interesting cases.

\pause
\vspace{1em}
We propose a syntax for higher inductive-inductive types (HIITs), which
covers almost every HIT in the wild.

\pause
\vspace{1em}
We have formalized this work in Agda, and also implemented a HIIT-checker
and eliminator-generator as a standalone program, both available at \url{https://bitbucket.org/akaposi/elims}.

\pause
\vspace{1em} Future work: (higher) categorical semantics, existence.

\end{frame}

%% --------------------------------------------------------------------------------

\begin{frame}{Outline}
\tableofcontents
\end{frame}

%% --------------------------------------------------------------------------------

\section{Inductive types, in general}

%% --------------------------------------------------------------------------------

%% \begin{frame}{Inductive types}

%%   We want to extend a type theory with freely and finitely generated structures, in a way such that:\pause
%%   \begin{itemize}
%%   \item Consistency is preserved.\pause
%%   \item We can do \emph{induction} on the structure, internalizing the property of being ``freely generated''.\pause
%%   \item Induction has appropriate \emph{computational behavior}.\pause
%%   \end{itemize}
%% \vspace{1em}
%% We want to characterize large classes of inductive types (higher inductive-inductive) which satisfy the above criteria.

%% \end{frame}

\begin{frame}[fragile]{Natural numbers as usual}

In pseudo-Agda.

\begin{verbatim}
    data ℕ : Type where
      zero : ℕ
      suc  : ℕ → ℕ

    ℕ-ind :
        (P : ℕ → Type)
      → P zero
      → ((n : ℕ) → P n → P (suc n))
      → (n : ℕ) → P n
    ℕ-ind P z s zero    = z
    ℕ-ind P z s (suc n) = s n (ℕ-ind P z s n)

\end{verbatim}
\end{frame}

\begin{frame}[fragile]{Alternatively}

\begin{verbatim}
    ℕ-Algebra : Type
    ℕ-Algebra = Σ(N : Type) × N × (N → N)

    ℕ-DisplayedAlg : ℕ-Algebra → Type
    ℕ-DisplayedAlg (N, z, s) =
      Σ(Nd : N → Type) × Nd z × ((n : N) → Nd n → Nd (s n))

    ℕ-Section : (α : ℕ-Algebra) → ℕ-DisplayedAlg α → Type
    ℕ-Section (N, z, s) (Nd, zd, sd) =
      Σ(Ns : (n : N) → Nd n) × (Ns z = zd)
                             × ((n : N) → Ns (s n) = sd n (Ns n))
\end{verbatim}
\small{Then, the following are definable in Agda/Coq:}
\begin{verbatim}
    ℕ           : ℕ-Algebra
    ℕ-Induction : (D : ℕ-DisplayedAlg ℕ) → ℕ-Section ℕ D
\end{verbatim}
\footnotesize{(We borrow ``displayed'' from \cite{displayedCategories})}
\end{frame}

\begin{frame}[fragile]

{\color{ForestGreen}Initial algebra}, {\color{blue}displayed algebra over initial algebra}, {\color{red}section of displayed algebra}.

{\color{ForestGreen}\begin{verbatim}
    data ℕ : Type where
      zero : ℕ
      suc  : ℕ → ℕ\end{verbatim}\vspace{-1em}}
\begin{verbatim}
    ℕ-ind :
\end{verbatim}\vspace{-2.2em}
{\color{blue}\begin{verbatim}
        (P : ℕ → Type)
      → P zero
      → ((n : ℕ) → P n → P (suc n))
\end{verbatim}}\vspace{-2.2em}
{\color{red}\begin{verbatim}
      → (n : ℕ) → P n
    ℕ-ind P z s zero    = z
    ℕ-ind P z s (suc n) = s n (ℕ-ind P z s n)
\end{verbatim}}

\end{frame}

\begin{frame}[fragile]{Induction in general}

For each inductive type, we need notions of:

\begin{verbatim}
    Algebra       : Type
    DisplayedAlg  : Algebra → Type
    Section       : (α : Algebra) → DisplayedAlg α → Type
\end{verbatim}
\pause
Such that the following exist (we don't show this in the current work):

\begin{verbatim}
    InitialAlg : Algebra
    Induction  : (D : DisplayedAlg InitialAlg) → Section InitialAlg D
\end{verbatim}
\pause
{\footnotesize(There are more laws and operations on displayed algebras and sections which we could sensibly require)}
\end{frame}

\begin{frame}{Terminology}

\begin{center}
  \begin{tabular}{c | c}
    constructors & initial algebra \\
    induction motives and methods & displayed algebra over initial algebra \\
    eliminators and $\beta$-rules & section of displayed algebra
  \end{tabular}
\end{center}


\end{frame}


\section{Syntax and induction for HIITs}

\begin{frame}
\vspace{1em}
\begin{center}
\large{Desired features of valid HIIT signatures}
\end{center}
\end{frame}

\begin{frame}[fragile]{Possible dependencies}

Type/term/path constructors depending on any previous constructor.

\vspace{1em}
Example for type-type and term-term dependencies: a fragment of a syntax of a type theory \cite{ttintt}.

\begin{verbatim}
    Con : Type
    Ty  : Con → Type
    ∙   : Con
    _▶_ : (Γ : Con) → Ty Γ → Con
    Pi  : (Γ : Con)(A : Ty Γ) → Ty (Γ ▶ A) → Ty Γ
    ...
\end{verbatim}

Other examples: Cauchy reals, surreal numbers.
\end{frame}

\begin{frame}[fragile]{Referring to external signature}

  We want to refer to already existing ``external'' constants.

\vspace{1em}
For example, to natural numbers and a given \texttt{A} element type for length-indexed vectors.
\begin{verbatim}
    Vec  : ℕ → Type
    nil  : Vec zero
    cons : (n : ℕ) → A → Vec n → Vec (suc n)
\end{verbatim}

\end{frame}

\begin{frame}[fragile]{Path constructors}

At possibly higher dimensions, with recursive paths, e.g. in set truncation for some external \texttt{A} type:
\begin{verbatim}
    ‖A‖₀  : Type
    |_|₀  : A → ‖A‖₀
    trunc : (x y : ‖A‖₀)(p q : x = y) → p = q
\end{verbatim}
Possibly with path induction on previous paths, as in the definition of the torus, where \texttt{◾} denotes path composition:
\begin{verbatim}
    T² : Type
    b  : T²
    p  : b = b
    q  : b = b
    t  : p ◾ q = q ◾ p
\end{verbatim}
\end{frame}

\begin{frame}[fragile]{Strict positivity}

An illegal signature:
\vspace{1.5em}
\setmonofont[Scale=0.9]{DejaVu Sans Mono}
\begin{verbatim}
                   Tm  : Type
                   con : (Tm → Tm) → Tm
\end{verbatim}
\setmonofont[Scale=0.7]{DejaVu Sans Mono}

\end{frame}

\begin{frame}{HIIT signatures}

Type dependencies are best described by a type theory.

\pause
\vspace{1.5em}
Every HIIT signature is a context in a \emph{type theory of signatures}.

\pause
\vspace{1.5em}
Strict positivity is enforced by a \emph{universe} and typing rules for functions.

\pause
\vspace{1.5em}
We compute notions of algebras, displayed algebras and sections by induction on the syntax.

\end{frame}

\begin{frame}{Theory of signatures: setup}

Algebras, displayed algebras, sections given by syntactic translation from ToS to
a conventional ``target'' type theory.

\pause
\vspace{1em}
The target theory has $\cR{\Sigma}$-types, dependent functions (denoted $\cR{(x : A)\ra B}$), identity, unit type and Russell-style universes, and has expressions in \cR{red}.

\pause
\vspace{1em}
In the ToS, everything additionally depends on a target theory context, which serves as the source of non-inductive external symbols.

\end{frame}

\begin{frame}{Theory of signatures (1)}

Getting closed inductive-inductive types: by a universe and a ``strictly positive'' function space.

\[
\begin{gathered}
  \infer{\targetass\Delta \vdash \U}{}
\end{gathered}
\hspace{2em}
\begin{gathered}
  \infer{\targetass\Delta \vdash \underline{a}}{\targetass\Delta \vdash a : \U}
\end{gathered}
\]

\[
\begin{gathered}
  \infer{\targetass\Delta \vdash (x:a)\ra B}{\targetass\Delta \vdash a : \U && \targetass\Delta,x:\underline{a} \vdash B}
\end{gathered}
\hspace{2em}
\begin{gathered}
  \infer{\targetass\Delta \vdash t\, u : B[x \mapsto u]}{\targetass\Delta \vdash t : (x:a)\ra B && \targetass\Delta \vdash u : \underline{a}}
\end{gathered}
\]

\vspace{1em}
Signature of natural numbers:
\[
\cR{\cdot}\vdash\cdot,\,\,\,Nat : \U,\,\,\,zero:\underline{Nat},\,\,\,suc:Nat\ra\underline{Nat}
\]

\end{frame}


\begin{frame}{Theory of signatures (2)}

Universe is closed under equality of small terms, yielding recursive equalities and higher constructors.

\[
\begin{gathered}
  \infer{\targetass\Delta \vdash t=_a u : \U}{\targetass\Delta \vdash a : \U && \targetass\Delta \vdash t : \underline{a} && \targetass\Delta \vdash u : \underline{a}}
\end{gathered}
\hspace{2em}
\begin{gathered}
  \infer{\targetass\Delta \vdash \refl : \underline{t=_a t}}{\targetass\Delta \vdash t : \underline{a}}
\end{gathered}
\]
{\footnotesize(+ path induction with propositional $\beta$-rule)}
\vspace{2em}

Signature of circle:
\[
\cR{\cdot}\vdash\cdot,\,\,\,S^1:\U,\,\,\,base:\underline{S^1},\,\,\,loop:\underline{base =_{S^1} base}
\]

\end{frame}

\begin{frame}{Theory of signatures (3)}

  Non-inductive parameters, infinitary constructors (application rules omitted):

\[
\begin{gathered}
  \infer{\cR{\Gamma};\Delta \vdash (\cR{x : A})\ra B}
        {\cR{\Gamma \vdash A : \Type} && \cR{\Gamma}\vdash\Delta && (\cR{\Gamma,\,x : A}); \Delta \vdash B}
\end{gathered}
\]
\[
\begin{gathered}
  \infer{\cR{\Gamma};\Delta \vdash (\cR{x : A})\ra b : \U}
        {\cR{\Gamma \vdash A : \Type} && \cR{\Gamma}\vdash\Delta && (\cR{\Gamma,\,x : A});\Delta \vdash b : \U}
\end{gathered}
\]

\vspace{1em}
Signature of $W$-types:
\[
\cR{S : \Type,\,P: S\ra\Type}\vdash\,\cdot,\,\,\,W:\U,\,\,\,sup: (\cR{s : S})\ra((\cR{p : P\,s})\ra W)\ra \underline{W}
\]

\end{frame}

\begin{frame}{Algebras: standard model}

Specification:
\[
\infer{\cR{\Gamma \vdash \Delta^\A : \Type}}{\cR{\Gamma}\vdash\Delta}
\hspace{2em}
\infer{\cR{\Gamma\vdash A^\A : \Delta^\A \ra \Type}}{\cR{\Gamma};\Delta\vdash A}
\hspace{2em}
\infer{\cR{\Gamma\vdash t^\A : (\delta : \Delta^\A)\ra A^\A\,\delta}}{\cR{\Gamma};\Delta\vdash t : A}
\]

Action:
\begin{alignat*}{5}
  & \cdot^\A && :\equiv \cR{\top} \\
  & (\Delta,x:A)^\A && :\equiv \cR{\Sigma(\delta : \Delta^\A)\times A^\A\,\delta} \\
  & x^\A\,\cR{\delta} && :\equiv x^{\text{th}}\text{ component in } \cR{\delta} \\ % TODO: write De Bruijn versions
  & \U^\A\,\cR{\delta} && :\equiv \cR{\Type} \\
  & (\underline{a})^\A\,\cR{\delta} && :\equiv \cR{a^\A\,\delta} \\
  & ((x:a)\ra B)^\A\,\cR{\delta} && :\equiv \cR{(x : a^\A\,\delta)\ra B^\A\,(\delta,\,x)}\\
  & \ldots
\end{alignat*}

\end{frame}

%---------------------------------------------------------------------

\begin{frame}{Displayed algebras: logical predicate interpretation}

Analogously to \cite{bernardy12parametricity}. Specification:
\[
\infer{\cR{\Gamma \vdash \Delta^\D : \Delta^\A \ra \Type}}
      {\cR{\Gamma}\vdash\Delta}
\hspace{1em}
\infer{\cR{\Gamma \vdash
           A^\D : (\delta : \Delta^\A)\ra \Delta^\D\,\delta\ra A^\A\,\delta\ra\Type}}
      {\cR{\Gamma};\Delta\vdash A}
\]
\[
\infer{\cR{\Gamma \vdash t^\D : (\delta : \Delta^\A)\ra(\delta^D : \Delta^\D\,\delta)\ra A^\D\,\delta\,\delta^D\,(t^\A\,\delta)}}
      {\cR{\Gamma};\Delta\vdash t : A}
\]
Action:
\begin{alignat*}{5}
  & \cdot^\D\,\cR{\delta} && :\equiv \cR{\top} \\
  & (\Delta,\,x:A)^\D\,\cR{\delta\,\delta^D} && :\equiv
    \cR{\Sigma(\delta^D : \Delta^\D\,\delta)\times A^\D\,\delta\,\delta^\D} \\
  %% & x^\D\,\cR{\delta\,\delta^D} && :\equiv x^{\text{th}}\text{ component in } \cR{\delta^D} \\
  & \U^\D\,\cR{\delta\,\delta^\D\,A} && :\equiv \cR{A\ra\Type} \\
  %% & (\underline{a})^\D\,\cR{\delta\,\delta^D\,t} && :\equiv \cR{a^\D\,\delta\,\delta^D\,t} \\
  & ((x:a)\ra B)^\D\,\cR{\delta\,\delta^D\,f} && :\equiv
    \cR{ (x : a^\A\,\delta)(x^D : a^\D\,\delta\,\delta^D\,x)
      \ra B^\D\,(\delta\,x)\,(\delta^D\,x^D)\,(f\,x)} \\
  & \ldots
\end{alignat*}


\end{frame}


\begin{frame}
  \begin{itemize}
  \item<1-> Algebras and displayed algebras are given by known interpretations.
  \item<2-> Moreover, these interpretations work regardless of strict positivity.
  \item<3-> This was an early motivation of \cite{reynolds83abstraction} for logical relations vs. homomorphisms, since the latter don't work for negative signatures.
  \item<4-> For strictly positive signatures, we can recover homomorphisms and sections (which are ``dependent'' homomorphisms).
  \end{itemize}
\end{frame}

\begin{frame}{Sections (1)}

  Specification:

\[
\infer{\cR{\Gamma \vdash \Delta^\S :
       (\delta : \Delta^\A)\ra \Delta^\D\,\delta \ra \Type}}
      {\cR{\Gamma}\vdash\Delta}
\]
\[
\infer{\cR{\Gamma \vdash
           A^\S : (\delta : \Delta^\A)(\delta^D : \Delta^\D\,\delta)
           (\delta^S : \Delta^\S\delta\,\delta^D)(\alpha : A^\A\,\delta)
           \ra A^\D\,\delta\,\delta^D\,\alpha\ra\Type}}
      {\cR{\Gamma};\Delta\vdash A}
\]
\[
\infer{\cR{\Gamma \vdash
           t^\S : (\delta : \Delta^\A)(\delta^D : \Delta^\D\,\delta)
           (\delta^S : \Delta^\S\delta\,\delta^D)\ra
           A^\S\,\delta\,\delta^D\,\delta^S\,(t^\A\,\delta)\,(t^\D\,\delta\,\delta^D)
           }}
      {\cR{\Gamma};\Delta\vdash t : A}
      \]

\vspace{1em}
Every context is is interpreted as a dependent relation between an algebra
and a displayed algebra over it.

\end{frame}

\begin{frame}{Sections (2)}

\begin{alignat*}{5}
  & \U^\S\cR{\delta^S\,A\,A^D} && :\equiv \cR{(x : A)\ra A^D\,x} \\
  & (\underline{a})^\S\cR{\delta^S\,t\,t^D} && :\equiv
    \cR{a^\S\,\delta^S\,t = t^D} \\
  & ((x:a)\ra B)^\S \cR{\delta^S\,f\,f^D} && :\equiv \\
  & && \hspace{-6em} \cR{(x : A^\A\,\delta)\ra B^\S\,(\delta,\,x)\,(\delta^D,\,a^\S\,\delta^S\,x)\,(\delta^S,\,\refl)\,(f\,x)\,(f^D\,x\,(a^\S\,\delta^S\,x))} \\
  & \ldots
\end{alignat*}

For identity types, $\refl$, path induction: we construct $n+1$-level paths by induction on $n$-level paths from induction hypotheses.

\vspace{2em}
{\footnotesize(See details in article/formalization)}

\end{frame}

\begin{frame}{Induction for HIITs}

  Now, for a $\cR{\Gamma} \vdash \Delta$ signature and an algebra $\cR{\Gamma \vdash initAlg : \Delta^\A}$, the type of induction is $\cR{(D : \Delta^\D\,initAlg)\ra \Delta^\S\,initAlg\,D}$.

\end{frame}

\section{WIP and future work}

\begin{frame}

  \begin{itemize}
  \item <1->
     Homomorphisms of algebras are not homotopy sets in general, so we
     would need higher categories for HIIT semantics.
  \item <2-> This is hard, so let's first assume uniqueness of
    identity proofs in the target theory, and develop semantics for
    QIITs using strict categories of algebras.
  \item <3->
    This is WIP, but categorical semantics appears to work out nicely,
    and existence of initial algebras as well.
  \end{itemize}

\end{frame}

\begin{frame}

  \center{\large{Thank you!}}

\end{frame}


%% --------------------------------------------------------------------------------

\begin{frame}[allowframebreaks]{References}
  \bibliography{b}
\end{frame}


\end{document}
