{-# language OverloadedStrings #-}
{-# language ViewPatterns #-}
{-# language PatternGuards #-}
{-# language PatternSynonyms #-}
import Data.List
import Data.Maybe
import Data.Char
import Data.Monoid
import Prelude hiding (pi)
import Control.Arrow hiding (app)
import Text.Parsec
import qualified Text.Parsec.Token as P
import Text.Parsec.Language (haskellDef)
import qualified Text.PrettyPrint.ANSI.Leijen as T
import System.Environment

------------------------------ expressions

type Name = String

data Exp
    = Global Name       -- global variable
    | Var Int           -- local De-Bruijn variable
    | App Exp Exp       -- application
    | Lam Name Exp      -- lambda (De-Bruijn, with variable name suggestion)

pattern Type      = Global "Type"    -- Type : Type
pattern T         = Global "T"       -- T : Type
pattern Tt        = Global "tt"      -- tt : T
pattern Pi n t t' = Global "Pi" `App` t `App` Lam n t' -- dependent function type (De-Bruijn, with variable name suggestion)

------------------------------ toolbox

-- True iff index n used in e
used n e = case e of
    Global{} -> False
    Var n' -> n' == n
    App a b -> used n a || used n b
    Lam _ f -> used (n+1) f

-- global variable names
globalNames = nub . f where
  f e = case e of
    Global n -> [n]
    Var{} -> []
    App a b -> f a ++ f b
    Lam _ e -> f e

-- lift De-Bruijn indices
lift n x e = case e of
    Global{} -> e
    Var n' | n' >= n   -> Var (n' + x)
           | otherwise -> e
    a `App` b -> lift n x a `App` lift n x b
    Lam ns f -> Lam ns $ lift (n+1) x f

-- substitute a local variable
subst n x e = case e of
    Global{} -> e
    Var n' | n' == n   -> lift 0 n x
           | n' > n    -> Var (n' - 1)
           | otherwise -> e
--    App (App (App (Global "Paths") T) a) b -> App (App (App (Global "Paths") T) Tt) Tt
    a `App` b -> subst n x a `app` subst n x b
    Lam ns f -> Lam ns $ subst (n+1) x f

-- substitute a global variable
substGlobal i n x e = case e of
    Global n' | n == n'   -> lift 0 i x
              | otherwise -> e
    Var{} -> e
    a `App` b -> substGlobal i n x a `app` substGlobal i n x b
    Lam ns f -> Lam ns $ substGlobal (i+1) n x f

-- application with beta-reduction
Lam _ f `app` x = subst 0 x f
f `app` x = f `App` x

infixl 1 `App`, `app`

lam = Lam "_"

pi n t t' = Global "Pi" `App` t `App` lam' n t'
lam' n t = Lam n $ substGlobal 0 n (Var 0) $ lift 0 1 t

------------------------------ pretty print

instance Show Exp where show = pshow . showExp

pshow doc = T.displayS (T.renderPretty 0.3 160 doc) ""

showExp x = sh 0 $ rename [] x
  where
    sh p x = case x of
        Global n -> T.text n
        (getPis -> (a, zs@(_:_)))
            -> paren (p > 1) $ T.group $ T.nest 1 $ foldr (\a b -> printBinder a T.<$> ("->" T.<+> b)) (sh 0 a) zs
        (getApps -> x: xs@(_:_)) -> paren (p > 3) $ sh 4 x T.<+> (T.group $ T.align $ T.vsep $ map (sh 4) xs)
        Lam v f -> paren (p > 0) $ "\\" <> T.text v T.<+> "->" T.<+> sh 0 f

    printBinder ("_", t) = sh 2 t
    printBinder (n, t) = "(" <> T.align (T.text n <> ":" T.<+> sh 0 t <> ")")

    paren True x = "(" <> T.align (x <> ")")
    paren False x = x

    gs = globalNames x

    rename vs e = case e of
        Global{} -> e
        Var i -> Global $ (vs ++ ["V" ++ show j | j <- [0..]]) !! i
        a `App` b -> rename vs a `App` rename vs b
        Lam n e -> Lam v (rename (v:vs) e)
          where v = if used 0 e then newName n (gs ++ vs) else "_"

getPis (Pi n t e) = ((n, t):) <$> getPis e
getPis a = (a, [])

getApps (App a b) = getApps a ++ [b]
getApps a = [a]

newName n vs = head $ filter (`notElem` vs) $ if n == "_" then flip (:) <$> nums <*> ['a'..'z'] else (n ++) <$> nums
  where
    nums = "": map show [1..]

------------------------------ main algorithm

{-
e : t                               ~~>   e' : t' e
--------------
v : t                               ~~>   v' : t' v
Type : Type                         ~~>   (\(X : Type) -> X -> Type) : Type -> Type
((v: t) -> e) : Type                ~~>   (\(f : (v: t) -> e) -> (v: t) -> (v': t' v) -> e' (f v)) : ((v: t) -> e) -> Type
(\(v : t) -> ev) : (v : t) -> e     ~~>   (\(v : t) -> \(v': t' v) -> ev') : (v: t) -> (v': t' v) -> e' ev
f x : t                             ~~>   f' x x' : t' (f x)
-}

pName n = n ++ "*"  -- parameterised name

-- parametrisation
pp gamma e = case e of
    -- the rule for pi is not needed here, because Pi* is in genv below
    -- this rule is here only for better variable names in output
    Pi n t e -> lam $ Pi n (lift 0 1 t) $ Pi (sName n) (pp gamma (lift 0 2 t) `app` Var 0) $ (pp gamma $ lift 0 1 $ lift 1 1 e) `app` (Var 2 `app` Var 1)

    Global n -> fromMaybe (Global (pName n)) $ lookup (pName n) gamma
    Var n    -> Var (n-1)
    App f e  -> pp gamma f `app` e `app` pp gamma e
    Lam n x  -> Lam n $ Lam (pName n) $ pp gamma $ lift 0 1 x

p_ gamma = pp ga
  where
    ga = genv ++ map (\(n, t) -> (pName n, witness $ pp ga t `app` Global n)) gamma

genv = map (second $ rp parseExp)
    [ (,) "Type*"  "\\X -> X -> Type"
    , (,) "Pi*"    "\\t -> \\tp -> \\e -> \\ep -> \\f -> (v: t) -> (vp: tp v) -> ep v vp (f v)"
    , (,) "Paths*" "\\X -> \\Xp -> \\a -> \\ap -> \\b -> \\bp -> \\p -> Paths (Xp b) (transport X Xp a b p ap) bp"
    , (,) "refl*"  "\\X -> \\Xp -> \\r -> \\rp -> refl (Xp r) rp"
    ]

-- witness calculation
witness Type = T
witness T  = Tt
witness (Pi n _ t) = Lam n $ witness t
-- the rule for Paths witness is probably wrong
-- this rule is not needed if we have a genv-entry for each global Paths constructor
witness e@(Global "Paths" `App` c `App` a `App` b) = Global "refl" `App` c `App` b  -- probably wrong
witness x = error $ "TODO: witness: " ++ show x

--------------- strip trivial arguments (which has type T)

sName n = n ++ "'"  -- parameterised name (simplified)

strip' e = case e of
    Pi _ (witness' -> Just t) e -> first (False:) $ strip' $ subst 0 t e
    Pi n t t' -> first (True:) $ Pi n t `fmap` (strip' t')
    e -> ([], e)

witness' T = Just Tt
witness' (Pi n _ t) = Lam n <$> witness' t
witness' _ = Nothing

mkSubst ss_ e = iterate lam (foldl App e $ map (Var . fst) $ filter snd $ zip [n-1,n-2..] ss) !! n
  where
    n = length ss
    ss = reverse . dropWhile id . reverse $ ss_

dup' _ [] = []
dup' gamma ((n, t): ts) = (pName n, p_ gamma t `app` Global n): dup' gamma ts

dup _ _ [] = []
dup gamma ss ((n, t): ts) = (sName n, foldr (uncurry $ substGlobal 0) b ss): dup gamma ((pName n, mkSubst a (Global (sName n))): ss) ts
  where
    (a, b) = strip' $ p_ gamma t `app` Global n

-------------------------------------------------- environments

data Env = Env
    [(Name, Exp)]   -- first part, preserved
    [(Name, Exp)]   -- second part, subject of parametricity transformation

instance Show Env where show = pshow . showEnv

showEnv (Env ga de) = T.vsep $ map f ga ++ ["====="] ++ map f de where
    f (n, t) = T.text n T.<+> ":" T.<+> T.align (showExp t)

checkEnv env@(Env global local) = check [] $ global ++ local
  where
    check def [] = True
    check def ((a, t): ts)
        | a `elem` def = error $ a ++ " is multiply defined"
        | otherwise = case filter (`notElem` ("Type": "Pi": def)) (globalNames t) of
            [] -> check (a:def) ts
            (n: _) -> error $ n ++ " is not defined\n"

dropUnusedGlobal (Env global x) = Env (fixDrop global) x
  where
    gnames = nub . concatMap (globalNames . snd)
    fixDrop global
        | length global == length global' = global
        | otherwise = fixDrop global'
      where
        global' = filter ((`elem` gnames (global ++ x)) . fst) global

-- parameterise environment
pEnv :: Env -> Env
pEnv (Env gamma delta) = Env gamma (concat $ zipWith (\a {-b-} c -> [a, {-b,-} c]) delta {-(dup' gamma delta)-} $ dup gamma [] delta)

-- make eliminator
mkElim :: Env -> Exp
mkElim (Env gamma delta) = foldr (uncurry pi) (f (Global n0) t0) es
  where
    es@((n0, t0): _) = dup gamma [] delta

    f e (Pi n t t') = Pi n t $ f (app (lift 0 1 e) $ Var 0) t'
    f e Type = e

------------------------------------------ parsing

lexer       = P.makeTokenParser $ haskellDef {P.reservedNames = ["_"], P.identLetter = alphaNum <|> oneOf "_"}

parens      = P.parens lexer
braces      = P.braces lexer
identifier  = P.identifier lexer
reserved    = P.reserved lexer
reservedOp  = P.reservedOp lexer
whiteSpace  = P.whiteSpace lexer
semi        = P.semi lexer

parseExp = uncurry pi <$> try (parens ((,) <$> identifier <* reserved ":" <*> parseExp)) <* reserved "->" <*> parseExp
    <|> do
      a <- parseApp
      pi "_" a <$ reserved "->" <*> parseExp <|> return a

parseApp = foldl1 App <$> many1 parseAtom

parseAtom = Global <$> identifier
    <|> parens parseExp
    <|> lam' <$ reservedOp "\\" <*> identifier <* reserved "->" <*> parseExp

parseBind = (,) <$> identifier <* reserved ":" <*> parseExp
parseEq = (,) <$> identifier <* reserved "=" <*> parseExp

type P = Parsec String ()

parseBinds :: P [(Name, Exp)]
parseBinds = many (try $ semi *> parseBind)

parseEnv = (,) <$ try (semi <* reservedOp "==========") <*> parseBinds <* semi <* reservedOp "=====" <*> parseBinds

parseFile s = rp (many parseEnv) $ unlines $ map addSemi $ lines s
  where
    addSemi cs@(c: _) | not (isSpace c) && c /= '-' && c /= '{' = ';': cs
    addSemi cs = cs 

----------------- main

main = do
    [input] <- getArgs
    s <- readFile input
    putStr $ pshow $ T.vcat $ map ff $ parseFile s
  where
    ff (global, local) | checkEnv env
        = T.vcat ["environment:", T.indent 4 $ showEnv $ dropUnusedGlobal $ pEnv env, "", "eliminator:", T.indent 4 $ showExp $ mkElim env, ""]
      where
        env = Env (builtins ++ global) local

rp p = either (error . show) id . runParser (whiteSpace *> p <* eof) () ""

builtins = map (rp parseBind)
    [ "Type : Type"
    , "Pi : (A: Type) -> (A -> Type) -> Type"
    , "Paths : (A: Type) -> A -> A -> Type"
    , "refl : (A: Type) -> (r: A) -> Paths A r r"
    , "transport : (X: Type) -> (P: X-> Type) -> (a: X) -> (b: X) -> Paths X a b -> P a -> P b"
    , "cong : (A: Type) -> (B: Type) -> (f: A -> B) -> (a: A) -> (b: A) -> Paths A a b -> Paths B (f a) (f b)"
    , "T : Type"
    , "tt : T"
    ]

