
{-# language
  OverloadedStrings, Strict, LambdaCase, DeriveFunctor, TupleSections,
  UnicodeSyntax, DeriveFoldable, TypeFamilies, UnicodeSyntax,
  PatternSynonyms, MultiParamTypeClasses, FunctionalDependencies,
  FlexibleContexts, FlexibleInstances, CPP, UndecidableInstances #-}

{-# options_ghc -fwarn-incomplete-patterns #-}

import Prelude hiding (lookup)
import Data.List hiding (lookup)
import Control.Arrow
import Data.Foldable
import Data.String
import Control.Monad
import Control.Monad.Except
import System.IO.Unsafe
import Data.IORef
import Debug.Trace

{-
  Todos/features

  1. Should we make the output of P/E unannotated terms, so
     that the P/E implementation becomes much smaller? (and normalize + elaborate
     missing types later)

  2. Make the elaborator for RInp insert El-s instead of writing it by hand.

  3. Type checker for Tm so that we can check outputs.

  4. Flip args on priting J so that it becomes more readable in output (done...)

  5. Do a version which normalizes output assuming definitional β rules
-}


type Name    = String
data List a  = END | List a ::: a deriving (Functor, Foldable)
type Bind a  = (Name, a)
type Con a   = List (Bind a)

lookup ∷ Eq a ⇒ a → List (a, b) → Maybe b
lookup a END              = Nothing
lookup a (as ::: (a', b)) = if a == a' then Just b else lookup a as

-- | Raw input TT with reduced type annotation.
--   We elaborate this to Inp.
data RInp
  = RVar Name
  | REl RInp
  | RApp RInp RInp
  | RPi RInp (Bind RInp)
  | RId RInp RInp                  -- ^ u ≡ t
  | RTransp (Bind RInp) RInp RInp  -- ^ transp P eq pt
  | RI

data Inp
  = IVar Name
  | IEl Inp
  | IApp (Inp, Bind Inp) Inp Inp           -- ^ app A (x.B) t u
  | IPi Inp (Bind Inp)
  | IId Inp Inp Inp                        -- ^ Id A t u
  | ITransp Inp Inp Inp (Bind Inp) Inp Inp -- ^ transp A t u (x.P) eq pt
  | II

-- | Output type theory.
data Tm
  = Var Name
  | App Tm Tm
  | Pi Tm (Bind Tm)
  | Lam (Bind Tm)
  | Id Tm Tm Tm
  | Refl Tm Tm
  | J Tm Tm Tm Tm Tm Tm
  | U

-- | Weak head normal output.
data WTm
  = WVar Name
  | WApp WTm ~WTm
  | WPi WTm (Bind (WTm → WTm))
  | WLam (Bind (WTm → WTm))
  | WId WTm WTm WTm
  | WRefl WTm WTm
  | WJ WTm WTm WTm ~WTm WTm WTm
  | WU


-- Nameful embedded constructors
--------------------------------------------------------------------------------

instance IsString Tm   where fromString = Var
instance IsString Inp  where fromString = IVar
instance IsString RInp where fromString = RVar

class HasPi a b where
  (==>) ∷ a → b → b
  infixr 4 ==>

instance {-# incoherent #-}(a ~ Name, b ~ Tm) => HasPi (a, b) Tm where
  (x, a) ==> b = Pi a (x, b)

instance (a ~ Tm) => HasPi a Tm where
  a ==> b = Pi a ("_", b)

instance {-# incoherent #-}(a ~ Name, b ~ Inp) => HasPi (a, b) Inp where
  (x, a) ==> b = IPi a (x, b)

instance (a ~ Inp) => HasPi a Inp where
  a ==> b = IPi a ("_", b)

instance {-# incoherent #-}(a ~ Name, b ~ RInp) => HasPi (a, b) RInp where
  (x, a) ==> b = RPi a (x, b)

instance (a ~ RInp) => HasPi a RInp where
  a ==> b = RPi a ("_", b)

infixl 8 ∙
class HasApp a       where (∙) ∷ a → a → a
instance HasApp Tm   where (∙) = App
instance HasApp RInp where (∙) = RApp

class HasVar a       where var ∷ Name → a
instance HasVar Tm   where var = Var
instance HasVar RInp where var = RVar

pattern Λ x t = Lam (x, t)

-- HOAS embedded constructors
--------------------------------------------------------------------------------

λ ∷ Name → (Tm → Tm) → Tm
λ x f = Lam (x, f (Var x))

class PiHOAS a where
  π ∷ Name → a → (a → a) → a

instance PiHOAS Tm where
  π x a f = Pi a (x, f (Var x))
  {-# inline π #-}

instance PiHOAS RInp where
  π x a f = RPi a (x, f (RVar x))
  {-# inline π #-}

cons ∷ (Name, a) → Con a → Con a
cons x END        = END ::: x
cons x (as ::: y) = cons x as ::: y

consHOAS ∷ HasVar a ⇒ Name → a → (a → Con a) → Con a
consHOAS x a f = cons (x, a) (f (var x))

#define LAM_(x) λ "x'" $ \x →
#define PI_(x, a) π "x'" a $ \x →
#define LAM(x) λ "x" $ \x →
#define PI(x, a) π "x" a $ \x →
#define DECL(x, a) consHOAS "x" a $ \x →

-- Tm evaluation
--------------------------------------------------------------------------------

class Eval a v | a → v, v → a where
  eval ∷ Con v → a → v

class Eval a v ⇒ Quote a v | a → v, v → a where
  quote ∷ v → a

class Nf a where
  nf ∷ a → a

instance Eval Tm WTm where
  eval vs = \case
    Var x           → maybe (WVar x) id (lookup x vs)
    App t u         → case (eval vs t, eval vs u) of
                        (WLam (_, t), u) → t u
                        (t          , u) → WApp t u
    Lam (x, t)      → WLam (x, \u → eval (vs ::: (x, u)) t)
    Pi a (x, b)     → WPi (eval vs a) (x, \u → eval (vs ::: (x, u)) b)
    U               → WU
    Id a t u        → WId (eval vs a) (eval vs t) (eval vs u)
    Refl a t        → WRefl (eval vs a) (eval vs t)
    J a t b pr u eq → case eval vs eq of
                        WRefl a t → eval vs pr
                        eq        → WJ (eval vs a) (eval vs t) (eval vs b)
                                       (eval vs pr) (eval vs u) eq

instance Quote Tm WTm where
  quote = \case
    WVar x           → Var x
    WApp t u         → quote t ∙ quote u
    WLam (x, t)      → Lam (x, quote (t (WVar x)))
    WPi a (x, b)     → Pi (quote a) (x, quote (b (WVar x)))
    WU               → U
    WId a t u        → Id (quote a) (quote t) (quote u)
    WRefl a t        → Refl (quote a) (quote t)
    WJ a t b pr u eq → J (quote a) (quote t) (quote b) (quote pr) (quote u) (quote eq)

instance Nf Tm where
  nf = quote . eval END

instance Nf a ⇒ Nf (Con a) where
  nf = fmap (fmap nf)

-- Inp elaboration
--------------------------------------------------------------------------------

type Elab = Either String

unique ∷ IORef Int
unique = unsafePerformIO (newIORef 0)
{-# noinline unique #-}

fresh ∷ IO Int
fresh = do
  u ← readIORef unique
  writeIORef unique (u + 1)
  pure u
{-# noinline fresh #-}

-- |  FIXME: use NbE as usual instead of hacky single substitution.
instI :: Inp → Bind Inp → Inp
instI t' (x', t) = go t where
  goBind (x, t) | x == x'   = (x, t)
  goBind (x, t) | free x t' =
    let n = unsafePerformIO fresh -- lol
        x'' = x ++ show n
    in (x'', go (instI (IVar x'') (x, t)))
  goBind (x, t) = (x, go t)

  free ∷ Name → Inp → Bool
  free x = go where
    goBind (x', b) = if x == x' then False else go b
    go (IVar x') = x == x'
    go (IEl t)   = go t
    go (IApp (a, b) t u) = goBind b || go t || go u
    go (IPi a b) = go a || goBind b
    go (ITransp a t u b eq pt) = go a || go t || go u || goBind b || go eq || go pt
    go (IId a t u) = go a || go t || go u
    go II          = False

  go = \case
    IVar x                → if x == x' then t' else IVar x
    IEl t                 → IEl (go t)
    IApp (a, b) t u       → IApp (go a, goBind b) (go t) (go u)
    IPi a b               → IPi (go a) (goBind b)
    IId a t u             → IId (go a) (go t) (go u)
    ITransp a t u b eq pt → ITransp (go a) (go t) (go u) (goBind b) (go eq) (go pt)
    II                    → II

-- | Γ ⊢ A ~ A'
convTy :: Inp → Inp → Bool
convTy t t' = go 0 t t' where
  goBind g b b' = go (g + 1) (instI gen b) (instI gen b')
    where gen = IVar (show g)

  go g (IVar x)                (IVar x')                  = x == x'
  go g (IEl t)                 (IEl t')                   = go g t t'
  go g (IApp _ t u)            (IApp _ t' u')             = go g t t' && go g u u'
  go g (IPi a b)               (IPi a' b')                = go g a a' && goBind g b b'
  go g (IId _ t u)             (IId _ t' u')              = go g t t' && go g u u'
  go g (ITransp _ _ _ b eq pt) (ITransp _ _ _ b' eq' pt') = goBind g b b' && go g eq eq' && go g pt pt'
  go g II                      II                         = True
  go g _                       _                          = False

-- | Γ ⊢ A ∋ t
hasTy ∷ Con Inp → RInp → Inp → Elab Inp
hasTy ts t want = do
  (t, has) ← inferI ts t
  unless (convTy has want) $
    throwError ("Expected type " ++ show want ++ ", inferred " ++ show has ++ " for " ++ show t)
  pure t

-- | Γ ⊢ A  => ∃ a. (A ≡ El a)
isSmallTy ∷ Inp → Elab Inp
isSmallTy = \case
  IEl a → pure a
  _     → throwError "expected small type"

-- | Γ ⊢ A
isTy ∷ Con Inp → RInp → Elab Inp
isTy ts = \case
  RPi a (x, b) → do
    a <- hasTy ts a II
    b <- isTy (ts ::: (x, IEl a)) b
    pure (IPi a (x, b))

  REl t →
    IEl <$> hasTy ts t II

  RId t u → do
    (t, tty) ← inferI ts t
    tty      ← isSmallTy tty
    (u, uty) ← inferI ts u
    uty      ← isSmallTy uty
    unless (convTy tty uty) $
      throwError "mismatching types in equality"
    pure (IId tty t u)
  RI → pure II
  t → throwError (show t ++ " is not a type")

-- | Γ ⊢ t ∈ A
inferI ∷ Con Inp → RInp → Elab (Inp, Inp)
inferI ts = \case
  RVar x → do
    maybe (throwError "variable not in scope") (pure . (IVar x,)) (lookup x ts)

  RApp t u → do
    (t, tty) ← inferI ts t
    case tty of
      IPi a (x, b) → do
        u ← hasTy ts u (IEl a)
        pure (IApp (a, (x, b)) t u, instI u (x, b))
      _ → throwError "expected function type"

  RTransp (x, b) eq pt → do
    (eq, eqt) ← inferI ts eq
    case eqt of
      IId a t u → do
        b  ← hasTy (ts ::: (x, IEl a)) b II
        pt ← hasTy ts pt (instI t (x, b))
        pure (ITransp a t u (x, b) eq pt, instI u (x, b))
      _ → throwError "expected identity type"

  _ → throwError "not a term"

elabCon ∷ Con RInp → Elab (Con Inp)
elabCon END              = pure END
elabCon (con ::: (x, t)) = do
  con <- elabCon con
  t <- isTy con t
  pure (con ::: (x, t))

--------------------------------------------------------------------------------

transp ∷ Tm → Tm → Tm → Tm → Tm → Tm → Tm
transp a t u b eq pt = J a t (LAM_(u) LAM_(_) b ∙ u) pt u eq

apd ∷ Tm → Tm → Tm → Tm → Tm → Tm → Tm
apd a b f t u eq =
  J a t
    (LAM_(u) LAM_(eq) Id (b ∙ u) (transp a t u b eq (f ∙ t)) (f ∙ u))
    (Refl (b ∙ t) (f ∙ t)) u eq

-- Convert (Bind Inp) to various Tm functions
--------------------------------------------------------------------------------

em1 ∷ Bind Inp → Tm
em1 (x, t) = Λ x $ em t

p1 ∷ Bind Inp → Tm
p1 (x, t) = Λ x $ Λ (p x) $ p t

e1 ∷ Bind Inp → Tm
e1 (x, t) = Λ x $ Λ (p x) $ Λ (e x) $ e t

-- Embedding
--------------------------------------------------------------------------------

em ∷ Inp → Tm
em = \case
  IVar x     → Var x
  II         → U
  IEl t      → em t
  IPi a b    → Pi (em a) (em <$> b)
  IId a t u  → Id (em a) (em t) (em u)
  IApp _ t u → em t ∙ em u
  ITransp a t u (x, b) eq pt →
    J (em a) (em t) (Λ x $ LAM_(_) em b) (em pt) (em u) (em eq)

-- P-ing
--------------------------------------------------------------------------------

class P a b | a → b, b → a where p ∷ a → b
instance P Name Name where p = (++ "ᴾ")

idᴾ ∷ Tm → Tm → Tm → Tm → Tm → Tm → Tm
idᴾ a aᴾ t tᴾ u uᴾ =
  LAM_(eq) Id
      (aᴾ ∙ u)
      (J a t (LAM_(u) LAM_(_) aᴾ ∙ u) tᴾ u eq)
      uᴾ

transpᴾ ∷
  Tm → Tm → Tm → Tm → Tm → Tm →
  Tm → Tm → Tm → Tm → Tm → Tm → Tm
transpᴾ a aᴾ b bᴾ t tᴾ u uᴾ eq eqᴾ pt ptᴾ =
  J a t
    (LAM_(u) LAM_(eq)
        PI_(uᴾ, (aᴾ ∙ u))
        PI_(eqᴾ, (idᴾ a aᴾ t tᴾ u uᴾ ∙ eq))
        bᴾ ∙ u ∙ uᴾ ∙ transp a t u b eq pt)
    (LAM_(uᴾ) LAM_(eqᴾ)
      J (aᴾ ∙ t) tᴾ
        (LAM_(uᴾ) LAM_(_) bᴾ ∙ t ∙ uᴾ ∙ pt)
        ptᴾ
        uᴾ eqᴾ)
    u eq ∙ uᴾ ∙ eqᴾ

instance P Inp Tm where
  p = \case
    IVar x                → Var (p x)
    IEl t                 → p t
    IApp _ t u            → p t ∙ em u ∙ p u
    IPi a (x, b)          → LAM_(f) ((x, em a) ==> (p x, p a ∙ Var x) ==> p b ∙ (f ∙ Var x))
    II                    → LAM_(a) (a ==> U)
    IId a t u             → idᴾ (em a) (p a) (em t) (p t) (em u) (p u)
    ITransp a t u b eq pt → transpᴾ (em a) (p a) (em1 b) (p1 b) (em t) (p t)
                                    (em u) (p u) (em eq) (p eq) (em pt) (p pt)

instance P (Con Inp) (Con Tm) where
  p END              = END
  p (con ::: (x, t)) = p con ::: (x, em t) ::: (p x, p t ∙ var x)

-- E-ing
--------------------------------------------------------------------------------

class E a b | a → b, b → a where e ∷ a → b
instance E Name Name where e = (++ "ᴱ")

appᴱ ∷
  Tm → Tm → Tm → Tm → Tm → Tm →
  Tm → Tm → Tm → Tm → Tm → Tm →
  Tm
appᴱ a aᴾ aᴱ b bᴾ bᴱ t tᴾ tᴱ u uᴾ uᴱ =
  J (aᴾ ∙ u) (aᴱ ∙ u)
    (LAM_(uᴾ) LAM_(uᴱ) bᴱ ∙ u ∙ uᴾ ∙ uᴱ ∙ (t ∙ u) ∙ (tᴾ ∙ u ∙ uᴾ))
    (tᴱ ∙ u)
    uᴾ uᴱ

idᴱ ∷
  Tm → Tm → Tm → Tm → Tm → Tm → Tm → Tm → Tm → Tm
idᴱ a aᴾ aᴱ t tᴾ tᴱ u uᴾ uᴱ =
  LAM_(eq) LAM_(eqᴾ)
    Id (idᴾ a aᴾ t tᴾ u uᴾ ∙ eq)
       (transp (aᴾ ∙ u) (aᴱ ∙ u) uᴾ (LAM_(uᴾ) Id (aᴾ ∙ u) (transp a t u aᴾ eq tᴾ) uᴾ) uᴱ
         (transp (aᴾ ∙ t) (aᴱ ∙ t) tᴾ (LAM_(tᴾ) Id (aᴾ ∙ u) (transp a t u aᴾ eq tᴾ) (aᴱ ∙ u)) tᴱ
           (apd a aᴾ aᴱ t u eq)))
       eqᴾ

transpᴱ ∷
  Tm → Tm → Tm → Tm → Tm → Tm →
  Tm → Tm → Tm → Tm → Tm → Tm →
  Tm → Tm → Tm → Tm → Tm → Tm
  → Tm
transpᴱ a aᴾ aᴱ b bᴾ bᴱ t tᴾ tᴱ u uᴾ uᴱ eq eqᴾ eqᴱ pt ptᴾ ptᴱ =
  J (aᴾ ∙ u) (aᴱ ∙ u)
    (LAM_(uᴾ) LAM_(uᴱ)
      PI_(eqᴾ, (idᴾ a aᴾ t tᴾ u uᴾ ∙ eq))
      PI_(eqᴱ, (idᴱ a aᴾ aᴱ t tᴾ tᴱ u uᴾ uᴱ ∙ eq ∙ eqᴾ))
      Id (bᴾ ∙ u ∙ uᴾ ∙ transp a t u b eq pt)
         (bᴱ ∙ u ∙ uᴾ ∙ uᴱ ∙ transp a t u b eq pt)
         (transpᴾ a aᴾ b bᴾ t tᴾ u uᴾ eq eqᴾ pt ptᴾ))
    (LAM_(eq) LAM_(eqᴱ)
      J a t
        (LAM_(u) LAM_(eq)
          PI_(eqᴾ, (idᴾ a aᴾ t tᴾ u (aᴱ ∙ u) ∙ eq))
          PI_(eqᴱ, (idᴱ a aᴾ aᴱ t tᴾ tᴱ u (aᴱ ∙ u) (Refl (aᴾ ∙ u) (aᴱ ∙ u)) ∙ eq ∙ eqᴾ))
          Id (bᴾ ∙ u ∙ (aᴱ ∙ u) ∙ transp a t u b eq pt)
             (bᴱ ∙ u ∙ (aᴱ ∙ u) ∙ Refl (aᴾ ∙ u) (aᴱ ∙ u) ∙ transp a t u b eq pt)
             (transpᴾ a aᴾ b bᴾ t tᴾ u (aᴱ ∙ u) eq eqᴾ pt ptᴾ))
        (LAM_(eqᴾ) LAM_(eqᴱ)
          J (aᴾ ∙ t) (aᴱ ∙ t)
            (LAM_(tᴾ) LAM_(tᴱ)
              PI_(ptᴾ, (bᴾ ∙ t ∙ tᴾ ∙ pt))
              PI_(eqᴾ, (Id (aᴾ ∙ t) tᴾ (aᴱ ∙ t)))
              PI_(eqᴱ, (idᴱ a aᴾ aᴱ t tᴾ tᴱ t (aᴱ ∙ t) (Refl (aᴾ ∙ t) (aᴱ ∙ t)) ∙ Refl a t ∙ eqᴾ))
              PI_(ptᴱ, (Id (bᴾ ∙ t ∙ tᴾ ∙ pt) (bᴱ ∙ t ∙ tᴾ ∙ tᴱ ∙ pt) ptᴾ))
              Id (bᴾ ∙ t ∙ (aᴱ ∙ t) ∙ pt)
                 (bᴱ ∙ t ∙ (aᴱ ∙ t) ∙ Refl (aᴾ ∙ t) (aᴱ ∙ t) ∙ pt)
                 (transpᴾ a aᴾ b bᴾ t tᴾ t (aᴱ ∙ t) (Refl a t) eqᴾ pt ptᴾ))
            (LAM_(ptᴾ) LAM_(eqᴾ) LAM_(eqᴱ) LAM_(ptᴱ)
              J (Id (aᴾ ∙ t) (aᴱ ∙ t) (aᴱ ∙ t)) (Refl (aᴾ ∙ t) (aᴱ ∙ t))
                (LAM_(eqᴾ) LAM_(eqᴱ)
                  Id (bᴾ ∙ t ∙ (aᴱ ∙ t) ∙ pt)
                     (bᴱ ∙ t ∙ (aᴱ ∙ t) ∙ Refl (aᴾ ∙ t) (aᴱ ∙ t) ∙ pt)
                     (transpᴾ a aᴾ b bᴾ t (aᴱ ∙ t) t (aᴱ ∙ t) (Refl a t) eqᴾ pt ptᴾ))
                ptᴱ
                eqᴾ eqᴱ)
            tᴾ tᴱ ∙ ptᴾ ∙ eqᴾ ∙ eqᴱ ∙ ptᴱ)
        u eq ∙ eqᴾ ∙ eqᴱ)
    uᴾ uᴱ ∙ eqᴾ ∙ eqᴱ

instance E Inp Tm where
  e = \case
    IVar x → Var (e x)
    II     → LAM_(a) LAM_(aᴾ) PI_(x, a) aᴾ ∙ x
    IEl a  → LAM_(x) LAM_(xᴾ) Id (p a ∙ x) (e a ∙ x) xᴾ

    IPi a (x, b) →
      LAM_(f) LAM_(fᴾ)
        (x, em a) ==>
          (Λ (p x) $ Λ (e x) $ e b)
            ∙ (e a ∙ Var x)
            ∙ Refl (p a ∙ Var x) (e a ∙ Var x)
            ∙ (f ∙ Var x)
            ∙ (fᴾ ∙ Var x ∙ (e a ∙ Var x))

    IApp (a, b) t u →
      appᴱ (em a) (p a) (e a) (em1 b) (p1 b) (e1 b)
           (em t) (p t) (e t) (em u) (p u) (e u)

    IId a t u →
      idᴱ (em a) (p a) (e a) (em t) (p t) (e t) (em u) (p u) (e u)

    ITransp a t u b eq pt →
      transpᴱ (em a)  (p a)  (e a)  (em t)  (p t)  (e t)
              (em u)  (p u)  (e u)  (em1 b) (p1 b) (e1 b)
              (em eq) (p eq) (e eq) (em pt) (p pt) (e pt)

instance E (Con Inp) (Con Tm) where
  e END              = END
  e (con ::: (x, t)) = e con ::: (x, em t) ::: (p x, p t ∙ var x) ::: (e x, e t ∙ var x ∙ var (p x))

-- Printing
--------------------------------------------------------------------------------

prettyTm ∷ Int → Tm → ShowS
prettyTm prec = go (prec /= 0) where

  unwords' ∷ [ShowS] → ShowS
  unwords' = foldr1 (\x acc → x . (' ':) . acc)

  spine ∷ Tm → Tm → [Tm]
  spine f a = go f [a] where
    go (App f a) args = go f (a : args)
    go t         args = t:args

  lams ∷ Name → Tm → ([Name], Tm)
  lams x t = go [x] t where
    go xs (Lam (x, t)) = go (x:xs) t
    go xs t            = (xs, t)

  freeIn ∷ Name → Tm → Bool
  freeIn x = \case
    Var x'          → x == x'
    App f a         → freeIn x f || freeIn x a
    Lam (x', t)     → if x == x' then False else freeIn x t
    Pi a (x', b)    → freeIn x a || if x == x' then False else freeIn x b
    Id a t u        → any (freeIn x) [a, t, u]
    J a t b pr u eq → any (freeIn x) [a, t, b, pr, u, eq]
    Refl a t        → any (freeIn x) [a, t]
    U               → False

  go ∷ Bool → Tm → ShowS
  go p = \case
    Var x       → (x++)
    App f a     → showParen p (unwords' $ map (go True) (spine f a))

    Lam (x, t)  → case t of
      App t (Var x') | x == x', not (freeIn x t) → go p t
      _ → case lams x t of
        (xs, t) → showParen p (("λ "++) . (unwords (reverse xs)++) . (". "++) . go False t)

    Pi a (x, b) → showParen p (arg . (" → "++) . go False b)
      where arg = if freeIn x b then showParen True ((x++) . (" : "++) . go False a)
                                else go False a
    U               → ('U':)
    Id a t u        → showParen p (go False t . (" ≡ "++) . go False u)
    Refl a t        → go p ("refl" ∙ t)
    J a t b pr u eq → case b of
      Λ x (Λ y b) | not (freeIn y b) → go p ("tr" ∙ Λ x b ∙ eq ∙ pr)
      _           → go p ("J" ∙ b ∙ eq ∙ pr)

printInp2Tm ∷ Inp → Tm
printInp2Tm = go where
  go = \case
    IVar x                     → Var x
    IEl t                      → "El" ∙ go t
    IApp _ t u                 → go t ∙ go u
    IPi a (x, b)               → Pi (go a) (x, go b)
    IId a t u                  → Id (go a) (go t) (go u)
    II                         → "I"
    ITransp a t u (x, b) eq pt → "transp" ∙ Λ x (go b) ∙ go eq ∙ go pt

printRInp2Tm ∷ RInp → Tm
printRInp2Tm = go where
  go = \case
    RVar x               → Var x
    REl t                → "El" ∙ go t
    RApp t u             → go t ∙ go u
    RPi a (x, b)         → Pi (go a) (x, go b)
    RId t u              → Id "_" (go t) (go u)
    RI                   → "I"
    RTransp (x, b) eq pt → "transp" ∙ Λ x (go b) ∙ go eq ∙ go pt

instance Show Tm where showsPrec = prettyTm
instance Show Inp where showsPrec n a = showsPrec n (printInp2Tm a)
instance Show RInp where showsPrec n a = showsPrec n (printRInp2Tm a)

instance Show a ⇒ Show (Con a) where
  show = intercalate "\n" . map (\(x, t) -> x ++ " : " ++ show t) . toList

testE con = nf . e <$> elabCon con

--------------------------------------------------------------------------------

bool ∷ Con RInp
bool =
  DECL(bool, RI)
  DECL(true, (REl bool))
  DECL(false, (REl bool))
  END

nat ∷ Con RInp
nat =
  DECL(nat, RI)
  DECL(zero, (REl nat))
  DECL(suc, (PI(n, nat) REl nat))
  END

s1 ∷ Con RInp
s1 =
  DECL(s1, RI)
  DECL(base, (REl s1))
  DECL(loop, (RId base base))
  END

{-
s1    : U
s1ᴾ   : s1 → U
s1ᴱ   : (x' : s1) → s1ᴾ x'
base  : s1
baseᴾ : s1ᴾ base
baseᴱ : s1ᴱ base ≡ baseᴾ
loop  : base ≡ base
loopᴾ : transp s1ᴾ loop baseᴾ ≡ baseᴾ
loopᴱ :
  transp (λ u'. transp s1ᴾ loop baseᴾ ≡ u') baseᴱ
    (transp (λ u'. transp s1ᴾ loop u' ≡ s1ᴱ base) baseᴱ
      (J (λ u' eq'. transp s1ᴾ eq' (s1ᴱ base) ≡ s1ᴱ u') loop (refl (s1ᴱ base))))
  ≡ loopᴾ
-}

interval ∷ Con RInp
interval =
  DECL(i,     RI)
  DECL(left,  (REl i))
  DECL(right, (REl i))
  DECL(seg,   (RId left right))
  END

{-
Right i : U
iᴾ      : i → U
iᴱ      : (x' : i) → iᴾ x'
left    : i
leftᴾ   : iᴾ left
leftᴱ   : iᴱ left ≡ leftᴾ
right   : i
rightᴾ  : iᴾ right
rightᴱ  : iᴱ right ≡ rightᴾ
seg     : left ≡ right
segᴾ    : transp iᴾ seg leftᴾ ≡ rightᴾ
segᴱ    :
  transp (λ u'. transp iᴾ seg leftᴾ ≡ u') rightᴱ
    (transp (λ u'. transp iᴾ seg u' ≡ iᴱ right) leftᴱ
      (J (λ u' eq'. transp iᴾ eq' (iᴱ left) ≡ iᴱ u') seg (refl (iᴱ left))))
  ≡ segᴾ
-}

identity ∷ Con RInp
identity =
  DECL(a,    RI)
  DECL(x,    (REl a))
  DECL(id,   (PI(y, a) RI))
  DECL(refl, (REl (id ∙ x)))
  END

-- | Note transport below. If we substitute out the (a, x) parameters,
--   the transport goes away (TODO: write parameter substitution operation)
{-
a     : U
aᴾ    : a → U
aᴱ    : (x' : a) → aᴾ x'
x     : a
xᴾ    : aᴾ x
xᴱ    : aᴱ x ≡ xᴾ
id    : a → U
idᴾ   : (y : a) → aᴾ y → id y → U
idᴱ   : (y : a) → (x' : id y) → idᴾ y (aᴱ y) x'
refl  : id x
reflᴾ : idᴾ x xᴾ refl
reflᴱ : (transp (λ uᴾ'. (x' : id x) → idᴾ x uᴾ' x') xᴱ (idᴱ x)) refl ≡ reflᴾ
-}

ttdecl ∷ Con RInp
ttdecl =
  DECL(con,    RI)
  DECL(ty,     (PI(γ, con) RI))
  DECL(tm,     (PI(γ, con) PI(a, (ty ∙ γ)) RI))
  DECL(sub,    (PI(γ, con) PI(δ, con) RI))
  DECL(tysub,  (PI(γ, con) PI(δ, con) PI(σ, (sub ∙ γ ∙ δ)) PI(a, (ty ∙ δ)) REl (ty ∙ γ)))
  DECL(ε,      (REl con))
  DECL(cons,   (PI(γ, con) PI(a, (ty ∙ γ)) REl con))
  DECL(idₛ,    (PI(γ, con) REl (sub ∙ γ ∙ γ)))
  DECL(compₛ,  (PI(γ, con) PI(δ, con) PI(σ, con) PI(ν, (sub ∙ δ ∙ γ)) PI(ξ, (sub ∙ σ ∙ δ)) REl (sub ∙ σ ∙ γ)))
  DECL(εₛ,     (PI(γ, con) REl (sub ∙ γ ∙ ε)))
  DECL(consₛ,  (PI(γ, con) PI(δ, con) PI(ν, (sub ∙ γ ∙ δ)) PI(a, (ty ∙ δ)) PI(t, (tm ∙ γ ∙ (tysub ∙ γ ∙ δ ∙ ν ∙ a)))REl (sub ∙ γ ∙ (cons ∙ δ ∙ a))))
  END


{-
con  : U
conᴾ : con → U
conᴱ : (x' : con) → conᴾ x'
ty   : con → U
tyᴾ  : (γ : con) → conᴾ γ → ty γ → U
tyᴱ  : (γ : con) → (x' : ty γ) → tyᴾ γ (conᴱ γ) x'
tm   : (γ : con) → ty γ → U
tmᴾ  : (γ : con) → (γᴾ : conᴾ γ) → (a : ty γ) → tyᴾ γ γᴾ a → tm γ a → U
tmᴱ  : (γ : con) → (a : ty γ) → (x' : tm γ a) → tmᴾ γ (conᴱ γ) a (tyᴱ γ a) x'
sub  : con → con → U
subᴾ : (γ : con) → conᴾ γ → (δ : con) → conᴾ δ → sub γ δ → U
subᴱ : (γ : con) → (δ : con) → (x' : sub γ δ) → subᴾ γ (conᴱ γ) δ (conᴱ δ) x'
-}

propTrunc ∷ Con RInp
propTrunc =
  DECL(a, RI)
  DECL(trunc, RI)
  DECL(emb, (PI(x, a) REl trunc))
  DECL(squash, (PI(x, a) PI(y, a) RId (emb ∙ x) (emb ∙ y)))
  END

{-
a       : U
aᴾ      : a → U
aᴱ      : (x' : a) → aᴾ x'
trunc   : U
truncᴾ  : trunc → U
truncᴱ  : (x' : trunc) → truncᴾ x'
emb     : a → trunc
embᴾ    : (x : a) → aᴾ x → truncᴾ (emb x)
embᴱ    : (x : a) → truncᴱ (emb x) ≡ embᴾ x (aᴱ x)
squash  : (x : a) → (y : a) → emb x ≡ emb y
squashᴾ : (x : a) → (xᴾ : aᴾ x) → (y : a) → (yᴾ : aᴾ y) → transp truncᴾ (squash x y) (embᴾ x xᴾ) ≡ embᴾ y yᴾ
squashᴱ :
  (x : a) → (y : a) →
    transp (λ u'. transp truncᴾ (squash x y) (embᴾ x (aᴱ x)) ≡ u') (embᴱ y)
      (transp (λ u'. transp truncᴾ (squash x y) u' ≡ truncᴱ (emb y)) (embᴱ x)
        (J (λ u' eq'. transp truncᴾ eq' (truncᴱ (emb x)) ≡ truncᴱ u') (squash x y) (refl (truncᴱ (emb x)))))
    ≡ squashᴾ x (aᴱ x) y (aᴱ y)
-}

category ∷ Con RInp
category =
  DECL(obj, RI)
  DECL(morph, (PI(i, obj) PI(j, obj) RI))
  DECL(id, (PI(i, obj) REl (morph∙i∙i)))
  DECL(comp, (PI(i, obj) PI(j, obj) PI(k, obj) PI(f, (morph∙j∙k)) PI(g, (morph∙i∙j)) REl (morph∙i∙k)))
  DECL(idl, (PI(i, obj) PI(j, obj) PI(f, (morph∙i∙j)) RId (comp∙i∙j∙j∙(id∙j)∙f) f))
  DECL(idr, (PI(i, obj) PI(j, obj) PI(f, (morph∙i∙j)) RId (comp∙i∙i∙j∙f∙(id∙i)) f))
  DECL(ass,
       (PI(i, obj) PI(j, obj) PI(k, obj) PI(l, obj) PI(f, (morph∙k∙l)) PI(g, (morph∙j∙k)) PI(h, (morph∙i∙j))
        RId (comp∙i∙k∙l∙f∙(comp∙i∙j∙k∙g∙h))
            (comp∙i∙j∙l∙(comp∙j∙k∙l∙f∙g)∙h)))
  END


{-
obj       : U
objᴾ      : obj → U
objᴱ      : (x' : obj) → objᴾ x'
morph     : obj → obj → U
morphᴾ    : (i : obj) → objᴾ i → (j : obj) → objᴾ j → morph i j → U
morphᴱ    : (i : obj) → (j : obj) → (x' : morph i j) → morphᴾ i (objᴱ i) j (objᴱ j) x'
id        : (i : obj) → morph i i
idᴾ       : (i : obj) → (iᴾ : objᴾ i) → morphᴾ i iᴾ i iᴾ (id i)
idᴱ       : (i : obj) → morphᴱ i i (id i) ≡ idᴾ i (objᴱ i)
comp      : (i : obj) → (j : obj) → (k : obj) → morph j k → morph i j → morph i k
compᴾ     : (i : obj) → (iᴾ : objᴾ i) → (j : obj) → (jᴾ : objᴾ j) → (k : obj) → (kᴾ : objᴾ k) → (f : morph j k) → morphᴾ j jᴾ k kᴾ f → (g : morph i j) → morphᴾ i iᴾ j jᴾ g → morphᴾ i iᴾ k kᴾ (comp i j k f g)

compᴱ     : (i : obj) → (j : obj) → (k : obj) → (f : morph j k) → (g : morph i j) → morphᴱ i k (comp i j k f g) ≡ compᴾ i (objᴱ i) j (objᴱ j) k (objᴱ k) f (morphᴱ j k f) g (morphᴱ i j g)

-- Coherence laws??
------------------------------------------------------------

idl       : (i : obj) → (j : obj) → (f : morph i j) → comp i j j (id j) f ≡ f
idlᴾ      : (i : obj) → (iᴾ : objᴾ i) → (j : obj) → (jᴾ : objᴾ j) → (f : morph i j) → (fᴾ : morphᴾ i iᴾ j jᴾ f) → transp (morphᴾ i iᴾ j jᴾ) (idl i j f) (compᴾ i iᴾ j jᴾ j jᴾ (id j) (idᴾ j jᴾ) f fᴾ) ≡ fᴾ


idlᴱ      : (i : obj) → (j : obj) → (f : morph i j) → transp (λ u'. transp (morphᴾ i (objᴱ i) j (objᴱ j)) (idl i j f) u' ≡ morphᴱ i j f) ((transp (λ uᴾ'. (g : morph i j) → morphᴱ i j (comp i j j (id j) g) ≡ compᴾ i (objᴱ i) j (objᴱ j) j (objᴱ j) (id j) uᴾ' g (morphᴱ i j g)) (idᴱ j) (compᴱ i j j (id j))) f) (J (λ u' eq'. transp (morphᴾ i (objᴱ i) j (objᴱ j)) eq' (morphᴱ i j (comp i j j (id j) f)) ≡ morphᴱ i j u') (idl i j f) (refl (morphᴱ i j (comp i j j (id j) f)))) ≡ idlᴾ i (objᴱ i) j (objᴱ j) f (morphᴱ i j f)


idr       : (i : obj) → (j : obj) → (f : morph i j) → comp i i j f (id i) ≡ f

idrᴾ      : (i : obj) → (iᴾ : objᴾ i) → (j : obj) → (jᴾ : objᴾ j) → (f : morph i j) → (fᴾ : morphᴾ i iᴾ j jᴾ f) → transp (morphᴾ i iᴾ j jᴾ) (idr i j f) (compᴾ i iᴾ i iᴾ j jᴾ f fᴾ (id i) (idᴾ i iᴾ)) ≡ fᴾ

idrᴱ      : (i : obj) → (j : obj) → (f : morph i j) → transp (λ u'. transp (morphᴾ i (objᴱ i) j (objᴱ j)) (idr i j f) u' ≡ morphᴱ i j f) (transp (λ uᴾ'. morphᴱ i j (comp i i j f (id i)) ≡ compᴾ i (objᴱ i) i (objᴱ i) j (objᴱ j) f (morphᴱ i j f) (id i) uᴾ') (idᴱ i) (compᴱ i i j f (id i))) (J (λ u' eq'. transp (morphᴾ i (objᴱ i) j (objᴱ j)) eq' (morphᴱ i j (comp i i j f (id i))) ≡ morphᴱ i j u') (idr i j f) (refl (morphᴱ i j (comp i i j f (id i))))) ≡ idrᴾ i (objᴱ i) j (objᴱ j) f (morphᴱ i j f)

ass       : (i : obj) → (j : obj) → (k : obj) → (l : obj) → (f : morph k l) → (g : morph j k) → (h : morph i j) → comp i k l f (comp i j k g h) ≡ comp i j l (comp j k l f g) h


assᴾ      : (i : obj) → (iᴾ : objᴾ i) → (j : obj) → (jᴾ : objᴾ j) → (k : obj) → (kᴾ : objᴾ k) → (l : obj) → (lᴾ : objᴾ l) → (f : morph k l) → (fᴾ : morphᴾ k kᴾ l lᴾ f) → (g : morph j k) → (gᴾ : morphᴾ j jᴾ k kᴾ g) → (h : morph i j) → (hᴾ : morphᴾ i iᴾ j jᴾ h) → transp (morphᴾ i iᴾ l lᴾ) (ass i j k l f g h) (compᴾ i iᴾ k kᴾ l lᴾ f fᴾ (comp i j k g h) (compᴾ i iᴾ j jᴾ k kᴾ g gᴾ h hᴾ)) ≡ compᴾ i iᴾ j jᴾ l lᴾ (comp j k l f g) (compᴾ j jᴾ k kᴾ l lᴾ f fᴾ g gᴾ) h hᴾ

assᴱ :
  (i : obj) → (j : obj) → (k : obj) → (l : obj) → (f : morph k l) → (g : morph j k) → (h : morph i j) →

transp (λ u'. transp (morphᴾ i (objᴱ i) l (objᴱ l)) (ass i j k l f g h) (compᴾ i (objᴱ i) k (objᴱ k) l (objᴱ l) f (morphᴱ k l f) (comp i j k g h) (compᴾ i (objᴱ i) j (objᴱ j) k (objᴱ k) g (morphᴱ j k g) h (morphᴱ i j h))) ≡ u') ((transp (λ uᴾ'. (g : morph i j) → morphᴱ i l (comp i j l (comp j k l f g) g) ≡ compᴾ i (objᴱ i) j (objᴱ j) l (objᴱ l) (comp j k l f g) uᴾ' g (morphᴱ i j g)) (compᴱ j k l f g) (compᴱ i j l (comp j k l f g))) h) (transp (λ u'. transp (morphᴾ i (objᴱ i) l (objᴱ l)) (ass i j k l f g h) u' ≡ morphᴱ i l (comp i j l (comp j k l f g) h)) (transp (λ uᴾ'. morphᴱ i l (comp i k l f (comp i j k g h)) ≡ compᴾ i (objᴱ i) k (objᴱ k) l (objᴱ l) f (morphᴱ k l f) (comp i j k g h) uᴾ') (compᴱ i j k g h) (compᴱ i k l f (comp i j k g h))) (J (λ u' eq'. transp (morphᴾ i (objᴱ i) l (objᴱ l)) eq' (morphᴱ i l (comp i k l f (comp i j k g h))) ≡ morphᴱ i l u') (ass i j k l f g h) (refl (morphᴱ i l (comp i k l f (comp i j k g h))))))
≡
assᴾ i (objᴱ i) j (objᴱ j) k (objᴱ k) l (objᴱ l) f (morphᴱ k l f) g (morphᴱ j k g) h (morphᴱ i j h)
-}
