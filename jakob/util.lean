
universes u v w
def cast_heq_cast {α β γ δ : Sort u} {h₁ : α = β} {h₂ : γ = δ} (a : α) (g : γ)
  (p : a == g) : (cast h₁ a == cast h₂ g) :=
begin
  induction h₁, induction h₂, exact p
end

def heq_iff_cast {α β : Sort u} (a : α) (b : β) :
  (a == b) ↔ ∃ (q : α = β), cast q a = b :=
begin
  constructor,
  { intro p, induction p, exact ⟨rfl, rfl⟩ },
  { intro e, cases e with q r, induction r,
    induction q, apply heq.symm, apply cast_heq }
end

def hfunext_dom {A A' B : Type} (p : A = A') {f : A → B} {g : A' → B}
  (q : Π x, f x = g (cast p x)) : f == g :=
begin
  cases p, fapply heq_of_eq, fapply funext, intro x,
  exact q x
end

def hfunext_cod {A : Type} {B B' : A → Type}
  (p : B = B') {f : Π x, B x} {g : Π x, B' x}
  (q : Π x, f x == g x) : f == g :=
begin
  cases p,
  fapply heq_of_eq, apply funext, intro x,
  exact eq_of_heq (q x)
end

def hfunext_both {A A' : Sort u} {B : A → Sort v} {B' : A' → Sort v}
  (p : A = A') (q : Π x, B x == B' (cast p x))
  {f : Π x, B x} {g : Π x, B' x} (r : Π x, f x == g (cast p x)) : f == g :=
begin
  induction p, dsimp[cast] at *,
  have q' : B = B', fapply funext, intro x, apply eq_of_heq, exact q x,
  induction q',
  fapply heq_of_eq, fapply funext, intro x, fapply eq_of_heq, exact r x
end

def eq_mpr_eq_cast (α β : Sort u) (a : α) (q : β = α) :
  eq.mpr q a = cast (eq.symm q) a :=
by induction q; refl

def cast_congr_arg {A : Sort u} {B : A → Sort v} {C : A → Sort w} {a a' : A} (p : a = a')
  (f : Π {a}, B a → C a) {b : B a} :
  cast (congr_arg C p) (f b) = f (cast (congr_arg B p) b) :=
by { induction p, refl }