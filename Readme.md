This repository contains (among other things), the source and formalisations for the paper

 * A Syntax for Higher Inductive-Inductive Types

by Ambrus Kaposi and Andr�s Kov�cs.

Materials related to this paper:

|filename|description|
| --------------- |:------------------------:|
|[fscd](fscd)            |sources and pdf of paper         |
|[haskell/elims-demo](haskell/elims-demo)| Haskell implementation |
|[agda/HIITSyntaxPaper](agda/HIITSyntaxPaper)|Formalisation in Agda |
