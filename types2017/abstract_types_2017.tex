\documentclass[a4paper]{easychair}

\usepackage{doc}
\usepackage{makeidx}

\usepackage{cite}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{hyperref}
\usepackage{proof}
\usepackage{stmaryrd}
\usepackage{tikz}

\DeclareMathSizes{10}{9}{8}{7}

%% Document
%%
\begin{document}\pagenumbering{gobble}

%% Front Matter
%%
\title{Derivation of elimination principles from a context}

\titlerunning{Derivation of elimination principles from a context}

\author{
  Ambrus Kaposi\inst{1}
  \and
  Andr{\'a}s Kov{\'a}cs\inst{1}
  \and
  Bal{\'a}zs K{\H o}m{\H u}ves\inst{2}
  \and
  P{\'e}ter Divi{\'a}nszky\inst{1}
}

\institute{
  E{\"o}tv{\"o}s Lor{\'a}nd University, Budapest, Hungary \\
  \email{\{akaposi|andraskovacs\}@caesar.elte.hu,
    divipp@gmail.com}
  \and
  Falkstenen AB \\
  \email{bkomuves@gmail.com}
}

\authorrunning{Kaposi and Kov{\'a}cs}

\clearpage

\maketitle

\input{abbrevs.tex}

\pagestyle{empty}

%---------------------------------------------------------------------

\vspace{1em}

We describe a syntactic method for deriving the specification of the
dependent eliminator (induction principle) from the type formation
rules and constructors of an inductive type. We observe that the work
on parametricity for dependent types by Bernardy et al
\cite{bernardy12parametricity} can be used for this purpose. Logical
predicates give the motives and methods for the eliminator, while
logical relations provide the $\beta$ computation rules. The method
applies to indexed inductive types, inductive inductive types, higher
inductive types and the combinations of these as well.

The construction does not involve syntactic checks like strict
positivity. It only derives the the type of the eliminator and the
computation rules as equalities but does not validate the existence of
such constants. We describe the algorithm through examples.

\subsubsection*{An inductive type as a context}

The specification of an inductive type can be given as a context. The
variable names are names for the types and the constructors. For
example, natural numbers can be given as the context
\[
  \N:\U,\,\,\, \zero:\N,\,\,\, \suc : \N \ra \N.
  \]
We only look at closed inductive types (otherwise, the specification
would be a telescope). The higher inductive type of the interval is
given as
\[
 \mathsf{I}:\U,\,\,\, \mathsf{left}:\mathsf{I},\,\,\, \mathsf{right}:\mathsf{I},\,\,\, \mathsf{segment}:\mathsf{left}\equiv\mathsf{right}.
\]
A fragment of the intrinsically typed syntax of type theory is given
by the context
\begin{alignat*}{5}
  & \Con:\U,\,\,\,\Ty:\Con\ra\U,\,\,\, \cdot:\Con,\,\,\, \rhd:(\Gamma:\Con)\ra\Ty\,\Gamma\ra\Con ,\,\,\, \iota : (\Gamma:\Con)\ra\Ty\,\Gamma, \\
  & \Pi : (\Gamma:\Con)(A:\Ty\,\Gamma)\ra\Ty\,(\Gamma\rhd A) \ra \Ty\,\Gamma.
\end{alignat*}

\subsubsection*{Logical predicate interpretation}

The type theory in which we write the above contexts has universes
{\`a} la Russel, dependent function space and identity type. We define
the logical predicate interpretation for this theory {\`a} la Bernardy
\cite{bernardy12parametricity}. That is, we define an operation
$\blank^\P$ from the syntax to the syntax which maps a context to an
extended context, a type to a predicate in the extended context and a
term to a witness of the predicate corresponding to its type. The
following rules specify the operation. The third rule is called
parametricity or abstraction theorem.
\begin{equation*}
  \begin{gathered}
    \infer{\Gamma^\P \vdash}{\Gamma \vdash}
  \end{gathered}
  \hspace{2em}
  \begin{gathered}
    \infer{\Gamma^\P \vdash A^\P : A \ra \U}{\Gamma \vdash A : \U}
  \end{gathered}
  \hspace{2em}
  \begin{gathered}
    \infer{\Gamma^\P \vdash t^\P : A^\P\,t}{\Gamma \vdash t : A}
  \end{gathered}
\end{equation*}
Contexts are doubled: the empty context stays the same, i.e.
$\cdot^\P := \cdot$. For the extended context, we add a copy of the
original context and witnesses of the logical predicate for each
variable: $(\Gamma,x:A)^\P := \Gamma^\P,x:A, x_\M : A^\P\,x$. The
witnesses are denoted by the variable with an index $_\M$.

Terms are interpreted as follows. We look up the witness for a
variable from the extended context. The universe is interpreted as
predicate space, function type as the predicate which says that the
function preserves predicates. The predicate for equality is equality
of witnesses over the original equality. The interpretation of the
eliminator $\J$ is omitted for sake of brevity --- it can be given by
repeated usage of $\J$ itself.
\begin{alignat*}{5}
  & x^\P && := x_\M && (\lambda x .t)^\P && := \lambda x \,x_\M.t^\P \\
  & \U^\P && := \lambda A.A \ra \U && (f\,a)^\P && := f^\P\,a\,a^\P \\
  & ((x:A)\ra B)^\P && := \lambda f . (x:A)(x_\M:A^\P\,x)\ra B^\P\,(f\,x) \hspace{9em} && \refl^\P && := \refl \\
  & (a \equiv_A a')^\P && := \lambda w .\transport\,A^\P\,w\,a^\P \equiv_{A^\P\,a'}\,a'^\P
\end{alignat*}
A binary version $\blank^\R$ can be defined in an analogous
way. $\blank^\R$ triples contexts, it produces two copies of the
original context and witnesses of logical relations pointwise.

\subsubsection*{Deriving the specification of the eliminator}

Applying the operator $\blank^\P$ on a context extends it with the
motives and methods for the eliminator. For the first two examples
above, we get the following additional elements:
\begin{alignat*}{5}
  & \N_\M:\N\ra\U,\,\, \zero_\M:\N_\M\,\zero,\,\, \suc_\M : (n:\N)(n_\M:\N_\M\,n)\ra\N_\M\,(\suc\,n) \\
  & \mathsf{I}_\M:\mathsf{I}\ra\U,\,\, \mathsf{left}_\M:\mathsf{I}_\M\,\mathsf{left},\,\, \mathsf{right}_\M:\mathsf{I}_\M\,\mathsf{right},\,\, \mathsf{segment}_\M:\transport\,\mathsf{I}_\M\,\mathsf{segment}\,\mathsf{left}_\M\equiv \mathsf{right}_\M
\end{alignat*}
The computation rules can be derived using the binary logical relation
interpretation $\blank^\R$. For example, the relation $\N_\M$ will
have type $\N_0 \ra \N_1 \ra \U$ (instead of $\N \ra \U$) where $\N_0$
and $\N_1$ are the two copies of $\N$. If we substitute $\N_\M$ for a
graph of a function $f_\N : \N_0 \ra \N_1$ in the whole extended
context, we get the notion of a homomorphism between the natural
number algebras $(\N_0,\zero_0,\suc_0)$ and $(\N_1,\zero_1,\suc_1)$.
\begin{alignat*}{5}
  & \N_0:\U,\,\,\N_1:\U,\,\, f_\N : \N_0 \ra \N_1,\,\, \zero_0:\N_0,\,\, \zero_1:\N_1,\,\, \zero_\M : f_\N\,\zero_0\equiv\zero_1,\,\, \suc_0 : \N_0 \ra \N_0,\,\, \\
  & \suc_1 : \N_1 \ra \N_1,\,\, \suc_\M : (n_0:\N_0)(n_1:\N_1)(n_\M:f_\N\,n_0\equiv n_1)\ra f_\N\,(\suc_0\,n_0)\equiv(\suc_1\,n_1)
\end{alignat*}
With a singleton contraction operation the type of $\suc_\M$ becomes
$(n_0:\N_0)\ra f_\N\,(\suc_0\,n_0)\equiv(\suc_1\,(f_\N\,n_0))$ which
is exactly the computation rule of the nondependent eliminator.

The computation rules for the dependent eliminator can be given by a
``dependent'' variant of the binary logical relation
interpretation. Here the relation for $\N$ has type $(n : \N)\ra
\N_\M\,n \ra \U$.

\subsubsection*{Further steps}

If we restrict the contexts to strictly positive ones (this can be
achieved using an empty universe and a restricted function space), we
can define an operation which gives the type of the eliminator and the
computation rules directly. We are currently working on the definition
of this operator. We are also planning to extend the approach to
coinductive types and to define a type theory with support for
(higher) (co)inductive types using this approach.

\subsubsection*{References}

\renewcommand{\section}[2]{} \bibliography{b}{}
\bibliographystyle{plain}

\end{document}
