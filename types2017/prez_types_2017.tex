\documentclass{beamer}

\usetheme{default}
\useoutertheme{infolines}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{hyperref}
\usepackage{proof}
\usepackage{tikz-cd}
\usepackage[utf8]{inputenc}
%% \changefontsizes{12pt}

\usetheme{Boadilla}
\setbeamertemplate{navigation symbols}{}
\setbeamertemplate{footline}[frame number]
\setbeamerfont{itemize/enumerate subbody}{size=\normalsize}
\setbeamerfont{itemize/enumerate subsubbody}{size=\normalsize}

\makeatletter
\DeclareRobustCommand{\sqcdot}{\mathbin{\mathpalette\morphic@sqcdot\relax}}
\newcommand{\morphic@sqcdot}[2]{%
  \sbox\z@{$\m@th#1\centerdot$}%
  \ht\z@=.33333\ht\z@
  \vcenter{\box\z@}%
}
\makeatother

\input{abbrevs.tex}

\begin{document}

\begin{frame}[plain]
  \vspace{0.5cm}
  \begin{center}
    \textcolor{blue}{
      {\Large Derivation of Elimination Principles from Contexts \\\vspace{0.2em}}
    }
    
    \vspace{0.7cm}

    Andr{\'a}s Kov{\'a}cs \\
    {\small E{\"o}tv{\"o}s Lor{\'a}nd University, Budapest} \\

    \vspace{0.7cm}
    
    joint work with Ambrus Kaposi, P{\'e}ter Divi{\'a}nszky \\ and Bal{\'a}zs K\H{o}m\H{u}ves \\

    \vspace{0.7cm}

    TYPES 2017, Budapest \\
    31 May 2017
    
  \end{center}
\end{frame}

%% --------------------------------------------------------------------------------

\begin{frame}{Goals}
  We would like to
  \begin{itemize}
  \item Compute exact types of eliminators, recursors and their propositional $\beta$-rules for higher-inductive-inductive types (HIIT), as generally as possible.
  \item Show that the HIIT definitions yield categories with morphisms given by recursors (WIP).
  \item Show that our specified HIITs exist (future work).
  \end{itemize}
\vspace{1em}
We use syntactic translations both for getting eliminators/recursors and proving properties about them.

\end{frame}

%% --------------------------------------------------------------------------------

\begin{frame}{Unary parametricity translation}

  \begin{itemize}
  \item First, we observe that the unary parametricity translation by Bernardy, Jansson and Paterson (which we name here $-^\P$) yields the types of methods for eliminators.
  
  \[
\begin{gathered}
  \infer{\vdash \Gamma^\P}{\vdash \Gamma}
\end{gathered}
\hspace{2em}
\begin{gathered}
  \infer{\Gamma^\P \vdash A^\P : A \ra \mathsf{U}}{\Gamma \vdash A : \mathsf{U}}
\end{gathered}
\hspace{2em}
\begin{gathered}
  \infer{\Gamma^\P \vdash t^\P : A^\P\,t}{\Gamma \vdash t : A}
\end{gathered}
\]
  
  \item If we view contexts as specifications for inductive types, then the parametricity translation extends the context with induction methods.

    \[(\N:\mathsf{U},\,\,\, \zero:\N,\,\,\, \suc : \N \ra \N)^\P\]
    \[=\]
    \begin{alignat*}{5}
      & (\N && :\mathsf{U},\,\,\, && \N^\P && :\N\ra\mathsf{U},\,\, \\
      & \zero && :\N,\,\,\, && \zero^\P && :\N^\P\,\zero,\,\,  \\
      & \suc && : \N \ra \N,\,\,\, && \suc^\P && : (n:\N)(n^\P:\N^\P\,n)\ra\N^\P\,(\suc\,n))
    \end{alignat*}    
  \end{itemize}
\end{frame}

%% --------------------------------------------------------------------------------

\begin{frame}

  \begin{itemize}
  \item However, $-^\P$ does not give us types for eliminators and $\beta$-rules.
  \item Binary parametricity translation does not work either.
    \begin{itemize}
    \item It gives us \textit{relations} on structures, but we want \textit{morphisms}.
    \item In particular, we want functions for eliminators instead of relations.
    \end{itemize}
  \item Parametricity translation is too general: it also works on contexts which don't correspond to inductive types, such as:
    \[(\mathsf{A}:\mathsf{U},\,\,\, \mathsf{con}:(\mathsf{A} \ra \mathsf{A}) \ra \mathsf{A})\]
  \item Solution: use a restricted type theory
    \begin{itemize}
    \item In which \textit{every} context is valid as an inductive definition.
    \item Which admits translations yielding eliminators and morphisms.
    \item \textit{Which is closed under these translations}, hence they can be iterated.
    \end{itemize}
  \end{itemize}

\end{frame}

%% --------------------------------------------------------------------------------
\begin{frame}{$\lambda F$}
  \begin{itemize}
  \item We call the type theory $\lambda F$, with "$F$" standing for "finitary".
  \item $\lambda F$ contexts specify closed higher inductive-inductive types.
  \item Parameter types of constructors can't be functions or large types, hence "finitary".
  \item Higher path constructors and recursive higher paths are allowed, as well as use of $\J$, $\refl$, $\lambda$ and function application.
  \item Supports any number of type, point and path constructors in any order, as long the context is well-typed.
  \end{itemize}
\end{frame}

%% --------------------------------------------------------------------------------
\begin{frame}{$\lambda F$}
  \begin{itemize}
    \item We have a universe $\I$ of small types which is only closed under identity types.
    \[
    \begin{gathered}
      \infer{\Gamma \vdash \I}{\vspace{1em}}
    \end{gathered}
    \hspace{2em}
    \begin{gathered}
      \infer{\Gamma \vdash \underline{a}}{\Gamma \vdash a : \I}
    \end{gathered}
    \hspace{2em}
    \begin{gathered}
      \infer{\Gamma \vdash t = u : \I}{\Gamma \vdash a : \I && \Gamma \vdash t : \underline{a} && \Gamma \vdash u : \underline{a}}
    \end{gathered}
    \]    
  \item \textbf{The restriction}: function types have domains in $\I$:
    \[
    \begin{gathered}
      \infer{\Gamma \vdash (x:a)\ra B}{\Gamma \vdash a : \I && \Gamma,x:\underline{a} \vdash B}
    \end{gathered}
    \]
  \item Usual rules for $\J$, $\lambda$, $\refl$ and function application.
  \item We also have definitional $\beta$ for $\J$.
  \end{itemize}
\end{frame}

%% --------------------------------------------------------------------------------

\begin{frame}{Some example $\lambda F$ contexts}

  \begin{itemize}
  \item Intervals: \\
    (Int : I, left : $\underline{\mathsf{Int}}$, right : $\underline{\mathsf{Int}}$, seg : $\underline{\mathsf{left} =\mathsf{right}}$)    
  \item The sorts of an intrinsic syntax of type theory: \\
    (Con : I, \\
     Ty : Con $\ra$ I, \\
     Sub : Con $\ra$ Con $\ra$ I, \\
     Tm : ($\Gamma$ : Con) $\ra$ Ty $\Gamma$ $\ra$ I)
  \item h-sets: \\
    (A : I, isSet : (x y : A)(p q : x = y) $\ra$ $\,\underline{\mathsf{p} = \mathsf{q}}$)
  \end{itemize}  
\end{frame}  

%% --------------------------------------------------------------------------------

\begin{frame}{Other $\lambda F$ examples}
  \begin{itemize}
  \item When viewed as inductive types:
    \begin{itemize}
    \item Classic closed HITs: interval, spheres, cell complexes.
    \item Intrinsic HIIT syntaxes of dependent type theory.
    \end{itemize}
  \item When viewed as structures:
    \begin{itemize}
    \item Rings, groups, etc.
    \item Categories {\`a} la HoTT book: precategories, strict categories, univalent categories.
    \end{itemize}
  \item Some weird but legal contexts owing to the fact the we can freely use $\J$.
  \end{itemize}          
\end{frame}

%% --------------------------------------------------------------------------------

\begin{frame}{Non-examples}
  \begin{itemize}
  \item Types with functions in constructors or function indices.
  \item Sort-of non-examples: types with type parameters, e. g. generic lists.
    \begin{itemize}
    \item Parameters can be "emulated".
    \end{itemize}
  \end{itemize}
\end{frame}  

%% --------------------------------------------------------------------------------

\begin{frame}{Elimination translation}

  \begin{itemize}  
  \item The \textit{elimination translation} $-^\E$ triples a context, adding for each entry an induction method given by $-^\P$, and additionally:
    \begin{itemize}
    \item If the entry is a type constructor, an \textit{eliminator}.
    \item If the entry is a point constructor, a propositional \textit{$\beta$-rule}.
    \item If the entry is a path constructor, a path $\beta$-rule which may be also called a \textit{coherence} rule.
    \end{itemize}

  \item Note that the three kinds of entries are not differentiated in the syntax. The different action of $-^\E$ follows from the structure of their types.
    
  \end{itemize}
\end{frame}

%% --------------------------------------------------------------------------------

\begin{frame}{Example: $-^\E$ for the interval}


\begin{alignat*}{5}
  & \mathsf{Int}       && : \I \\ 
  & \mathsf{Int}^\P    && : \mathsf{Int} \ra \I \\
  & \mathsf{Int}^\E    && : (i : \mathsf{Int}) \ra \underline{\mathsf{Int}^\P\,i} \\
  & \mathsf{left}      && : \underline{\mathsf{Int}} \\
  & \mathsf{left}^\P    && : \underline{\mathsf{Int}^\P\,\mathsf{left}} \\
  & \mathsf{left}^\E   && : \underline{\mathsf{Int}^\E\,\mathsf{left}= \mathsf{left}^\P} \\
  & \mathsf{right}     && : \underline{\mathsf{Int}} \\
  & \mathsf{right}^\P  && : \underline{\mathsf{Int}^\P\,\mathsf{right}} \\
  & \mathsf{right}^\E  && : \underline{\mathsf{Int}^\E\,\mathsf{right}= \mathsf{right}^\P} \\
  & \mathsf{seg}       && : \underline{\mathsf{left} =\mathsf{right}} \\
  & \mathsf{seg}^\P    && : \underline{\mathsf{transp}\,\mathsf{Int}^\P\,\mathsf{seg}\,\mathsf{left}^\P= \mathsf{right}^\P} \\
  & \mathsf{seg}^\E    && : \underline{\mathsf{ap}\,(\mathsf{transp}\,\mathsf{Int}^\P\,\mathsf{seg})\,(\mathsf{left}^\E\,^{-1})\,\sqcdot\, \mathsf{apd}\,\mathsf{Int}^\E\,\mathsf{seg}\,\sqcdot\,\mathsf{right}^\E= \mathsf{seg}^\P }
\end{alignat*}

\end{frame}

%% --------------------------------------------------------------------------------

\begin{frame}{$-^\E$ specification and implementation on contexts and types}
\[
\begin{gathered}
  \infer{\vdash \Gamma^\E}{\vdash \Gamma}
\end{gathered}
\hspace{2em}
\begin{gathered}
  \infer{\Gamma^\E,x:A,x^\P:A^{\P,x} \vdash A^{\E,x,x^\P}}{\Gamma \vdash A}
\end{gathered}
\hspace{2em}
\begin{gathered}
  \infer{\Gamma^\E \vdash t^\E : A^{\E,x,x^\P}[x\mapsto t,x^\P\mapsto t^\P]}{\Gamma \vdash t : A}
\end{gathered}
\hspace{2em}
\begin{gathered}
  \infer{\sigma^\E : \Gamma^\E \Ra \Delta^\E}{\sigma : \Gamma \Ra \Delta}
\end{gathered}
\]
\begin{alignat*}{5}  
  & \cdot^\E && := \cdot \\
  & (\Gamma,x:A)^\E && := (\Gamma^\E,\,x:A,\,x^\P : A^{\P,x},\,x^\E :A^{\E,x,x^\P}) \\
  & \I^{\E,i,i^\P} && := (x:i)\ra \underline{i^\P\,x} \\
  & (\underline{a})^{\E,x,x^\P} && := \underline{a^\E\,x \equiv_{a^\P\,x} x^\P} \\
  & \big((x:a)\ra B\big)^{\E,f,f^\P} && := (x:a)\ra \\
  & && B^{\E,y,y^\P}[x^\P\mapsto a^\E\,x, x^\E\mapsto\refl, y\mapsto f\,x, y^\P \mapsto f^\P\,x\,(a^\E\,x)]
\end{alignat*}  
\end{frame}  

\begin{frame}{$-^\E$ implementation on terms}

  \begin{itemize}
  \item We omit definitions here.
  \item The only complicated case is for $\J$.
    \begin{itemize}
    \item Its implementation has to witness that applying elimination to a $\J$-expression is related by $-^\E$ to the result of $-^\P$-ing the $\J$-expression.
    \item Our current best implementation contains \textit{eight} $\J$ applications.
    \end{itemize}
  \item In all cases, the output of $-^\E$ is in $\lambda F$, so we can iterate it.
  \item $\lambda F$ is the smallest type theory we could so far find which is closed under $-^\E$.
  \end{itemize}
  
\end{frame}

\begin{frame}{Morphism/Recursor translation}
  \begin{itemize}  
  \item The \textit{morphism translation} $-^\R$ also triples a context, returning two copies of the original context together with a morphism between the copies.
  \item It can be viewed as a non-dependent version of $-^\E$
  \item Its iterated application yields higher morphisms.
  \end{itemize}  

\end{frame}  

%% --------------------------------------------------------------------------------

\begin{frame}[fragile]{Getting 2-morphisms from squares}
  \begin{itemize}
  \item $-^\R$ does not directly give us n-morphisms.
  \item For example, $\Gamma^{\R\,\R}$ is a square of morphisms.
  \item We get the usual 2-morphism by substitution with identity morphisms.

  \end{itemize}
  \begin{columns}[c]

    \column{.5\textwidth}

    \begin{center}
    \begin{tikzcd}[row sep=3.5em, column sep=3.5em]
      A \ar[r, "f", ""{name=F}] \arrow[d, "h", swap] & B \arrow[d, "i"] \\
      C \ar[r, "g", ""{name=G}, swap]                & D
      \ar[Rightarrow, "{\,j}", from=F, to=G, shorten <= 8pt, shorten >= 9pt]
    \end{tikzcd}
    \end{center}

    \column{.5\textwidth}

    \begin{center}
    \begin{tikzcd}[row sep=4em, column sep=8em]
      A \ar[r, "f", bend left=40, ""{name=U, below}]
        \ar[r, "{g\,[C\mapsto A, D\mapsto B]}", bend right=40, ""{name=D, above}, swap]
      & B
      \ar[Rightarrow, "{j\,[h\mapsto id_A, i\mapsto id_B]}" description, from=U, to=D]
    \end{tikzcd}
    \end{center}
    
  \end{columns}
  \begin{itemize}
  \item Analogously for higher morphisms.
  \item The result is also in $\lambda F$.
  \item Construction of identity morphisms is WIP.
  \end{itemize}
    
\end{frame}

\begin{frame}{Future work}

\begin{itemize}
  \item Prove that morphisms yield categories.
    \begin{itemize}
      \item Most conveniently: by even more syntactic translations.
      \item Issues with non-hSet morphisms.
    \end{itemize}
  \item Connect to existing HIT work.
  \item Prove that translations respect conversion.
\end{itemize}
\end{frame}

%% --------------------------------------------------------------------------------

\begin{frame}{Development artifacts}

\begin{itemize}
  \item Agda formalization of $-^\E$ and $-^\R$ with shallow embedding.
  \item Haskell implementation of an older version of $-^\E$ without iteration.
  \item Available at \url{https://github.com/AndrasKovacs/elims}
\end{itemize}
\end{frame}

\begin{frame}

\center\large Thank you!

\end{frame}

\end{document}


