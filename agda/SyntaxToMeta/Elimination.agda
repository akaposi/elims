{-# OPTIONS --without-K #-}

module Elimination where

open import Lib
open import InductionMethods

Setᴱ : {A : Set}(Aᴹ : Setᴹ A) → Set₁
Setᴱ {A} Aᴹ = (x : A) → Aᴹ x → Set

Uᴱ : (A : Set) → Setᴹ A → Set
Uᴱ a aₘ = (x : a) → aₘ x

Elᴱ : {a : Set}(aᴹ : Uᴹ a)(aᴱ : Uᴱ a aᴹ) → Setᴱ ( aᴹ)
Elᴱ {a} aᴹ aᴱ x xₘ = aᴱ x ≡ xₘ

-- inductive functions

Πᴱ : {a : Set}    (aᴹ : Uᴹ a)                          (aᴱ : Uᴱ a aᴹ)
     {B : a → Set}(Bᴹ : ∀ {x}(xₘ :  aᴹ x) → Setᴹ (B x))(Bᴱ : ∀ {x} xₘ (xₑ : Elᴱ aᴹ aᴱ x xₘ) → Setᴱ (Bᴹ xₘ))
     → Setᴱ (Πᴹ aᴹ Bᴹ)
Πᴱ {a} aᴹ aᴱ {B} Bᴹ Bᴱ f fₘ = (x : a) → Bᴱ {x} (aᴱ x) refl (f x) (fₘ x (aᴱ x))

appᴱ :
  {a : Set}    (aᴹ : Uᴹ a)                          (aᴱ : Uᴱ a aᴹ)
  {B : a → Set}(Bᴹ : ∀ {x}(xₘ :  aᴹ x) → Setᴹ (B x))(Bᴱ : ∀ {x} xₘ (xₑ : Elᴱ aᴹ aᴱ x xₘ) → Setᴱ (Bᴹ xₘ))
  {t : Π a B}  (tᴹ : Πᴹ aᴹ Bᴹ t)                    (tᴱ : Πᴱ aᴹ aᴱ Bᴹ Bᴱ t tᴹ)
  {u : a}      (uᴹ :  aᴹ u)                         (uᴱ : Elᴱ aᴹ aᴱ u uᴹ)
  → Bᴱ uᴹ uᴱ (t u) (tᴹ u uᴹ)
appᴱ {a} aᴹ aᴱ {B} Bᴹ Bᴱ {t} tᴹ tᴱ {u} uᴹ uᴱ =
  J (aᴹ u) (aᴱ u) (λ xᴹ xᴱ → Bᴱ {u} xᴹ xᴱ (t u) (tᴹ u xᴹ)) (tᴱ u) {uᴹ} uᴱ

lamᴱ :
  {a : Set}      (aᴹ : Uᴹ a)                           (aᴱ : Uᴱ a aᴹ)
  {B : a → Set}  (Bᴹ : ∀ {x}(xₘ :  aᴹ x) → Setᴹ (B x)) (Bᴱ : ∀ {x} xₘ (xₑ : Elᴱ aᴹ aᴱ x xₘ) → Setᴱ (Bᴹ xₘ))
  {t : ∀ x → B x}(tᴹ : ∀ {x}(xₘ :  aᴹ x) → Bᴹ xₘ (t x))(tᴱ : ∀ {x} xₘ (xₑ : Elᴱ aᴹ aᴱ x xₘ) → Bᴱ xₘ xₑ (t x) (tᴹ xₘ))
  → Πᴱ aᴹ aᴱ Bᴹ Bᴱ t (λ x xₘ → tᴹ xₘ)
lamᴱ {a} aᴹ aᴱ {B} Bᴹ Bᴱ {t} tᴹ tᴱ = λ x → tᴱ (aᴱ x) refl

Πβᴱ :
  {a : Set}       (aᴹ : Uᴹ a)                           (aᴱ : Uᴱ a aᴹ)
  {B : a → Set}   (Bᴹ : ∀ {x}(xₘ :  aᴹ x) → Setᴹ (B x)) (Bᴱ : ∀ {x} xₘ (xₑ : Elᴱ aᴹ aᴱ x xₘ) → Setᴱ (Bᴹ xₘ))
  {t : ∀ x → B x} (tᴹ : ∀ {x}(xₘ :  aᴹ x) → Bᴹ xₘ (t x))(tᴱ : ∀ {x} xₘ (xₑ : Elᴱ aᴹ aᴱ x xₘ) → Bᴱ xₘ xₑ (t x) (tᴹ xₘ))
  {u : a}         (uᴹ :  aᴹ u)                          (uᴱ : Elᴱ aᴹ aᴱ u uᴹ)
  → appᴱ aᴹ aᴱ Bᴹ Bᴱ (lamᴹ aᴹ Bᴹ tᴹ) (lamᴱ aᴹ aᴱ Bᴹ Bᴱ tᴹ tᴱ) uᴹ uᴱ ≡ tᴱ uᴹ uᴱ
Πβᴱ aᴹ aᴱ Bᴹ Bᴱ {t} tᴹ tᴱ {u} uᴹ uᴱ =
  J _ (aᴱ u)
     (λ uᴹ uᴱ → appᴱ aᴹ aᴱ Bᴹ Bᴱ (lamᴹ aᴹ Bᴹ tᴹ) (lamᴱ aᴹ aᴱ Bᴹ Bᴱ tᴹ tᴱ) uᴹ uᴱ ≡ tᴱ uᴹ uᴱ)
     refl uᴱ

Πηᴱ :
  {a : Set}    (aᴹ : Uᴹ a)                          (aᴱ : Uᴱ a aᴹ)
  {B : a → Set}(Bᴹ : ∀ {x}(xₘ :  aᴹ x) → Setᴹ (B x))(Bᴱ : ∀ {x} xₘ (xₑ : Elᴱ aᴹ aᴱ x xₘ) → Setᴱ (Bᴹ xₘ))
  {t : Π a B}  (tᴹ : Πᴹ aᴹ Bᴹ t)                    (tᴱ : Πᴱ aᴹ aᴱ Bᴹ Bᴱ t tᴹ)
  → lamᴱ aᴹ aᴱ Bᴹ Bᴱ (appᴹ aᴹ Bᴹ tᴹ) (appᴱ aᴹ aᴱ Bᴹ Bᴱ tᴹ tᴱ) ≡ tᴱ
Πηᴱ _ _ _ _ _ _ = refl

-- non-inductive functions

Πₙᵢᴱ :
  (A : Set)
  {B : A → Set}   (Bᴹ : ∀ a → Setᴹ (B a))   (Bᴱ : ∀ a → Setᴱ (Bᴹ a))
  → Setᴱ (Πₙᵢᴹ A Bᴹ)
Πₙᵢᴱ A {B} Bᴹ Bᴱ f fᴹ = ∀ a → Bᴱ a (f a) (fᴹ a)

appₙᵢᴱ :
  ∀ (A : Set)
    {B : A → Set}  (Bᴹ : ∀ a → Setᴹ (B a)) (Bᴱ : ∀ a → Setᴱ (Bᴹ a))
    {t : Πₙᵢ A B}  (tᴹ : Πₙᵢᴹ A Bᴹ t)      (tᴱ : Πₙᵢᴱ A Bᴹ Bᴱ t tᴹ)
    (a : A)
  → Bᴱ a (t a) (tᴹ a)
appₙᵢᴱ A {B} Bᴹ Bᴱ {t} tᴹ tᴱ a = tᴱ a

lamₙᵢᴱ :
  ∀ (A : Set)
    {B : A → Set} (Bᴹ : ∀ a → Setᴹ (B a)) (Bᴱ : ∀ a → Setᴱ (Bᴹ a))
    {t : Πₙᵢ A B} (tᴹ : ∀ a → Bᴹ a (t a)) (tᴱ : ∀ a → Bᴱ a (t a) (tᴹ a))
  → Πₙᵢᴱ A Bᴹ Bᴱ t tᴹ
lamₙᵢᴱ A {B} Bᴹ Bᴱ {t} tᴹ tᴱ = λ a → tᴱ a

Πₙᵢᴱβ :
  ∀ (A : Set)
    {B : A → Set}  (Bᴹ : ∀ a → Setᴹ (B a)) (Bᴱ : ∀ a → Setᴱ (Bᴹ a))
    {t : Πₙᵢ A B} (tᴹ : ∀ a → Bᴹ a (t a)) (tᴱ : ∀ a → Bᴱ a (t a) (tᴹ a))
    (a : A)
  → appₙᵢᴱ A Bᴹ Bᴱ (lamₙᵢᴹ A Bᴹ tᴹ) (lamₙᵢᴱ A Bᴹ Bᴱ tᴹ tᴱ) a ≡ tᴱ a
Πₙᵢᴱβ A {B} Bᴹ Bᴱ {t} tᴹ tᴱ a = refl

Πₙᵢηᴱ :
  ∀ (A : Set)
    {B : A → Set}  (Bᴹ : ∀ a → Setᴹ (B a)) (Bᴱ : ∀ a → Setᴱ (Bᴹ a))
    {t : Πₙᵢ A B}  (tᴹ : Πₙᵢᴹ A Bᴹ t)      (tᴱ : Πₙᵢᴱ A Bᴹ Bᴱ t tᴹ)
  → lamₙᵢᴱ A Bᴹ Bᴱ (appₙᵢᴹ A Bᴹ tᴹ) (appₙᵢᴱ A Bᴹ Bᴱ tᴹ tᴱ) ≡ tᴱ
Πₙᵢηᴱ _ _ _ _ _ = refl

-- small non-inductive functions
Πₙᵢₛᴱ :
  (A : Set)
  {b : A → Set} (bᴹ : ∀ a → Uᴹ (b a)) (bᴱ : ∀ a → Uᴱ (b a) (bᴹ a))
  → Uᴱ (Πₙᵢₛ A b) (Πₙᵢₛᴹ A bᴹ)
Πₙᵢₛᴱ A {b} bᴹ bᴱ = λ f a → bᴱ a (f a)

appₙᵢₛᴱ :
  (A : Set)
  {b : A → Set}  (bᴹ : ∀ a → Uᴹ (b a)) (bᴱ : ∀ a → Uᴱ (b a) (bᴹ a))
  {t : Πₙᵢₛ A b} (tᴹ : Πₙᵢₛᴹ A bᴹ t)   (tᴱ : Elᴱ (Πₙᵢₛᴹ A bᴹ) (Πₙᵢₛᴱ A bᴹ bᴱ) t tᴹ)
  (a : A)
  → Elᴱ (bᴹ a) (bᴱ a) (t a) (tᴹ a)
appₙᵢₛᴱ A {b} bᴹ bᴱ {t} tᴹ tᴱ a = ap (λ f → f a) tᴱ

lamₙᵢₛᴱ :
  ∀ (A : Set)
    {b : A → Set} (bᴹ : ∀ a → Uᴹ (b a))   (bᴱ : ∀ a → Uᴱ (b a) (bᴹ a))
    {t : Πₙᵢₛ A b}(tᴹ : ∀ a → bᴹ a (t a)) (tᴱ : ∀ a → Elᴱ (bᴹ a) (bᴱ a) (t a) (tᴹ a))
  → Elᴱ (Πₙᵢₛᴹ A bᴹ) (Πₙᵢₛᴱ A bᴹ bᴱ) t tᴹ
lamₙᵢₛᴱ A {b} bᴹ bᴱ {t} tᴹ tᴱ = funext (λ a → tᴱ a)

Πₙᵢₛβᴱ :
  ∀ (A : Set)
    {b : A → Set} (bᴹ : ∀ a → Uᴹ (b a))   (bᴱ : ∀ a → Uᴱ (b a) (bᴹ a))
    {t : Πₙᵢₛ A b}(tᴹ : ∀ a → bᴹ a (t a)) (tᴱ : ∀ a → Elᴱ (bᴹ a) (bᴱ a) (t a) (tᴹ a))
    (u : A)
  → appₙᵢₛᴱ A bᴹ bᴱ (lamₙᵢₛᴹ A bᴹ tᴹ) (lamₙᵢₛᴱ A bᴹ bᴱ tᴹ tᴱ) u ≡ tᴱ u
Πₙᵢₛβᴱ A {b} bᴹ bᴱ {t} tᴹ tᴱ u =
  let (p , q , r , s) = funexts (λ a → bᴱ a (t a)) tᴹ
  in happly (r tᴱ) u

Πₙᵢₛηᴱ :
  (A : Set)
  {b : A → Set}  (bᴹ : ∀ a → Uᴹ (b a)) (bᴱ : ∀ a → Uᴱ (b a) (bᴹ a))
  {t : Πₙᵢₛ A b} (tᴹ : Πₙᵢₛᴹ A bᴹ t)   (tᴱ : Elᴱ (Πₙᵢₛᴹ A bᴹ) (Πₙᵢₛᴱ A bᴹ bᴱ) t tᴹ)
  → lamₙᵢₛᴱ A bᴹ bᴱ (appₙᵢₛᴹ A bᴹ tᴹ) (appₙᵢₛᴱ A bᴹ bᴱ tᴹ tᴱ) ≡ tᴱ
Πₙᵢₛηᴱ A {b} bᴹ bᴱ {t} tᴹ tᴱ =
  let (p , q , r , s) = funexts (λ a → bᴱ a (t a)) tᴹ
  in s tᴱ

-- Identity

≡ᴱ :
  {a : Set}(aᴹ : Uᴹ a)    (aᴱ : Uᴱ a aᴹ)
  {t : a}  (tᴹ : Elᴹ aᴹ t)(tᴱ : Elᴱ aᴹ aᴱ t tᴹ)
  {u : a}  (uᴹ : Elᴹ aᴹ u)(uᴱ : Elᴱ aᴹ aᴱ u uᴹ)
  → Uᴱ (t ≡ u) (≡ᴹ aᴹ tᴹ uᴹ)
≡ᴱ {a} aᴹ aᴱ {t} tᴹ tᴱ {u} uᴹ uᴱ =
  λ e → tr (λ xᴹ → tr aᴹ e xᴹ ≡ uᴹ) tᴱ (tr (λ yᴹ → tr aᴹ e (aᴱ t) ≡ yᴹ) uᴱ (apd aᴱ e))

reflᴱ :
  {a : Set}(aᴹ : Uᴹ a)    (aᴱ : Uᴱ a aᴹ)
  {t : a}  (tᴹ : Elᴹ aᴹ t)(tᴱ : Elᴱ aᴹ aᴱ t tᴹ)
  → Elᴱ (≡ᴹ aᴹ tᴹ tᴹ) (≡ᴱ aᴹ aᴱ tᴹ tᴱ tᴹ tᴱ) refl (reflᴹ aᴹ tᴹ)
reflᴱ {a} aᴹ aᴱ {t} tᴹ tᴱ
  = J (aᴹ t)
       (aᴱ t)
       (λ xᴹ xᴱ →
         tr (λ yᴹ → ≡ᴹ aᴹ yᴹ xᴹ refl) xᴱ
         (tr (λ yᴹ → ≡ᴹ aᴹ (aᴱ t) yᴹ refl) xᴱ refl) ≡ reflᴹ aᴹ xᴹ)
       refl
       {tᴹ} tᴱ

Jᴱ :
  {a : Set}(aᴹ  : Uᴹ a) (aᴱ  : Uᴱ a aᴹ)
  {t : a}  (tᴹ  : aᴹ t) (tᴱ  : Elᴱ aᴹ aᴱ t tᴹ)

  {P : (x : a)(z : t ≡ x) → Set}
    (Pᴹ : ∀{x}(xᴹ : aᴹ x){z}(zᴹ : (≡ᴹ aᴹ tᴹ xᴹ) z) → Setᴹ (P x z))
      (Pᴱ : ∀{x} xᴹ (xᴱ : Elᴱ aᴹ aᴱ x xᴹ){z} zᴹ (zᴱ : Elᴱ (≡ᴹ aᴹ tᴹ xᴹ) (≡ᴱ aᴹ aᴱ tᴹ tᴱ xᴹ xᴱ) z zᴹ)
            → Setᴱ {P x z} (Pᴹ xᴹ zᴹ))

  {pr : P t refl}
    (prᴹ : Pᴹ tᴹ (reflᴹ aᴹ tᴹ) pr)
      (prᴱ : Pᴱ tᴹ tᴱ (reflᴹ aᴹ tᴹ) (reflᴱ aᴹ aᴱ tᴹ tᴱ) pr prᴹ)

  {u : a}(uᴹ : aᴹ u)(uᴱ : Elᴱ aᴹ aᴱ u uᴹ)

  {eq : t ≡ u}
    (eqᴹ : (≡ᴹ aᴹ tᴹ uᴹ) eq)
      (eqᴱ : Elᴱ (≡ᴹ aᴹ tᴹ uᴹ) (≡ᴱ aᴹ aᴱ tᴹ tᴱ uᴹ uᴱ) eq eqᴹ)

  → Pᴱ uᴹ uᴱ eqᴹ eqᴱ (J a t P pr eq) (Jᴹ aᴹ tᴹ Pᴹ prᴹ uᴹ eqᴹ)
Jᴱ {a} aᴹ aᴱ {t} tᴹ tᴱ {P} Pᴹ Pᴱ {pr} prᴹ prᴱ {u} uᴹ uᴱ {eq} eqᴹ eqᴱ =
  J (tr aᴹ eq tᴹ ≡ uᴹ)
    (≡ᴱ aᴹ aᴱ tᴹ tᴱ uᴹ uᴱ eq)
    (λ eqᴹ eqᴱ → Pᴱ uᴹ uᴱ eqᴹ eqᴱ (J a t P pr eq) (Jᴹ aᴹ tᴹ Pᴹ prᴹ uᴹ eqᴹ))
    (J (aᴹ u) (aᴱ u)
       (λ uᴹ uᴱ → Pᴱ uᴹ uᴱ (≡ᴱ aᴹ aᴱ tᴹ tᴱ uᴹ uᴱ eq) (refl {x = ≡ᴱ aᴹ aᴱ tᴹ tᴱ uᴹ uᴱ eq}) (J a t P pr eq)
                     (Jᴹ aᴹ tᴹ Pᴹ prᴹ uᴹ (≡ᴱ aᴹ aᴱ tᴹ tᴱ uᴹ uᴱ eq)))
       (J a t
          (λ u eq → Pᴱ (aᴱ u) refl (≡ᴱ aᴹ aᴱ tᴹ tᴱ (aᴱ u) refl eq) refl (J a t P pr eq)
                       (Jᴹ aᴹ tᴹ Pᴹ prᴹ (aᴱ u) (≡ᴱ aᴹ aᴱ tᴹ tᴱ (aᴱ u) refl eq)))
          (J _ (aᴱ t) (λ tᴹ tᴱ →
                (Pᴹ  : {x : a} (xᴹ :  aᴹ x) {z : t ≡ x} →
                       (≡ᴹ aᴹ tᴹ xᴹ) z → Setᴹ (P x z))
                (Pᴱ  : {x : a} (xᴹ : aᴹ x) (xᴱ : Elᴱ aᴹ aᴱ x xᴹ)
                      {z : t ≡ x} (zᴹ : ≡ᴹ aᴹ tᴹ xᴹ z) →
                      Elᴱ (≡ᴹ aᴹ tᴹ xᴹ) (≡ᴱ aᴹ aᴱ tᴹ tᴱ xᴹ xᴱ) z zᴹ → Setᴱ (Pᴹ xᴹ zᴹ))
                (prᴹ : Pᴹ tᴹ (reflᴹ aᴹ tᴹ) pr)
                (prᴱ : Pᴱ tᴹ tᴱ (reflᴹ aᴹ tᴹ) (reflᴱ aᴹ aᴱ tᴹ tᴱ) pr prᴹ)
                →
                  Pᴱ (aᴱ t) refl (≡ᴱ aᴹ aᴱ tᴹ tᴱ (aᴱ t) refl refl)
                   refl pr (Jᴹ aᴹ tᴹ Pᴹ prᴹ (aᴱ t) (≡ᴱ aᴹ aᴱ tᴹ tᴱ (aᴱ t) refl refl)))
                (λ Pᴹ Pᴱ prᴹ prᴱ → prᴱ)
                tᴱ Pᴹ Pᴱ prᴹ prᴱ)
          eq)
       uᴱ)
    eqᴱ

Jᴱβ :
  {a : Set} (aᴹ  : Uᴹ a)  (aᴱ  : Uᴱ a aᴹ)
  {t : a}   (tᴹ  :  aᴹ t) (tᴱ  : Elᴱ aᴹ aᴱ t tᴹ)

  {P : (x : a)(z : t ≡ x) → Set}
    (Pᴹ : ∀{x}(xᴹ : aᴹ x){z}(zᴹ : (≡ᴹ aᴹ tᴹ xᴹ) z) → Setᴹ (P x z))
      (Pᴱ : ∀{x} xᴹ (xᴱ : Elᴱ aᴹ aᴱ x xᴹ){z} zᴹ (zᴱ : Elᴱ (≡ᴹ aᴹ tᴹ xᴹ) (≡ᴱ aᴹ aᴱ tᴹ tᴱ xᴹ xᴱ) z zᴹ)
            → Setᴱ {P x z} (Pᴹ xᴹ zᴹ))

  {pr : P t refl}
    (prᴹ : Pᴹ tᴹ (reflᴹ aᴹ tᴹ) pr)
      (prᴱ : Pᴱ tᴹ tᴱ (reflᴹ aᴹ tᴹ) (reflᴱ aᴹ aᴱ tᴹ tᴱ) pr prᴹ)

  → Jᴱ {a} aᴹ aᴱ {t} tᴹ tᴱ {P} Pᴹ Pᴱ {pr} prᴹ prᴱ {t} tᴹ tᴱ {refl} (reflᴹ aᴹ tᴹ) (reflᴱ aᴹ aᴱ tᴹ tᴱ)
  ≡ prᴱ
Jᴱβ {a} aᴹ aᴱ {t} .(aᴱ t) refl {P} Pᴹ Pᴱ {pr} prᴹ prᴱ = refl
