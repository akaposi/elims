{-# OPTIONS --without-K #-}

module Lib where

open import Relation.Binary.PropositionalEquality using (_≡_; refl) public
open import Data.Product public

J : ∀ {α β}(A : Set α)(a : A)(B : ∀ b → a ≡ b → Set β) → B a refl → ∀ {b} p → B b p
J _ _ B pr refl = pr

infixr 5 _◾_
_◾_ : ∀ {α}{A : Set α}{x y z : A} → x ≡ y → y ≡ z → x ≡ z
refl ◾ p = p

_⁻¹ : ∀ {α}{A : Set α}{x y : A} → x ≡ y → y ≡ x
refl ⁻¹ = refl

ap : ∀ {α β}{A : Set α}{B : Set β}(f : A → B){x y} → x ≡ y → f x ≡ f y
ap f refl = refl

happly : ∀ {α β}{A : Set α}{B : A → Set β}{f g : ∀ a → B a} → f ≡ g → ∀ a → f a ≡ g a
happly p a = ap (λ f → f a) p

isEquiv : ∀{α β}{A : Set α}{B : Set β}(f : A → B) → Set _
isEquiv {A = A}{B} f = ∃₂ λ (g h : B → A) → (∀ b → f (g b) ≡ b) × (∀ b → g (f b) ≡ b)

postulate
  funexts : ∀ {α β A B} f g → isEquiv (happly {α}{β}{A}{B}{f}{g})

funext : ∀{α β}{A : Set α}{B : A → Set β}{f g : ∀ a → B a} → (∀ a → f a ≡ g a) → f ≡ g
funext {f = f}{g} = proj₁ (funexts f g)

inv⁻¹ : ∀ {α}{A : Set α}{t u : A}(eq : t ≡ u) → (eq ⁻¹ ◾ eq) ≡ refl
inv⁻¹ {A = A}{t}{u} eq = J A t (λ u eq → (eq ⁻¹ ◾ eq) ≡ refl) refl {u} eq

tr : ∀ {α β}{A : Set α}{a b : A}(B : A → Set β) → a ≡ b → B a → B b
tr B refl ba = ba

apd : ∀ {α β}{A : Set α}{B : A → Set β}(f : ∀ a → B a){x y : A}(p : x ≡ y) → tr B p (f x) ≡ f y
apd {B = B} f {x} {y} refl = refl

Π : ∀{α β}(A : Set α)(B : A → Set β) → Set _
Π A B = (x : A) → B x

Πₙᵢ  = Π
Πₙᵢₛ = Π
