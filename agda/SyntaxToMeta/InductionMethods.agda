{-# OPTIONS --without-K #-}

module InductionMethods where

open import Lib

Setᴹ : Set → Set₁
Setᴹ A = A → Set

Uᴹ : Set → Set₁
Uᴹ A = A → Set

Elᴹ : ∀{a}(aᴹ : Uᴹ a) → Setᴹ a -- Elᴹ is left implicit from now on
Elᴹ aᴹ x = aᴹ x

-- inductive functions

Πᴹ : ∀ {a}(aᴹ : Uᴹ a){B : a → Set}(Bᴹ : ∀ {x} (xₘ : aᴹ x) → Setᴹ (B x)) → Setᴹ (Π a B)
Πᴹ {a} aᴹ {B} Bᴹ f = (x : a)(xₘ : aᴹ x) → Bᴹ xₘ (f x)

appᴹ :
  ∀ {a}(aᴹ : Uᴹ a){B : a → Set}(Bᴹ : ∀ {x}(xₘ : aᴹ x) → Setᴹ (B x))
    {t : Π a B}(tᴹ : Πᴹ aᴹ Bᴹ t){u : a} (uᴹ : aᴹ u)
  → Bᴹ uᴹ (t u)
appᴹ {a} aᴹ {B} Bᴹ {t} tᴹ {u} uᴹ = tᴹ u uᴹ

lamᴹ :
  ∀ {a}(aᴹ : Uᴹ a){B : a → Set}(Bᴹ : ∀ {x}(xₘ : aᴹ x) → Setᴹ (B x))
    {t : Π a B}(tᴹ : ∀ {x}(xₘ : aᴹ x) → Bᴹ xₘ (t x))
    → Πᴹ aᴹ Bᴹ t
lamᴹ {a} aᴹ {B} Bᴹ {t} tᴹ = λ x xₘ → tᴹ xₘ

Πβᴹ :
  ∀{a}(aᴹ : Uᴹ a){B : a → Set}(Bᴹ : ∀ {x}(xₘ :  aᴹ x) → Setᴹ (B x))
  {t : Π a B}(tᴹ : ∀ {x}(xₘ :  aᴹ x) → Bᴹ xₘ (t x))
  {u : a} (uᴹ : aᴹ u)
  → appᴹ aᴹ Bᴹ (lamᴹ aᴹ Bᴹ tᴹ) uᴹ ≡ tᴹ uᴹ
Πβᴹ _ _ _ _ = refl

Πηᴹ :
  ∀ {a}(aᴹ : Uᴹ a){B : a → Set}(Bᴹ : ∀ {x}(xₘ :  aᴹ x) → Setᴹ (B x))
    {t : Π a B}(tᴹ : Πᴹ aᴹ Bᴹ t)
  → lamᴹ aᴹ Bᴹ (appᴹ aᴹ Bᴹ tᴹ) ≡ tᴹ
Πηᴹ _ _ _ = refl

-- non-inductive functions

Πₙᵢᴹ : (A : Set){B : A → Set}(Bᴹ : ∀ a → Setᴹ (B a)) → Setᴹ (Π A B)
Πₙᵢᴹ A {B} Bᴹ f = ∀ a → Bᴹ a (f a)

appₙᵢᴹ :
  ∀ (A : Set)
    {B : A → Set}  (Bᴹ : ∀ a → Setᴹ (B a))
    {t : Πₙᵢ A B}  (tᴹ : Πₙᵢᴹ A Bᴹ t)
    (u : A)
  → Bᴹ u (t u)
appₙᵢᴹ A {B} Bᴹ {t} tᴹ u = tᴹ u

lamₙᵢᴹ :
  ∀ (A : Set)
    {B : A → Set} (Bᴹ : ∀ a → Setᴹ (B a))
    {t : Πₙᵢ A B}(tᴹ : ∀ a → Bᴹ a (t a))
  → Πₙᵢᴹ A Bᴹ t
lamₙᵢᴹ A {B} Bᴹ {t} tᴹ = λ a → tᴹ a

Πₙᵢβᴹ :
  ∀ {A : Set}
    {B : A → Set}  (Bᴹ : ∀ a → Setᴹ (B a))
    {t : Πₙᵢ A B}(tᴹ : ∀ a → Bᴹ a (t a))
    (u : A)
  → appₙᵢᴹ A Bᴹ (lamₙᵢᴹ A Bᴹ tᴹ) u ≡ tᴹ u
Πₙᵢβᴹ _ _ _  = refl

Πₙᵢηᴹ :
  ∀ (A : Set)
    {B : A → Set}  (Bᴹ : ∀ a → Setᴹ (B a))
    {t : Πₙᵢ A B}  (tᴹ : Πₙᵢᴹ A Bᴹ t)
  → lamₙᵢᴹ A Bᴹ (appₙᵢᴹ A Bᴹ tᴹ) ≡ tᴹ
Πₙᵢηᴹ _ _ _ = refl

-- small non-inductive functions (exactly the same as non-inductive for ᴹ)

Πₙᵢₛᴹ : (A : Set){b : A → Set}(Bᴹ : ∀ a → Uᴹ (b a)) → Uᴹ (Πₙᵢₛ A b)
Πₙᵢₛᴹ A {B} bᴹ = λ f → ∀ a → bᴹ a (f a)

appₙᵢₛᴹ :
  ∀ (A : Set)
    {b : A → Set}  (bᴹ : ∀ a → Uᴹ (b a))
    {t : Πₙᵢₛ A b} (tᴹ : Πₙᵢₛᴹ A bᴹ t)
    (u : A)
  → bᴹ u (t u)
appₙᵢₛᴹ A {B} _ {t} tᴹ u = tᴹ u

lamₙᵢₛᴹ :
  ∀ (A : Set)
    {b : A → Set} (bᴹ : ∀ a → Uᴹ (b a))
    {t : Πₙᵢₛ A b}(tᴹ : ∀ a → bᴹ a (t a))
  → Πₙᵢₛᴹ A bᴹ t
lamₙᵢₛᴹ A {B} _ {t} tᴹ = λ a → tᴹ a

Πₙᵢₛβᴹ :
  ∀ {A : Set}
    {b : A → Set}  (bᴹ : ∀ a → Uᴹ (b a))
    {t : Πₙᵢₛ A b}(tᴹ : ∀ a → bᴹ a (t a))
    (u : A)
  → appₙᵢₛᴹ A bᴹ (lamₙᵢₛᴹ A bᴹ tᴹ) u ≡ tᴹ u
Πₙᵢₛβᴹ _ _ _  = refl

Πₙᵢₛηᴹ :
  ∀ (A : Set)
    {b : A → Set}   (bᴹ : ∀ a → Setᴹ (b a))
    {t : Πₙᵢₛ A b}  (tᴹ : Πₙᵢₛᴹ A bᴹ t)
  → lamₙᵢₛᴹ A bᴹ (appₙᵢₛᴹ A bᴹ tᴹ) ≡ tᴹ
Πₙᵢₛηᴹ _ _ _ = refl


-- Identity

≡ᴹ :
  {a : Set}    (aᴹ : Uᴹ a)
  {t : a}      (tᴹ :  aᴹ t)
  {u : a}      (uᴹ :  aᴹ u)
  → Uᴹ (t ≡ u)
≡ᴹ {a} aᴹ {t} tᴹ {u} uᴹ = λ z → tr aᴹ z tᴹ ≡ uᴹ

reflᴹ :
  {a : Set}  (aᴹ : Uᴹ a)
  {t : a}    (tᴹ :  aᴹ t)
  → (≡ᴹ aᴹ tᴹ tᴹ) refl
reflᴹ {a} aᴹ {t} tᴹ = refl

Jᴹ :
  {a  : Set}                  (aᴹ  : Uᴹ a)
  {t  : a}                    (tᴹ  : aᴹ t)
  {P  : ∀ x (z : t ≡ x) → Set}(Pᴹ  : ∀ {x} (xₘ : aᴹ x) {z} zₘ → Setᴹ (P x z))
  {pr : P t refl}             (prᴹ : Pᴹ tᴹ (reflᴹ aᴹ tᴹ) pr)
  {u  : a}                    (uᴹ  : aᴹ u)
  {eq : t ≡ u}                (eqᴹ : (≡ᴹ aᴹ tᴹ uᴹ) eq)
  → Pᴹ uᴹ eqᴹ (J a t P pr eq)
Jᴹ {a} aᴹ {t} tᴹ {P} Pᴹ {pr} prᴹ {u} uᴹ {eq} eqᴹ =
  J (aᴹ u) (tr aᴹ eq tᴹ)
     (λ xᴹ zᴹ → Pᴹ {u} xᴹ {eq} zᴹ (J a t P pr eq))
     (J a t
        (λ x z → Pᴹ {x} (tr aᴹ z tᴹ) {z} refl (J a t P pr z))
        prᴹ eq)
     eqᴹ

≡βᴹ :
  {a  : Set}                   (aᴹ  : Uᴹ a)
  {t  : a}                     (tᴹ  : aᴹ t)
  {P  : ∀ x (z : t ≡ x) → Set} (Pᴹ  : ∀ {x} (xₘ :  aᴹ x) {z} zₘ → Setᴹ (P x z))
  {pr : P t refl}              (prᴹ : Pᴹ tᴹ (reflᴹ aᴹ tᴹ) pr)
  → Jᴹ aᴹ tᴹ Pᴹ prᴹ tᴹ (reflᴹ aᴹ tᴹ) ≡ prᴹ
≡βᴹ _ _ _ _ = refl
