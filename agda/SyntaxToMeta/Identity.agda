a{-# OPTIONS --without-K #-}

module Identity where

open import Lib
open import Homomorphism

{-

-- H
--------------------------------------------------------------------------------

       ⊢ Γ
──────────────────
Γᴴ : Γᶜ → Γᶜ → Set

                    Γ ⊢ A
────────────────────────────────────────────────
Aᴴ : {γ₀ γ₁ : Γᶜ} → Γᴴ γ₀ γ₁ → Aᶜ γ → Aᶜ γ → Set

                     Γ ⊢ t : A
────────────────────────────────────────────────────────
tᴴ : {γ₀ γ₁ : Γᶜ}(γᴴ : Γᴴ γ₀ γ₁) → Aᴴ γᴴ (tᶜ γ₀) (tᶜ γ₁)

∙ᴴ          γ₀       γ₁       = ⊤
(Γ, x : A)ᴴ (γ₀, α₀) (γ₁, α₁) = (γᴴ : Γᴴ γ₀ γ₁) × (αᴴ : Aᴴ γᴴ α₀ α₁)

Uᴴ      γᴴ a₀ a₁ = a₀ → a₁
(El a)ᴴ γᴴ α₀ α₁ = aᴴ γᴴ α₀ = α₁

((x : a) → B)ᴴ γᴴ f₀ f₁ = (α₀ : aᶜ γ) → Bᴴ (γᴴ, refl (aᴴ γᴴ α₀)) (f₀ α₀) (f₁ (aᴴ γᴴ α₀))

xᴴ γᴴ = lookup x γᴴ

(t u)ᴴ γᴴ =
  J (aᶜ γ₁)
    (aᴴ γᴴ (uᶜ γ₀))
    (λ u₁ uᴴ → Bᴴ (γᴴ, uᴴ) (tᶜ γ₀ (uᶜ γ₀)) (tᶜ γ₁ u₁))
    (tᴴ γᴴ (uᶜ γ₀))
    (uᶜ γ₀)
    (uᴴ γᴴ)

(Id a t u)ᴴ γᴴ : tᶜ γ₀ = uᶜ γ₀ → tᶜ γ₁ = uᶜ γ₁
               := λ e. (tᴴ γᴴ ⁻¹) ◾ ap (aᴴ γᴴ) e ◾ uᴴ γᴴ

(refl t)ᴴ γᴴ : (El (Id a t t))ᴴ γᴴ (refl (tᶜ γ)) (refl (tᶜ γ))
             : (Id a t t)ᴴ γᴴ refl = refl
             : (tᴴ γᴴ ⁻¹) ◾ tᴴ γᴴ = refl
             := J (aᶜ γ) (tᶜ γ) (λ _ e → e ⁻¹ ◾ e = refl) refl (tᶜ γ) (tᴴ γᴴ)
             := inv⁻¹ (tᴴ γᴴ ⁻¹)

-- id
--------------------------------------------------------------------------------

         ⊢ Γ
────────────────────────
id Γ : (γ : Γᶜ) → Γᴴ γ γ

                Γ ⊢ A
───────────────────────────────────────────
id A : {γ : Γᶜ}(α : Aᶜ γ) → Aᴴ (id Γ γ) α α

               Γ ⊢ t : A
───────────────────────────────────────────
id t : {γ : Γᶜ} → id A (tᶜ γ) = tᴴ (id Γ γ)


id ∙           γ      = tt
id (Γ , x : A) (γ, α) = (id Γ γ, id A α)

id U α = λ (x : α). x

id (El a) α : (El a)ᴴ (id Γ γ) α α
            : aᴴ (id Γ γ) α = α

      (id a : id U (aᶜ γ) = aᴴ (id Γ γ))
      (id a : (λ x. x) = aᴴ (id Γ γ))

      := happly (id a ⁻¹) α

id ((x : a) → B) f : ((x : a) → B)ᴴ (id Γ γ) f f
                   : (α : aᶜ γ) → Bᴴ (id Γ γ, refl (aᴴ (id Γ γ) α)) (f α) (f (aᴴ (id Γ γ) α))

      (id B : ((γ, α) : (γ : Γᶜ) × aᶜ γ)(β : Bᶜ (γ, α)) → Bᴴ (id (Γ, x : El a) (γ, α)) β β
      (id B (γ, α) (f α) : Bᴴ (id (Γ, x : El a) (γ, α)) (f α) (f α))
      (id B (γ, α) (f α) : Bᴴ (id Γ γ, id (El a) α) (f α) (f α))
      (id B (γ, α) (f α) : Bᴴ (id Γ γ, happly (id a ⁻¹) α) (f α) (f α))

      (id a : (λ x. x) = aᴴ (id Γ γ))

      := λ (α : aᶜ γ).
         J (aᶜ γ → aᶜ γ) (λ x. x)
           (λ aᴴidΓγ ida. Bᴴ (id Γ γ, happly (ida ⁻¹) α) (f α) (f α)
                        → Bᴴ (id Γ γ, refl (aᴴidΓγ α)  ) (f α) (f (aᴴidΓγ α)))
           (λ idB. idB)
           (aᴴ (id Γ γ))
           (id a)
           (id B {γ, α} (f α))

id (t u) (γ : Γᶜ) : id (B[x ⊢> u]) (tᶜ γ (uᶜ γ)) = (t u)ᴴ (id Γ γ)
                  : id (B[x ⊢ u]) (tᶜ γ (uᶜ γ))
                    =
                    J (aᶜ γ)
                      (aᴴ (id Γ γ) (uᶜ γ))
                      (λ u₁ uᴴ → Bᴴ (id Γ γ, uᴴ) (tᶜ γ (uᶜ γ)) (tᶜ γ u₁))
                      (tᴴ (id Γ γ) (uᶜ γ))
                      (uᶜ γ₀)
                      (uᴴ (id Γ γ))

                  : id B {γ , u} (tᶜ γ (uᶜ γ))
                    =
                    J (aᶜ γ)
                      (aᴴ (id Γ γ) (uᶜ γ₀))
                      (λ u₁ uᴴ → Bᴴ (id Γ γ, uᴴ) (tᶜ γ (uᶜ γ)) (tᶜ γ u₁))
                      (tᴴ (id Γ γ) (uᶜ γ))
                      (uᶜ γ)
                      (uᴴ (id Γ γ))

  id a : (λ x. x) = aᴴ (id Γ γ)

  id t : id ((x : a) → B) (tᶜ γ) = tᴴ (id Γ γ)
       : λ (α : aᶜ γ).
           J (aᶜ γ → aᶜ γ) (λ x. x)
             (λ aᴴidΓγ ida. Bᴴ (id Γ γ, happly (ida ⁻¹) α) (tᶜ γ α) (tᶜ γ α)
                          → Bᴴ (id Γ γ, refl (aᴴidΓγ α)  ) (tᶜ γ α) (tᶜ γ (aᴴidΓγ α)))
             (λ idB. idB)
             (aᴴ (id Γ γ))
             (id a)
             (id B {γ, α} (tᶜ γ α))
         =
         tᴴ (id Γ γ)

  id u : id (El a) (uᶜ γ)        = uᴴ (id Γ γ)
       : happly (id a ⁻¹) (uᶜ γ) = uᴴ (id Γ γ)

  1. Transport over (id u) to rewrite

    Goal : id B {γ , u} (tᶜ γ (uᶜ γ))
           =
           J (aᶜ γ)
             (aᴴ (id Γ γ) (uᶜ γ))
             (λ u₁ uᴴ → Bᴴ (id Γ γ, uᴴ) (tᶜ γ (uᶜ γ)) (tᶜ γ u₁))
             (tᴴ (id Γ γ) (uᶜ γ))
             (uᶜ γ)
             (happly (id a ⁻¹) (uᶜ γ))

  2. J on (id a) to rewrite:

    id_t : (λ (α : aᶜ γ). (id B {γ, α} (tᶜ γ α))) = tᴴ (id Γ γ)

    Goal : id B {γ , u} (tᶜ γ (uᶜ γ)) = tᴴ (id Γ γ) (uᶜ γ)

  3. happly id_t (uᶜ γ)    QED


id (Id a t u) {γ} : id U (tᶜ γ = uᶜ γ) = (Id a t u)ᴴ (id Γ γ)
                  : (λ e. e) = (λ e. (tᴴ (id Γ γ) ⁻¹) ◾ ap (aᴴ (Id Γ γ)) e ◾ uᴴ (id Γ γ))

  id a : (λ x. x)                = aᴴ (id Γ γ)
  id t : happly (id a ⁻¹) (tᶜ γ) = tᴴ (id Γ γ)
  id u : happly (id a ⁻¹) (uᶜ γ) = uᴴ (id Γ γ)

  1. Transport over (id t) and (id u)

    Goal : (λ e. e) = (λ e. (happly (id a ⁻¹) (tᶜ γ) ⁻¹) ◾ ap (aᴴ (Id Γ γ)) e ◾ happly (id a ⁻¹) (tᶜ γ))

  2. J on (id a)

    Goal : (λ e. e) = (λ e. ap (λ x. x) e ◾ refl)

  3. Use funext, then obvious  QED

  := transp (λ p. (λ e. e) = (λ e. p ⁻¹ ◾ ap (aᴴ (Id Γ γ)) e ◾ uᴴ (id Γ γ))) (id t)
     (transp (λ p. (λ e. e) = (λ e. happly (id a ⁻¹) (tᶜ γ) ⁻¹) ◾ ap (aᴴ (Id Γ γ)) e ◾ p) (id u)
       (J (aᶜ γ → aᶜ γ) (λ x. x)
          (λ aᴴidΓγ ida → (λ e. e) = (λ e. (happly (ida ⁻¹) (tᶜ γ) ⁻¹) ◾ ap aᴴidΓγ e ◾ happly (ida ⁻¹) (tᶜ γ)))
          (funext (λ e → J (aᶜ γ) (tᶜ γ) (λ _ e → e = ap (λ x. x) e ◾ refl) refl (uᶜ γ) e))
          (aᴴ (id Γ γ))
          (id a)))

id (refl t) {γ} : id (El (Id a t t))        (refl (tᶜ γ)) = (refl t)ᴴ (id Γ γ)
                : id (El (Id a t t))        (refl (tᶜ γ)) = inv⁻¹ (tᴴ (id Γ γ))
                : happly (id (Id a t t) ⁻¹) (refl (tᶜ γ)) = inv⁻¹ (tᴴ (id Γ γ))


  id (Id a t t) ≡
    transp (λ p. (λ e. e) = (λ e. p ⁻¹ ◾ ap (aᴴ (Id Γ γ)) e ◾ tᴴ (id Γ γ))) (id t)
         (transp (λ p. (λ e. e) = (λ e. happly (id a ⁻¹) (tᶜ γ) ⁻¹) ◾ ap (aᴴ (Id Γ γ)) e ◾ p) (id t)
           (J (aᶜ γ → aᶜ γ) (λ x. x)
              (λ aᴴidΓγ ida → (λ e. e) = (λ e. (happly (ida ⁻¹) (tᶜ γ) ⁻¹) ◾ ap aᴴidΓγ e ◾ happly (ida ⁻¹) (tᶜ γ)))
              (funext (λ e → J (aᶜ γ) (tᶜ γ) (λ _ e → e = ap (λ x. x) e ◾ refl) refl (tᶜ γ) e))
              (aᴴ (id Γ γ))
              (id a)))

  id a : (λ x. x) = aᴴ (id Γ γ)
  id t : happly (id a ⁻¹) (tᶜ γ) = tᴴ (id Γ γ)

  1. J on (id t)

    id (Id a t t) ≡
      (J (aᶜ γ → aᶜ γ) (λ x. x)
         (λ aᴴidΓγ ida → (λ e. e) = (λ e. (happly (ida ⁻¹) (tᶜ γ) ⁻¹) ◾ ap aᴴidΓγ e ◾ happly (ida ⁻¹) (tᶜ γ)))
         (funext (λ e → J (aᶜ γ) (tᶜ γ) (λ _ e → e = ap (λ x. x) e ◾ refl) refl (tᶜ γ) e))
         (aᴴ (id Γ γ))
         (id a)))

  2. J on (id a)

    id (Id a t t) ≡
      (funext (λ e → J (aᶜ γ) (tᶜ γ) (λ _ e → e = ap (λ x. x) e ◾ refl) refl (tᶜ γ) e))

    Goal : happly (funext (λ e → J (aᶜ γ) (tᶜ γ) (λ _ e → e = ap (λ x. x) e ◾ refl) refl (tᶜ γ) e) ⁻¹) (refl (tᶜ γ))
           = refl

  3. Lift ⁻¹

    Goal : happly (funext (λ e → J (aᶜ γ) (tᶜ γ) (λ _ e → e = ap (λ x. x) e ◾ refl) refl (tᶜ γ) e)) (refl (tᶜ γ)) ⁻¹
           = refl

  4. Strong funext:

    Goal :   refl
           = refl  QED


-}
