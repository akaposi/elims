{-# OPTIONS --without-K #-}

module Homomorphism where

open import Lib

Setᴴ : (A₀ A₁ : Set) → Set₁
Setᴴ A₀ A₁ = A₀ → A₁ → Set

Uᴴ : (a₀ a₁ : Set) → Set
Uᴴ a₀ a₁ = a₀ → a₁

Elᴴ : {a₀ a₁ : Set}(aᴴ : Uᴴ a₀ a₁) → Setᴴ a₀ a₁
Elᴴ {a₀}{a₁} aᴴ x₀ x₁ = aᴴ x₀ ≡ x₁

-- inductive functions
Πᴴ : {a₀ : Set}     {a₁ : Set}     (aᴴ : Uᴴ a₀ a₁)
     {B₀ : a₀ → Set}{B₁ : a₁ → Set}(Bᴴ : ∀ {x₀ x₁} (xᴴ : Elᴴ aᴴ x₀ x₁) → Setᴴ (B₀ x₀) (B₁ x₁))
     → Setᴴ (Π a₀ B₀) (Π a₁ B₁)
Πᴴ {a₀}{a₁} aᴴ {B₀}{B₁} Bᴴ f₀ f₁ = (x₀ : a₀) → Bᴴ {x₀}{aᴴ x₀} refl (f₀ x₀) (f₁ (aᴴ x₀))

appᴴ :
  {a₀ : Set}     {a₁ : Set}     (aᴴ : Uᴴ a₀ a₁)
  {B₀ : a₀ → Set}{B₁ : a₁ → Set}(Bᴴ : ∀ {x₀ x₁} (xᴴ : Elᴴ aᴴ x₀ x₁) → Setᴴ (B₀ x₀) (B₁ x₁))
  {t₀ : Π a₀ B₀} {t₁ : Π a₁ B₁} (tᴴ : Πᴴ aᴴ Bᴴ t₀ t₁)
  {u₀ : a₀}      {u₁ : a₁}      (uᴴ : Elᴴ aᴴ u₀ u₁)
  → Bᴴ uᴴ (t₀ u₀) (t₁ u₁)
appᴴ {a₀}{a₁} aᴴ {B₀}{B₁} Bᴴ {t₀}{t₁} tᴴ {u₀}{u₁} uᴴ =
  J a₁ (aᴴ u₀) (λ u₁ uᴴ → Bᴴ uᴴ (t₀ u₀) (t₁ u₁)) (tᴴ u₀) uᴴ

lamᴴ :
  {a₀ : Set}         {a₁ : Set}         (aᴴ : Uᴴ a₀ a₁)
  {B₀ : a₀ → Set}    {B₁ : a₁ → Set}    (Bᴴ : ∀ {x₀ x₁} (xᴴ : Elᴴ aᴴ x₀ x₁) → Setᴴ (B₀ x₀) (B₁ x₁))
  {t₀ : ∀ x₀ → B₀ x₀}{t₁ : ∀ x₁ → B₁ x₁}(tᴴ : ∀ {x₀ x₁} (xᴴ : Elᴴ aᴴ x₀ x₁) → Bᴴ xᴴ (t₀ x₀) (t₁ x₁))
  → Πᴴ aᴴ Bᴴ (λ x₀ → t₀ x₀) (λ x₁ → t₁ x₁)
lamᴴ {a₀}{a₁} aᴴ {B₀}{B₁} Bᴴ {t₀}{t₁} tᴴ = λ x₀ → tᴴ {x₀}{aᴴ x₀} refl

Πβᴴ :
  {a₀ : Set}         {a₁ : Set}         (aᴴ : Uᴴ a₀ a₁)
  {B₀ : a₀ → Set}    {B₁ : a₁ → Set}    (Bᴴ : ∀ {x₀ x₁} (xᴴ : Elᴴ aᴴ x₀ x₁) → Setᴴ (B₀ x₀) (B₁ x₁))
  {t₀ : ∀ x₀ → B₀ x₀}{t₁ : ∀ x₁ → B₁ x₁}(tᴴ : ∀ {x₀ x₁} (xᴴ : Elᴴ aᴴ x₀ x₁) → Bᴴ xᴴ (t₀ x₀) (t₁ x₁))
  {u₀ : a₀}          {u₁ : a₁}          (uᴴ : Elᴴ aᴴ u₀ u₁)
  → appᴴ aᴴ Bᴴ (lamᴴ aᴴ Bᴴ tᴴ) uᴴ ≡ tᴴ uᴴ
Πβᴴ {a₀}{a₁} aᴴ {B₀}{B₁} Bᴴ {t₀}{t₁} tᴴ {u₀}{u₁} uᴴ =
  J _ _ (λ u₁ uᴴ → J a₁ (aᴴ u₀) (λ u₂ uᴴ₁ → Bᴴ uᴴ₁ (t₀ u₀) (t₁ u₂)) (tᴴ refl) uᴴ ≡ tᴴ uᴴ) refl uᴴ

Πηᴴ :
  {a₀ : Set}         {a₁ : Set}         (aᴴ : Uᴴ a₀ a₁)
  {B₀ : a₀ → Set}    {B₁ : a₁ → Set}    (Bᴴ : ∀ {x₀ x₁} (xᴴ : Elᴴ aᴴ x₀ x₁) → Setᴴ (B₀ x₀) (B₁ x₁))
  {t₀ : Π a₀ B₀} {t₁ : Π a₁ B₁} (tᴴ : Πᴴ aᴴ Bᴴ t₀ t₁)
  → lamᴴ aᴴ Bᴴ (appᴴ aᴴ Bᴴ {t₀}{t₁} tᴴ) ≡ tᴴ
Πηᴴ {a₀}{a₁} aᴴ {B₀}{B₁} Bᴴ {t₀}{t₁} tᴴ = refl

-- non-inductive functions
Πₙᵢᴴ :
  (A : Set)
  {B₀ B₁ : A → Set} (Bᴴ : (a : A) → Setᴴ (B₀ a) (B₁ a))
  → Setᴴ (Πₙᵢ A B₀) (Πₙᵢ A B₁)
Πₙᵢᴴ A Bᴴ f₀ f₁ = (α : A) → Bᴴ α (f₀ α) (f₁ α)

appₙᵢᴴ :
  (A : Set)
  {B₀ B₁ : A → Set}              (Bᴴ : (a : A) → Setᴴ (B₀ a) (B₁ a))
  {t₀ : Πₙᵢ A B₀}{t₁ : Πₙᵢ A B₁} (tᴴ : Πₙᵢᴴ A Bᴴ t₀ t₁)
  (α : A)
  → Bᴴ α (t₀ α) (t₁ α)
appₙᵢᴴ A {B₀}{B₁} Bᴴ {t₀}{t₁} tᴴ α = tᴴ α

lamₙᵢᴴ :
  (A : Set)
  {B₀ B₁ : A → Set}                          (Bᴴ : (a : A) → Setᴴ (B₀ a) (B₁ a))
  {t₀ : (α : A) → B₀ α}{t₁ : (α : A) → B₁ α} (tᴴ : (α : A) → Bᴴ α (t₀ α) (t₁ α))
  → Πₙᵢᴴ A Bᴴ t₀ t₁
lamₙᵢᴴ A {B₀}{B₁} Bᴴ {t₀}{t₁} tᴴ α = tᴴ α

Πₙᵢᴴβ :
  (A : Set)
  {B₀ B₁ : A → Set}                          (Bᴴ : (a : A) → Setᴴ (B₀ a) (B₁ a))
  {t₀ : (α : A) → B₀ α}{t₁ : (α : A) → B₁ α} (tᴴ : (α : A) → Bᴴ α (t₀ α) (t₁ α))
  (α : A)
  → appₙᵢᴴ A Bᴴ tᴴ α ≡ tᴴ α
Πₙᵢᴴβ _ _ _ _ = refl

Πₙᵢηᴴ :
  (A : Set)
  {B₀ B₁ : A → Set}              (Bᴴ : (a : A) → Setᴴ (B₀ a) (B₁ a))
  {t₀ : Πₙᵢ A B₀}{t₁ : Πₙᵢ A B₁} (tᴴ : Πₙᵢᴴ A Bᴴ t₀ t₁)
  → lamₙᵢᴴ A Bᴴ (appₙᵢᴴ A Bᴴ tᴴ) ≡ tᴴ
Πₙᵢηᴴ _ _ _ = refl

-- small non-inductive functions
Πₙᵢₛᴴ :
  (A : Set)
  {b₀ b₁ : A → Set}(bᴴ : (α : A) → Uᴴ (b₀ α) (b₁ α))
  → Uᴴ (Πₙᵢₛ A b₀) (Πₙᵢₛ A b₁)
Πₙᵢₛᴴ A {b₀}{b₁} bᴴ = λ f α → bᴴ α (f α)

appₙᵢₛᴴ :
  (A : Set)
  {b₀ b₁ : A → Set}(bᴴ : (α : A) → Uᴴ (b₀ α) (b₁ α))
  {t₀ : Πₙᵢₛ A b₀} {t₁ : Πₙᵢₛ A b₁}(tᴴ : Elᴴ (Πₙᵢₛᴴ A bᴴ) t₀ t₁)
  (α : A)
  → Elᴴ (bᴴ α) (t₀ α) (t₁ α)
appₙᵢₛᴴ A {b₀}{b₁} bᴴ {t₀}{t₁} tᴴ α = happly tᴴ α

lamₙᵢₛᴴ :
  (A : Set)
  {b₀ b₁ : A → Set}(bᴴ : (α : A) → Uᴴ (b₀ α) (b₁ α))
  {t₀ : (α : A) → b₀ α}{t₁ : (α : A) → b₁ α}(tᴴ : (α : A) → Elᴴ (bᴴ α) (t₀ α) (t₁ α))
  → Elᴴ (Πₙᵢₛᴴ A bᴴ) t₀ t₁
lamₙᵢₛᴴ A {b₀}{b₁} bᴴ {t₀}{t₁} tᴴ = funext tᴴ

Πₙᵢₛβᴴ :
  (A : Set)
  {b₀ b₁ : A → Set}(bᴴ : (α : A) → Uᴴ (b₀ α) (b₁ α))
  {t₀ : (α : A) → b₀ α}{t₁ : (α : A) → b₁ α}(tᴴ : (α : A) → Elᴴ (bᴴ α) (t₀ α) (t₁ α))
  (α : A)
  → appₙᵢₛᴴ A bᴴ (lamₙᵢₛᴴ A bᴴ tᴴ) α ≡ tᴴ α
Πₙᵢₛβᴴ A {b₀}{b₁} bᴴ {t₀}{t₁} tᴴ α =
  let (p , q , r , s) = funexts (λ a → bᴴ a (t₀ a)) t₁
  in happly (r tᴴ) α

Πₙᵢₛηᴴ :
  (A : Set)
  {b₀ b₁ : A → Set}(bᴴ : (α : A) → Uᴴ (b₀ α) (b₁ α))
  {t₀ : Πₙᵢₛ A b₀} {t₁ : Πₙᵢₛ A b₁}(tᴴ : Elᴴ (Πₙᵢₛᴴ A bᴴ) t₀ t₁)
  → lamₙᵢₛᴴ A bᴴ (appₙᵢₛᴴ A bᴴ tᴴ) ≡ tᴴ
Πₙᵢₛηᴴ A {b₀}{b₁} bᴴ {t₀}{t₁} tᴴ =
  let (p , q , r , s) = funexts (λ a → bᴴ a (t₀ a)) t₁
  in s tᴴ

-- Identity
≡ᴴ :
  {a₀ a₁ : Set}(aᴴ : Uᴴ a₀ a₁)
  {t₀ : a₀}{t₁ : a₁}(tᴴ : Elᴴ aᴴ t₀ t₁)
  {u₀ : a₀}{u₁ : a₁}(uᴴ : Elᴴ aᴴ u₀ u₁)
  → Uᴴ (t₀ ≡ u₀) (t₁ ≡ u₁)
≡ᴴ {a₀}{a₁} aᴴ {t₀}{t₁} tᴴ {u₀}{u₁} uᴴ = λ e → (tᴴ ⁻¹) ◾ ap aᴴ e ◾ uᴴ

reflᴴ :
  {a₀ a₁ : Set}(aᴴ : Uᴴ a₀ a₁)
  {t₀ : a₀}{t₁ : a₁}(tᴴ : Elᴴ aᴴ t₀ t₁)
  → Elᴴ (≡ᴴ aᴴ tᴴ tᴴ) refl refl
reflᴴ {a₀}{a₁} aᴴ {t₀}{t₁} tᴴ = inv⁻¹ tᴴ

Jᴴ :
  {a₀ : Set}{a₁ : Set}(aᴴ : Uᴴ a₀ a₁)
  {t₀ : a₀} {t₁ : a₁} (tᴴ : Elᴴ aᴴ t₀ t₁)

  {P₀ : (x₀ : a₀)(z₀ : t₀ ≡ x₀) → Set}
    {P₁ : (x₁ : a₁)(z₁ : t₁ ≡ x₁) → Set}
      (Pᴴ : ∀ {x₀ x₁} (xᴴ : Elᴴ aᴴ x₀ x₁) {z₀ z₁} (zᴴ : Elᴴ (≡ᴴ aᴴ tᴴ xᴴ) z₀ z₁) → Setᴴ (P₀ x₀ z₀) (P₁ x₁ z₁))

  {pr₀ : P₀ t₀ refl}
    {pr₁ : P₁ t₁ refl}
      (prᴴ : Pᴴ tᴴ (reflᴴ aᴴ tᴴ) pr₀ pr₁)

  {u₀ : a₀}      {u₁ : a₁}      (uᴴ : Elᴴ aᴴ u₀ u₁)
  {eq₀ : t₀ ≡ u₀}{eq₁ : t₁ ≡ u₁}(eqᴴ : Elᴴ (≡ᴴ aᴴ tᴴ uᴴ) eq₀ eq₁)

  → Pᴴ uᴴ eqᴴ (J a₀ t₀ P₀ pr₀ eq₀) (J a₁ t₁ P₁ pr₁ eq₁)
Jᴴ {a₀}{a₁} aᴴ {t₀}{t₁} tᴴ {P₀}{P₁} Pᴴ {pr₀}{pr₁} prᴴ {u₀}{u₁} uᴴ {eq₀}{eq₁} eqᴴ =
  J _ _ (λ eq₁ eqᴴ → Pᴴ uᴴ eqᴴ (J a₀ t₀ P₀ pr₀ eq₀) (J a₁ t₁ P₁ pr₁ eq₁))
    (J _ _ (λ u₁ uᴴ → Pᴴ uᴴ refl (J a₀ t₀ P₀ pr₀ eq₀) (J a₁ t₁ P₁ pr₁ ((tᴴ ⁻¹) ◾ ap aᴴ eq₀ ◾ uᴴ)))
      (J _ _ (λ u₀ eq₀ → Pᴴ refl refl (J a₀ t₀ P₀ pr₀ eq₀) (J a₁ t₁ P₁ pr₁ ((tᴴ ⁻¹) ◾ ap aᴴ eq₀ ◾ refl)))
        (J _ _ (λ t₁ tᴴ →
           (P₁  : (x₁ : a₁) → t₁ ≡ x₁ → Set)
           (Pᴴ : ∀ {x₀ x₁} (xᴴ : Elᴴ aᴴ x₀ x₁) {z₀ z₁} (zᴴ : Elᴴ (≡ᴴ aᴴ tᴴ xᴴ) z₀ z₁)
                 → Setᴴ (P₀ x₀ z₀) (P₁ x₁ z₁))
           (pr₁ : P₁ t₁ refl)
           (prᴴ : Pᴴ tᴴ (J a₁ (aᴴ t₀) (λ u eq → (eq ⁻¹) ◾ eq ≡ refl) refl tᴴ) pr₀ pr₁)
           → Pᴴ refl refl pr₀ (J a₁ t₁ P₁ pr₁ ((tᴴ ⁻¹) ◾ refl)))
           (λ P₁ Pᴴ pr₁ prᴴ → prᴴ)
           tᴴ P₁ Pᴴ pr₁ prᴴ)
        eq₀)
      uᴴ)
    eqᴴ

Jᴴβ :
  {a₀ : Set}{a₁ : Set}(aᴴ : Uᴴ a₀ a₁)
  {t₀ : a₀} {t₁ : a₁} (tᴴ : Elᴴ aᴴ t₀ t₁)

  {P₀ : (x₀ : a₀)(z₀ : t₀ ≡ x₀) → Set}
    {P₁ : (x₁ : a₁)(z₁ : t₁ ≡ x₁) → Set}
      (Pᴴ : ∀ {x₀ x₁} (xᴴ : Elᴴ aᴴ x₀ x₁) {z₀ z₁} (zᴴ : Elᴴ (≡ᴴ aᴴ tᴴ xᴴ) z₀ z₁) → Setᴴ (P₀ x₀ z₀) (P₁ x₁ z₁))
  {pr₀ : P₀ t₀ refl}
    {pr₁ : P₁ t₁ refl}
      (prᴴ : Pᴴ tᴴ (reflᴴ aᴴ tᴴ) pr₀ pr₁)
  → Jᴴ aᴴ tᴴ Pᴴ prᴴ tᴴ (reflᴴ aᴴ tᴴ) ≡ prᴴ
Jᴴβ {a₀} {a₁} aᴴ {t₀} {.(aᴴ t₀)} refl {P₀} {P₁} Pᴴ {pr₀} {pr₁} prᴴ = refl
