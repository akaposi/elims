n
## Goals:

- Nicely normalized goal types
- Fast typechecking
- Control over how we print stuff (show pragma?)
- Principled lemmas

## Non-priority:

- Modularity

## Notes

## Ambrus Σ-models

- Very nice
- Existence of recursor/eliminator also straightforward
- We should show that Recursion.agda is implementable from
  Elimination.agda
