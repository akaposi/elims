
open import StrictLib

infixl 5 _,_
infixl 7 _[_]T
infixl 5 _,s_
infix  6 _∘_
infixl 8 _[_]t

postulate
  Con : Set
  Ty  : Con → Set
  Tm  : ∀ Γ → Ty Γ → Set
  Tms : Con → Con → Set

  ∙     : Con
  _,_   : (Γ : Con) → Ty Γ → Con

  _[_]T : ∀{Γ Δ} → Ty Δ → Tms Γ Δ → Ty Γ

  id    : ∀{Γ} → Tms Γ Γ
  _∘_   : ∀{Γ Δ Σ} → Tms Δ Σ → Tms Γ Δ → Tms Γ Σ
  ε     : ∀{Γ} → Tms Γ ∙
  _,s_  : ∀{Γ Δ}(σ : Tms Γ Δ){A : Ty Δ} → Tm Γ (A [ σ ]T) → Tms Γ (Δ , A)
  π₁    : ∀{Γ Δ}{A : Ty Δ} → Tms Γ (Δ , A) → Tms Γ Δ

  _[_]t : ∀{Γ Δ}{A : Ty Δ} → Tm Δ A → (σ : Tms Γ Δ) → Tm Γ (A [ σ ]T)
  π₂    : ∀{Γ Δ}{A : Ty Δ}(σ : Tms Γ (Δ , A)) → Tm Γ (A [ π₁ σ ]T)

  [id]T : ∀{Γ}{A : Ty Γ} → A [ id ]T ≡ A
  [][]T : ∀{Γ Δ Σ}{A : Ty Σ}{σ : Tms Γ Δ}{δ : Tms Δ Σ}
          → A [ δ ]T [ σ ]T ≡ A [ δ ∘ σ ]T

  [id]t : ∀ {Γ A}{t : Tm Γ A} → tr (Tm Γ) [id]T (t [ id ]t) ≡ t
  [][]t : ∀ {Γ Δ Σ A}{t : Tm Σ A}{σ : Tms Δ Σ}{δ : Tms Γ Δ}
          → tr (Tm Γ) [][]T (t [ σ ]t [ δ ]t) ≡ t [ σ ∘ δ ]t

  idl   : ∀{Γ Δ}{σ : Tms Γ Δ} → (id ∘ σ) ≡ σ
  idr   : ∀{Γ Δ}{σ : Tms Γ Δ} → (σ ∘ id) ≡ σ
  ass   : ∀{Γ Δ Σ Ω}{σ : Tms Σ Ω}{δ : Tms Δ Σ}{ν : Tms Γ Δ}
        → (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν)

  ,∘    : ∀{Γ Δ Σ}{δ : Tms Γ Δ}{σ : Tms Σ Γ}{A : Ty Δ}{t : Tm Γ (A [ δ ]T)}
        → ((δ ,s t) ∘ σ) ≡ ((δ ∘ σ) ,s tr (Tm Σ) [][]T (t [ σ ]t))
  π₁β   : ∀{Γ Δ}{A : Ty Δ}{σ : Tms Γ Δ}{t : Tm Γ (A [ σ ]T)}
        → (π₁ (σ ,s t)) ≡ σ
  πη    : ∀{Γ Δ}{A : Ty Δ}{σ : Tms Γ (Δ , A)}
        → (π₁ σ ,s π₂ σ) ≡ σ
  εη    : ∀{Γ}{σ : Tms Γ ∙}
        → σ ≡ ε
  π₂β   : ∀{Γ Δ}{A : Ty Δ}{σ : Tms Γ Δ}{t : Tm Γ (A [ σ ]T)}
        → π₂ (σ ,s t) ≡ tr (λ σ → Tm Γ (A [ σ ]T)) (π₁β ⁻¹) t

wk : ∀{Γ}{A : Ty Γ} → Tms (Γ , A) Γ
wk = π₁ id

vz : ∀{Γ}{A : Ty Γ} → Tm (Γ , A) (A [ wk ]T)
vz = π₂ id

vs : ∀{Γ}{A B : Ty Γ} → Tm Γ A → Tm (Γ , B) (A [ wk ]T)
vs x = x [ wk ]t

<_> : ∀{Γ}{A : Ty Γ} → Tm Γ A → Tms Γ (Γ , A)
< t > = id ,s tr (Tm _) ([id]T ⁻¹) t

infix 4 <_>

_^_ : {Γ Δ : Con}(σ : Tms Γ Δ)(A : Ty Δ) → Tms (Γ , A [ σ ]T) (Δ , A)
σ ^ A = (σ ∘ wk) ,s tr (Tm _) [][]T vz

infixl 5 _^_

postulate
  U    : ∀{Γ} → Ty Γ
  U[]  : ∀{Γ Δ}{σ : Tms Γ Δ} → (U [ σ ]T) ≡ U
  El   : ∀{Γ}(a : Tm Γ U) → Ty Γ
  El[] : ∀{Γ Δ}{σ : Tms Γ Δ}{a : Tm Δ U}
       → (El a [ σ ]T) ≡ (El (tr (Tm Γ) U[] (a [ σ ]t)))

-- Inductive functions
------------------------------------------------------------

--   Π : ∀{Γ}(a : Tm Γ U)(B : Ty (Γ , El a)) → Ty Γ

--   Π[] : ∀{Γ Δ}{σ : Tms Γ Δ}{a : Tm Δ U}{B : Ty (Δ , El a)}
--       → (Π a B) [ σ ]T ≡ Π (tr (Tm Γ) U[] (a [ σ ]t))
--                            (tr (λ x → Ty (Γ , x)) El[] (B [ σ ^ El a ]T))

--   app : ∀{Γ}{a : Tm Γ U}{B : Ty (Γ , El a)} → Tm Γ (Π a B) → Tm (Γ , El a) B
--   lam : ∀{Γ}{a : Tm Γ U}{B : Ty (Γ , El a)} → Tm (Γ , El a) B → Tm Γ (Π a B)

--   lam[] : ∀{Γ Δ}{σ : Tms Γ Δ}{a : Tm Δ U}{B : Ty (Δ , El a)}{t : Tm (Δ , El a) B}
--     → tr (Tm Γ) Π[] (lam t [ σ ]t) ≡
--         lam (tr2 (λ A → Tm (Γ , A)) El[] refl (t [ σ ^ El a ]t))

--   Πβ : ∀{Γ}{a : Tm Γ U}{B : Ty (Γ , El a)}{t : Tm (Γ , El a) B} → app (lam t) ≡ t
--   Πη : ∀{Γ}{a : Tm Γ U}{B : Ty (Γ , El a)}{t : Tm Γ (Π a B)}    → lam (app t) ≡ t

-- _$_ : ∀ {Γ}{a : Tm Γ U}{B : Ty (Γ , El a)}(t : Tm Γ (Π a B))(u : Tm Γ (El a)) → Tm Γ (B [ < u > ]T)
-- t $ u = (app t) [ < u > ]t

-- -- non-inductive functions
-- postulate
--   ΠNI : ∀ {Γ}(A : Set) → (A → Ty Γ) → Ty Γ

--   ΠNI[] : ∀ {Γ Δ}{σ : Tms Γ Δ}{A : Set}{B : A → Ty Δ}
--           → ΠNI A B [ σ ]T ≡ ΠNI A (λ α → B α [ σ ]T)

--   lamNI : ∀ {Γ}{A}{B : A → Ty Γ} → (∀ α → Tm Γ (B α)) → Tm Γ (ΠNI A B)
--   appNI : ∀ {Γ}{A}{B : A → Ty Γ} → Tm Γ (ΠNI A B) → (∀ α → Tm Γ (B α))

--   lamNI[] : ∀ {Γ Δ}{A}{σ : Tms Γ Δ}{B : A → Ty Δ}(t : ∀ α → Tm Δ (B α))
--             → tr (Tm Γ) ΠNI[] (lamNI t [ σ ]t) ≡ lamNI (λ α → t α [ σ ]t)

--   ΠNIβ : ∀ {Γ}{A}{B : A → Ty Γ}{t : ∀ α → Tm Γ (B α)}
--          → appNI (lamNI t) ≡ t

--   ΠNIη : ∀ {Γ}{A}{B : A → Ty Γ}{t : Tm Γ (ΠNI A B)}
--          → lamNI (appNI t) ≡ t

-- -- small non-inductive functions
-- postulate
--   Πₙᵢ : ∀ {Γ}(A : Set) → (A → Tm Γ U) → Tm Γ U

--   Πₙᵢ[] : ∀ {Γ Δ}{σ : Tms Γ Δ}{A : Set}{b : A → Tm Δ U}
--           → tr (Tm Γ) U[] (Πₙᵢ A b [ σ ]t) ≡ Πₙᵢ A (λ α → tr (Tm Γ) U[] (b α [ σ ]t))

--   lamₙᵢ : ∀ {Γ}{A}{b : A → Tm Γ U} → (∀ α → Tm Γ (El (b α))) → Tm Γ (El (Πₙᵢ A b))
--   appₙᵢ : ∀ {Γ}{A}{b : A → Tm Γ U} → Tm Γ (El (Πₙᵢ A b)) → (∀ α → Tm Γ (El (b α)))

--   lamₙᵢ[] : ∀ {Γ Δ}{A : Set}{σ : Tms Γ Δ}{b : A → Tm Δ U}(t : ∀ α → Tm Δ (El (b α)))
--             → tr (Tm Γ) (El[] ◾ ap El Πₙᵢ[]) (lamₙᵢ t [ σ ]t)
--             ≡ lamₙᵢ (λ α → tr (Tm Γ) El[] (t α [ σ ]t))

--   Πₙᵢβ : ∀ {Γ}{A}{b : A → Tm Γ U}{t : ∀ α → Tm Γ (El (b α))}
--          → appₙᵢ (lamₙᵢ t) ≡ t

--   Πₙᵢη : ∀ {Γ}{A}{B : A → Tm Γ U}{t : Tm Γ (El (Πₙᵢ A B))}
--          → lamₙᵢ (appₙᵢ t) ≡ t

-- identity (J TODO)
postulate
  Id   : ∀ {Γ}(a : Tm Γ U) → Tm Γ (El a) → Tm Γ (El a) → Tm Γ U
  Id[] : ∀ {Γ Δ}{σ : Tms Γ Δ}{a : Tm Δ U}{t u : Tm Δ (El a)}
         → tr (Tm Γ) (U[]) (Id a t u [ σ ]t) ≡
             (Id (tr (Tm Γ) U[] (a [ σ ]t))
                 (tr (Tm Γ) El[] (t [ σ ]t))
                 (tr (Tm Γ) El[] (u [ σ ]t)))

  Refl : ∀ {Γ}(a : Tm Γ U)(t : Tm Γ (El a)) → Tm Γ (El (Id a t t))

  Refl[] : ∀ {Γ Δ}{σ : Tms Γ Δ}{a : Tm Δ U}{t : Tm Δ (El a)}
           → tr (Tm Γ) (El[] ◾ ap El Id[])
             (Refl a t [ σ ]t) ≡
               (Refl
                 (tr (Tm Γ) U[] (a [ σ ]t))
                 (tr (Tm Γ) El[] (t [ σ ]t)))

-- J preliminaries
------------------------------------------------------------

zU : ∀ {Γ} → Tm (Γ , U) U
zU = tr (Tm _) U[] vz

infixl 3 sU_
sU_ : ∀ {Γ A} → Tm Γ U → Tm (Γ , A) U
sU_ t = tr (Tm _) U[] (vs t)

zEl : ∀ {Γ a} → Tm (Γ , El a) (El (sU a))
zEl = tr (Tm _) El[] vz

infixl 3 sEl_
sEl_ : ∀ {Γ a B} → Tm Γ (El a) → Tm (Γ , B) (El (sU a))
sEl_ t = tr (Tm _) El[] (vs t)

zId : ∀ {Γ a t u} → Tm (Γ , El (Id a t u)) (El (Id (sU a) (sEl t) (sEl u)))
zId = tr (Tm _) (El[] ◾ ap El Id[]) vz

infixl 3 sId_
sId_ : ∀ {Γ a t u B} → Tm Γ (El (Id a t u)) → Tm (Γ , B) (El (Id (sU a) (sEl t) (sEl u)))
sId_ t = tr (Tm _) (El[] ◾ ap El Id[]) (vs t)

_^El_ :
  {Γ Δ : Con}(σ : Tms Γ Δ)(a : Tm Δ U)
  → Tms (Γ , El (tr (Tm Γ) (U[] ) (a [ σ ]t))) (Δ , El a)
σ ^El a = σ ∘ wk ,s tr (Tm _) (ap (_[ wk ]T) (El[]  ⁻¹) ◾ ([][]T)) vz

infixl 5 _^El_

------------------------------------------------------------

π₁∘ : ∀{Γ Δ Σ}{A : Ty Δ}{δ : Tms Γ (Δ , A)}{ν : Tms Σ Γ}
     → π₁ δ ∘ ν ≡ π₁ (δ ∘ ν)
π₁∘ {Γ}{Δ}{Σ}{A}{δ}{ν}
  = (π₁β ⁻¹ ◾ ap π₁ (,∘ ⁻¹)) ◾ ap (λ x → π₁ (x ∘ ν)) πη

π₂β̃ :
  {Γ Δ : Con} {A : Ty Δ} {σ : Tms Γ Δ} {t : Tm Γ (A [ σ ]T)} →
  π₂ (σ ,s t) ≃ t
π₂β̃ {Γ} {Δ} {A} {σ} {t} = (π₂β ~) ◾̃ untr _ (π₁β ⁻¹) t

π₂[]̃ : ∀{Γ Δ Σ}{A : Ty Δ}{σ : Tms Γ (Δ , A)}{δ : Tms Σ Γ}
      → π₂ σ [ δ ]t ≃ π₂ (σ ∘ δ)
π₂[]̃ {σ = σ}{δ} =  ({!!} ◾̃ ap̃̃ π₂ (,∘ ⁻¹)) ◾̃ ap̃̃ (λ x → π₂ (x ∘ δ)) πη

wkβ : ∀{Γ Δ A}{σ : Tms Γ Δ}{t : Tm Γ (A [ σ ]T)}
      → wk ∘ (σ ,s t) ≡ σ
wkβ = π₁∘ ◾ ap π₁ idl ◾ π₁β

vzβ :
  ∀ {Γ Δ}{A : Ty Δ}{σ : Tms Γ Δ}{t : Tm Γ (A [ σ ]T)}
    → tr (Tm Γ) ([][]T ◾ ap (A [_]T) wkβ)
         (vz [ σ ,s t ]t) ≡ t
vzβ {Γ} {Δ} {A} {σ} {t} =
  {!!}

  -- vz [ < t > ]t ≡ t

[][]t̃ :
  {Γ Δ : Con} {Σ : Con} {A : Ty Σ} {t : Tm Σ A} {σ : Tms Δ Σ}
  {δ : Tms Γ Δ}
  → (t [ σ ]t [ δ ]t) ≃ (t [ σ ∘ δ ]t)
[][]t̃ {Γ} {Δ} {Σ} {t = t} {σ} {δ} =
  untr (Tm Γ) [][]T (t [ σ ]t [ δ ]t) ⁻¹̃ ◾̃ ([][]t {t = t}{σ}{δ} ~)

[id]t̃ :
  {Γ : Con} {A : Ty Γ} {t : Tm Γ A}
  → (t [ id ]t) ≃ t
[id]t̃ {Γ} {A} {t} =
  untr (Tm Γ) [id]T (t [ id ]t) ⁻¹̃ ◾̃ ([id]t ~)

Id[]-lem :
  ∀ {Γ}
  (a  : Tm Γ U)
  (t  : Tm Γ (El a))
  (u  : Tm Γ (El a))
  → Id a t u
  ≡  tr (Tm Γ) U[] (Id (sU a) zEl zEl [ id ,s tr (Tm Γ) ([id]T ⁻¹) u ]t)
Id[]-lem {Γ} a t u =
    ap3̃ Id
      (uñ (
           [id]t̃ ⁻¹̃
         ◾̃ ap̃̃ (a [_]t)(wkβ ⁻¹)
         ◾̃ [][]t̃ ⁻¹̃
         ◾̃ ap2̃̃ (λ A (t : Tm _ A) → t [ < u > ]t)
             U[] (untr (Tm (Γ , El a)) U[] (a [ π₁ id ]t) ⁻¹̃)
         ◾̃ untr (Tm Γ) U[] _ ⁻¹̃))
      (
       {!
          !}
        ◾̃ ap2̃̃ (λ A (t : Tm _ A) → t [ < u > ]t) El[]
            (untr (Tm (Γ , El a)) El[] (π₂ id) ⁻¹̃)
        ◾̃ untr (Tm Γ) El[] _ ⁻¹̃)
      {!!}
    -- IdΓ̃≡
    --   (uñ ({!ap3̃!} ◾̃ untr (Tm Γ) U[] _ ⁻¹̃))
    --   {!!}
    --   {!!}
  ◾ Id[] ⁻¹

-- postulate
--   IdJ :
--     ∀ {Γ}
--       (a  : Tm Γ U)
--       (t  : Tm Γ (El a))
--       (P  : Ty (Γ , El a , El (Id (sU a) zEl zEl)))
--       (pr : Tm Γ (P [ id ,s
--                       tr (Tm _) ([id]T ⁻¹) t ,s
--                       tr (Tm _) (ap El (Id[]-lem a t t) ◾ El[] ⁻¹) (Refl a t) ]T))
--       (u  : Tm Γ (El a))
--       (eq : Tm Γ (El (Id a t u)))
--     → Tm Γ (P [ id ,s
--                 tr (Tm _) ([id]T ⁻¹) u ,s
--                 tr (Tm Γ) (ap El (Id[]-lem a t u) ◾ El[] ⁻¹) eq ]T)
