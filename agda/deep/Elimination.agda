{-# OPTIONS --rewriting #-}

{-
USAGE:
  - copy & paste everything below to a new module
  - remove postulates & add implementation
  - Rename and export stuff as needed
-}

module Elimination where

open import Lib
open import Syntax

infixl 5 _,ᴹ_
infixl 7 _[_]Tᴹ
infixl 5 _,sᴹ_
infix  6 _∘ᴹ_
infixl 8 _[_]tᴹ

postulate
  i : Level
  j : Level

  Conᴹ : Con → Set i
  Tyᴹ  : ∀ {Γ}(Γᴹ : Conᴹ Γ) → Ty Γ → Set i
  Tmᴹ  : ∀ {Γ}(Γᴹ : Conᴹ Γ){A : Ty Γ}(Aᴹ : Tyᴹ Γᴹ A) → Tm Γ A → Set j
  Tmsᴹ : ∀{Γ}(Γᴹ : Conᴹ Γ){Δ}(Δᴹ : Conᴹ Δ) → Tms Γ Δ → Set j

  ∙ᴹ   : Conᴹ ∙
  _,ᴹ_ : ∀{Γ}(Γᴹ : Conᴹ Γ){A}(Aᴹ : Tyᴹ Γᴹ A) → Conᴹ (Γ , A)

  _[_]Tᴹ :
    ∀{Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{A : Ty Δ}(Aᴹ : Tyᴹ Δᴹ A){σ : Tms Γ Δ}(σᴹ : Tmsᴹ Γᴹ Δᴹ σ)
      → Tyᴹ Γᴹ (A [ σ ]T)

  idᴹ  : ∀ {Γ}{Γᴹ : Conᴹ Γ} → Tmsᴹ Γᴹ Γᴹ (id {Γ})
  _∘ᴹ_ : ∀ {Γ Γᴹ Δ Δᴹ Σ Σᴹ}{σ : Tms Δ Σ}(σᴹ : Tmsᴹ Δᴹ Σᴹ σ){δ : Tms Γ Δ}(δᴹ : Tmsᴹ Γᴹ Δᴹ δ)
         → Tmsᴹ Γᴹ Σᴹ (σ ∘ δ)

  εᴹ : ∀ {Γ}{Γᴹ : Conᴹ Γ} → Tmsᴹ {Γ} Γᴹ ∙ᴹ ε

  _,sᴹ_ : ∀ {Γ Γᴹ Δ Δᴹ}{σ : Tms Γ Δ}(σᴹ : Tmsᴹ Γᴹ Δᴹ σ){A : Ty Δ}{Aᴹ : Tyᴹ Δᴹ A}
          {t : Tm Γ (A [ σ ]T)}(tᴹ : Tmᴹ Γᴹ (_[_]Tᴹ {A = A} Aᴹ {σ} σᴹ) t)
        → Tmsᴹ Γᴹ (_,ᴹ_ {Δ} Δᴹ {A} Aᴹ) (σ ,s t)

  π₁ᴹ : ∀ {Γ Γᴹ Δ Δᴹ}{A : Ty Δ}{Aᴹ : Tyᴹ Δᴹ A}
          {σ : Tms Γ (Δ , A)}(σᴹ : Tmsᴹ Γᴹ (_,ᴹ_ Δᴹ {A} Aᴹ) σ)
        → Tmsᴹ Γᴹ Δᴹ (π₁ σ)

  _[_]tᴹ :
    ∀ {Γ Γᴹ Δ Δᴹ}{A : Ty Δ}{Aᴹ : Tyᴹ Δᴹ A}
      {t : Tm Δ A}(tᴹ : Tmᴹ Δᴹ Aᴹ t)
      {σ : Tms Γ Δ}(σᴹ : Tmsᴹ Γᴹ Δᴹ σ)
    → Tmᴹ Γᴹ (_[_]Tᴹ {A = A} Aᴹ {σ} σᴹ) (t [ σ ]t)

  π₂ᴹ : ∀ {Γ Γᴹ Δ Δᴹ}{A : Ty Δ}{Aᴹ : Tyᴹ Δᴹ A}
          {σ : Tms Γ (Δ , A)}(σᴹ : Tmsᴹ Γᴹ (_,ᴹ_ Δᴹ {A} Aᴹ) σ)
        → Tmᴹ Γᴹ (_[_]Tᴹ {A = A} Aᴹ {π₁ σ} (π₁ᴹ {Aᴹ = Aᴹ}{σ} σᴹ)) (π₂ σ)

  [id]Tᴹ : ∀{Γ Γᴹ}{A : Ty Γ}{Aᴹ : Tyᴹ Γᴹ A}
           → tr (Tyᴹ Γᴹ) ([id]T {Γ}{A}) (_[_]Tᴹ {A = A} Aᴹ {id} (idᴹ {Γ}{Γᴹ})) ≡ Aᴹ

  [][]Tᴹ : ∀{Γ Γᴹ Δ Δᴹ Σ Σᴹ}{A : Ty Σ}{Aᴹ : Tyᴹ Σᴹ A}{σ : Tms Γ Δ}{σᴹ : Tmsᴹ Γᴹ Δᴹ σ}
            {δ : Tms Δ Σ}{δᴹ : Tmsᴹ Δᴹ Σᴹ δ}
           → tr (Tyᴹ {Γ} Γᴹ) ([][]T {A = A}{σ}{δ})
             (_[_]Tᴹ {A = A [ δ ]T} (_[_]Tᴹ {A = A} Aᴹ {σ = δ} δᴹ) {σ = σ} σᴹ) ≡
             _[_]Tᴹ {A = A} Aᴹ {σ = δ ∘ σ} (_∘ᴹ_ {Σ = Σ}{Σᴹ}{σ = δ} δᴹ {δ = σ} σᴹ)

  idlᴹ : ∀{Γ Γᴹ Δ Δᴹ}{σ : Tms Γ Δ}{σᴹ : Tmsᴹ Γᴹ Δᴹ σ}
       → tr (Tmsᴹ Γᴹ Δᴹ) (idl {σ = σ}) (_∘ᴹ_ {σ = id {Δ}} (idᴹ{Δ}{Δᴹ}) {σ} σᴹ) ≡ σᴹ

  idrᴹ : ∀{Γ Γᴹ Δ Δᴹ}{σ : Tms Γ Δ}{σᴹ : Tmsᴹ Γᴹ Δᴹ σ}
       → tr (Tmsᴹ Γᴹ Δᴹ) (idr {σ = σ}) (_∘ᴹ_ {Σᴹ = Δᴹ }{σ = σ} σᴹ {id {Γ}} (idᴹ {Γ}{Γᴹ})) ≡ σᴹ

  assᴹ : ∀ {Γ Γᴹ Δ Δᴹ Σ Σᴹ Ω Ωᴹ}
           {σ : Tms Σ Ω}{σᴹ : Tmsᴹ Σᴹ Ωᴹ σ}
           {δ : Tms Δ Σ}{δᴹ : Tmsᴹ Δᴹ Σᴹ δ}
           {ν : Tms Γ Δ}{νᴹ : Tmsᴹ Γᴹ Δᴹ ν}
        → tr (Tmsᴹ Γᴹ Ωᴹ) ass ((σᴹ ∘ᴹ δᴹ) ∘ᴹ νᴹ) ≡ (σᴹ ∘ᴹ (δᴹ ∘ᴹ νᴹ))

  ,∘ᴹ : ∀ {Γ Γᴹ Δ Δᴹ Σ Σᴹ}
         {δ : Tms Γ Δ}{δᴹ : Tmsᴹ Γᴹ Δᴹ δ}
         {σ : Tms Σ Γ}{σᴹ : Tmsᴹ Σᴹ Γᴹ σ}
         {A : Ty Δ}{Aᴹ : Tyᴹ Δᴹ A}
         {t : Tm Γ (A [ δ ]T)}{tᴹ : Tmᴹ Γᴹ (Aᴹ [ δᴹ ]Tᴹ) t}
       → tr (Tmsᴹ Σᴹ (Δᴹ ,ᴹ Aᴹ)) ,∘ ((δᴹ ,sᴹ tᴹ) ∘ᴹ σᴹ)
         ≡ δᴹ ∘ᴹ σᴹ ,sᴹ trᴹ (Tmᴹ Σᴹ) [][]T [][]Tᴹ refl (tᴹ [ σᴹ ]tᴹ)

  π₁βᴹ : ∀ {Γ Γᴹ Δ Δᴹ}
          {A : Ty Δ}{Aᴹ : Tyᴹ Δᴹ A}
          {σ : Tms Γ Δ}{σᴹ : Tmsᴹ Γᴹ Δᴹ σ}
          {t : Tm Γ (A [ σ ]T)}{tᴹ : Tmᴹ Γᴹ (Aᴹ [ σᴹ ]Tᴹ) t}
        → tr (Tmsᴹ Γᴹ Δᴹ) π₁β (π₁ᴹ (σᴹ ,sᴹ tᴹ)) ≡ σᴹ
  πηᴹ : ∀{Γ Γᴹ Δ Δᴹ}{A : Ty Δ}{Aᴹ : Tyᴹ Δᴹ A}{σ : Tms Γ (Δ , A)}{σᴹ : Tmsᴹ Γᴹ (Δᴹ ,ᴹ Aᴹ) σ}
       → tr (Tmsᴹ Γᴹ (Δᴹ ,ᴹ Aᴹ)) πη (π₁ᴹ σᴹ ,sᴹ π₂ᴹ σᴹ) ≡ σᴹ

  εηᴹ : ∀{Γ Γᴹ}{σ : Tms Γ ∙}{σᴹ : Tmsᴹ Γᴹ ∙ᴹ σ}
        → tr (Tmsᴹ Γᴹ ∙ᴹ) εη σᴹ ≡ εᴹ

  π₂βᴹ : ∀{Γ Γᴹ Δ Δᴹ}
          {A : Ty Δ}{Aᴹ : Tyᴹ Δᴹ A}
          {σ : Tms Γ Δ}{σᴹ : Tmsᴹ Γᴹ Δᴹ σ}
          {t : Tm Γ (A [ σ ]T)}{tᴹ : Tmᴹ Γᴹ (Aᴹ [ σᴹ ]Tᴹ) t}
        → tr (Tmᴹ Γᴹ (Aᴹ [ π₁ᴹ (σᴹ ,sᴹ tᴹ) ]Tᴹ)) π₂β (π₂ᴹ (σᴹ ,sᴹ tᴹ)) ≡
          trᴹ (λ σᴹ → Tmᴹ Γᴹ (Aᴹ [ σᴹ ]Tᴹ)) (π₁β ⁻¹) (⁻¹ᴹ _ π₁β (π₁βᴹ {Aᴹ = Aᴹ}{tᴹ = tᴹ})) refl tᴹ

wkᴹ : ∀{Γ Γᴹ}{A : Ty Γ}{Aᴹ : Tyᴹ Γᴹ A} → Tmsᴹ (Γᴹ ,ᴹ Aᴹ) Γᴹ wk
wkᴹ = π₁ᴹ idᴹ

vzᴹ : ∀{Γ Γᴹ}{A : Ty Γ}{Aᴹ : Tyᴹ Γᴹ A} → Tmᴹ (Γᴹ ,ᴹ Aᴹ) (Aᴹ [ wkᴹ ]Tᴹ) vz
vzᴹ = π₂ᴹ idᴹ

vsᴹ :
  ∀{Γ}{Γᴹ}{A}{Aᴹ}
   {B : Ty Γ}{Bᴹ : Tyᴹ Γᴹ B}
   {t : Tm Γ A}(tᴹ : Tmᴹ Γᴹ Aᴹ t)
   → Tmᴹ (Γᴹ ,ᴹ Bᴹ) (Aᴹ [ wkᴹ ]Tᴹ) (vs t)
vsᴹ tᴹ = tᴹ [ wkᴹ ]tᴹ

<_>ᴹ :
  ∀ {Γ Γᴹ}{A : Ty Γ}{Aᴹ : Tyᴹ Γᴹ A}
    {t : Tm Γ A}(tᴹ : Tmᴹ Γᴹ Aᴹ t)
  → Tmsᴹ Γᴹ (Γᴹ ,ᴹ Aᴹ) (< t >)
<_>ᴹ {Γ} {Γᴹ} {A} {Aᴹ} {t} tᴹ =
  idᴹ ,sᴹ trᴹ (Tmᴹ Γᴹ) ([id]T ⁻¹) (⁻¹ᴹ (Tyᴹ Γᴹ) [id]T [id]Tᴹ) refl tᴹ

infix 4 <_>ᴹ

_^ᴹ_ :
  ∀ {Γ Γᴹ Δ Δᴹ}
    {σ : Tms Γ Δ}(σᴹ : Tmsᴹ Γᴹ Δᴹ σ)
    {A : Ty Δ}(Aᴹ : Tyᴹ Δᴹ A) → Tmsᴹ (Γᴹ ,ᴹ Aᴹ [ σᴹ ]Tᴹ) (Δᴹ ,ᴹ Aᴹ) (σ ^ A)
_^ᴹ_ {Γ} {Γᴹ} {Δ} {Δᴹ} {σ} σᴹ {A} Aᴹ =
  σᴹ ∘ᴹ wkᴹ ,sᴹ trᴹ (Tmᴹ (Γᴹ ,ᴹ Aᴹ [ σᴹ ]Tᴹ)) [][]T [][]Tᴹ refl vzᴹ

infixl 5 _^ᴹ_

postulate
  Uᴹ  : ∀{Γ}{Γᴹ : Conᴹ Γ} → Tyᴹ Γᴹ U

  U[]ᴹ : ∀{Γ Γᴹ Δ Δᴹ}{σ : Tms Γ Δ}{σᴹ : Tmsᴹ Γᴹ Δᴹ σ}
    → tr (Tyᴹ Γᴹ) U[] (Uᴹ [ σᴹ ]Tᴹ) ≡ (Uᴹ {Γ}{Γᴹ})

  Elᴹ : ∀{Γ Γᴹ}{a : Tm Γ U}(aᴹ : Tmᴹ Γᴹ Uᴹ a) → Tyᴹ Γᴹ (El a)

  El[]ᴹ : ∀ {Γ Γᴹ Δ Δᴹ}{σ : Tms Γ Δ}{σᴹ : Tmsᴹ Γᴹ Δᴹ σ}
            {a : Tm Δ U}{aᴹ : Tmᴹ Δᴹ Uᴹ a}
       → tr (Tyᴹ Γᴹ) El[] (Elᴹ aᴹ [ σᴹ ]Tᴹ) ≡ Elᴹ (trᴹ (Tmᴹ Γᴹ) U[] U[]ᴹ refl (aᴹ [ σᴹ ]tᴹ))

  Πᴹ :
    ∀{Γ Γᴹ}{a : Tm Γ U}(aᴹ : Tmᴹ Γᴹ Uᴹ a){B : Ty (Γ , El a)}(Bᴹ : Tyᴹ (Γᴹ ,ᴹ Elᴹ aᴹ) B)
    → Tyᴹ Γᴹ (Π a B)

  Π[]ᴹ : ∀ {Γ Γᴹ Δ Δᴹ}
           {σ : Tms Γ Δ}{σᴹ : Tmsᴹ Γᴹ Δᴹ σ}
           {a : Tm Δ U}{aᴹ : Tmᴹ Δᴹ Uᴹ a}
           {B : Ty (Δ , El a)}{Bᴹ : Tyᴹ (Δᴹ ,ᴹ Elᴹ aᴹ) B}
         → tr (Tyᴹ Γᴹ) Π[] (Πᴹ aᴹ Bᴹ [ σᴹ ]Tᴹ) ≡
           Πᴹ (trᴹ (Tmᴹ Γᴹ) U[] U[]ᴹ refl (aᴹ [ σᴹ ]tᴹ))
              (trᴹ (λ Aᴹ → Tyᴹ (Γᴹ ,ᴹ Aᴹ)) El[] El[]ᴹ refl (Bᴹ [ σᴹ ^ᴹ Elᴹ aᴹ ]Tᴹ))

  appᴹ : ∀ {Γ Γᴹ}
           {a : Tm Γ U}{aᴹ : Tmᴹ Γᴹ Uᴹ a}
           {B : Ty (Γ , El a)}{Bᴹ : Tyᴹ (Γᴹ ,ᴹ Elᴹ aᴹ) B}
           {t : Tm Γ (Π a B)}(tᴹ : Tmᴹ Γᴹ (Πᴹ aᴹ Bᴹ) t)
           → Tmᴹ (Γᴹ ,ᴹ Elᴹ aᴹ) Bᴹ (app t)

  lamᴹ : ∀ {Γ Γᴹ}
           {a : Tm Γ U}{aᴹ : Tmᴹ Γᴹ Uᴹ a}
           {B : Ty (Γ , El a)}{Bᴹ : Tyᴹ (Γᴹ ,ᴹ Elᴹ aᴹ) B}
           {t : Tm (Γ , El a) B}(tᴹ : Tmᴹ (Γᴹ ,ᴹ Elᴹ aᴹ) Bᴹ t)
           → Tmᴹ Γᴹ (Πᴹ aᴹ Bᴹ) (lam t)

  lam[]ᴹ : ∀ {Γ Γᴹ Δ Δᴹ}{σ : Tms Γ Δ}{σᴹ : Tmsᴹ Γᴹ Δᴹ σ}
            {a : Tm Δ U}{aᴹ : Tmᴹ Δᴹ Uᴹ a}
            {B : Ty (Δ , El a)}{Bᴹ : Tyᴹ (Δᴹ ,ᴹ Elᴹ aᴹ) B}
            {t : Tm (Δ , El a) B}{tᴹ : Tmᴹ (Δᴹ ,ᴹ Elᴹ aᴹ) Bᴹ t}
          → tr (Tmᴹ Γᴹ (Πᴹ aᴹ Bᴹ [ σᴹ ]Tᴹ)) lam[] (lamᴹ tᴹ [ σᴹ ]tᴹ)
            ≡ trᴹ (Tmᴹ Γᴹ) (Π[] ⁻¹) (⁻¹ᴹ (Tyᴹ Γᴹ) Π[] Π[]ᴹ) refl
              (lamᴹ (tr2ᴹ (λ Aᴹ → Tmᴹ (Γᴹ ,ᴹ Aᴹ)) El[] El[]ᴹ refl refl refl
                          (tᴹ [ σᴹ ^ᴹ Elᴹ aᴹ ]tᴹ)))

  Πβᴹ : ∀{Γ Γᴹ}{a : Tm Γ U}{aᴹ : Tmᴹ Γᴹ Uᴹ a}
               {B : Ty (Γ , El a)}{Bᴹ : Tyᴹ (Γᴹ ,ᴹ Elᴹ aᴹ) B}
               {t : Tm (Γ , El a) B}{tᴹ : Tmᴹ (Γᴹ ,ᴹ Elᴹ aᴹ) Bᴹ t}
               → tr (Tmᴹ (Γᴹ ,ᴹ Elᴹ aᴹ) Bᴹ) Πβ (appᴹ (lamᴹ tᴹ)) ≡ tᴹ

  Πηᴹ : ∀{Γ Γᴹ}{a : Tm Γ U}{aᴹ : Tmᴹ Γᴹ Uᴹ a}
               {B : Ty (Γ , El a)}{Bᴹ : Tyᴹ (Γᴹ ,ᴹ Elᴹ aᴹ) B}
               {t : Tm Γ (Π a B)}{tᴹ : Tmᴹ Γᴹ (Πᴹ aᴹ Bᴹ) t}
               → tr (Tmᴹ Γᴹ (Πᴹ aᴹ Bᴹ)) Πη (lamᴹ (appᴹ tᴹ)) ≡ tᴹ

-- Skipping path β rules, since UIP makes them redundant
--------------------------------------------------------------------------------

postulate
  Conᴱ : (Γ : Con) → Conᴹ Γ
  Tyᴱ  : ∀ {Γ} → (A : Ty Γ) → Tyᴹ (Conᴱ Γ) A
  Tmᴱ  : ∀ {Γ A} → (t : Tm Γ A) → Tmᴹ (Conᴱ Γ) (Tyᴱ A) t
  Tmsᴱ : ∀ {Γ Δ} → (σ : Tms Γ Δ) → Tmsᴹ (Conᴱ Γ) (Conᴱ Δ) σ

postulate
  ∙ᴱ   : Conᴱ ∙ ≡ ∙ᴹ
  ,ᴱ   : ∀ Γ A → Conᴱ (Γ , A) ≡ Conᴱ Γ ,ᴹ Tyᴱ A
  []Tᴱ : ∀ Γ Δ (A : Ty Δ) (σ : Tms Γ Δ) → Tyᴱ (A [ σ ]T) ≡ Tyᴱ A [ Tmsᴱ σ ]Tᴹ
{-# REWRITE ∙ᴱ ,ᴱ []Tᴱ #-}

postulate
  idᴱ  : ∀ {Γ} → Tmsᴱ (id {Γ}) ≡ idᴹ
  ∘ᴱ   : ∀ {Γ Δ Σ}(σ : Tms Δ Σ)(δ : Tms Γ Δ) → Tmsᴱ (σ ∘ δ) ≡ Tmsᴱ σ ∘ᴹ Tmsᴱ δ
  ,sᴱ  : ∀ {Γ Δ}(σ : Tms Γ Δ){A : Ty Δ}(t : Tm Γ (A [ σ ]T)) → Tmsᴱ (σ ,s t) ≡ Tmsᴱ σ ,sᴹ Tmᴱ t
  εᴱ   : ∀ {Γ} → Tmsᴱ (ε {Γ}) ≡ εᴹ
  π₁ᴱ  : ∀ {Γ Δ}{A : Ty Δ}(σ : Tms Γ (Δ , A)) → Tmsᴱ (π₁ σ) ≡ π₁ᴹ (Tmsᴱ σ)
{-# REWRITE idᴱ ∘ᴱ ,sᴱ εᴱ π₁ᴱ #-}

postulate
  []tᴱ   : ∀{Γ Δ}{A : Ty Δ}(t : Tm Δ A)(σ : Tms Γ Δ) → Tmᴱ (t [ σ ]t) ≡ Tmᴱ t [ Tmsᴱ σ ]tᴹ
  π₂ᴱ    : ∀{Γ Δ}{A : Ty Δ}(σ : Tms Γ (Δ , A)) → Tmᴱ (π₂ σ) ≡ π₂ᴹ (Tmsᴱ σ)
  Uᴱ     : ∀ {Γ} → Tyᴱ (U {Γ}) ≡ Uᴹ
{-# REWRITE []tᴱ π₂ᴱ Uᴱ #-}

postulate
  Elᴱ : ∀ {Γ}{a : Tm Γ U} → Tyᴱ (El a) ≡ Elᴹ (Tmᴱ a)
{-# REWRITE Elᴱ #-}

postulate
  Πᴱ : ∀ {Γ}(a : Tm Γ U)(B : Ty (Γ , El a)) → Tyᴱ (Π a B) ≡ Πᴹ (Tmᴱ a) (Tyᴱ B)
{-# REWRITE Πᴱ #-}

postulate
  appᴱ : ∀ {Γ}{a : Tm Γ U}{B : Ty (Γ , El a)}(t : Tm Γ (Π a B))
         → Tmᴱ (app t) ≡ appᴹ (Tmᴱ t)
  lamᴱ : ∀ {Γ}{a : Tm Γ U}{B : Ty (Γ , El a)}(t : Tm (Γ , El a) B)
         → Tmᴱ (lam t) ≡ lamᴹ (Tmᴱ t)
{-# REWRITE appᴱ lamᴱ #-}
