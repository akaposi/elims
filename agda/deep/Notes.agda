{-# OPTIONS --without-K #-}

open import Lib hiding (suc; zero)
-- open import Syntax

-- Theories for restricted codes
------------------------------------------------------------

{-

-- Only non-indexed inductive types
------------------------------------------------------------

─────
Γ ⊢ U

Γ ⊢ a : U
─────────
Γ ⊢ El a

Γ ⊢ a : U  Γ ⊢ b : U
────────────────────
    Γ ⊢ a → b

-- Indexed (but not inductive-inductive)
------------------------------------------------------------

─────
Γ ⊢ U

Γ ⊢ a : U
─────────
Γ ⊢ El a

T ∈ Set  (α ∈ T) → Γ ⊢ Aα
─────────────────────────
   Γ ⊢ (α ∈ T) → Aα

Γ ⊢ a : U   Γ ⊢ b : U
─────────────────────
   Γ ⊢ a → b

-}

-- data Con : Set
-- data Ty (Γ : Con) : Set

-- data Con where
--   ∙    : Con
--   _,ₜ_ : (Γ : Con) → Ty Γ → Con
--   _,ₛ  : Con → Con

-- data Sort : Con → Set where
--   vz  : ∀ {Γ} → Sort (Γ ,ₛ)
--   vsₜ : ∀ {Γ A} → Sort Γ → Sort (Γ ,ₜ A)
--   vsₛ : ∀ {Γ} → Sort Γ → Sort (Γ ,ₛ)

-- data Ty (Γ : Con) where
--   sort : Sort Γ → Ty Γ
--   _⇒_  : Sort Γ → Ty Γ → Ty Γ

------------------------------------------------------------

-- open import Data.Nat

-- data Sort : ℕ → Set where
--   vz : ∀ {n} → Sort (suc n)
--   vs : ∀ {n} → Sort n → Sort (suc n)

-- data Ty (n : ℕ) : Set where
--   sort : Sort n → Ty n
--   _⇒_  : Sort n → Ty n → Ty n

-- data Con : ℕ → Set where
--   ∙    : Con 0
--   _,ₜ_ : ∀ {n} → Con n → Ty n → Con n
--   _,ₛ  : ∀ {n} → Con n → Con (suc n)

-- infixr 4 _⇒_
-- infixl 5 _,ₜ_
-- infixl 5 _,ₛ_

------------------------------------------------------------

-- open import Data.Nat

-- data Sort : Set₁ where
--   U   : Sort
--   _⇒_ : Set → Sort → Sort

-- data Sorts : Set₁ where
--   ∙   : Sorts
--   _,_ : Sorts → Sort → Sorts

-- data Var : Sorts → Sort → Set₁ where
--   vz : ∀ {Δ A} → Var (Δ , A) A
--   vs : ∀ {Δ A B} → Var Δ A → Var (Δ , B) A

-- data Ne : Sorts → Sort → Set₁ where
--   var : ∀ {Δ A} → Var Δ A → Ne Δ A
--   app : ∀ {Δ A B} → Ne Δ (A ⇒ B) → A → Ne Δ B

-- data Ty (Δ : Sorts) : Set₁ where
--   sort : Ne Δ U → Ty Δ
--   _⇒_  : Ne Δ U → Ty Δ → Ty Δ
--   _⇒'_ : (T : Set) → (T → Ty Δ) → Ty Δ

-- data Con : Sorts → Set₁ where
--   ∙    : Con ∙
--   _,ₜ_ : ∀ {Δ} → Con Δ → Ty Δ → Con Δ
--   _,ₛ_ : ∀ {Δ} → Con Δ → (S : Sort) → Con (Δ , S)

-- infixr 4 _⇒'_
-- infixr 4 _⇒_
-- infixl 5 _,ₜ_
-- infixl 5 _,ₛ_

------------------------------------------------------------

-- data Sort : Set₁ where
--   U   : Sort
-- --  _⇒_ : Set → Sort → Sort

-- data Con : Set₁
-- data Ty (Γ : Con) : Set₁

-- data Con where
--   ∙    : Con
--   _,ₜ_ : (Γ : Con) → Ty Γ → Con
--   _,ₛ_ : (Γ : Con) → Sort → Con

-- data Var : Con → Sort → Set₁ where
--   vz  : ∀ {Δ S} → Var (Δ ,ₛ S) S
--   vsₜ : ∀ {Δ S B} → Var Δ S → Var (Δ ,ₜ B) S
--   vsₛ : ∀ {Δ S S'} → Var Δ S → Var (Δ ,ₛ S') S

-- data Ne (Γ : Con) : Sort → Set₁ where
--   var : ∀ {S} → Var Γ S → Ne Γ S
-- --  app : ∀ {T S} → Ne Γ (T ⇒ S) → T → Ne Γ S

-- data Ty (Γ : Con) where
--   sort : Ne Γ U → Ty Γ
--   _⇒_  : Ne Γ U → Ty Γ → Ty Γ
--   _⇒'_ : (T : Set) → (T → Ty Γ) → Ty Γ

-- infixr 4 _⇒'_
-- infixr 4 _⇒_
-- infixl 5 _,ₜ_
-- infixl 5 _,ₛ_

------------------------------------------------------------
