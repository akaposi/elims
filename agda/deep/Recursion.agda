{-# OPTIONS --rewriting #-}

{-
USAGE:
  - copy & paste everything below to a new module
  - remove postulates & add implementation
  - Rename and export stuff as needed
-}

module Recursion where

open import Lib
import Syntax as S

infixl 5 _,_
infixl 7 _[_]T
infixl 5 _,s_
infix  6 _∘_
infixl 8 _[_]t

postulate
  i : Level

  j : Level

  Con : Set i

  Ty : Con → Set i

  Tm : ∀ Γ → Ty Γ → Set j

  Tms : Con → Con → Set j

  ∙ : Con

  _,_ : (Γ : Con) → Ty Γ → Con

  _[_]T : ∀{Γ Δ} → Ty Δ → Tms Γ Δ → Ty Γ

  id : ∀{Γ} → Tms Γ Γ

  _∘_ : ∀{Γ Δ Σ} → Tms Δ Σ → Tms Γ Δ → Tms Γ Σ

  ε : ∀{Γ} → Tms Γ ∙

  _,s_  : ∀{Γ Δ A}(σ : Tms Γ Δ) → Tm Γ (A [ σ ]T) → Tms Γ (Δ , A)

  π₁ : ∀{Γ Δ}{A : Ty Δ} → Tms Γ (Δ , A) →  Tms Γ Δ

  _[_]t : ∀{Γ Δ}{A : Ty Δ} → Tm Δ A → (σ : Tms Γ Δ) → Tm Γ (A [ σ ]T)

  π₂ : ∀{Γ Δ}{A : Ty Δ}(σ : Tms Γ (Δ , A)) → Tm Γ (A [ π₁ σ ]T)

  [id]T : ∀{Γ}{A : Ty Γ} → A [ id ]T ≡ A

  [][]T : ∀{Γ Δ Σ}{A : Ty Σ}{σ : Tms Γ Δ}{δ : Tms Δ Σ}
          → A [ δ ]T [ σ ]T ≡ A [ δ ∘ σ ]T

  idl : ∀{Γ Δ}{δ : Tms Γ Δ} → (id ∘ δ) ≡ δ

  idr : ∀{Γ Δ}{δ : Tms Γ Δ} → (δ ∘ id) ≡ δ

  ass : ∀{Γ Δ Σ Ω}{σ : Tms Σ Ω}{δ : Tms Δ Σ}{ν : Tms Γ Δ}
        → (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν)

  ,∘ : ∀{Γ Δ Σ}{δ : Tms Γ Δ}{σ : Tms Σ Γ}{A : Ty Δ}{t : Tm Γ (A [ δ ]T)}
        → ((_,s_ {Γ}{Δ}{A} δ t) ∘ σ) ≡
          ((δ ∘ σ) ,s tr (Tm Σ) ([][]T {Σ}{Γ}{Δ}{A}{σ}{δ}) (_[_]t {Σ}{Γ}{A [ δ ]T} t σ))

  π₁β : ∀{Γ Δ}{A : Ty Δ}{σ : Tms Γ Δ}{t : Tm Γ (A [ σ ]T)}
        → (π₁ (_,s_ {Γ}{Δ}{A} σ t)) ≡ σ

  πη : ∀{Γ Δ}{A : Ty Δ}{δ : Tms Γ (Δ , A)}
        → (π₁ δ ,s π₂ δ) ≡ δ

  εη : ∀{Γ}{σ : Tms Γ ∙}
        → σ ≡ ε

  π₂β : ∀{Γ Δ}{A : Ty Δ}{σ : Tms Γ Δ}{t : Tm Γ (A [ σ ]T)}
        → tr (λ x → Tm Γ (A [ x ]T)) (π₁β {Γ}{Δ}{A}{σ}{t}) (π₂ (_,s_ {Γ}{Δ}{A} σ t)) ≡ t

wk : ∀{Γ}{A : Ty Γ} → Tms (Γ , A) Γ
wk = π₁ id

vz : ∀{Γ}{A : Ty Γ} → Tm (Γ , A) (A [ wk ]T)
vz = π₂ id

vs : ∀{Γ}{A B : Ty Γ} → Tm Γ A → Tm (Γ , B) (A [ wk ]T)
vs x = x [ wk ]t

<_> : ∀{Γ}{A : Ty Γ} → Tm Γ A → Tms Γ (Γ , A)
< t > = id ,s tr (Tm _) ([id]T ⁻¹) t

infix 4 <_>

_^_ : {Γ Δ : Con}(σ : Tms Γ Δ)(A : Ty Δ) → Tms (Γ , A [ σ ]T) (Δ , A)
_^_ {Γ}{Δ} σ A = (σ ∘ wk) ,s tr (Tm (Γ , A [ σ ]T)) ([][]T {Γ , A [ σ ]T}{Γ}{Δ}{A}{wk{Γ}{A [ σ ]T}}{σ}) vz

infixl 5 _^_

postulate
  U : ∀{Γ} → Ty Γ

  U[] : ∀{Γ Δ}{σ : Tms Γ Δ} → (U [ σ ]T) ≡ U

  El : ∀{Γ}(a : Tm Γ U) → Ty Γ

  El[] : ∀{Γ Δ}{σ : Tms Γ Δ}{a : Tm Δ U}
       → (El a [ σ ]T) ≡ (El (tr (Tm Γ) (U[] {σ = σ}) (a [ σ ]t)))

  Π : ∀{Γ}(a : Tm Γ U)(B : Ty (Γ , El a)) → Ty Γ

  Π[] : ∀{Γ Δ}{σ : Tms Γ Δ}{a : Tm Δ U}{B : Ty (Δ , El a)}
      → (Π a B) [ σ ]T ≡
        Π (tr (Tm Γ) (U[] {σ = σ}) (a [ σ ]t))
          (tr (λ x → Ty (Γ , x)) (El[] {σ = σ}{a}) (B [ σ ^ El a ]T))

  app : ∀{Γ}{a : Tm Γ U}{B : Ty (Γ , El a)} → Tm Γ (Π a B) → Tm (Γ , El a) B

  lam : ∀{Γ}{a : Tm Γ U}{B : Ty (Γ , El a)} → Tm (Γ , El a) B → Tm Γ (Π a B)

  lam[] : ∀{Γ Δ}{σ : Tms Γ Δ}{a : Tm Δ U}{B : Ty (Δ , El a)}{t : Tm (Δ , El a) B}
    → tr (Tm _) (Π[] {σ = σ}{a}{B})
           (lam t [ σ ]t) ≡
           (lam (tr2 (λ x → Tm (Γ , x)) (El[] {σ = σ}{a}) refl
             (t [ σ ^ El a ]t)))

  Πβ : ∀{Γ}{a : Tm Γ U}{B : Ty (Γ , El a)}{t : Tm (Γ , El a) B} → app (lam t) ≡ t

  Πη : ∀{Γ}{a : Tm Γ U}{B : Ty (Γ , El a)}{t : Tm Γ (Π a B)} → lam (app t) ≡ t

  ΠNI : ∀ {Γ}(A : Set) → (A → Ty Γ) → Ty Γ

  ΠNI[] : ∀ {Γ Δ}{σ : Tms Γ Δ}{A : Set}{B : A → Ty Δ}
          → ΠNI A B [ σ ]T ≡ ΠNI A (λ α → B α [ σ ]T)

  lamNI : ∀ {Γ}{A}{B : A → Ty Γ} → (∀ α → Tm Γ (B α)) → Tm Γ (ΠNI A B)
  appNI : ∀ {Γ}{A}{B : A → Ty Γ} → Tm Γ (ΠNI A B) → (∀ α → Tm Γ (B α))

  lamNI[] : ∀ {Γ Δ}{A}{σ : Tms Γ Δ}{B : A → Ty Δ}{t : ∀ α → Tm Δ (B α)}
            → tr (Tm Γ) ΠNI[] (lamNI t [ σ ]t) ≡ lamNI (λ α → t α [ σ ]t)

  ΠNIβ : ∀ {Γ}{A}{B : A → Ty Γ}{t : ∀ α → Tm Γ (B α)}
         → appNI (lamNI t) ≡ t

  ΠNIη : ∀ {Γ}{A}{B : A → Ty Γ}{t : Tm Γ (ΠNI A B)}
         → lamNI (appNI t) ≡ t

  Πₙᵢ : ∀ {Γ}(A : Set) → (A → Tm Γ U) → Tm Γ U

  Πₙᵢ[] : ∀ {Γ Δ}{σ : Tms Γ Δ}{A : Set}{b : A → Tm Δ U}
          → tr (Tm Γ) U[] (Πₙᵢ A b [ σ ]t) ≡ Πₙᵢ A (λ α → tr (Tm Γ) U[] (b α [ σ ]t))

  lamₙᵢ : ∀ {Γ}{A}{b : A → Tm Γ U} → (∀ α → Tm Γ (El (b α))) → Tm Γ (El (Πₙᵢ A b))
  appₙᵢ : ∀ {Γ}{A}{b : A → Tm Γ U} → Tm Γ (El (Πₙᵢ A b)) → (∀ α → Tm Γ (El (b α)))

  lamₙᵢ[] : ∀ {Γ Δ}{A : Set}{σ : Tms Γ Δ}{b : A → Tm Δ U}{t : ∀ α → Tm Δ (El (b α))}
            → tr (Tm Γ) (El[] ◾ ap El Πₙᵢ[]) (lamₙᵢ t [ σ ]t)
            ≡ lamₙᵢ (λ α → tr (Tm Γ) El[] (t α [ σ ]t))

  Πₙᵢβ : ∀ {Γ}{A}{b : A → Tm Γ U}{t : ∀ α → Tm Γ (El (b α))}
         → appₙᵢ (lamₙᵢ t) ≡ t

  Πₙᵢη : ∀ {Γ}{A}{B : A → Tm Γ U}{t : Tm Γ (El (Πₙᵢ A B))}
         → lamₙᵢ (appₙᵢ t) ≡ t

--------------------------------------------------------------------------------

_^El_ :
  {Γ Δ : Con}(σ : Tms Γ Δ)(a : Tm Δ U)
  → Tms (Γ , El (tr (Tm Γ) (U[] ) (a [ σ ]t))) (Δ , El a)
σ ^El a = σ ∘ wk ,s tr (Tm _) (ap (_[ wk ]T) (El[]  ⁻¹) ◾ ([][]T)) vz

infixl 5 _^El_

--------------------------------------------------------------------------------

postulate
  Id   : ∀ {Γ}(a : Tm Γ U) → Tm Γ (El a) → Tm Γ (El a) → Tm Γ U
  Id[] : ∀ {Γ Δ}{σ : Tms Γ Δ}{a : Tm Δ U}{t u : Tm Δ (El a)}
         → tr (Tm Γ) (U[]) (Id a t u [ σ ]t) ≡
             (Id (tr (Tm Γ) U[] (a [ σ ]t))
                 (tr (Tm Γ) El[] (t [ σ ]t))
                 (tr (Tm Γ) El[] (u [ σ ]t)))

  Refl : ∀ {Γ}(a : Tm Γ U)(t : Tm Γ (El a)) → Tm Γ (El (Id a t t))

  Refl[] : ∀ {Γ Δ}{σ : Tms Γ Δ}{a : Tm Δ U}{t : Tm Δ (El a)}
           → tr (Tm Γ) (El[] ◾ ap El Id[])
             (Refl a t [ σ ]t) ≡
               (Refl
                 (tr (Tm Γ) U[] (a [ σ ]t))
                 (tr (Tm Γ) El[] (t [ σ ]t)))

  Transp :
    ∀ {Γ}{a : Tm Γ U}(P : Ty (Γ , El a))
      {t u : Tm Γ (El a)}
      (pt : Tm Γ (P [ < t > ]T))
      (eq : Tm Γ (El (Id a t u)))
    → Tm Γ (P [ < u > ]T)

  -- todo : simpler coherence
  <>∘ :
    {Γ Δ  : Con}
    (σ  : Tms Γ Δ)
    (a  : Tm Δ U)
    (t  : Tm Δ (El a))
    → < t > ∘ σ ≡ (σ ^El a) ∘ < tr (Tm Γ) El[] (t [ σ ]t) >

  Transp[] :
    ∀ {Γ Δ}{σ : Tms Γ Δ}
      {a : Tm Δ U}(P : Ty (Δ , El a))
      {t u : Tm Δ (El a)}
      (pt : Tm Δ (P [ < t > ]T))
      (eq : Tm Δ (El (Id a t u)))
    → tr (Tm Γ) ([][]T ◾ ap (P [_]T) (<>∘ σ a u) ◾ [][]T ⁻¹)
      (Transp P pt eq [ σ ]t)
      ≡ Transp
          (P [ σ ^El a ]T)
          (tr (Tm Γ) ([][]T ◾ ap (P [_]T) (<>∘ σ a t) ◾ [][]T ⁻¹) (pt [ σ ]t))
          ((tr (Tm Γ) ((El[] {σ = σ} ◾ ap El (Id[] {σ = σ}))) (eq [ σ ]t)))

  Transpβ :
    ∀ {Γ}{a : Tm Γ U}(P : Ty (Γ , El a))
      {t : Tm Γ (El a)}
      (pt : Tm Γ (P [ < t > ]T))
    → Transp P pt (Refl a t) ≡ pt


--------------------------------------------------------------------------------

postulate
  Conᴿ : S.Con → Con
  Tyᴿ  : ∀ {Γ} → S.Ty Γ → Ty (Conᴿ Γ)
  Tmᴿ  : ∀ {Γ A} → S.Tm Γ A → Tm (Conᴿ Γ) (Tyᴿ A)
  Tmsᴿ : ∀ {Γ Δ} → S.Tms Γ Δ → Tms (Conᴿ Γ) (Conᴿ Δ)

postulate
  ∙ᴿ   : Conᴿ S.∙ ≡ ∙
  ,ᴿ   : ∀ Γ A → Conᴿ (Γ S., A) ≡ Conᴿ Γ , Tyᴿ A
  []Tᴿ : ∀ Γ Δ (A : S.Ty Δ) (σ : S.Tms Γ Δ) → Tyᴿ (A S.[ σ ]T) ≡ Tyᴿ A [ Tmsᴿ σ ]T
{-# REWRITE ∙ᴿ ,ᴿ []Tᴿ #-}

postulate
  idᴿ  : ∀ {Γ} → Tmsᴿ (S.id {Γ}) ≡ id
  ∘ᴿ   : ∀ {Γ Δ Σ}(σ : S.Tms Δ Σ)(δ : S.Tms Γ Δ) → Tmsᴿ (σ S.∘ δ) ≡ Tmsᴿ σ ∘ Tmsᴿ δ
  ,sᴿ  : ∀ {Γ Δ}(σ : S.Tms Γ Δ){A : S.Ty Δ}(t : S.Tm Γ (A S.[ σ ]T)) → Tmsᴿ (σ S.,s t) ≡ Tmsᴿ σ ,s Tmᴿ t
  εᴿ   : ∀ {Γ} → Tmsᴿ (S.ε {Γ}) ≡ ε
  π₁ᴿ  : ∀ {Γ Δ}{A : S.Ty Δ}(σ : S.Tms Γ (Δ S., A)) → Tmsᴿ (S.π₁ σ) ≡ π₁ (Tmsᴿ σ)
{-# REWRITE idᴿ ∘ᴿ ,sᴿ εᴿ π₁ᴿ #-}

postulate
  []tᴿ   : ∀{Γ Δ}{A : S.Ty Δ}(t : S.Tm Δ A)(σ : S.Tms Γ Δ) → Tmᴿ (t S.[ σ ]t) ≡ Tmᴿ t [ Tmsᴿ σ ]t
  π₂ᴿ    : ∀{Γ Δ}{A : S.Ty Δ}(σ : S.Tms Γ (Δ S., A)) → Tmᴿ (S.π₂ σ) ≡ π₂ (Tmsᴿ σ)
  Uᴿ     : ∀ {Γ} → Tyᴿ (S.U {Γ}) ≡ U
{-# REWRITE []tᴿ π₂ᴿ Uᴿ #-}

postulate
  Elᴿ : ∀ {Γ}{a : S.Tm Γ S.U} → Tyᴿ (S.El a) ≡ El (Tmᴿ a)
{-# REWRITE Elᴿ #-}

postulate
  Πᴿ : ∀ {Γ}(a : S.Tm Γ S.U)(B : S.Ty (Γ S., S.El a)) → Tyᴿ (S.Π a B) ≡ Π (Tmᴿ a) (Tyᴿ B)
{-# REWRITE Πᴿ #-}

postulate
  appᴿ : ∀ {Γ}{a : S.Tm Γ S.U}{B : S.Ty (Γ S., S.El a)}(t : S.Tm Γ (S.Π a B))
         → Tmᴿ (S.app t) ≡ app (Tmᴿ t)
  lamᴿ : ∀ {Γ}{a : S.Tm Γ S.U}{B : S.Ty (Γ S., S.El a)}(t : S.Tm (Γ S., S.El a) B)
         → Tmᴿ (S.lam t) ≡ lam (Tmᴿ t)
{-# REWRITE appᴿ lamᴿ #-}

postulate
  ΠNIᴿ   : ∀ {Γ}{A : Set}{B : A → S.Ty Γ} → Tyᴿ (S.ΠNI A B) ≡ ΠNI A (λ α → Tyᴿ (B α))
{-# REWRITE ΠNIᴿ #-}

postulate
  appNIᴿ : ∀ {Γ}{A : Set}{B : A → S.Ty Γ}{t : S.Tm Γ (S.ΠNI A B)}{α : A}
           → Tmᴿ (S.appNI t α) ≡ appNI (Tmᴿ t) α
  lamNIᴿ : ∀ {Γ}{A : Set}{B : A → S.Ty Γ}{t : ∀ α → S.Tm Γ (B α)}
           → Tmᴿ (S.lamNI t) ≡ lamNI (λ α → Tmᴿ (t α))
{-# REWRITE appNIᴿ lamNIᴿ #-}

postulate
  Πₙᵢᴿ   : ∀ {Γ}{A : Set}{b : A → S.Tm Γ S.U} → Tmᴿ (S.Πₙᵢ A b) ≡ Πₙᵢ A (λ α → Tmᴿ (b α))
{-# REWRITE Πₙᵢᴿ #-}

postulate
  appₙᵢᴿ : ∀ {Γ}{A : Set}{b : A → S.Tm Γ S.U}{t : S.Tm Γ (S.El (S.Πₙᵢ A b))}{α : A}
           → Tmᴿ (S.appₙᵢ t α) ≡ appₙᵢ (Tmᴿ t) α
  lamₙᵢᴿ : ∀ {Γ}{A : Set}{b : A → S.Tm Γ S.U}{t : ∀ α → S.Tm Γ (S.El (b α))}
           → Tmᴿ (S.lamₙᵢ t) ≡ lamₙᵢ (λ α → Tmᴿ (t α))
{-# REWRITE appₙᵢᴿ lamₙᵢᴿ #-}

-- Todo : Identity
