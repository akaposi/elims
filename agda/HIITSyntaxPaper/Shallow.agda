{-# OPTIONS --without-K #-}

{-

Shallow formalization for the paper "Syntax for Higher Inductive-Inductive Types".

  * We shallowly embed both source and target theories into Agda. So, types become Agda types, functions
    Agda functions, and so on.
  * For each ᴹ and ᴱ translation case, we build and Agda function which constructs the translation result
    assuming all induction hypotheses.
  * The translations here are understood to be defined using recursion (as opposed to induction),
    so elements of the source syntax never even appear in this formalisation, and ᶜ is also implicit.
  * Dependency of Γ;Γ context are also left implicit. We model terms in extended contexts (such as the
    B domain of a ((x : a) → B) type) by Agda function which take as arguments all additional context entries.
    Hence, if we are in implicit Γ context, and (Γ, x : El a ⊢ B), then we use (Bᴹ : (x : El a)(xᴹ : (El a)ᴹ x) → Uᴹ B)
-}

-- Definitions & shorthands in target theory
--------------------------------------------------------------------------------

open import Relation.Binary.PropositionalEquality renaming (_≡_ to Id)
open import Data.Product

_≡_ = Id
infix 4 _≡_

J : ∀ {α β}{A : Set α}{a : A}(B : ∀ b → a ≡ b → Set β) → B a refl → ∀ {b} p → B b p
J B pr refl = pr

_◾_ = trans
infixr 5 _◾_
ap = cong
_⁻¹ = sym

tr : ∀ {α β}{A : Set α}{a b : A}(B : A → Set β) → a ≡ b → B a → B b
tr B eq t = J (λ u p → B u) t eq

apd : ∀ {α β}{A : Set α}(B : A → Set β)(f : ∀ a → B a)(x y : A)(p : x ≡ y) → tr B p (f x) ≡ f y
apd B f t u p = J (λ u p → tr B p (f t) ≡ f u) refl p

Π : (A : Set)(B : A → Set) → Set
Π A B = (x : A) → B x

ap-id : ∀ {α}{A : Set α}{x y : A}(p : x ≡ y) → ap (λ x → x) p ≡ p
ap-id refl = refl

inv : ∀ {α}{A : Set α}{x y : A}(p : x ≡ y) → (p ⁻¹) ◾ p ≡ refl
inv refl = refl

--------------------------------------------------------------------------------

U = Set

-- non-inductive function space
Πₙᵢ  = Π

-- small non-inductive (infinitary) function space
Πₙᵢₛ = Π

--------------------------------------------------------------------------------
-- Induction methods
--------------------------------------------------------------------------------

Setᴹ : Set → Set₁
Setᴹ A = A → Set

Uᴹ : Set → Set₁
Uᴹ A = A → Set

Elᴹ : ∀{a}(aᴹ : Uᴹ a) → Setᴹ a -- Elᴹ is identity, so it is left implicit from now on
Elᴹ aᴹ x = aᴹ x

-- inductive functions

Πᴹ : ∀ {a}(aᴹ : Uᴹ a){B : a → Set}(Bᴹ : ∀ {x} (xₘ : aᴹ x) → Setᴹ (B x)) → Setᴹ (Π a B)
Πᴹ {a} aᴹ {B} Bᴹ f = (x : a)(xₘ : aᴹ x) → Bᴹ xₘ (f x)

appᴹ :
  ∀ {a}(aᴹ : Uᴹ a){B : a → Set}(Bᴹ : ∀ {x}(xₘ : aᴹ x) → Setᴹ (B x))
    {t : Π a B}(tᴹ : Πᴹ aᴹ Bᴹ t){u : a} (uᴹ : aᴹ u)
  → Bᴹ uᴹ (t u)
appᴹ {a} aᴹ {B} Bᴹ {t} tᴹ {u} uᴹ = tᴹ u uᴹ

-- non-inductive functions

Πₙᵢᴹ : (A : Set){B : A → Set}(Bᴹ : ∀ a → Setᴹ (B a)) → Setᴹ (Π A B)
Πₙᵢᴹ A {B} Bᴹ f = ∀ a → Bᴹ a (f a)

appₙᵢᴹ :
  ∀ (A : Set)
    {B : A → Set}  (Bᴹ : ∀ a → Setᴹ (B a))
    {t : Πₙᵢ A B}  (tᴹ : Πₙᵢᴹ A Bᴹ t)
    (u : A)
  → Bᴹ u (t u)
appₙᵢᴹ A {B} Bᴹ {t} tᴹ u = tᴹ u

-- small non-inductive functions (infinitary parameters)

Πₙᵢₛᴹ : (A : Set){b : A → U}(Bᴹ : ∀ a → Uᴹ (b a)) → Uᴹ (Πₙᵢₛ A b)
Πₙᵢₛᴹ A {B} bᴹ = λ f → ∀ a → bᴹ a (f a)

appₙᵢₛᴹ :
  ∀ (A : Set)
    {b : A → Set}  (bᴹ : ∀ a → Uᴹ (b a))
    {t : Πₙᵢₛ A b} (tᴹ : Πₙᵢₛᴹ A bᴹ t)
    (u : A)
  → bᴹ u (t u)
appₙᵢₛᴹ A {B} _ {t} tᴹ u = tᴹ u


-- Identity

≡ᴹ :
  {a : U} (aᴹ : Uᴹ a)
  {t : a} (tᴹ : aᴹ t)
  {u : a} (uᴹ : aᴹ u)
  → Uᴹ (t ≡ u)
≡ᴹ {a} aᴹ {t} tᴹ {u} uᴹ = λ z → tr aᴹ z tᴹ ≡ uᴹ

reflᴹ :
  {a : U} (aᴹ : Uᴹ a)
  {t : a} (tᴹ : aᴹ t)
  → (≡ᴹ aᴹ tᴹ tᴹ) refl
reflᴹ {a} aᴹ {t} tᴹ = refl {x = tᴹ}

Jᴹ :
  (a  : U)                  (aᴹ  : Uᴹ a)
  (t  : a)                  (tᴹ  : aᴹ t)
  (p  : ∀ x (z : t ≡ x) → U)(pᴹ  : ∀ x (xₘ : aᴹ x) z zₘ → Uᴹ (p x z))
  (pr : p t refl)           (prᴹ : pᴹ _ tᴹ _ (reflᴹ aᴹ tᴹ) pr)
  (u  : a)                  (uᴹ  : aᴹ u)
  (eq : t ≡ u)              (eqᴹ : (≡ᴹ aᴹ tᴹ uᴹ) eq)
  → pᴹ _ uᴹ _ eqᴹ (J p pr eq)
Jᴹ a aᴹ t tᴹ p pᴹ pr prᴹ u uᴹ eq eqᴹ =
  J (λ xᴹ zᴹ → pᴹ u xᴹ eq zᴹ (J p pr eq))
    (J (λ x z → pᴹ x (tr aᴹ z tᴹ) z refl (J p pr z))
      prᴹ eq)
    eqᴹ

Jβᴹ :
  {a  : U}                   (aᴹ  : Uᴹ a)
  {t  : a}                   (tᴹ  : aᴹ t)
  {p  : ∀ x (z : t ≡ x) → U} (pᴹ  : ∀ x (xₘ :  aᴹ x) z zₘ → Uᴹ (p x z))
  {pr : p t refl}            (prᴹ : pᴹ _ tᴹ _ (reflᴹ aᴹ tᴹ) pr)
  → ≡ᴹ (pᴹ t tᴹ refl refl) (Jᴹ _ _ _ _ _ pᴹ _ prᴹ _ tᴹ refl refl) prᴹ refl -- Note: this last refl is Jβᶜ
Jβᴹ _ _ _ _ = refl

--------------------------------------------------------------------------------
-- Elimination
--------------------------------------------------------------------------------

Setᴱ : {A : Set}(Aᴹ : Setᴹ A) → Set₁
Setᴱ {A} Aᴹ = (x : A) → Aᴹ x → Set

Uᴱ : (A : Set) → Setᴹ A → Set
Uᴱ a aₘ = (x : a) → aₘ x

Elᴱ : (a : U)(aᴹ : Uᴹ a)(aᴱ : Uᴱ a aᴹ) → Setᴱ ( aᴹ)
Elᴱ a aᴹ aᴱ x xₘ = aᴱ x ≡ xₘ

-- inductive functions

Πᴱ : {a : U}      (aᴹ : Uᴹ a)                          (aᴱ : Uᴱ a aᴹ)
     {B : a → Set}(Bᴹ : ∀ {x}(xₘ :  aᴹ x) → Setᴹ (B x))(Bᴱ : ∀ {x} xₘ (xₑ : Elᴱ _ aᴹ aᴱ x xₘ) → Setᴱ (Bᴹ xₘ))
     → Setᴱ (Πᴹ aᴹ Bᴹ)
Πᴱ {a} aᴹ aᴱ {B} Bᴹ Bᴱ f fₘ = (x : a) → Bᴱ {x} (aᴱ x) (refl {x = aᴱ x}) (f x) (fₘ x (aᴱ x))


-- (t u)ᴱ

appᴱ :
  {a : U}      (aᴹ : Uᴹ a)                          (aᴱ : Uᴱ a aᴹ)
  {B : a → Set}(Bᴹ : ∀ {x}(xₘ :  aᴹ x) → Setᴹ (B x))(Bᴱ : ∀ {x} xₘ (xₑ : Elᴱ _ aᴹ aᴱ x xₘ) → Setᴱ (Bᴹ xₘ))
  {t : Π a B}  (tᴹ : Πᴹ aᴹ Bᴹ t)                    (tᴱ : Πᴱ aᴹ aᴱ Bᴹ Bᴱ t tᴹ)
  {u : a}      (uᴹ :  aᴹ u)                         (uᴱ : Elᴱ _ aᴹ aᴱ u uᴹ)
  → Bᴱ uᴹ uᴱ (t u) (tᴹ u uᴹ)
appᴱ {a} aᴹ aᴱ {B} Bᴹ Bᴱ {t} tᴹ tᴱ {u} uᴹ uᴱ =
  (J (λ uᴹ uᴱ → Bᴱ {u} uᴹ uᴱ (t u) (tᴹ u uᴹ)) (tᴱ u)) {uᴹ} uᴱ

-- non-inductive functions

Πₙᵢᴱ :
  (A : Set)
  {B : A → Set}   (Bᴹ : ∀ a → Setᴹ (B a))   (Bᴱ : ∀ a → Setᴱ (Bᴹ a))
  → Setᴱ (Πₙᵢᴹ A Bᴹ)
Πₙᵢᴱ A {B} Bᴹ Bᴱ f fᴹ = (a : A) → Bᴱ a (f a) (fᴹ a)

appₙᵢᴱ :
  ∀ (A : Set)
    {B : A → Set}  (Bᴹ : ∀ a → Setᴹ (B a)) (Bᴱ : ∀ a → Setᴱ (Bᴹ a))
    {t : Πₙᵢ A B}  (tᴹ : Πₙᵢᴹ A Bᴹ t)      (tᴱ : Πₙᵢᴱ A Bᴹ Bᴱ t tᴹ)
    (a : A)
  → Bᴱ a (t a) (tᴹ a)
appₙᵢᴱ A {B} Bᴹ Bᴱ {t} tᴹ tᴱ a = tᴱ a

-- small non-inductive functions
Πₙᵢₛᴱ :
  (A : Set)
  {b : A → U} (bᴹ : ∀ a → Uᴹ (b a)) (bᴱ : ∀ a → Uᴱ (b a) (bᴹ a))
  → Uᴱ (Πₙᵢₛ A b) (Πₙᵢₛᴹ A bᴹ)
Πₙᵢₛᴱ A {b} bᴹ bᴱ = λ f a → bᴱ a (f a)

appₙᵢₛᴱ :
  (A : Set)
  {b : A → U}    (bᴹ : ∀ a → Uᴹ (b a)) (bᴱ : ∀ a → Uᴱ (b a) (bᴹ a))
  {t : Πₙᵢₛ A b} (tᴹ : Πₙᵢₛᴹ A bᴹ t)   (tᴱ : Elᴱ _ (Πₙᵢₛᴹ A bᴹ) (Πₙᵢₛᴱ A bᴹ bᴱ) t tᴹ)
  (a : A)
  → Elᴱ _ (bᴹ a) (bᴱ a) (t a) (tᴹ a)
appₙᵢₛᴱ A {b} bᴹ bᴱ {t} tᴹ tᴱ a = ap (λ f → f a) {λ a → bᴱ a (t a)}{tᴹ} tᴱ

-- Identity

≡ᴱ :
  (a : U)(aᴹ : Uᴹ a)    (aᴱ : Uᴱ a aᴹ)
  (t : a)(tᴹ : Elᴹ aᴹ t)(tᴱ : Elᴱ _ aᴹ aᴱ t tᴹ)
  (u : a)(uᴹ : Elᴹ aᴹ u)(uᴱ : Elᴱ _ aᴹ aᴱ u uᴹ)
  → Uᴱ (t ≡ u) (≡ᴹ aᴹ tᴹ uᴹ)
≡ᴱ a aᴹ aᴱ t tᴹ tᴱ u uᴹ uᴱ =
  λ e → tr (λ xᴹ → tr aᴹ e xᴹ ≡ uᴹ) tᴱ (tr (λ yᴹ → tr aᴹ e (aᴱ t) ≡ yᴹ) uᴱ (apd aᴹ aᴱ t u e))

reflᴱ :
  {a : U} (aᴹ : Uᴹ a)    (aᴱ : Uᴱ a aᴹ)
  {t : a} (tᴹ : Elᴹ aᴹ t)(tᴱ : Elᴱ _ aᴹ aᴱ t tᴹ)
  → Elᴱ _ (≡ᴹ aᴹ tᴹ tᴹ) (≡ᴱ a aᴹ aᴱ t tᴹ tᴱ t tᴹ tᴱ) refl (reflᴹ aᴹ tᴹ)
reflᴱ {a} aᴹ aᴱ {t} tᴹ tᴱ =
 J (λ xᴹ xᴱ →
         tr (λ yᴹ → yᴹ ≡ xᴹ) xᴱ (tr (λ yᴹ → aᴱ t ≡ yᴹ) xᴱ refl) ≡ refl)
       refl
       tᴱ

-- Alternative, more structured implementation
-- ≡ᴱ :
--   (a : U)(aᴹ : Uᴹ a)    (aᴱ : Uᴱ a aᴹ)
--   (t : a)(tᴹ : Elᴹ aᴹ t)(tᴱ : Elᴱ _ aᴹ aᴱ t tᴹ)
--   (u : a)(uᴹ : Elᴹ aᴹ u)(uᴱ : Elᴱ _ aᴹ aᴱ u uᴹ)
--   → Uᴱ (t ≡ u) (≡ᴹ aᴹ tᴹ uᴹ)
-- ≡ᴱ a aᴹ aᴱ t tᴹ tᴱ u uᴹ uᴱ =
--   λ e → ap (tr aᴹ e) (tᴱ ⁻¹) ◾ apd aᴹ aᴱ t u e ◾ uᴱ

-- reflᴱ :
--   {a : U} (aᴹ : Uᴹ a)    (aᴱ : Uᴱ a aᴹ)
--   {t : a} (tᴹ : Elᴹ aᴹ t)(tᴱ : Elᴱ _ aᴹ aᴱ t tᴹ)
--   → Elᴱ _ (≡ᴹ aᴹ tᴹ tᴹ) (≡ᴱ a aᴹ aᴱ t tᴹ tᴱ t tᴹ tᴱ) refl (reflᴹ aᴹ tᴹ)
-- reflᴱ {a} aᴹ aᴱ {t} tᴹ tᴱ =
--   ap (_◾ tᴱ) (ap-id (tᴱ ⁻¹)) ◾ inv tᴱ


Jᴱ :
  {a : U}(aᴹ  : Uᴹ a) (aᴱ  : Uᴱ a aᴹ)
  {t : a}(tᴹ  : aᴹ t) (tᴱ  : Elᴱ _ aᴹ aᴱ t tᴹ)

  {p : (x : a)(z : t ≡ x) → Set}
    (pᴹ : ∀ x (xᴹ : aᴹ x) z (zᴹ : (≡ᴹ aᴹ tᴹ xᴹ) z) → Uᴹ (p x z))
      (pᴱ : ∀ x xᴹ (xᴱ : Elᴱ _ aᴹ aᴱ x xᴹ) z zᴹ (zᴱ : Elᴱ _ (≡ᴹ aᴹ tᴹ xᴹ) (≡ᴱ _ aᴹ aᴱ _ tᴹ tᴱ _ xᴹ xᴱ) z zᴹ)
            → Uᴱ (p x z) (pᴹ _ xᴹ _ zᴹ))

  {pr : p t refl}
    (prᴹ : pᴹ _ tᴹ _ (reflᴹ aᴹ tᴹ) pr)
      (prᴱ : Elᴱ _ (pᴹ _ tᴹ _ (reflᴹ aᴹ tᴹ)) (pᴱ _ tᴹ tᴱ _ (reflᴹ aᴹ tᴹ) (reflᴱ aᴹ aᴱ tᴹ tᴱ)) pr prᴹ)

  {u : a}(uᴹ : aᴹ u)(uᴱ : Elᴱ _ aᴹ aᴱ u uᴹ)

  {eq : t ≡ u}
    (eqᴹ : (≡ᴹ aᴹ tᴹ uᴹ) eq)
      (eqᴱ : Elᴱ _ (≡ᴹ aᴹ tᴹ uᴹ) (≡ᴱ _ aᴹ aᴱ _ tᴹ tᴱ _ uᴹ uᴱ) eq eqᴹ)

  → Elᴱ _ (pᴹ _ uᴹ _ eqᴹ) (pᴱ _ uᴹ uᴱ _ eqᴹ eqᴱ) (J p pr eq) (Jᴹ _ aᴹ _ tᴹ _ pᴹ _ prᴹ _ uᴹ _ eqᴹ)
Jᴱ {a} aᴹ aᴱ {t} tᴹ tᴱ {p} pᴹ pᴱ {pr} prᴹ prᴱ {u} uᴹ uᴱ {eq} eqᴹ eqᴱ =
   J (λ eqᴹ eqᴱ →
         Elᴱ (p u eq) (pᴹ u uᴹ eq eqᴹ) (pᴱ u uᴹ uᴱ eq eqᴹ eqᴱ)
         (J p pr eq) (Jᴹ a aᴹ t tᴹ p pᴹ pr prᴹ u uᴹ eq eqᴹ))
      (J (λ uᴹ uᴱ → Elᴱ (p u eq) (pᴹ u uᴹ eq (≡ᴱ a aᴹ aᴱ t tᴹ tᴱ u uᴹ uᴱ eq))
                    (pᴱ u uᴹ uᴱ eq (≡ᴱ a aᴹ aᴱ t tᴹ tᴱ u uᴹ uᴱ eq) refl) (J p pr eq)
                    (Jᴹ a aᴹ t tᴹ p pᴹ pr prᴹ u uᴹ eq (≡ᴱ a aᴹ aᴱ t tᴹ tᴱ u uᴹ uᴱ eq)))
         (J (λ u eq → Elᴱ (p u eq)
              (pᴹ u (aᴱ u) eq (≡ᴱ a aᴹ aᴱ t tᴹ tᴱ u (aᴱ u) refl eq))
              (pᴱ u (aᴱ u) refl eq (≡ᴱ a aᴹ aᴱ t tᴹ tᴱ u (aᴱ u) refl eq) refl)
              (J p pr eq)
              (Jᴹ a aᴹ t tᴹ p pᴹ pr prᴹ u (aᴱ u) eq
               (≡ᴱ a aᴹ aᴱ t tᴹ tᴱ u (aᴱ u) refl eq)))
             (J (λ tᴹ tᴱ →
                  (pᴹ : (x : a) (xᴹ : aᴹ x) (z : Id t x) → ≡ᴹ aᴹ tᴹ xᴹ z → Uᴹ (p x z))
                  (pᴱ : (x : a) (xᴹ : aᴹ x) (xᴱ : Elᴱ a aᴹ aᴱ x xᴹ) (z : Id t x)
                        (zᴹ : Id (tr aᴹ z tᴹ) xᴹ) →
                        Elᴱ (t ≡ x) (≡ᴹ aᴹ tᴹ xᴹ) (≡ᴱ a aᴹ aᴱ t tᴹ tᴱ x xᴹ xᴱ) z zᴹ →
                        Uᴱ (p x z) (pᴹ x xᴹ z zᴹ))
                  (prᴹ : pᴹ t tᴹ refl (reflᴹ aᴹ tᴹ) pr)
                  (prᴱ : Elᴱ (p t refl) (pᴹ t tᴹ refl (reflᴹ aᴹ tᴹ))
                        (pᴱ t tᴹ tᴱ refl (reflᴹ aᴹ tᴹ) (reflᴱ aᴹ aᴱ tᴹ tᴱ)) pr prᴹ)
                  → Elᴱ (p t refl)
                     (pᴹ t (aᴱ t) refl (≡ᴱ a aᴹ aᴱ t tᴹ tᴱ t (aᴱ t) refl refl))
                     (pᴱ t (aᴱ t) refl refl (≡ᴱ a aᴹ aᴱ t tᴹ tᴱ t (aᴱ t) refl refl)
                     refl) pr
                     (Jᴹ a aᴹ t tᴹ p pᴹ pr prᴹ t (aᴱ t) refl
                     (≡ᴱ a aᴹ aᴱ t tᴹ tᴱ t (aᴱ t) refl refl)))
                 (λ pᴹ pᴱ prᴹ prᴱ → prᴱ)
                 tᴱ pᴹ pᴱ prᴹ prᴱ)
             eq)
         uᴱ)
      eqᴱ

Jβᴱ :
  {a : Set}(aᴹ  : Uᴹ a) (aᴱ  : Uᴱ a aᴹ)
  {t : a}  (tᴹ  : aᴹ t) (tᴱ  : Elᴱ _ aᴹ aᴱ t tᴹ)

  {p : (x : a)(z : t ≡ x) → Set}
    (pᴹ : ∀ x (xᴹ : aᴹ x) z (zᴹ : (≡ᴹ aᴹ tᴹ xᴹ) z) → Uᴹ (p x z))
      (pᴱ : ∀ x xᴹ (xᴱ : Elᴱ _ aᴹ aᴱ x xᴹ) z zᴹ (zᴱ : Elᴱ _ (≡ᴹ aᴹ tᴹ xᴹ) (≡ᴱ _ aᴹ aᴱ _ tᴹ tᴱ _ xᴹ xᴱ) z zᴹ)
            → Uᴱ (p x z) (pᴹ _ xᴹ _ zᴹ))

  {pr : p t refl}
    (prᴹ : pᴹ _ tᴹ _ (reflᴹ aᴹ tᴹ) pr)
      (prᴱ : Elᴱ _ (pᴹ _ tᴹ _ (reflᴹ aᴹ tᴹ)) (pᴱ _ tᴹ tᴱ _ (reflᴹ aᴹ tᴹ) (reflᴱ aᴹ aᴱ tᴹ tᴱ)) pr prᴹ)

  → Elᴱ (J p pr refl ≡ pr)
        (≡ᴹ (pᴹ t tᴹ refl refl) (Jᴹ a aᴹ t tᴹ p pᴹ pr prᴹ t tᴹ refl refl) prᴹ)
        (≡ᴱ (p t refl) (pᴹ t tᴹ refl refl) (pᴱ t tᴹ tᴱ refl refl (reflᴱ aᴹ aᴱ tᴹ tᴱ))
            (J p pr refl) (Jᴹ a aᴹ t tᴹ p pᴹ pr prᴹ t tᴹ refl refl)
                          (Jᴱ {a} aᴹ aᴱ {t} tᴹ tᴱ {p} pᴹ pᴱ {pr} prᴹ prᴱ tᴹ tᴱ refl (reflᴱ aᴹ aᴱ tᴹ tᴱ)) pr prᴹ prᴱ)
        refl
        (Jβᴹ aᴹ tᴹ pᴹ prᴹ)

Jβᴱ {a} aᴹ aᴱ {t} tᴹ tᴱ {p} pᴹ pᴱ {pr} prᴹ prᴱ =
  J (λ prᴹ prᴱ → Elᴱ (pr ≡ pr)
         (≡ᴹ (pᴹ t tᴹ refl refl) (Jᴹ a aᴹ t tᴹ p pᴹ pr prᴹ t tᴹ refl refl)
          prᴹ)
         (≡ᴱ (p t refl) (pᴹ t tᴹ refl refl)
          (pᴱ t tᴹ tᴱ refl refl (reflᴱ aᴹ aᴱ tᴹ tᴱ)) pr
          (Jᴹ a aᴹ t tᴹ p pᴹ pr prᴹ t tᴹ refl refl)
          (Jᴱ aᴹ aᴱ tᴹ tᴱ pᴹ pᴱ prᴹ prᴱ tᴹ tᴱ (reflᴹ aᴹ tᴹ)
           (reflᴱ aᴹ aᴱ tᴹ tᴱ))
          pr prᴹ prᴱ)
         refl (Jβᴹ aᴹ tᴹ pᴹ prᴹ))
      (J (λ tᴹ tᴱ →
               (pᴹ  : (x : a) (xᴹ : aᴹ x) (z : Id t x) → ≡ᴹ aᴹ tᴹ xᴹ z → Uᴹ (p x z))
             → (pᴱ  : (x : a) (xᴹ : aᴹ x) (xᴱ : Elᴱ a aᴹ aᴱ x xᴹ) (z : Id t x)
                  (zᴹ : Id (tr aᴹ z tᴹ) xᴹ) →
                  Elᴱ (t ≡ x) (≡ᴹ aᴹ tᴹ xᴹ) (≡ᴱ a aᴹ aᴱ t tᴹ tᴱ x xᴹ xᴱ) z zᴹ →
                  Uᴱ (p x z) (pᴹ x xᴹ z zᴹ))
             → Elᴱ (pr ≡ pr)
             (≡ᴹ (pᴹ t tᴹ refl refl)
              (Jᴹ a aᴹ t tᴹ p pᴹ pr
               (pᴱ t tᴹ tᴱ refl (reflᴹ aᴹ tᴹ) (reflᴱ aᴹ aᴱ tᴹ tᴱ) pr) t tᴹ refl
               refl)
              (pᴱ t tᴹ tᴱ refl (reflᴹ aᴹ tᴹ) (reflᴱ aᴹ aᴱ tᴹ tᴱ) pr))
             (≡ᴱ (p t refl) (pᴹ t tᴹ refl refl)
              (pᴱ t tᴹ tᴱ refl refl (reflᴱ aᴹ aᴱ tᴹ tᴱ)) pr
              (Jᴹ a aᴹ t tᴹ p pᴹ pr
               (pᴱ t tᴹ tᴱ refl (reflᴹ aᴹ tᴹ) (reflᴱ aᴹ aᴱ tᴹ tᴱ) pr) t tᴹ refl
               refl)
              (Jᴱ aᴹ aᴱ tᴹ tᴱ pᴹ pᴱ
               (pᴱ t tᴹ tᴱ refl (reflᴹ aᴹ tᴹ) (reflᴱ aᴹ aᴱ tᴹ tᴱ) pr) refl tᴹ tᴱ
               (reflᴹ aᴹ tᴹ) (reflᴱ aᴹ aᴱ tᴹ tᴱ))
              pr (pᴱ t tᴹ tᴱ refl (reflᴹ aᴹ tᴹ) (reflᴱ aᴹ aᴱ tᴹ tᴱ) pr) refl)
             refl
             (Jβᴹ aᴹ tᴹ pᴹ
              (pᴱ t tᴹ tᴱ refl (reflᴹ aᴹ tᴹ) (reflᴱ aᴹ aᴱ tᴹ tᴱ) pr)))
          (λ pᴹ pᴱ → refl)
          tᴱ pᴹ pᴱ)
      prᴱ
