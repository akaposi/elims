

{- Constructing the initial inductive-inductive Con-Ty model, with UIP. -}

module IIReductionExamples.ConTyRecUIP where


open import StrictLib

-- Intrinsic models
--------------------------------------------------------------------------------

-- Models
record ConTy {ℓ} : Set (suc ℓ) where
  infixl 5 _▶_
  field
    Con  : Set ℓ
    Ty   : Con → Set ℓ
    ∙    : Con
    _▶_  : (Γ : Con) → Ty Γ → Con
    U    : (Γ : Con) → Ty Γ
    Π    : (Γ : Con)(A : Ty Γ)(B : Ty (Γ ▶ A)) → Ty Γ

-- Morphisms
record ConTyᴿ {ℓ₁}{ℓ₂}(M : ConTy {ℓ₁})(N : ConTy {ℓ₂}) : Set (ℓ₁ ⊔ ℓ₂) where
  constructor mkConTyᴿ
  private module M = ConTy M
  private module N = ConTy N
  field
    Con : M.Con → N.Con
    Ty  : ∀{Γ} → M.Ty Γ → N.Ty (Con Γ)
    •   : Con M.∙ ≡ N.∙
    ▶   : ∀{Γ A} → Con (Γ M.▶ A) ≡ Con Γ N.▶ Ty A
    U   : ∀{Γ} → Ty (M.U Γ) ≡ N.U (Con Γ)
    Π   : ∀{Γ A B} → Ty (M.Π Γ A B) ≡ N.Π (Con Γ) (Ty A) (tr N.Ty ▶ (Ty B))


-- Presyntax
--------------------------------------------------------------------------------

infixl 5 _▶_

data Con : Set
data Ty  : Set

data Con where
  ∙   : Con
  _▶_ : Con → Ty → Con

data Ty where
  U : Con → Ty
  Π : Con → Ty → Ty → Ty

-- Well-formedness
--------------------------------------------------------------------------------

Conw : (Γ : Con) → Set
Tyw  : (A : Ty)  → Con → Set
Conw   ∙         = ⊤
Conw   (Γ ▶ A)   = Conw Γ × Tyw A Γ
Tyw    (U Γ)     = λ Δ → Conw Γ × (Γ ≡ Δ)
Tyw    (Π Γ A B) = λ Δ → Conw Γ × Tyw A Γ × Tyw B (Γ ▶ A) × (Γ ≡ Δ)

-- Syntactic model
--------------------------------------------------------------------------------

ConTySyn : ConTy {zero}
ConTySyn = record {
    Con = ∃ Conw
  ; Ty  = λ {(Γ , _) → Σ Ty λ A → Tyw A Γ}
  ; ∙   = ∙ , tt
  ; _▶_ = λ {(Γ , Γw) (A , Aw) → (Γ ▶ A) , Γw , Aw}
  ; U   = λ {(Γ , Γw) → U Γ , Γw , refl}
  ; Π   = λ {(Γ , Γw)(A , Aw)(B , Bw) → Π Γ A B , Γw , Aw , Bw , refl}}

module Syn = ConTy ConTySyn


-- Initiality
--------------------------------------------------------------------------------

module _ {α}(M : ConTy {α}) where
  module M = ConTy M

  Con~ : (Γ : Con) → M.Con → Set α
  Ty~  : (A : Ty)  → ∀ γ → M.Ty γ → Set α
  Con~   ∙           γ = γ ≡ M.∙
  Con~   (Γ ▶ A)     γ = ∃₂ λ γ₀ α → Con~ Γ γ₀ × Ty~ A γ₀ α × (γ ≡ γ₀ M.▶ α)
  Ty~    (U Γ)       γ α = Con~ Γ γ × (α ≡ M.U γ)
  Ty~    (Π Γ A B)   γ α = Con~ Γ γ × ∃₂ λ α₀ α₁ → Ty~ A γ α₀ × Ty~ B (γ M.▶ α₀) α₁ × (α ≡ M.Π γ α₀ α₁)

  -- Determinism
  Γ~≡ : (Γ : Con) → ∀ γ γ' → Con~ Γ γ  → Con~ Γ γ' → γ ≡ γ'
  A~≡ : (A : Ty)  → ∀ γ α α' → Ty~ A γ α → Ty~ A γ α' → α ≡ α'
  Γ~≡   ∙           γ γ' ~γ ~γ' = ~γ ◾ ~γ' ⁻¹
  Γ~≡   (Γ ▶ A)     _ _ (γ , α , ~γ , ~α , refl) (γ' , α' , ~γ' , ~α' , refl) with Γ~≡ Γ γ γ' ~γ ~γ'
  ...                 | refl = ap (γ' M.▶_) (A~≡ A γ α α' ~α ~α')
  A~≡   (U Γ)       γ α α' (_ , ~α) (_ , ~α') = ~α ◾ ~α' ⁻¹
  A~≡   (Π Γ A B)   γ _ _ (~γ  , α₀  , α₁  , ~α₀  , ~α₁  , refl)
                          (~γ' , α₀' , α₁' , ~α₀' , ~α₁' , refl) with A~≡ A γ α₀ α₀' ~α₀ ~α₀'
  ...                 | refl = ap (M.Π γ α₀') (A~≡ B (γ M.▶ α₀') α₁ α₁' ~α₁ ~α₁')

  -- Totality on well-formed values
  ⟦Con⟧ : (Γ : Con) → Conw Γ → ∃ (Con~ Γ)
  ⟦Ty⟧  : (A : Ty) → ∀ Γ → Tyw A Γ → ∃₂ λ γ α → Con~ Γ γ × Ty~ A γ α
  ⟦Con⟧   ∙           _ = M.∙ , refl
  ⟦Con⟧   (Γ ▶ A)     (Γw , Aw) with ⟦Con⟧ Γ Γw | ⟦Ty⟧ A Γ Aw
  ...                   | (γ , ~γ) | (γ' , α , ~γ' , ~α) with Γ~≡ Γ γ γ' ~γ ~γ'
  ...                   | refl = (γ M.▶ α) , (γ , α , ~γ , ~α , refl)
  ⟦Ty⟧ (U Γ)     _ (Γw , refl) with ⟦Con⟧ Γ Γw
  ...                   | (γ , ~γ) = γ , M.U γ , ~γ , ~γ , refl
  ⟦Ty⟧ (Π Γ A B) Δ (Γw , Aw , Bw , refl) with ⟦Con⟧ Γ Γw | ⟦Ty⟧ A Γ Aw
  ...                   | (γ , ~γ) | (γ' , α , ~γ' , ~α) with Γ~≡ Γ γ γ' ~γ ~γ'
  ...                   | refl with ⟦Ty⟧ B (Γ ▶ A) Bw
  ...                   | (_ , β , (γ'' , α' , ~γ'' , ~α' , refl) , ~β) with Γ~≡ Γ γ γ'' ~γ ~γ''
  ...                   | refl with A~≡ A γ'' α α' ~α ~α'
  ...                   | refl = γ , M.Π γ α β , ~γ , (~γ , α , β , ~α , ~β , refl)

  Conᴿ : Syn.Con → M.Con
  Conᴿ (Γ , Γw) = ₁ (⟦Con⟧ Γ Γw)

  Tyᴿ : ∀ {Γ} → Syn.Ty Γ → M.Ty (Conᴿ Γ)
  Tyᴿ {Γ , Γw} (A , Aw) with ⟦Con⟧ Γ Γw | ⟦Ty⟧ A Γ Aw
  ... | γ , ~γ | γ' , α , ~γ' , ~α with Γ~≡ Γ γ γ' ~γ ~γ'
  ... | refl = α

  ▶ᴿ : ∀ {Γ A} → Conᴿ (Γ Syn.▶ A) ≡ Conᴿ Γ M.▶ Tyᴿ A
  ▶ᴿ {Γ , Γw}{A , Aw} with ⟦Con⟧ Γ Γw | ⟦Ty⟧ A Γ Aw
  ... | (γ , ~γ) | (γ' , α , ~γ' , ~α) with Γ~≡ Γ γ γ' ~γ ~γ'
  ... | refl = refl

  Uᴿ : ∀ {Γ} → Tyᴿ (Syn.U Γ) ≡ M.U (Conᴿ Γ)
  Uᴿ {Γ , Γw} with ⟦Con⟧ Γ Γw
  ... | γ , ~γ with Γ~≡ Γ γ γ ~γ ~γ
  ... | refl = refl

  Πᴿ : ∀ {Γ A B} → Tyᴿ (Syn.Π Γ A B) ≡ M.Π (Conᴿ Γ) (Tyᴿ A) (tr M.Ty (▶ᴿ {Γ}{A}) (Tyᴿ B))
  Πᴿ {Γ , Γw}{A , Aw}{B , Bw} with ⟦Con⟧ Γ Γw | ⟦Ty⟧ A Γ Aw
  ... | (γ , ~γ) | (γ' , α , ~γ' , ~α) with Γ~≡ Γ γ γ' ~γ ~γ'
  ... | refl with ⟦Ty⟧ B (Γ ▶ A) Bw
  ... | (_ , β , (γ'' , α' , ~γ'' , ~α' , refl) , ~β) with Γ~≡ Γ γ γ'' ~γ ~γ''
  ... | refl with A~≡ A γ'' α α' ~α ~α'
  ... | refl with Γ~≡ Γ γ'' γ'' ~γ ~γ
  ... | refl = refl

  -- recursor
  ConTyRec : ConTyᴿ ConTySyn M
  ConTyRec = record
    { Con = Conᴿ
    ; Ty  = Tyᴿ
    ; •   = refl
    ; ▶   = λ {Γ}{A} → ▶ᴿ {Γ}{A}
    ; U   = λ {Γ} → Uᴿ {Γ}
    ; Π   = λ {Γ}{A}{B} → Πᴿ {Γ}{A}{B}
    }

  -- uniqueness
  unique : (σ δ : ConTyᴿ ConTySyn M) → σ ≡ δ
  unique (mkConTyᴿ Conᴿ Tyᴿ ∙≡ ▶≡ U≡ Π≡) (mkConTyᴿ Conᴿ' Tyᴿ' ∙≡' ▶≡' U≡' Π≡')
    = uñ (ap6̃̃ mkConTyᴿ
        (ext λ {(Γ , Γw) → Con≡ Γ Γw})
        (extĩ (λ _ → ext̃ (λ {(A , Aw) → Ty≡ A Aw})))
        (UIP̃' _ _ (Con≡ ∙ tt) refl)
        (extĩ (λ {(Γ , Γw) → extĩ λ {(A , Aw)
              → UIP̃' _ _ (Con≡ (Γ ▶ A) (Γw , Aw)) (ap2̃ M._▶_ (Con≡ Γ Γw) (Ty≡ A Aw))}}))
        (extĩ λ {(Γ , Γw) → UIP̃'' _ _ (Ty≡ (U Γ) (Γw , refl)) (ap̃̃ M.U (Con≡ Γ Γw))})
        (extĩ λ {(Γ , Γw) → extĩ λ {(A , Aw) → extĩ λ {(B , Bw)
              → UIP̃'' _ _ (Ty≡ (Π Γ A B) _)
                          (ap3̃̃ M.Π (Con≡ Γ Γw)
                                   (Ty≡ A Aw)
                                   (untr M.Ty ▶≡ _ ◾̃ Ty≡ B Bw ◾̃ untr M.Ty ▶≡' _ ⁻¹̃))}}}))
    where
      Conw-prop : ∀ Γ (p q : Conw Γ) → p ≡ q
      Tyw-prop  : ∀ Γ A (p q : Tyw A Γ) → p ≡ q
      Conw-prop ∙       p       q         = refl
      Conw-prop (Γ ▶ A) (p , q) (p' , q') = ap _,_ (Conw-prop Γ p p') ⊗ Tyw-prop Γ A q q'
      Tyw-prop Γ (U .Γ) (p , refl) (p' , refl) = ap (_, refl) (Conw-prop Γ p p')
      Tyw-prop Γ (Π .Γ A B) (p , q , r , refl) (p' , q' , r' , refl)
        = ap (λ a b c → a , b , c , refl)
               (Conw-prop Γ p p')
             ⊗ Tyw-prop Γ A q q'
             ⊗ Tyw-prop (Γ ▶ A) B r r'

      Con≡ : ∀ Γ Γw → Conᴿ (Γ , Γw) ≡ Conᴿ' (Γ , Γw)
      Ty≡ : ∀ {Γ}{Γw} A Aw → Tyᴿ {Γ , Γw} (A , Aw) ≃ Tyᴿ' {Γ , Γw} (A , Aw)
      Con≡ ∙       Γw        = ∙≡ ◾ ∙≡' ⁻¹
      Con≡ (Γ ▶ A) (Γw , Aw) = ▶≡ ◾ ap2̃ M._▶_ (Con≡ Γ Γw) (Ty≡ A Aw) ◾ ▶≡' ⁻¹
      Ty≡ {Γ} {Γw} (U Δ)     (Δw , refl) rewrite Conw-prop Γ Γw Δw =
        (U≡ ~) ◾̃ ap̃̃ M.U (Con≡ Γ Δw) ◾̃ ((U≡' ⁻¹) ~)
      Ty≡ {Γ} {Γw} (Π Δ A B) (Δw , Aw , Bw , refl) rewrite Conw-prop Γ Γw Δw =
          (Π≡ ~)
        ◾̃ ap3̃̃ M.Π (Con≡ Γ Δw) (Ty≡ A Aw) (untr M.Ty ▶≡ _ ◾̃ Ty≡ B Bw ◾̃ untr M.Ty ▶≡' _ ⁻¹̃)
          ◾̃ (Π≡' ⁻¹ ~)
