{-# OPTIONS --without-K --safe #-}

open import Level
open import Relation.Binary.PropositionalEquality
  renaming (cong to ap; subst to tr)
open import Data.Product
  renaming (proj₁ to ₁; proj₂ to ₂)
open import Data.Unit

infix 6 _⁻¹
infixr 5 _◾_
_⁻¹ : ∀ {i}{A : Set i}{x y : A} → x ≡ y → y ≡ x
refl ⁻¹ = refl

_◾_ : ∀ {i}{A : Set i}{x y z : A} → x ≡ y → y ≡ z → x ≡ z
refl ◾ q = q

--------------------------------------------------------------------------------

-- Algebras
record Alg : Set₁ where
  constructor alg
  infixl 5 _▶_
  field
    Con  : Set
    Ty   : Con → Set
    ∙    : Con
    _▶_  : (Γ : Con) → Ty Γ → Con
    U    : (Γ : Con) → Ty Γ
    Π    : (Γ : Con)(A : Ty Γ)(B : Ty (Γ ▶ A)) → Ty Γ

-- Displayed algebras
record Algᴰ {i}{j} (γ : Alg) : Set (suc (i ⊔ j)) where
  private module γ = Alg γ
  infixl 5 _▶_
  field
    Con : γ.Con → Set i
    Ty  : ∀ {Γ}(Γᴰ : Con Γ) → γ.Ty Γ → Set j
    ∙   : Con γ.∙
    _▶_ : ∀ {Γ}(Γᴰ : Con Γ) {A} (Aᴰ : Ty Γᴰ A) → Con (Γ γ.▶ A)
    U   : ∀ {Γ}(Γᴰ : Con Γ) → Ty Γᴰ (γ.U Γ)
    Π   : ∀ {Γ}(Γᴰ : Con Γ) {A} (Aᴰ : Ty Γᴰ A) {B} (Bᴰ : Ty (Γᴰ ▶ Aᴰ) B)
          → Ty Γᴰ (γ.Π Γ A B)

-- Displayed algebra sections
record Algˢ {i}{j} (γ : Alg)(δ : Algᴰ {i}{j} γ) : Set (i ⊔ j) where
  constructor algˢ
  private module γ = Alg γ
  private module δ = Algᴰ δ
  field
    Con : ∀ Γ → δ.Con Γ
    Ty  : ∀ {Γ}(A : γ.Ty Γ) → δ.Ty (Con Γ) A
    ∙   : Con γ.∙ ≡ δ.∙
    ▶   : ∀ {Γ A} → Con (Γ γ.▶ A) ≡ Con Γ δ.▶ Ty A
    U   : ∀ {Γ} → Ty (γ.U Γ) ≡ δ.U (Con Γ)
    Π   : ∀ {Γ A B} → Ty (γ.Π Γ A B) ≡
                      δ.Π (Con Γ) (Ty A) (tr (λ x → δ.Ty x B) ▶ (Ty B))

-- Syntax
--------------------------------------------------------------------------------

data Con : Set
data Ty  : Set

infixl 5 _▶_
data Con where
  ∙   : Con
  _▶_ : Con → Ty → Con

data Ty where
  U : Ty
  Π : Ty → Ty → Ty

Syn : Alg
Syn = alg Con (λ _ → Ty) ∙ _▶_ (λ _ → U) (λ _ → Π)

-- Eliminator
--------------------------------------------------------------------------------

module _ {i}{j}(M : Algᴰ {i}{j} Syn) where
  private module M = Algᴰ M

  ⟦Con⟧ : ∀ Γ → M.Con Γ
  ⟦Ty⟧  : ∀ A {Γ} {γ : M.Con Γ} → M.Ty γ A
  ⟦Con⟧ ∙           = M.∙
  ⟦Con⟧ (Γ ▶ A)     = ⟦Con⟧ Γ M.▶ ⟦Ty⟧ A
  ⟦Ty⟧  U           = M.U _
  ⟦Ty⟧  (Π A B)     = M.Π _ (⟦Ty⟧ A) (⟦Ty⟧ B)

  Elim : Algˢ Syn M
  Elim = algˢ ⟦Con⟧ (λ {Γ} A → ⟦Ty⟧ A) refl refl refl refl
