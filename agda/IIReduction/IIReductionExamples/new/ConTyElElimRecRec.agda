{-# OPTIONS --without-K #-}

{-
General solution without UIP: well-formedness and interpretation are
*both* recursive-recursive, and well-formedness constraints presyntax
*and* witnesses as indices, all the time. We get the well-formedness
predicate Jasper Hugunin's paper, but not the eliminator part.

Research idea: general semantics of recursion-recursion
-}

open import Level
open import Relation.Binary.PropositionalEquality
  renaming (cong to ap; subst to tr)
open import Data.Product
  renaming (proj₁ to ₁; proj₂ to ₂)
open import Data.Unit
open import Function

infix 6 _⁻¹
infixr 5 _◾_
_⁻¹ : ∀ {i}{A : Set i}{x y : A} → x ≡ y → y ≡ x
refl ⁻¹ = refl

_◾_ : ∀ {i}{A : Set i}{x y z : A} → x ≡ y → y ≡ z → x ≡ z
refl ◾ q = q

--------------------------------------------------------------------------------

-- Algebras
record Alg : Set₁ where
  constructor alg
  infixl 5 _▶_
  field
    Con  : Set
    Ty   : Con → Set
    ∙    : Con
    _▶_  : (Γ : Con) → Ty Γ → Con
    U    : (Γ : Con) → Ty Γ
    Π    : (Γ : Con)(A : Ty Γ)(B : Ty (Γ ▶ A)) → Ty Γ
    El   : ∀ Γ → Ty (Γ ▶ U Γ)

-- Displayed algebras
record Algᴰ {i}{j} (γ : Alg) : Set (suc (i ⊔ j)) where
  private module γ = Alg γ
  infixl 5 _▶_
  field
    Con : γ.Con → Set i
    Ty  : ∀ {Γ}(Γᴰ : Con Γ) → γ.Ty Γ → Set j
    ∙   : Con γ.∙
    _▶_ : ∀ {Γ}(Γᴰ : Con Γ) {A} (Aᴰ : Ty Γᴰ A) → Con (Γ γ.▶ A)
    U   : ∀ {Γ}(Γᴰ : Con Γ) → Ty Γᴰ (γ.U Γ)
    Π   : ∀ {Γ}(Γᴰ : Con Γ) {A} (Aᴰ : Ty Γᴰ A) {B} (Bᴰ : Ty (Γᴰ ▶ Aᴰ) B)
          → Ty Γᴰ (γ.Π Γ A B)
    El  : ∀ {Γ}(Γᴰ : Con Γ) → Ty (Γᴰ ▶ U Γᴰ) (γ.El Γ)

-- Displayed algebra sections
record Algˢ {i}{j} (γ : Alg)(δ : Algᴰ {i}{j} γ) : Set (i ⊔ j) where
  constructor algˢ
  private module γ = Alg γ
  private module δ = Algᴰ δ
  field
    Con : ∀ Γ → δ.Con Γ
    Ty  : ∀ {Γ}(A : γ.Ty Γ) → δ.Ty (Con Γ) A
    ∙   : Con γ.∙ ≡ δ.∙
    ▶   : ∀ {Γ A} → Con (Γ γ.▶ A) ≡ Con Γ δ.▶ Ty A
    U   : ∀ {Γ} → Ty (γ.U Γ) ≡ δ.U (Con Γ)
    Π   : ∀ {Γ A B} → Ty (γ.Π Γ A B) ≡
                      δ.Π (Con Γ) (Ty A) (tr (λ x → δ.Ty x B) ▶ (Ty B))
    El  : ∀ {Γ} → tr (λ x → δ.Ty x _) (▶ ◾ ap (Con Γ δ.▶_) U) (Ty (γ.El Γ))
                ≡ δ.El (Con Γ)

-- Syntax
--------------------------------------------------------------------------------


data Con : Set
data Ty  : Set

infixl 5 _▶_
data Con where
  ∙   : Con
  _▶_ : Con → Ty → Con

data Ty where
  U  : Con → Ty
  Π  : Con → Ty → Ty → Ty
  El : Con → Ty

{-# TERMINATING #-}
Conw : Con → Set
Tyw : ∀ Γ → Conw Γ → Ty → Set
Conw ∙ = ⊤
Conw (Γ ▶ A)       = ∃ λ (Γw : Conw Γ) → Tyw Γ Γw A
Tyw Γ Γw (U Δ)     = ∃ λ (Δw : Conw Δ) → (Δ , Δw) ≡ (Γ , Γw)
Tyw Γ Γw (Π Δ A B) = ∃₂ λ (Δw : Conw Δ)(Aw : Tyw Δ Δw A) → ∃ λ (Bw : Tyw (Δ ▶ A) (Δw , Aw) B)
                        → (Γ , Γw) ≡ (Δ , Δw)
Tyw Γ Γw (El Δ) = ∃ λ (Δw : Conw Δ) → (Δ ▶ U Δ , Δw , Δw , refl) ≡ (∃ Conw ∋ (Γ , Γw))

Syn : Alg
Syn = alg
  (∃ Conw)
  (λ {(Γ , Γw) → ∃ λ A → Tyw Γ Γw A})
  (∙ , tt)
  (λ {(Γ , Γw)(A , Aw) → (Γ ▶ A) , Γw , Aw})
  (λ {(Γ , Γw) → U Γ , Γw , refl})
  (λ {(Γ , Γw)(A , Aw)(B , Bw) → (Π Γ A B) , Γw , Aw , Bw , refl})
  (λ {(Γ , Γw) → El Γ , Γw , refl})

module _ {ℓ}{ℓ'}(M : Algᴰ {ℓ}{ℓ'} Syn) where
  private module M = Algᴰ M
  {-# TERMINATING #-}
  ⟦Con⟧ : ∀ Γ → M.Con Γ
  ⟦Ty⟧  : ∀ {Γ} A → M.Ty (⟦Con⟧ Γ) A
  ⟦Con⟧ (∙ , Γw) = M.∙
  ⟦Con⟧ (Γ ▶ A , Γw , Aw) = ⟦Con⟧ (Γ , Γw) M.▶ ⟦Ty⟧ (A , Aw)
  ⟦Ty⟧ (U Γ , Γw , refl) = M.U (⟦Con⟧ (Γ , Γw))
  ⟦Ty⟧ (Π Γ A B , Γw , Aw , Bw , refl) = M.Π (⟦Con⟧ (Γ , Γw)) (⟦Ty⟧ (A , Aw)) (⟦Ty⟧ (B , Bw))
  ⟦Ty⟧ (El Γ , Γw , refl) = M.El (⟦Con⟧ (Γ , Γw))

  Elim : Algˢ _ M
  Elim = algˢ ⟦Con⟧ ⟦Ty⟧ refl refl refl refl refl
