{-# OPTIONS --without-K --safe #-}

{-
Constructing Hugunin's "running example" IIT without UIP. Although
this does work here directly, but only because the type can be
trivially reforumlated as a simple inductive type
-}

open import Relation.Binary.PropositionalEquality
  renaming (cong to ap; subst to tr)
open import Data.Product
  renaming (proj₁ to ₁; proj₂ to ₂)
open import Data.Unit

module _ {X : Set} where

infix 6 _⁻¹
infixr 5 _◾_
_⁻¹ : ∀ {i}{A : Set i}{x y : A} → x ≡ y → y ≡ x
refl ⁻¹ = refl

_◾_ : ∀ {i}{A : Set i}{x y z : A} → x ≡ y → y ≡ z → x ≡ z
refl ◾ q = q

--------------------------------------------------------------------------------

record Alg : Set₁ where
  constructor alg
  field
    A   : Set
    B   : A → Set
    η   : X → A
    ext : ∀ a → B a → A
    inj : ∀ a → B a

-- displayed algebra
record Algᴰ (M : Alg) : Set₁ where
  constructor algᴰ
  private module M = Alg M
  field
    A   : M.A → Set
    B   : ∀ a → A a → M.B a → Set
    η   : ∀ x → A (M.η x)
    ext : ∀ a (aᴰ : A a) (b : M.B a) → B a aᴰ b → A (M.ext a b)
    inj : ∀ a (aᴰ : A a) → B a aᴰ (M.inj a)

-- displayed algebra section
record Algˢ (N : Alg)(M : Algᴰ N) : Set where
  constructor algˢ
  private module N = Alg N
  private module M = Algᴰ M
  field
    A   : ∀ a → M.A a
    B   : (a : N.A)(b : N.B a) → M.B a (A a) b
    η   : ∀ x → A (N.η x) ≡ M.η x
    ext : ∀ a (b : N.B a) → A (N.ext a b) ≡ M.ext a (A a) b (B a b)
    inj : ∀ a → B a (N.inj a) ≡ M.inj a (A a)

-- term algebra
--------------------------------------------------------------------------------

data A : Set
data B : Set
data A where
  η   : X → A
  ext : A → B → A
data B where
  inj : A → B

Aw : A → Set
Bw : B → A → Set
Aw (η x)      = ⊤
Aw (ext a b)  = Aw a × Bw b a
Bw (inj a) a' = a ≡ a'

Syn : Alg
Syn = alg
  (∃ Aw)
  (λ {(a , aw) → ∃ λ b → Bw b a})
  (λ x → η x , tt)
  (λ {(a , aw)(b , bw) → (ext a b) , aw , bw})
  (λ {(a , aw) → (inj a) , refl})

module Syn = Alg Syn

-- Eliminator
--------------------------------------------------------------------------------

module _ (M : Algᴰ Syn) where
  private module M = Algᴰ M

  -- graph of evaluation
  A~ : (a : Syn.A) → M.A a → Set
  B~ : ∀ a (b : Syn.B a) → (α : M.A a) → M.B a α b → Set
  A~ (η x     , aw)      α* = M.η x ≡ α*
  A~ (ext a b , aw , bw) α* =
    ∃ λ α → A~ (a , aw) α × ∃ λ β → B~ (a , aw) (b , bw) α β × M.ext _ α _ β ≡ α*
  B~ (_ , aw) (inj a , refl) α* β* = A~ (a , aw) α* × M.inj _ α* ≡ β*

  -- right-determinism
  A~≡ : (a : Syn.A) → ∀ α α' → A~ a α → A~ a α' → α ≡ α'
  B~≡ : ∀ a (b : Syn.B a) → ∀ α β β' → B~ a b α β → B~ a b α β' → β ≡ β'
  A~≡ (η x     , aw)      α α' α~ α~' = α~ ⁻¹ ◾ α~'
  A~≡ (ext a b , aw , bw) α α' (p , q , r , s , t) (p' , q' , r' , s' , t')
             with A~≡ (a , aw) _ _ q q'
  ... | refl with B~≡ _ (b , bw) _ _ _ s s'
  ... | refl = t ⁻¹ ◾ t'
  B~≡ (a , aw) (inj x , refl) α β β' (β~ , p) (β~' , q) = p ⁻¹ ◾ q

  -- uniqueness of ~≡
  A~refl : ∀ a α α~ → A~≡ a α α α~ α~ ≡ refl
  B~refl : ∀ a b α β β~ → B~≡ a b α β β β~ β~ ≡ refl
  A~refl (η x     , aw) α refl = refl
  A~refl (ext a b , aw , bw) _ (α , α~ , β , β~ , p)
    rewrite A~refl (a , aw) α α~ | B~refl (a , aw) (b , bw) α β β~ with p
  ... | refl = refl
  B~refl (_ , aw) (inj a , refl) α β (_ , refl) = refl

  -- left-totality
  ⟦A⟧ : ∀ a → ∃ (A~ a)
  ⟦B⟧ : ∀ a b → ∃ λ α → A~ a α × ∃ λ β → B~ a b α β
  ⟦A⟧ (η x , aw) = M.η x , refl
  ⟦A⟧ (ext a b , aw , bw) with ⟦B⟧ (a , aw) (b , bw)
  ... | α , α~ , β , β~ =
      M.ext _ α _ β , α , α~ , β , β~ , refl
  ⟦B⟧ (a , aw) (inj a , refl) with ⟦A⟧ (a , aw)
  ... | α , α~ = α , α~ , M.inj _ α , α~ , refl

  -- eliminator
  Aᴱ : ∀ a → M.A a
  Aᴱ a = ⟦A⟧ a .₁

  Bᴱ : ∀ a b → M.B a (Aᴱ a) b
  Bᴱ a b =
    tr (λ x → M.B a x b)
       (A~≡ a (⟦B⟧ a b .₁) (⟦A⟧ a .₁) (⟦B⟧ a b .₂ .₁) (⟦A⟧ a .₂))
       (⟦B⟧ a b .₂ .₂ .₁)

  ηᴱ : ∀ x → Aᴱ (Syn.η x) ≡ M.η x
  ηᴱ _ = refl

  extᴱ : ∀ a b → Aᴱ (Syn.ext a b) ≡ M.ext a (Aᴱ a) b (Bᴱ a b)
  extᴱ (a , aw) (b , bw) with ⟦A⟧ (a , aw) | ⟦B⟧ (a , aw) (b , bw)
  ... | α , α~ | α' , α~' , β , β~ with A~≡ (a , aw) α' α α~' α~
  ... | refl = refl

  injᴱ : ∀ a → Bᴱ a (Syn.inj a) ≡ M.inj a (Aᴱ a)
  injᴱ (a , aw) with ⟦A⟧ (a , aw)
  ... | α , α~ rewrite A~refl (a , aw) α α~ = refl

  Elim : Algˢ Syn M
  Elim = algˢ Aᴱ Bᴱ ηᴱ extᴱ injᴱ
