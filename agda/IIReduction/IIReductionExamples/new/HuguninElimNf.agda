{-# OPTIONS --without-K --safe #-}

{- Constructing Hugunin's running example without UIP, using
   an extremely simple alternative intrinsic representation
-}

open import Relation.Binary.PropositionalEquality
open import Data.Unit
open import Level

module _ {X : Set} where

--------------------------------------------------------------------------------

record Alg : Set₁ where
  constructor alg
  field
    A   : Set
    B   : A → Set
    η   : X → A
    ext : ∀ a → B a → A
    inj : ∀ a → B a

-- displayed algebra
record Algᴰ {i}{j} (M : Alg) : Set (suc (i ⊔ j)) where
  constructor algᴰ
  private module M = Alg M
  field
    A   : M.A → Set i
    B   : ∀ {a} → A a → M.B a → Set j
    η   : ∀ x → A (M.η x)
    ext : ∀ {a} (aᴰ : A a) (b : M.B a) → B aᴰ b → A (M.ext a b)
    inj : ∀ {a} (aᴰ : A a) → B aᴰ (M.inj a)

-- displayed algebra section
record Algˢ {i}{j} (N : Alg)(M : Algᴰ {i}{j} N) : Set (i ⊔ j) where
  constructor algˢ
  private module N = Alg N
  private module M = Algᴰ M
  field
    A   : ∀ a → M.A a
    B   : (a : N.A)(b : N.B a) → M.B (A a) b
    η   : ∀ {x} → A (N.η x) ≡ M.η x
    ext : ∀ {a}{b : N.B a} → A (N.ext a b) ≡ M.ext (A a) b (B a b)
    inj : ∀ {a} → B a (N.inj a) ≡ M.inj (A a)

-- Syntax
--------------------------------------------------------------------------------

data A : Set where
  η   : X → A
  ext : A → A

Syn : Alg
Syn = alg A (λ _ → ⊤) η (λ a _ → ext a) (λ _ → tt)

module Syn = Alg Syn

-- Eliminator
--------------------------------------------------------------------------------

module _ {i}{j}(M : Algᴰ {i}{j} Syn) where
  private module M = Algᴰ M

  ⟦A⟧ : ∀ a → M.A a
  ⟦A⟧ (η x)   = M.η x
  ⟦A⟧ (ext a) = M.ext (⟦A⟧ a) tt (M.inj (⟦A⟧ a))

  Elim : Algˢ Syn M
  Elim = algˢ ⟦A⟧ (λ a _ → M.inj (⟦A⟧ a)) refl refl refl
