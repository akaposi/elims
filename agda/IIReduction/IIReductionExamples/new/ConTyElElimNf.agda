{-# OPTIONS --without-K --safe #-}

{-
Constructing ConTyEl example without UIP, by using an intrinsic
indexed inductive presentation
-}

open import Level
open import Relation.Binary.PropositionalEquality
  renaming (cong to ap; subst to tr)
open import Data.Product
  renaming (proj₁ to ₁; proj₂ to ₂)
open import Data.Unit

infix 6 _⁻¹
infixr 5 _◾_
_⁻¹ : ∀ {i}{A : Set i}{x y : A} → x ≡ y → y ≡ x
refl ⁻¹ = refl

_◾_ : ∀ {i}{A : Set i}{x y z : A} → x ≡ y → y ≡ z → x ≡ z
refl ◾ q = q

--------------------------------------------------------------------------------

-- Algebras
record Alg : Set₁ where
  constructor alg
  infixl 5 _▶_
  field
    Con  : Set
    Ty   : Con → Set
    ∙    : Con
    _▶_  : (Γ : Con) → Ty Γ → Con
    U    : (Γ : Con) → Ty Γ
    Π    : (Γ : Con)(A : Ty Γ)(B : Ty (Γ ▶ A)) → Ty Γ
    El   : ∀ Γ → Ty (Γ ▶ U Γ)

-- Displayed algebras
record Algᴰ {i}{j} (γ : Alg) : Set (suc (i ⊔ j)) where
  private module γ = Alg γ
  infixl 5 _▶_
  field
    Con : γ.Con → Set i
    Ty  : ∀ {Γ}(Γᴰ : Con Γ) → γ.Ty Γ → Set j
    ∙   : Con γ.∙
    _▶_ : ∀ {Γ}(Γᴰ : Con Γ) {A} (Aᴰ : Ty Γᴰ A) → Con (Γ γ.▶ A)
    U   : ∀ {Γ}(Γᴰ : Con Γ) → Ty Γᴰ (γ.U Γ)
    Π   : ∀ {Γ}(Γᴰ : Con Γ) {A} (Aᴰ : Ty Γᴰ A) {B} (Bᴰ : Ty (Γᴰ ▶ Aᴰ) B)
          → Ty Γᴰ (γ.Π Γ A B)
    El  : ∀ {Γ}(Γᴰ : Con Γ) → Ty (Γᴰ ▶ U Γᴰ) (γ.El Γ)

-- Displayed algebra sections
record Algˢ {i}{j} (γ : Alg)(δ : Algᴰ {i}{j} γ) : Set (i ⊔ j) where
  constructor algˢ
  private module γ = Alg γ
  private module δ = Algᴰ δ
  field
    Con : ∀ Γ → δ.Con Γ
    Ty  : ∀ {Γ}(A : γ.Ty Γ) → δ.Ty (Con Γ) A
    ∙   : Con γ.∙ ≡ δ.∙
    ▶   : ∀ {Γ A} → Con (Γ γ.▶ A) ≡ Con Γ δ.▶ Ty A
    U   : ∀ {Γ} → Ty (γ.U Γ) ≡ δ.U (Con Γ)
    Π   : ∀ {Γ A B} → Ty (γ.Π Γ A B) ≡
                      δ.Π (Con Γ) (Ty A) (tr (λ x → δ.Ty x B) ▶ (Ty B))
    El  : ∀ {Γ} → tr (λ x → δ.Ty x _) (▶ ◾ ap (Con Γ δ.▶_) U) (Ty (γ.El Γ))
                ≡ δ.El (Con Γ)

-- Syntax
--------------------------------------------------------------------------------

open import Data.Bool

data Con : Bool → Set
data Ty  : Bool → Bool → Set

infixl 5 _▶_
data Con where
  ∙   : Con false
  _▶_ : ∀ {i j} → Con i → Ty i j → Con j

data Ty where
  U  : ∀ {i} → Ty i true
  Π  : ∀ {i j k} → Ty i j → Ty j k → Ty i false
  El : Ty true false

Syn : Alg
Syn = alg
  (∃ Con)
  (λ {(i , _) → ∃ (Ty i)})
  (_ , ∙)
  (λ {(i , Γ)(j , A) → _ , Γ ▶ A})
  (λ {(i , Γ) → _ , U})
  (λ {(i , Γ)(j , A)(k , B) → _ , Π A B})
  (λ {(i , Γ) → _ , El})

module Syn = Alg Syn

-- Eliminator
--------------------------------------------------------------------------------

module _ {ℓ}{ℓ'}(M : Algᴰ {ℓ}{ℓ'} Syn) where
  private module M = Algᴰ M

  Con~ : ∀ {i} (Γ : Con i) → M.Con (i , Γ) → Set ℓ
  Con~ {false} Γ       γ* = Lift ℓ ⊤
  Con~ {true}  (Γ ▶ U) γ* = ∃ λ γ → γ M.▶ M.U γ ≡ γ*

  ⟦Con⟧ : ∀ {i} Γ → ∃ λ (γ : M.Con (i , Γ)) → Con~ _ γ
  ⟦Ty⟧  : ∀ {i j} (A : Ty i j) {Γ} {γ : M.Con (i , Γ)} → Con~ {i} Γ γ → M.Ty γ (j , A)
  ⟦Con⟧ {false} ∙       = M.∙ , lift tt
  ⟦Con⟧ {false} (Γ ▶ A) = ⟦Con⟧ Γ .₁ M.▶ ⟦Ty⟧ A (⟦Con⟧ Γ .₂) , _
  ⟦Con⟧ {true}  (Γ ▶ U) = ⟦Con⟧ Γ .₁ M.▶ M.U _ , ⟦Con⟧ Γ .₁ , refl
  ⟦Ty⟧ U                   p          = M.U _
  ⟦Ty⟧ (Π {j = false} A B) p          = M.Π _ (⟦Ty⟧ A p) (⟦Ty⟧ B _)
  ⟦Ty⟧ (Π {j = true} U B)  p          = M.Π _ (M.U _) (⟦Ty⟧ B (_ , refl))
  ⟦Ty⟧ El {Γ ▶ U}          (_ , refl) = M.El _

  Conᴱ : ∀ Γ → M.Con Γ
  Conᴱ (i , Γ) = ⟦Con⟧ Γ .₁

  Tyᴱ : ∀ {Γ} A → M.Ty (Conᴱ Γ) A
  Tyᴱ {i , Γ} (j , A) = ⟦Ty⟧ A (⟦Con⟧ Γ .₂)

  ∙ᴱ : Conᴱ Syn.∙ ≡ M.∙
  ∙ᴱ = refl

  ▶ᴱ : ∀ {Γ}{A} →
      Conᴱ (Γ Syn.▶ A) ≡ Conᴱ Γ M.▶ Tyᴱ A
  ▶ᴱ {i , Γ} {false , A} = refl
  ▶ᴱ {i , Γ} {true , U}  = refl

  Uᴱ : ∀ {Γ} → Tyᴱ (Syn.U Γ) ≡ M.U (Conᴱ Γ)
  Uᴱ = refl

  Πᴱ : ∀ {Γ A B} → Tyᴱ (Syn.Π Γ A B) ≡
                   M.Π (Conᴱ Γ) (Tyᴱ A) (tr (λ x → M.Ty x B) ▶ᴱ (Tyᴱ B))
  Πᴱ {i , Γ} {false , A} {k , B} = refl
  Πᴱ {i , Γ} {true  , U} {k , B} = refl

  Elᴱ : ∀ {Γ} → Tyᴱ (Syn.El Γ) ≡ M.El (Conᴱ Γ)
  Elᴱ = refl

  Elim : Algˢ Syn M
  Elim = algˢ Conᴱ Tyᴱ ∙ᴱ ▶ᴱ Uᴱ Πᴱ Elᴱ
