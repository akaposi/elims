{-# OPTIONS --without-K #-}

-- Translating from Hugunin to internal CwF of algebras
--------------------------------------------------------------------------------

open import Relation.Binary.PropositionalEquality
  renaming (cong to ap; subst to tr)
open import Data.Product
  renaming (proj₁ to ₁; proj₂ to ₂)
open import Data.Unit
open import Level renaming (zero to lzero; suc to lsuc)

module _ {X : Set} where

infix 6 _⁻¹
infixr 5 _◾_
_⁻¹ : ∀ {i}{A : Set i}{x y : A} → x ≡ y → y ≡ x
refl ⁻¹ = refl

_◾_ : ∀ {i}{A : Set i}{x y z : A} → x ≡ y → y ≡ z → x ≡ z
refl ◾ q = q

--------------------------------------------------------------------------------

record Con : Set₁ where
  constructor con
  field
    A   : Set
    B   : A → Set
    η   : X → A
    ext : ∀ a → B a → A
    inj : ∀ a → B a

record Ty (Γ : Con) : Set₁ where
  constructor ty
  private module Γ = Con Γ
  field
    A   : Γ.A → Set
    B   : ∀ a → A a → Γ.B a → Set
    η   : ∀ x → A (Γ.η x)
    ext : ∀ a (aᴰ : A a) (b : Γ.B a) → B a aᴰ b → A (Γ.ext a b)
    inj : ∀ a (aᴰ : A a) → B a aᴰ (Γ.inj a)

record Tm (Γ : Con)(C : Ty Γ) : Set where
  constructor tm
  private module Γ = Con Γ
  private module C = Ty C
  field
    A   : ∀ a → C.A a
    B   : (a : Γ.A)(b : Γ.B a) → C.B a (A a) b
    η   : ∀ x → A (Γ.η x) ≡ C.η x
    ext : ∀ a (b : Γ.B a) → A (Γ.ext a b) ≡ C.ext a (A a) b (B a b)
    inj : ∀ a → B a (Γ.inj a) ≡ C.inj a (A a)

-- total algebra
infixl 5 _▶_
_▶_ : (Γ : Con)(A : Ty Γ) → Con
Γ ▶ A =
  con (Σ Γ.A A.A)
      (λ {(a , a*) → Σ (Γ.B a) (A.B a a*)})
      (λ x → Γ.η x , A.η x)
      (λ {(a , a*)(b , b*) → (Γ.ext a b) , (A.ext a a* b b*)})
      (λ {(a , a*) → (Γ.inj a) , (A.inj a a*)})
  where module Γ = Con Γ; module A = Ty A

-- terminal algebra
∙ : Con
∙ = con ⊤ (λ _ → ⊤) _ _ _

-- constant displayed algebra
K : (Γ : Con) → {Δ : Con} → Ty Δ
K Γ {Δ} = ty
  (λ _ → Γ.A)
  (λ _ a _ → Γ.B a)
  Γ.η
  (λ _ a _ b → Γ.ext a b )
  (λ _ a → Γ.inj a)
  where module Γ = Con Γ; module Δ = Con Δ

-- Σ-type
Sg : ∀ {Γ}(A : Ty Γ) → Ty (Γ ▶ A) → Ty Γ
Sg {Γ} A B = ty
  (λ Γa → Σ (A.A Γa) λ Aa → B.A (Γa , Aa))
  (λ {Γa (Aa , Ba) Γb → Σ (A.B Γa Aa Γb) λ Ab → B.B (Γa , Aa) Ba (Γb , Ab)})
  (λ x → A.η x , B.η x)
  (λ {Γa (Aa , Ba) Γb (Ab , Bb) → A.ext Γa Aa Γb Ab ,
                                  B.ext (Γa , Aa) Ba (Γb , Ab) Bb})
  (λ {a (Aa , Ba) → (A.inj a Aa) , (B.inj (a , Aa) Ba)})
  where module Γ = Con Γ; module A = Ty A; module B = Ty B

--------------------------------------------------------------------------------

module S where
  data A : Set
  data B : Set
  data A where
    η   : X → A
    ext : A → B → A
  data B where
    inj : A → B

PreSyn : Con
PreSyn = con S.A (λ _ → S.B) S.η S.ext S.inj

-- successor displayed algebra
WfSuc : ∀ {A} → Ty (PreSyn ▶ A)
WfSuc {A} = ty
  (λ {(a , _) → Aˢ a})
  (λ {(a , aw) aw' (b , bw) → Bˢ a aw b})
  (λ _ → tt)
  (λ {(a , aw) aw' (b , bw) bw' → aw , bw})
  (λ {(a , aw) aw' → refl})
  module Suc where
    module A = Ty A
    Aˢ : S.A → Set
    Bˢ : (a : S.A) → A.A a → S.B → Set
    Aˢ (S.η x)          = ⊤
    Aˢ (S.ext a b)      = Σ (A.A a) λ aw → A.B a aw b
    Bˢ a* aw* (S.inj a) = a ≡ a*

    -- Bˢ : ∀ a → Σ S.B (A.B (₁ a) (₂ a)) → Set
    -- Aˢ (S.η x)     = ⊤
    -- Aˢ (S.ext a b) = ∃₂ λ Aa Ab
    -- Bˢ (a* , Aa*) (S.inj a , Ab*) = Σ (a ≡ a*) λ {refl → A.inj a* Aa* ≡ Ab*}

open import Data.Nat

WfN : ℕ → Ty PreSyn
WfN zero    = K ∙
WfN (suc n) = Sg (WfN n) WfSuc

-- -- WfA : S.A → Set
-- -- WfB : ∀ a → WfA a → S.B → Set
-- -- WfA (S.η x)         = ⊤
-- -- WfA (S.ext a b)     = Σ (WfA a) λ aw → WfB _ aw b
-- -- WfB a aw (S.inj a') = a ≡ a'

-- Wf : Ty PreSyn
-- Wf = ty
--   (λ a → (n : ℕ) → WfN n .Ty.A a)
--   (λ a aw b → (n : ℕ) → WfN n .Ty.B a (aw n) b)
--   (λ x n → WfN n .Ty.η x)
--   (λ a aw b bw n → WfN n .Ty.ext a (aw n) b (bw n))
--   (λ a aw n → WfN n .Ty.inj a (aw n))

-- Syn : Con
-- Syn = PreSyn ▶ Wf

-- module Syn = Con Syn

-- iso : ∀ {i} → Set i → Set i → Set i
-- iso {i} A B = ∃₂ λ (f : B → A) (g : A → B) → (∀ x → f (g x) ≡ x) × (∀ x → g (f x) ≡ x)

-- η≃ : (n : ℕ)(x : X) → {!Wf .Ty.A!}
-- n≃ = {!!}

-- module _ (M : Ty Syn) where
--   private module M = Ty M

--   ⟦A⟧ : ∀ a → M.A a
--   ⟦B⟧ : ∀ {a} b → M.B a (⟦A⟧ a) b
--   ⟦A⟧ (S.η x     , aw) = {!M.η x!}
--   ⟦A⟧ (S.ext a b , aw) = {!!}
--   ⟦B⟧ {a} (b , bw) = {!!}
