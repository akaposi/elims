{-# OPTIONS --without-K #-}

{-
  Codes for mutual non-indexed inductive types
-}

module Inductive.Syntax where

open import Lib

infixl 4 _,_
infixr 5 _⇒_

data Sorts : Set where
  ∙   : Sorts
  _,U : Sorts → Sorts

data Varₛ : Sorts → Set where
  vz : ∀ {n} → Varₛ (n ,U)
  vs : ∀ {n} → Varₛ n → Varₛ (n ,U)

data Ty (n : Sorts) : Set where
  sort : Varₛ n → Ty n
  _⇒_  : Varₛ n → Ty n → Ty n

data Con : Sorts → Set where
  ∙    : Con ∙
  _,_  : ∀ {n} → Con n → Ty n → Con n
  _,U  : ∀ {n} → Con n → Con (n ,U)

private
  -- Con:U,Ty:U,•:Con,▶:Con→ Ty→U,Π:Con→Ty→Ty→Ty
  ex : Con (∙ ,U ,U)
  ex = ∙ ,U ,U , sort (vs vz) , vs vz ⇒ vz ⇒ sort (vs vz) , vs vz ⇒ vz ⇒ vz ⇒ sort vz

-- Substitution calculus
--------------------------------------------------------------------------------

data OPEₛ : Sorts → Sorts → Set where
  ∙    : OPEₛ ∙ ∙
  drop : ∀ {n m} → OPEₛ n m → OPEₛ (n ,U) m
  keep : ∀ {n m} → OPEₛ n m → OPEₛ (n ,U) (m ,U)

idₑ : ∀ {Γ} → OPEₛ Γ Γ
idₑ {∙}    = ∙
idₑ {Γ ,U} = keep idₑ

wk : ∀ {Γ} → OPEₛ (Γ ,U) Γ
wk = drop idₑ

Varₛₑ : ∀ {Γ Δ} → OPEₛ Γ Δ → Varₛ Δ → Varₛ Γ
Varₛₑ ∙        x      = x
Varₛₑ (drop σ) x      = vs (Varₛₑ σ x)
Varₛₑ (keep σ) vz     = vz
Varₛₑ (keep σ) (vs x) = vs (Varₛₑ σ x)

Tyₑ : ∀ {Γ Δ} → OPEₛ Γ Δ → Ty Δ → Ty Γ
Tyₑ σ (sort t) = sort (Varₛₑ σ t)
Tyₑ σ (t ⇒ A)  = Varₛₑ σ t ⇒ Tyₑ σ A

infixr 6 _∘ₑ_
_∘ₑ_ : ∀ {Γ Δ Σ} → OPEₛ Δ Σ → OPEₛ Γ Δ → OPEₛ Γ Σ
σ      ∘ₑ ∙      = σ
σ      ∘ₑ drop δ = drop (σ ∘ₑ δ)
drop σ ∘ₑ keep δ = drop (σ ∘ₑ δ)
keep σ ∘ₑ keep δ = keep (σ ∘ₑ δ)

idlₑ : ∀ {Γ Δ}(σ : OPEₛ Γ Δ) → idₑ ∘ₑ σ ≡ σ
idlₑ ∙        = refl
idlₑ (drop σ) = ap drop (idlₑ σ)
idlₑ (keep σ) = ap keep (idlₑ σ)

idrₑ : ∀ {Γ Δ}(σ : OPEₛ Γ Δ) → σ ∘ₑ idₑ ≡ σ
idrₑ ∙        = refl
idrₑ (drop σ) = ap drop (idrₑ σ)
idrₑ (keep σ) = ap keep (idrₑ σ)

assₑ :
  ∀ {Γ Δ Σ Ξ}(σ : OPEₛ Σ Ξ)(δ : OPEₛ Δ Σ)(ν : OPEₛ Γ Δ)
  → (σ ∘ₑ δ) ∘ₑ ν ≡ σ ∘ₑ (δ ∘ₑ ν)
assₑ σ        δ        ∙        = refl
assₑ σ        δ        (drop ν) = ap drop (assₑ σ δ ν)
assₑ σ        (drop δ) (keep ν) = ap drop (assₑ σ δ ν)
assₑ (drop σ) (keep δ) (keep ν) = ap drop (assₑ σ δ ν)
assₑ (keep σ) (keep δ) (keep ν) = ap keep (assₑ σ δ ν)

Varₛₑ-id : ∀ {Γ}(x : Varₛ Γ) → Varₛₑ idₑ x ≡ x
Varₛₑ-id vz     = refl
Varₛₑ-id (vs x) = ap vs (Varₛₑ-id x)

Varₛₑ-∘ :
  ∀ {Γ Δ Σ}(σ : OPEₛ Δ Σ)(δ : OPEₛ Γ Δ)(x : Varₛ Σ) → Varₛₑ (σ ∘ₑ δ) x ≡ Varₛₑ δ (Varₛₑ σ x)
Varₛₑ-∘ ∙        ∙        x      = refl
Varₛₑ-∘ σ        (drop δ) x      = ap vs (Varₛₑ-∘ σ δ x)
Varₛₑ-∘ (drop σ) (keep δ) x      = ap vs (Varₛₑ-∘ σ δ x)
Varₛₑ-∘ (keep σ) (keep δ) vz     = refl
Varₛₑ-∘ (keep σ) (keep δ) (vs x) = ap vs (Varₛₑ-∘ σ δ x)

Tyₑ-id : ∀ {Γ}(A : Ty Γ) → Tyₑ idₑ A ≡ A
Tyₑ-id (sort t) = ap sort (Varₛₑ-id t)
Tyₑ-id (t ⇒ A)  = ap _⇒_ (Varₛₑ-id t) ⊗ Tyₑ-id A

Tyₑ-∘ :
  ∀ {Γ Δ Σ}(σ : OPEₛ Δ Σ)(δ : OPEₛ Γ Δ)(A : Ty Σ) → Tyₑ (σ ∘ₑ δ) A ≡ Tyₑ δ (Tyₑ σ A)
Tyₑ-∘ σ δ (sort t) = ap sort (Varₛₑ-∘ σ δ t)
Tyₑ-∘ σ δ (t ⇒ A)  = ap _⇒_ (Varₛₑ-∘ σ δ t) ⊗ Tyₑ-∘ σ δ A

infixr 6 _ₑ∘ₛ_ _ₛ∘ₑ_ _∘ₛ_

data Subₛ (Γ : Sorts) : Sorts → Set where
  ε   : Subₛ Γ ∙
  _,_ : ∀ {Δ} → Subₛ Γ Δ → Varₛ Γ → Subₛ Γ (Δ ,U)

Varₛₛ : ∀ {Γ Δ} → Subₛ Γ Δ → Varₛ Δ → Varₛ Γ
Varₛₛ (σ , t) vz     = t
Varₛₛ (σ , _) (vs x) = Varₛₛ σ x

Tyₛ : ∀ {Γ Δ} → Subₛ Γ Δ → Ty Δ → Ty Γ
Tyₛ σ (sort t) = sort (Varₛₛ σ t)
Tyₛ σ (t ⇒ A)  = Varₛₛ σ t ⇒ Tyₛ σ A

_ₛ∘ₑ_ : ∀ {Γ Δ Σ} → Subₛ Δ Σ → OPEₛ Γ Δ → Subₛ Γ Σ
ε       ₛ∘ₑ δ = ε
(σ , t) ₛ∘ₑ δ = (σ ₛ∘ₑ δ) , Varₛₑ δ t

_ₑ∘ₛ_ : ∀ {Γ Δ Σ} → OPEₛ Δ Σ → Subₛ Γ Δ → Subₛ Γ Σ
∙      ₑ∘ₛ δ       = δ
drop σ ₑ∘ₛ (δ , t) = σ ₑ∘ₛ δ
keep σ ₑ∘ₛ (δ , t) = (σ ₑ∘ₛ δ) , t

dropₛₛ : ∀ {Γ Δ} → Subₛ Γ Δ → Subₛ (Γ ,U) Δ
dropₛₛ σ = σ ₛ∘ₑ wk

keepₛₛ : ∀ {Γ Δ} → Subₛ Γ Δ → Subₛ (Γ ,U) (Δ ,U)
keepₛₛ σ = dropₛₛ σ , vz

idₛₛ : ∀ {Γ} → Subₛ Γ Γ
idₛₛ {∙}    = ε
idₛₛ {Γ ,U} = keepₛₛ idₛₛ

⌜_⌝ᵒᵖᵉ : ∀ {Γ Δ} → OPEₛ Γ Δ → Subₛ Γ Δ
⌜ ∙      ⌝ᵒᵖᵉ = ε
⌜ drop σ ⌝ᵒᵖᵉ = dropₛₛ ⌜ σ ⌝ᵒᵖᵉ
⌜ keep σ ⌝ᵒᵖᵉ = keepₛₛ ⌜ σ ⌝ᵒᵖᵉ

_∘ₛ_ : ∀ {Γ Δ Σ} → Subₛ Δ Σ → Subₛ Γ Δ → Subₛ Γ Σ
ε       ∘ₛ δ = ε
(σ , t) ∘ₛ δ = (σ ∘ₛ δ) , Varₛₛ δ t

assₛₑₑ :
  ∀ {Γ Δ Σ Ξ}(σ : Subₛ Σ Ξ)(δ : OPEₛ Δ Σ)(ν : OPEₛ Γ Δ)
  → (σ ₛ∘ₑ δ) ₛ∘ₑ ν ≡ σ ₛ∘ₑ (δ ∘ₑ ν)
assₛₑₑ ε       δ ν = refl
assₛₑₑ (σ , t) δ ν = ap _,_  (assₛₑₑ σ δ ν) ⊗ (Varₛₑ-∘ δ ν t ⁻¹)

assₑₛₑ :
  ∀ {Γ Δ Σ Ξ}(σ : OPEₛ Σ Ξ)(δ : Subₛ Δ Σ)(ν : OPEₛ Γ Δ)
  → (σ ₑ∘ₛ δ) ₛ∘ₑ ν ≡ σ ₑ∘ₛ (δ ₛ∘ₑ ν)
assₑₛₑ ∙        δ       ν = refl
assₑₛₑ (drop σ) (δ , t) ν = assₑₛₑ σ δ ν
assₑₛₑ (keep σ) (δ , t) ν = ap (_, Varₛₑ ν t) (assₑₛₑ σ δ ν)

idlₑₛ : ∀ {Γ Δ}(σ : Subₛ Γ Δ) → idₑ ₑ∘ₛ σ ≡ σ
idlₑₛ ε       = refl
idlₑₛ (σ , t) = ap (_, t) (idlₑₛ σ)

idlₛₑ : ∀ {Γ Δ}(σ : OPEₛ Γ Δ) → idₛₛ ₛ∘ₑ σ ≡ ⌜ σ ⌝ᵒᵖᵉ
idlₛₑ ∙        = refl
idlₛₑ (drop σ) =
      ap (λ x → idₛₛ ₛ∘ₑ drop x) (idrₑ σ ⁻¹)
    ◾ assₛₑₑ idₛₛ σ wk ⁻¹
    ◾ ap dropₛₛ (idlₛₑ σ)
idlₛₑ (keep σ) =
  ap (_, vz) (
      (assₛₑₑ idₛₛ wk (keep σ)
    ◾ ap (λ x → idₛₛ ₛ∘ₑ drop x) (idlₑ σ ◾ idrₑ σ ⁻¹)
    ◾ assₛₑₑ idₛₛ σ wk ⁻¹
    ◾ ap (_ₛ∘ₑ wk) (idlₛₑ σ)))

idrₑₛ : ∀ {Γ Δ}(σ : OPEₛ Γ Δ) → σ ₑ∘ₛ idₛₛ ≡ ⌜ σ ⌝ᵒᵖᵉ
idrₑₛ ∙        = refl
idrₑₛ (drop σ) = assₑₛₑ σ idₛₛ wk ⁻¹ ◾ ap dropₛₛ (idrₑₛ σ)
idrₑₛ (keep σ) = ap (_, vz) ((assₑₛₑ σ idₛₛ wk ⁻¹ ◾ ap (_ₛ∘ₑ wk) (idrₑₛ σ)))

Varₛ-ₑ∘ₛ : ∀ {Γ Δ Σ}(σ : OPEₛ Δ Σ)(δ : Subₛ Γ Δ)(v : Varₛ Σ) → Varₛₛ (σ ₑ∘ₛ δ) v ≡ Varₛₛ δ (Varₛₑ σ v)
Varₛ-ₑ∘ₛ ∙        δ       v      = refl
Varₛ-ₑ∘ₛ (drop σ) (δ , t) v      = Varₛ-ₑ∘ₛ σ δ v
Varₛ-ₑ∘ₛ (keep σ) (δ , t) vz     = refl
Varₛ-ₑ∘ₛ (keep σ) (δ , t) (vs v) = Varₛ-ₑ∘ₛ σ δ v

Varₛ-ₛ∘ₑ : ∀ {Γ Δ Σ}(σ : Subₛ Δ Σ)(δ : OPEₛ Γ Δ)(v : Varₛ Σ) → Varₛₛ (σ ₛ∘ₑ δ) v ≡ Varₛₑ δ (Varₛₛ σ v)
Varₛ-ₛ∘ₑ (σ , _) δ vz     = refl
Varₛ-ₛ∘ₑ (σ , _) δ (vs v) = Varₛ-ₛ∘ₑ σ δ v

assₛₑₛ :
  ∀ {Γ Δ Σ Ξ}(σ : Subₛ Σ Ξ)(δ : OPEₛ Δ Σ)(ν : Subₛ Γ Δ)
  → (σ ₛ∘ₑ δ) ∘ₛ ν ≡ σ ∘ₛ (δ ₑ∘ₛ ν)
assₛₑₛ ε       δ ν = refl
assₛₑₛ (σ , t) δ ν = ap _,_ (assₛₑₛ σ δ ν) ⊗ (Varₛ-ₑ∘ₛ δ ν t ⁻¹)

assₛₛₑ :
  ∀ {Γ Δ Σ Ξ}(σ : Subₛ Σ Ξ)(δ : Subₛ Δ Σ)(ν : OPEₛ Γ Δ)
  → (σ ∘ₛ δ) ₛ∘ₑ ν ≡ σ ∘ₛ (δ ₛ∘ₑ ν)
assₛₛₑ ε       δ ν = refl
assₛₛₑ (σ , t) δ ν = ap _,_ (assₛₛₑ σ δ ν) ⊗ (Varₛ-ₛ∘ₑ δ ν t ⁻¹)

Varₛₛ-∘ : ∀ {Γ Δ Σ}(σ : Subₛ Δ Σ)(δ : Subₛ Γ Δ)(v : Varₛ Σ) → Varₛₛ (σ ∘ₛ δ) v ≡ Varₛₛ δ (Varₛₛ σ v)
Varₛₛ-∘ (σ , _) δ vz     = refl
Varₛₛ-∘ (σ , _) δ (vs v) = Varₛₛ-∘ σ δ v

Varₛₛ-id : ∀ {Γ}(v : Varₛ Γ) → Varₛₛ idₛₛ v ≡ v
Varₛₛ-id vz     = refl
Varₛₛ-id (vs v) = Varₛ-ₛ∘ₑ idₛₛ wk v ◾ ap (Varₛₑ wk) (Varₛₛ-id v) ◾ ap vs (Varₛₑ-id v)

idrₛ : ∀ {Γ Δ}(σ : Subₛ Γ Δ) → σ ∘ₛ idₛₛ ≡ σ
idrₛ ε       = refl
idrₛ (σ , t) = ap _,_ (idrₛ σ) ⊗ Varₛₛ-id t

idlₛ : ∀ {Γ Δ}(σ : Subₛ Γ Δ) → idₛₛ ∘ₛ σ ≡ σ
idlₛ ε       = refl
idlₛ (σ , t) = ap (_, t) (assₛₑₛ idₛₛ wk (σ , t) ◾ idlₛ (idₑ ₑ∘ₛ σ) ◾ idlₑₛ σ)

assₛ :
  ∀ {Γ Δ Σ Ξ}(σ : Subₛ Σ Ξ)(δ : Subₛ Δ Σ)(ν : Subₛ Γ Δ)
  → (σ ∘ₛ δ) ∘ₛ ν ≡ σ ∘ₛ (δ ∘ₛ ν)
assₛ ε       δ ν = refl
assₛ (σ , t) δ ν = ap _,_ (assₛ σ δ ν) ⊗ (Varₛₛ-∘ δ ν t ⁻¹)

Tyₛ-id : ∀ {Γ} A → Tyₛ (idₛₛ {Γ}) A ≡ A
Tyₛ-id (sort x) = ap sort (Varₛₛ-id x)
Tyₛ-id (x ⇒ A)  = ap _⇒_ (Varₛₛ-id x) ⊗ Tyₛ-id A

Tyₛ-∘ : ∀ {Γ Δ Σ}(σ : Subₛ Δ Σ)(δ : Subₛ Γ Δ)(A : Ty Σ) → Tyₛ (σ ∘ₛ δ) A ≡ Tyₛ δ (Tyₛ σ A)
Tyₛ-∘ σ δ (sort x) = ap sort (Varₛₛ-∘ σ δ x)
Tyₛ-∘ σ δ (x ⇒ A)  = ap _⇒_ (Varₛₛ-∘ σ δ x) ⊗ Tyₛ-∘ σ δ A
