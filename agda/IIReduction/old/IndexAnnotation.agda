{-# OPTIONS --without-K #-}

{-
  A syntax for indexing annotations on top of inductive presyntax codes.
  It contains just enough information to recover erased indices.
-}

module old.IndexAnnotation where

open import Lib
open import Inductive.Syntax hiding (Con; Ty; Sorts)
import Inductive.Syntax as I using (Con; Ty; Sorts)

data Sort (n : I.Sorts) : Set where
  ⇒U  : Sort n
  _⇒_ : Varₛ n → Sort n → Sort n

data Sorts : I.Sorts → Set where
  ∙   : Sorts ∙
  _,_ : ∀{n} → Sorts n → Sort n → Sorts (n ,U)

Sortₑ : ∀ {n m} → OPE n m → Sort m → Sort n
Sortₑ σ ⇒U      = ⇒U
Sortₑ σ (A ⇒ B) = Varₛₑ σ A ⇒ Sortₑ σ B

wkₛ : ∀ {n} → Sort n → Sort (n ,U)
wkₛ = Sortₑ wk

lookup : ∀ {n} → Varₛ n → Sorts n → Sort n
lookup vz     (Γ , A) = wkₛ A
lookup (vs A) (Γ , _) = wkₛ (lookup A Γ)

-- A spine of indices
data Spine {n}(Γ : I.Con n) : Sort n → Sort n → Set where
  []  : ∀ {A}     → Spine Γ A A
  _∷_ : ∀ {A B C} → Tm Γ (sort A) → Spine Γ B C → Spine Γ (A ⇒ B) C

data Ty {n}(Γ : Sorts n)(Δ : I.Con n) : I.Ty n → Set where
  sort : ∀ {A : Varₛ n} → Spine Δ (lookup A Γ) ⇒U → Ty Γ Δ (sort A)
  _⇒_  : ∀ {A : Varₛ n}{B : I.Ty n}
         → Spine Δ (lookup A Γ) ⇒U
         → Ty Γ (Δ , sort A) B → Ty Γ Δ (A ⇒ B)

data Con : ∀ {n}(Γ : Sorts n) → I.Con n → Set where
  ∙    : Con ∙ ∙
  _,_  : ∀ {n Γ Δ A} → Con {n} Γ Δ → Ty Γ Δ A → Con Γ (Δ , A)
  _,ₛ_ : ∀ {n Γ Δ  } → Con {n} Γ Δ → (S : Sort n) → Con (Γ , S) (Δ ,ₛ)

infixr 5 _∷_
infixl 4 _,_ _,ₛ_
infixr 5 _⇒_


-- ConTy example
------------------------------------------------------------

exw : Con
  {∙
   ,U          -- Con : U
   ,U}         -- Ty  : U
  (∙
   , ⇒U        -- Conw : Con → U
   , vz ⇒ ⇒U)  -- Tyw  : Con → Ty → U
  (∙
   ,ₛ
   ,ₛ

   , sort (vs vz)               -- ∙ : Con
   , vs vz ⇒ vz ⇒ sort (vs vz)  -- ▶ : Con → Ty → Con
   , vs vz ⇒ vz ⇒ vz ⇒ sort vz) -- Π : Con → Ty → Ty → Ty
exw =
    ∙
  ,ₛ _
  ,ₛ _

  -- ∙w : Conw ∙
  , sort []

  -- ▶w : ∀ Γ → Conw Γ → ∀ A → Tyw Γ A → Conw (Γ ▶ A)
  , [] ⇒ (ne (var vz) ∷ []) ⇒ sort []

  -- Πw : ∀ Γ → Conw Γ → ∀ A → Tyw Γ A → ∀ B → Tyw (Γ ▶ A) B → Tyw Γ (Π Γ A B)
  , [] ⇒ (ne (var vz) ∷ []) ⇒ (ne (app (app (var (vs (vs vz))) (ne (var (vs vz)))) (ne (var vz))) ∷ []) ⇒ sort (ne (var (vs (vs vz))) ∷ [])


--------------------------------------------------------------------------------
-- TODO: Substitution calculus
