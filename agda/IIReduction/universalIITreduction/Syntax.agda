
module Syntax where

open import StrictLib

data Con : ℕ → Set
data Ty (n : ℕ) : Set
data Tm (n : ℕ) : Set

infixl 3 _▶_
data Con where
  ∙   : Con 0
  _▶_ : ∀ {n} → Con n → Ty n → Con (suc n)

data Ty n where
  U  : Ty n
  El : Tm n → Ty n
  Π  : Tm n → Ty (suc n) → Ty n

data Tm n where
  var : Fin n → Tm n
  app : Tm n → Tm n → Tm n
