
module InitialAlg where

open import StrictLib hiding (id; _∘_)
import Syntax as S
open import Embedding hiding (∙)
import Embedding as E using (∙)
open import Substitution hiding (Sub; ∙)
import Substitution as S using (Sub; ∙)

open import Typing  hiding (∙; _▶_; U; El; Π; app)
import Typing as T using (∙; _▶_; U; El; Π; app)
open import Sanity hiding (∙)
import Sanity as San using (∙)


infixl 7 _[_]T
infixl 5 _,s_
infix  6 _∘_
infixl 8 _[_]t

Con : Set
Con = ∃₂ λ n (Γ : S.Con n) → Γ ⊢

Ty  : Con → Set
Ty (n , Γ , Γw) = ∃ (Γ ⊢_)

Tm  : (Γ : Con) → Ty Γ → Set
Tm (n , Γ , Γw) (A , Aw) = ∃ (Γ ⊢_∈ A)

Sub : Con → Con → Set
Sub (n , Γ , Γw ) (m , Δ , Δw) = ∃ (Sub⊢ Γ Δ)

Ty≡ : ∀ {n}{Γ : S.Con n}{A A' : ∃ (Γ ⊢_)} → ₁ A ≡ ₁ A' → A ≡ A'
Ty≡ {n} {Γ} {A , Aw} {.A , Aw'} refl = (A Σ.,_) & Γ⊢Aprop Aw Aw'

Tm≡ : ∀ {n}{Γ : S.Con n}{A : S.Ty n}(t t' : ∃ (Γ ⊢_∈ A)) → ₁ t ≡ ₁ t' → t ≡ t'
Tm≡ {n} {Γ} {A} (t , tw) (t' , tw') refl = (t Σ.,_) & Γ⊢t∈Aprop _ _

Sub≡ : ∀ {n m Γ Δ}{σ δ : ∃ (Sub⊢ {n} Γ {m} Δ)} → ₁ σ ≡ ₁ δ → σ ≡ δ
Sub≡ {σ = σ} {δ} refl = (₁ δ Σ.,_) & Sub⊢prop _ _

∙  : Con
∙ = 0 , S.∙ , _⊢.∙

_▶_ : (Γ : Con) → Ty Γ → Con
(n , Γ , Γw) ▶ (A , Aw) = suc n , (Γ S.▶ A) , (Γw T.▶ Aw)

_[_]T : ∀{Γ Δ} → Ty Δ → Sub Γ Δ → Ty Γ
(A , Aw) [ (σ , σw) ]T = (Tyₛ σ A) , Γ⊢Aₛ σw Aw

id : ∀{Γ} → Sub Γ Γ
id = idₛ , idₛ⊢

_∘_ : ∀{Γ Δ Σ} → Sub Δ Σ → Sub Γ Δ → Sub Γ Σ
_∘_ (σ , σw) (δ , δw) = σ ∘ₛ δ , ∘ₛ⊢ σw δw

ε : ∀{Γ} → Sub Γ ∙
ε {n , Γ , Γw} = S.∙ , Sub⊢.∙

_,s_  : ∀ {Γ Δ} (σ : Sub Γ Δ) {A : Ty Δ} → Tm Γ (_[_]T {Γ} {Δ} A σ) → Sub Γ (Δ ▶ A)
_,s_ (σ , σw) (t , tw) = (σ , t) , (σw , tw)

π₁ : ∀{Γ Δ}{A : Ty Δ} → Sub Γ (Δ ▶ A) → Sub Γ Δ
π₁ (_ , (σw , tw)) = _ , σw

_[_]t : {Γ Δ : Con} {A : Ty Δ} → Tm Δ A → (σ : Sub Γ Δ) → Tm Γ (_[_]T {Γ} {Δ} A σ)
(t , tw) [ (σ , σw) ]t = (Tmₛ σ t) , Γ⊢t∈Aₛ σw tw

π₂ : ∀{Γ Δ} {A : Ty Δ} (σ : Sub Γ (Δ ▶ A)) → Tm Γ (_[_]T {Γ} {Δ} A (π₁ {Γ} {Δ} {A} σ))
π₂ (_ , (σw , tw)) = _ , tw

[id]T : ∀ {Γ} {A : Ty Γ} → _≡_ {lzero} {Ty Γ} (_[_]T {Γ} {Γ} A (id {Γ})) A
[id]T {n , Γ , Γw}{A , Aw} = Ty≡ (Ty-idₛ A)

[][]T : {Γ Δ : Con} {Σ : Con} {A : Ty Σ} {σ : Sub Γ Δ}
         {δ : Sub Δ Σ} →
         _≡_ {lzero} {Ty Γ} (_[_]T {Γ} {Δ} (_[_]T {Δ} {Σ} A δ) σ)
             (_[_]T {Γ} {Σ} A (_∘_ {Γ} {Δ} {Σ} δ σ))
[][]T {Γ}{Δ}{Σ}{A}{σ}{δ} = Ty≡ (Ty-∘ₛ (₁ δ) (₁ σ) (₁ A) ⁻¹)

idl : {Γ Δ : Con} {σ : Sub Γ Δ} → _≡_ {lzero} {Sub Γ Δ} (_∘_ {Γ} {Δ} {Δ} (id {Δ}) σ) σ
idl {Γ}{Δ}{σ} = Sub≡ (idlₛ (₁ σ))

idr : {Γ Δ : Con} {σ : Sub Γ Δ} → _≡_ {lzero} {Sub Γ Δ} (_∘_ {Γ} {Γ} {Δ} σ (id {Γ})) σ
idr {Γ}{Δ}{σ} = Sub≡ (idrₛ (₁ σ))

ass : {Γ Δ : Con} {Σ : Con} {Ω : Con} {σ : Sub Σ Ω} {δ : Sub Δ Σ}
         {ν : Sub Γ Δ} →
         _≡_ {lzero} {Sub Γ Ω} (_∘_ {Γ} {Δ} {Ω} (_∘_ {Δ} {Σ} {Ω} σ δ) ν)
         (_∘_ {Γ} {Σ} {Ω} σ (_∘_ {Γ} {Δ} {Σ} δ ν))
ass {Γ}{Δ}{Σ}{Ω}{σ}{δ}{ν} = Sub≡ (assₛ (₁ σ) (₁ δ) (₁ ν))

--------------------------------------------------------------------------------

coeTm : ∀ {Γ}{A A' : Ty Γ}(p : ₁ A ≡ ₁ A') (t : Tm Γ A)
        → coe (Tm Γ & Ty≡ {A = A}{A'} p) t ≡ (₁ t , coe ((₁ (₂ Γ) ⊢ ₁ t ∈_) & p) (₂ t))
coeTm {Γ} {_ , Aw} {A' , Aw'} refl t with Γ⊢Aprop Aw Aw'
coeTm {Γ} {_ , Aw} {_ , .Aw} refl t | refl = coe-refl _ t

--------------------------------------------------------------------------------

,∘ :
       {Γ Δ : Con} {Σ : Con} {δ : Sub Γ Δ} {σ : Sub Σ Γ} {A : Ty Δ}
       {t : Tm Γ (_[_]T {Γ} {Δ} A δ)} →
       _≡_ {lzero} {Sub Σ (Δ ▶ A)}
       (_∘_ {Σ} {Γ} {Δ ▶ A} (_,s_ {Γ} {Δ} δ {A} t) σ)
       (_,s_ {Σ} {Δ} (_∘_ {Σ} {Γ} {Δ} δ σ) {A}
        (tr {lzero} {lzero} {Ty Σ} (Tm Σ)
         {_[_]T {Σ} {Γ} (_[_]T {Γ} {Δ} A δ) σ}
         {_[_]T {Σ} {Δ} A (_∘_ {Σ} {Γ} {Δ} δ σ)}
         ([][]T {Σ} {Γ} {Δ} {A} {σ} {δ})
         (_[_]t {Σ} {Γ} {_[_]T {Γ} {Δ} A δ} t σ)))
,∘ {Γ}{Δ}{Σ}{δ}{σ}{A}{t} =
  Sub≡ (_,_ (₁ δ ∘ₛ ₁ σ) & (
    (₁ & (coeTm {Σ} (Ty-∘ₛ (₁ δ) (₁ σ) (₁ A) ⁻¹)
                    (Tmₛ (₁ σ) (₁ t) , Γ⊢t∈Aₛ (₂ σ) (₂ t)))) ⁻¹))

π₁β : {Γ Δ : Con} {A : Ty Δ} {σ : Sub Γ Δ}
    {t : Tm Γ (_[_]T {Γ} {Δ} A σ)} →
    _≡_ {lzero} {Sub Γ Δ} (π₁ {Γ} {Δ} {A} (_,s_ {Γ} {Δ} σ {A} t)) σ
π₁β {Γ}{Δ}{A}{σ}{t} = refl

πη    : {Γ Δ : Con} {A : Ty Δ} {σ : Sub Γ (Δ ▶ A)} →
       _≡_ {lzero} {Sub Γ (Δ ▶ A)}
       (_,s_ {Γ} {Δ} (π₁ {Γ} {Δ} {A} σ) {A} (π₂ {Γ} {Δ} {A} σ)) σ
πη {σ = (_ , (σw , x))} = refl

εη    : {Γ : Con} {σ : Sub Γ ∙} → _≡_ {lzero} {Sub Γ ∙} σ (ε {Γ})
εη {Γ} {.Substitution.∙ , Sub⊢.∙} = refl

π₂β : {Γ Δ : Con} {A : Ty Δ} {σ : Sub Γ Δ}
      {t : Tm Γ (_[_]T {Γ} {Δ} A σ)} →
      _≡_ {lzero}
      {Tm Γ (_[_]T {Γ} {Δ} A (π₁ {Γ} {Δ} {A} (_,s_ {Γ} {Δ} σ {A} t)))}
      (π₂ {Γ} {Δ} {A} (_,s_ {Γ} {Δ} σ {A} t))
      (tr {lzero} {lzero} {Sub Γ Δ} (λ σ₁ → Tm Γ (_[_]T {Γ} {Δ} A σ₁))
       {σ} {π₁ {Γ} {Δ} {A} (_,s_ {Γ} {Δ} σ {A} t)}
       (_⁻¹ {lzero} {Sub Γ Δ} {π₁ {Γ} {Δ} {A} (_,s_ {Γ} {Δ} σ {A} t)} {σ}
        (π₁β {Γ} {Δ} {A} {σ} {t}))
       t)
π₂β {Γ}{Δ}{A}{σ}{t} = refl

--------------------------------------------------------------------------------

-- Note: this is NOT exactly the definition of ↑ in the model! The
-- model definition would result in brutal coercion explosion.
-- We handwave by defining ↑ to be the following,
-- and assuming that the model definition is equal to this one (TODO to prove)

infixl 5 _↑_
_↑_ : {Γ Δ : Con} (σ : Sub Γ Δ) (A : Ty Δ) → Sub (Γ ▶ _[_]T {Γ} {Δ} A σ) (Δ ▶ A)
_↑_ {n , Γ , Γw}{m , Δ , Δw} (σ , σw) (A , Aw) =
  ((σ ₛ∘ₑ wk , S.var zero)) ,
  (ₛ∘ₑ⊢ σw wk⊢ , coe (((Γ S.▶ Tyₛ σ A) ⊢ S.var zero ∈_) & (Ty-ₛ∘ₑ σ wk A ⁻¹))
                (_⊢_∈_.var zero))

--------------------------------------------------------------------------------

U : ∀{Γ} → Ty Γ
U {Γ} = S.U , T.U

U[]  : {Γ Δ : Con} {σ : Sub Γ Δ} →
         _≡_ {lzero} {Ty Γ} (_[_]T {Γ} {Δ} (U {Δ}) σ) (U {Γ})
U[] {Γ}{Δ}{σ} = refl

El : ∀{Γ}(a : Tm Γ (U {Γ})) → Ty Γ
El {n , Γ , Γw} (a , aw) = S.El a , T.El aw

El[] : {Γ Δ : Con} {σ : Sub Γ Δ} {a : Tm Δ (U {Δ})} →
       _≡_ {lzero} {Ty Γ} (_[_]T {Γ} {Δ} (El {Δ} a) σ)
       (El {Γ}
        (tr {lzero} {lzero} {Ty Γ} (Tm Γ) {_[_]T {Γ} {Δ} (U {Δ}) σ} {U {Γ}}
         (U[] {Γ} {Δ} {σ}) (_[_]t {Γ} {Δ} {U {Δ}} a σ)))
El[] {Γ}{Δ}{σ}{a} = refl

Π : {Γ : Con} (a : Tm Γ (U {Γ})) → Ty (Γ ▶ El {Γ} a) → Ty Γ
Π {n , Γ , Γw} (a , aw) (B , Bw) = (S.Π a B) , T.Π aw Bw


Π[] : {Γ Δ : Con} {σ : Sub Γ Δ} {a : Tm Δ (U {Δ})}
  {B : Ty (Δ ▶ El {Δ} a)} →
  _≡_ {lzero} {Ty Γ} (_[_]T {Γ} {Δ} (Π {Δ} a B) σ)
  (Π {Γ}
   (tr {lzero} {lzero} {Ty Γ} (Tm Γ) {_[_]T {Γ} {Δ} (U {Δ}) σ} {U {Γ}}
    (U[] {Γ} {Δ} {σ}) (_[_]t {Γ} {Δ} {U {Δ}} a σ))
   (tr {lzero} {lzero} {Ty Γ} (λ x → Ty (Γ ▶ x))
    {_[_]T {Γ} {Δ} (El {Δ} a) σ}
    {El {Γ}
     (tr {lzero} {lzero} {Ty Γ} (Tm Γ) {_[_]T {Γ} {Δ} (U {Δ}) σ} {U {Γ}}
      (U[] {Γ} {Δ} {σ}) (_[_]t {Γ} {Δ} {U {Δ}} a σ))}
    (El[] {Γ} {Δ} {σ} {a})
    (_[_]T {Γ ▶ _[_]T {Γ} {Δ} (El {Δ} a) σ} {Δ ▶ El {Δ} a} B
     (_↑_ {Γ} {Δ} σ (El {Δ} a)))))
Π[] {Γ}{Δ}{σ}{a}{B} = Ty≡ refl

app : {Γ : Con} {a : Tm Γ (U {Γ})} {B : Ty (Γ ▶ El {Γ} a)} →
        Tm Γ (Π {Γ} a B) → Tm (Γ ▶ El {Γ} a) B
app {n , Γ , Γw}{a , aw}{B , Bw} (t , tw) =
    S.app (Tmₑ wk t) (S.var zero)
  , coe (((Γ S.▶ S.El a) ⊢ S.app (Tmₑ (drop idₑ) t) (S.var zero) ∈_) &
           ( Ty-ₑ∘ₛ (keep wk) (keepₛ idₛ , S.var zero) B ⁻¹
           ◾ (λ x → Tyₛ (x , S.var zero) B) &
                idlₑₛ (idₛ ₛ∘ₑ drop idₑ) ◾ Ty-idₛ B))
      (T.app {_}{Γ S.▶ S.El a} {Tmₑ wk t}{S.var zero} (Γ⊢t∈Aₑ wk⊢ tw) (_⊢_∈_.var zero))


-- easy version of app[]
app[]* : ∀ {n m}{t : S.Tm m}{σ : S.Sub n m}
      → Tmₛ (keepₛ σ) (S.app (Tmₑ wk t) (S.var zero))
      ≡ S.app (Tmₑ wk (Tmₛ σ t)) (S.var zero)
app[]* {n}{m}{t}{σ} = S.app & (Tm-ₑ∘ₛ wk (keepₛ σ) t ⁻¹ ◾ (λ x → Tmₛ x t) & idlₑₛ (dropₛ σ) ◾ Tm-ₛ∘ₑ σ wk t) ⊗ refl

-- coeΣ : ∀ {α β}{A : Set α}{B B' : A → Set β}(p : Σ A B ≡ Σ A B') t u → ₁ (coe p (t , u)) ≡ t
-- coeΣ {A = A} {B} {B'} p t u = {!!}

-- hard version
app[] :
   {Γ Δ : Con} {σ : Sub Γ Δ} {a : Tm Δ (U {Δ})} {B : Ty (Δ ▶ El {Δ}
   a)} {t : Tm Δ (Π {Δ} a B)} → _≡_ {lzero} {Tm (Γ ▶ El {Γ} (tr
   {lzero} {lzero} {Ty Γ} (Tm Γ) {_[_]T {Γ} {Δ} (U {Δ}) σ} {U {Γ}}
   (U[] {Γ} {Δ} {σ}) (_[_]t {Γ} {Δ} {U {Δ}} a σ))) (tr {lzero}
   {lzero} {Ty Γ} (λ z → Ty (Γ ▶ z)) {_[_]T {Γ} {Δ} (El {Δ} a) σ}
   {El {Γ} (tr {lzero} {lzero} {Ty Γ} (Tm Γ) {_[_]T {Γ} {Δ} (U {Δ})
   σ} {U {Γ}} (U[] {Γ} {Δ} {σ}) (_[_]t {Γ} {Δ} {U {Δ}} a σ))} (El[]
   {Γ} {Δ} {σ} {a}) (_[_]T {Γ ▶ _[_]T {Γ} {Δ} (El {Δ} a) σ} {Δ ▶ El
   {Δ} a} B (_↑_ {Γ} {Δ} σ (El {Δ} a))))} (tr2 {lzero} {lzero}
   {lzero} {Ty Γ} {λ z → Ty (Γ ▶ z)} (λ A → Tm (Γ ▶ A)) {_[_]T {Γ}
   {Δ} (El {Δ} a) σ} {El {Γ} (tr {lzero} {lzero} {Ty Γ} (Tm Γ)
   {_[_]T {Γ} {Δ} (U {Δ}) σ} {U {Γ}} (U[] {Γ} {Δ} {σ}) (_[_]t {Γ}
   {Δ} {U {Δ}} a σ))} (El[] {Γ} {Δ} {σ} {a}) {_[_]T {Γ ▶ _[_]T {Γ}
   {Δ} (El {Δ} a) σ} {Δ ▶ El {Δ} a} B (_↑_ {Γ} {Δ} σ (El {Δ} a))}
   {tr {lzero} {lzero} {Ty Γ} (λ z → Ty (Γ ▶ z)) {_[_]T {Γ} {Δ} (El
   {Δ} a) σ} {El {Γ} (tr {lzero} {lzero} {Ty Γ} (Tm Γ) {_[_]T {Γ}
   {Δ} (U {Δ}) σ} {U {Γ}} (U[] {Γ} {Δ} {σ}) (_[_]t {Γ} {Δ} {U {Δ}} a
   σ))} (El[] {Γ} {Δ} {σ} {a}) (_[_]T {Γ ▶ _[_]T {Γ} {Δ} (El {Δ} a)
   σ} {Δ ▶ El {Δ} a} B (_↑_ {Γ} {Δ} σ (El {Δ} a)))} refl (_[_]t {Γ ▶
   _[_]T {Γ} {Δ} (El {Δ} a) σ} {Δ ▶ El {Δ} a} {B} (app {Δ} {a} {B}
   t) (_↑_ {Γ} {Δ} σ (El {Δ} a)))) (app {Γ} {tr {lzero} {lzero} {Ty
   Γ} (Tm Γ) {_[_]T {Γ} {Δ} (U {Δ}) σ} {U {Γ}} (U[] {Γ} {Δ} {σ})
   (_[_]t {Γ} {Δ} {U {Δ}} a σ)} {tr {lzero} {lzero} {Ty Γ} (λ z → Ty
   (Γ ▶ z)) {_[_]T {Γ} {Δ} (El {Δ} a) σ} {El {Γ} (tr {lzero} {lzero}
   {Ty Γ} (Tm Γ) {_[_]T {Γ} {Δ} (U {Δ}) σ} {U {Γ}} (U[] {Γ} {Δ} {σ})
   (_[_]t {Γ} {Δ} {U {Δ}} a σ))} (El[] {Γ} {Δ} {σ} {a}) (_[_]T {Γ ▶
   _[_]T {Γ} {Δ} (El {Δ} a) σ} {Δ ▶ El {Δ} a} B (_↑_ {Γ} {Δ} σ (El
   {Δ} a)))} (tr {lzero} {lzero} {Ty Γ} (Tm Γ) {_[_]T {Γ} {Δ} (Π {Δ}
   a B) σ} {Π {Γ} (tr {lzero} {lzero} {Ty Γ} (Tm Γ) {_[_]T {Γ} {Δ}
   (U {Δ}) σ} {U {Γ}} (U[] {Γ} {Δ} {σ}) (_[_]t {Γ} {Δ} {U {Δ}} a σ))
   (tr {lzero} {lzero} {Ty Γ} (λ z → Ty (Γ ▶ z)) {_[_]T {Γ} {Δ} (El
   {Δ} a) σ} {El {Γ} (tr {lzero} {lzero} {Ty Γ} (Tm Γ) {_[_]T {Γ}
   {Δ} (U {Δ}) σ} {U {Γ}} (U[] {Γ} {Δ} {σ}) (_[_]t {Γ} {Δ} {U {Δ}} a
   σ))} (El[] {Γ} {Δ} {σ} {a}) (_[_]T {Γ ▶ _[_]T {Γ} {Δ} (El {Δ} a)
   σ} {Δ ▶ El {Δ} a} B (_↑_ {Γ} {Δ} σ (El {Δ} a))))} (Π[] {Γ} {Δ}
   {σ} {a} {B}) (_[_]t {Γ} {Δ} {Π {Δ} a B} t σ)))
app[] {n , Γ , Γw}{m , Δ , Δw}{σ , σw}{a , aw}{B , Bw}{t , tw} =
  ap2̃ _,_ {!!} {!!}
