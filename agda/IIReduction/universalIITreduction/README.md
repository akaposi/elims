## Construction of closed inductive-inductive types using indexed inductive types

A signature for a closed inductive-inductive type (IIT) is a context in a small type theory called the theory of signatures. 
If the theory of signatures exists, all IITs can be constructed following [this](https://bitbucket.org/akaposi/finitaryqiit/raw/HEAD/paper.pdf).

We construct the intrinsic syntax of the theory of signatures using only indexed inductive types.

* [`Syntax.agda`](Syntax.agda): well-scoped presyntax of signatures usimng normal forms only
* [`Typing.agda`](Typing.agda): well-typedness predicates
* [`Embedding.agda`](Embedding.agda): order-preserving embeddings (OPEs)
* [`Substitution.agda`](Substitution.agda): substitutions as operations on the presyntax
* [`Sanity.agda`](Sanity.agda): OPEs and substitutions preserve typing, propositionality of typing
* [`IntrinsicModel.agda`](IntrinsicModel.agda): notion of model of the theory of signatures
* [`InitialAlg.agda`](InitialAlg.agda): construction of a the initial model

What is missing:

* notion of morphism between models of the theory of signatures
* uniqueness: there is only one morphism from the initial model to any other model
* recursor/initiality: there is a morphism from the initial model to any other model
