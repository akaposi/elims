
module Typing where

open import StrictLib
open import Syntax
open import Embedding
open import Substitution

lookup : ∀ {n} → Fin n → Con n → Ty n
lookup zero    (Γ ▶ A) = Tyₑ wk A
lookup (suc x) (Γ ▶ A) = Tyₑ wk (lookup x Γ)

data _⊢ : ∀ {n} → Con n → Set
data _⊢_ {n}(Γ : Con n) : Ty n → Set
data _⊢_∈_ {n}(Γ : Con n) : Tm n → Ty n → Set

infix 3 _⊢
infix 3 _⊢_
infix 3 _⊢_∈_

data _⊢ where
  ∙   : ∙ ⊢
  _▶_ : ∀ {n Γ}{A : Ty n} → Γ ⊢ → Γ ⊢ A → (Γ ▶ A) ⊢

data _⊢_ {n} Γ where
  U  : Γ ⊢ U
  El : ∀ {t} → Γ ⊢ t ∈ U → Γ ⊢ El t
  Π  : ∀ {a B} → Γ ⊢ a ∈ U → (Γ ▶ El a) ⊢ B → Γ ⊢ Π a B

data _⊢_∈_ {n} Γ where
  var  : ∀ x → Γ ⊢ var x ∈ lookup x Γ
  app  : ∀ {t u a B} → Γ ⊢ t ∈ Π a B → Γ ⊢ u ∈ El a → Γ ⊢ app t u ∈ Tyₛ (idₛ , u) B
