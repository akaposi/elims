
module Sanity where

open import StrictLib
open import Syntax
open import Embedding
open import Substitution
open import Typing

--  Embedding
--------------------------------------------------------------------------------

data OPE⊢ : ∀ {n m} → Con n → Con m → OPE n m → Set where
  ∙    : OPE⊢ ∙ ∙ ∙
  drop : ∀ {n m Γ Δ A σ} → OPE⊢ {n}{m} Γ Δ σ → OPE⊢ (Γ ▶ A) Δ (drop σ)
  keep : ∀ {n m Γ Δ A σ} → OPE⊢ {n}{m} Γ Δ σ → OPE⊢ (Γ ▶ Tyₑ σ A) (Δ ▶ A) (keep σ)

idₑ⊢ : ∀ {n Γ} → OPE⊢ {n} Γ Γ idₑ
idₑ⊢ {Γ = ∙}     = ∙
idₑ⊢ {Γ = Γ ▶ A} =
  coe ((λ x → OPE⊢ (Γ ▶ x) (Γ ▶ A) (keep idₑ)) & Ty-idₑ A)
      (keep idₑ⊢)

wk⊢ : ∀ {n Γ A} → OPE⊢ {suc n}{n}(Γ ▶ A) Γ wk
wk⊢ = drop idₑ⊢

lookupₑ : ∀ {n m Γ Δ σ} → OPE⊢ {n}{m} Γ Δ σ → ∀ x → lookup (∈ₑ σ x) Γ ≡ Tyₑ σ (lookup x Δ)
lookupₑ {Δ = Δ ▶ A} (drop {Γ = Γ} {σ = σ} σw) zero
  rewrite lookupₑ σw zero = Ty-∘ₑ σ wk (Tyₑ wk A) ⁻¹
        ◾ Ty-∘ₑ wk (drop (σ ∘ₑ idₑ)) A ⁻¹
        ◾ (λ x → Tyₑ (drop x) A) & (assₑ wk σ idₑ ⁻¹ ◾ idrₑ _)
        ◾ Ty-∘ₑ wk (drop σ) A
lookupₑ {Δ = Δ ▶ A} (keep {σ = σ} σw) zero =
  Ty-∘ₑ σ wk A ⁻¹ ◾ (λ x → Tyₑ (drop x) A) & (idrₑ σ ◾ idlₑ σ ⁻¹) ◾ Ty-∘ₑ wk (keep σ) A
lookupₑ {Δ = Δ ▶ _} (drop {Γ = Γ} {σ = σ} σw) (suc x)
  rewrite lookupₑ σw (suc x) =
       Ty-∘ₑ σ wk (Tyₑ wk (lookup x Δ)) ⁻¹
     ◾ Ty-∘ₑ wk (drop (σ ∘ₑ idₑ)) (lookup x Δ) ⁻¹
     ◾ (λ y → Tyₑ (drop y) (lookup x Δ)) &
         (assₑ wk σ idₑ ⁻¹ ◾ idrₑ (wk ∘ₑ σ)) ◾ Ty-∘ₑ wk (drop σ) (lookup x Δ)
lookupₑ (keep {Γ = Γ} {Δ} {σ = σ} σw) (suc x)
  rewrite lookupₑ σw x =
      Ty-∘ₑ σ wk (lookup x Δ) ⁻¹
    ◾ (λ y → Tyₑ (drop y) (lookup x Δ)) & (idrₑ σ ◾ idlₑ σ ⁻¹)
    ◾ Ty-∘ₑ wk (keep σ) (lookup x Δ)

mutual
  Γ⊢Aₑ :
    ∀ {n m Γ Δ A σ} → OPE⊢ {n}{m} Γ Δ σ → Δ ⊢ A → Γ ⊢ Tyₑ σ A
  Γ⊢Aₑ σ U       = U
  Γ⊢Aₑ σ (El t)  = El (Γ⊢t∈Aₑ σ t)
  Γ⊢Aₑ σ (Π a B) = Π (Γ⊢t∈Aₑ σ a ) (Γ⊢Aₑ (keep σ) B)

  Γ⊢t∈Aₑ :
    ∀ {n m Γ Δ t A σ} → OPE⊢ {n}{m} Γ Δ σ → Δ ⊢ t ∈ A → Γ ⊢ Tmₑ σ t ∈ Tyₑ σ A
  Γ⊢t∈Aₑ {Γ = Γ} {Δ} {σ = σ} σw (var x) =
    coe ((Γ ⊢ var (∈ₑ σ x) ∈_) & lookupₑ σw x)
        (var (∈ₑ σ x))
  Γ⊢t∈Aₑ {Γ = Γ} {σ = σ} σw (app {t} {u} {B = B} tw uw) =
    coe
      ((Γ ⊢ app (Tmₑ σ t) (Tmₑ σ u) ∈_) &
            (Ty-ₑ∘ₛ (keep σ) (idₛ , Tmₑ σ u) B ⁻¹
          ◾ (λ x → Tyₛ (x , Tmₑ σ u) B) & (idrₑₛ σ ◾ idlₛₑ σ ⁻¹)
          ◾ Ty-ₛ∘ₑ (idₛ , u) σ B))
      (app (Γ⊢t∈Aₑ σw tw) (Γ⊢t∈Aₑ σw uw))

-- Propositionality
--------------------------------------------------------------------------------

Prop : ∀ {α} → Set α → Set _
Prop A = (x y : A) → x ≡ y

Π-inj : ∀ {n a a' B B'} → Ty.Π {n} a B ≡ Π a' B' → (a ≡ a') × (B ≡ B')
Π-inj refl = refl , refl

mutual
  Γ⊢prop : ∀ {n}{Γ : Con n} → Prop (Γ ⊢)
  Γ⊢prop ∙         ∙           = refl
  Γ⊢prop (Γw ▶ Aw) (Γw' ▶ Aw') = _▶_ & Γ⊢prop Γw Γw' ⊗ Γ⊢Aprop Aw Aw'

  Γ⊢Aprop : ∀ {n}{Γ : Con n}{A} → Prop (Γ ⊢ A)
  Γ⊢Aprop U         U           = refl
  Γ⊢Aprop (El tw)   (El tw')    = El & Γ⊢t∈Aprop tw tw'
  Γ⊢Aprop (Π aw Bw) (Π aw' Bw') = Π & Γ⊢t∈Aprop aw aw' ⊗ Γ⊢Aprop Bw Bw'

  ∈unique :
    ∀ {n}{Γ : Con n}{t A B} → Γ ⊢ t ∈ A → Γ ⊢ t ∈ B → A ≡ B
  ∈unique (var x)     (var _)       = refl
  ∈unique (app tw uw) (app tw' uw') = Tyₛ _ & ₂ (Π-inj (∈unique tw tw'))

  Γ⊢t∈Aprop : ∀ {n}{Γ : Con n}{t A} → Prop (Γ ⊢ t ∈ A)
  Γ⊢t∈Aprop tw tw' = Γ⊢t∈Aprop' refl tw tw'

  Γ⊢t∈Aprop' :
    ∀ {n}{Γ : Con n}{t A B}(p : A ≡ B)(x : Γ ⊢ t ∈ A)(y : Γ ⊢ t ∈ B)
    → coe ((Γ ⊢ t ∈_) & p) x ≡ y
  Γ⊢t∈Aprop' p (app tw uw) (app tw' uw') with ∈unique tw tw' | p | tw' | uw'
  Γ⊢t∈Aprop' {Γ = Γ} refl (app {t} {u} tw uw) (app tw' uw') | refl | refl | tw'* | uw'*
    = app & Γ⊢t∈Aprop tw tw'* ⊗ Γ⊢t∈Aprop uw uw'*
  Γ⊢t∈Aprop' refl (var x) (var _) = refl

-- Substitution
--------------------------------------------------------------------------------

data Sub⊢ {n}(Γ : Con n) : ∀ {m} → Con m → Sub n m → Set where
  ∙   : Sub⊢ Γ ∙ ∙
  _,_ : ∀ {m Δ A t σ} → Sub⊢ Γ {m} Δ σ → Γ ⊢ t ∈ Tyₛ σ A → Sub⊢ Γ (Δ ▶ A) (σ , t)

Sub⊢prop : ∀ {n m Γ Δ σ}(σw σw' : Sub⊢ {n} Γ {m} Δ σ) → σw ≡ σw'
Sub⊢prop ∙        ∙            = refl
Sub⊢prop (σw , tw) (σw' , tw') = Sub⊢._,_ & Sub⊢prop σw σw' ⊗ Γ⊢t∈Aprop tw tw'

infixr 6 _ₛ∘⊢ₑ_
_ₛ∘⊢ₑ_ :
  ∀ {n m k Γ Δ Σ σ δ} → Sub⊢ {n} Γ {m} Δ σ → OPE⊢ Σ Γ δ → Sub⊢ {k} Σ {m} Δ (σ ₛ∘ₑ δ)
_ₛ∘⊢ₑ_ ∙ δw = ∙
_ₛ∘⊢ₑ_ {Σ = Σ} {δ = δ} (_,_ {Δ = Δ} {A} {t} {σ} σw tw) δw =
    (σw ₛ∘⊢ₑ δw) ,
    coe ((Σ ⊢ Tmₑ δ t ∈_) & (Ty-ₛ∘ₑ σ δ A ⁻¹)) (Γ⊢t∈Aₑ δw tw)

keepₛ⊢ :
  ∀ {n m Γ Δ σ A} → Sub⊢ {n} Γ {m} Δ σ → Sub⊢ (Γ ▶ Tyₛ σ A) (Δ ▶ A) (keepₛ σ)
keepₛ⊢ {Γ = Γ}{Δ}{σ}{A} σw =
  (σw ₛ∘⊢ₑ wk⊢) , coe (((Γ ▶ Tyₛ σ A) ⊢ var zero ∈_) & (Ty-ₛ∘ₑ σ wk A ⁻¹)) (var zero)


idₛ⊢ : ∀ {n Γ} → Sub⊢ {n} Γ Γ idₛ
idₛ⊢ {Γ = ∙}     = ∙
idₛ⊢ {Γ = Γ ▶ A} =
  coe ((λ x → Sub⊢ (Γ ▶ x) (Γ ▶ A) (keepₛ idₛ)) & Ty-idₛ A) (keepₛ⊢ idₛ⊢)

lookupₛ :
  ∀ {n m Γ Δ σ} → Sub⊢ {n} Γ {m} Δ σ → ∀ x → Γ ⊢ ∈ₛ σ x ∈ Tyₛ σ (lookup x Δ)
lookupₛ {Γ = Γ} (_,_ {A = A} {t} {σ} σw tw) zero =
  coe ((Γ ⊢ t ∈_) & ((λ x → Tyₛ x A) & (idlₑₛ σ ⁻¹) ◾ Ty-ₑ∘ₛ wk (σ , t) A)) tw
lookupₛ {Γ = Γ} (_,_ {Δ = Δ} {t = t} {σ} σw tw) (suc x) =
      coe ((Γ ⊢ ∈ₛ σ x ∈_) & ((λ σ → Tyₛ σ (lookup x Δ)) & (idlₑₛ σ ⁻¹) ◾ Ty-ₑ∘ₛ wk (σ , t) (lookup x Δ))) (lookupₛ σw x)

mutual
   Γ⊢Aₛ :
     ∀ {n m Γ Δ A σ} → Sub⊢ {n} Γ {m} Δ σ → Δ ⊢ A → Γ ⊢ Tyₛ σ A
   Γ⊢Aₛ σw U         = U
   Γ⊢Aₛ σw (El tw)   = El (Γ⊢t∈Aₛ σw tw)
   Γ⊢Aₛ σw (Π Aw Bw) = Π (Γ⊢t∈Aₛ σw Aw) (Γ⊢Aₛ (keepₛ⊢ σw) Bw)

   Γ⊢t∈Aₛ : ∀ {n m Γ Δ t A σ} → Sub⊢ {n} Γ {m} Δ σ → Δ ⊢ t ∈ A → Γ ⊢ Tmₛ σ t ∈ Tyₛ σ A
   Γ⊢t∈Aₛ σw (var x) = lookupₛ σw x

   Γ⊢t∈Aₛ {Γ = Γ} {σ = σ} σw (app {t} {u} {A} {B} tw uw)
     = coe ((Γ ⊢ app (Tmₛ σ t) (Tmₛ σ u) ∈_) &
              (Ty-∘ₛ (keepₛ σ) (idₛ , Tmₛ σ u) B ⁻¹
            ◾ (λ x → Tyₛ (x , Tmₛ σ u) B) & (
                 assₛₑₛ σ wk (idₛ , Tmₛ σ u)
               ◾ (σ ∘ₛ_) & idlₑₛ idₛ
               ◾ idrₛ σ
               ◾ idlₛ σ ⁻¹)
            ◾ Ty-∘ₛ (idₛ , u) σ B))
           (_⊢_∈_.app (Γ⊢t∈Aₛ σw tw) (Γ⊢t∈Aₛ σw uw))


∘ₛ⊢ : ∀ {n m k Γ Δ Σ σ δ} → Sub⊢ {m} Δ {k} Σ σ → Sub⊢ {n} Γ Δ δ → Sub⊢ Γ Σ (σ ∘ₛ δ)
∘ₛ⊢ ∙         δw = ∙
∘ₛ⊢ {Γ = Γ} {δ = δ} (_,_ {A = A} {t} {σ} σw tw) δw =
  (∘ₛ⊢ σw δw) , coe ((Γ ⊢ Tmₛ δ t ∈_) & (Ty-∘ₛ σ δ A ⁻¹)) (Γ⊢t∈Aₛ δw tw)

ₛ∘ₑ⊢ : ∀ {n m k Γ Δ Σ σ δ} → Sub⊢ {m} Δ {k} Σ σ → OPE⊢ {n} Γ Δ δ → Sub⊢ Γ Σ (σ ₛ∘ₑ δ)
ₛ∘ₑ⊢ ∙         δw = ∙
ₛ∘ₑ⊢ {Γ = Γ} {δ = δ} (_,_ {A = A} {t} {σ} σw tw) δw =
  (ₛ∘ₑ⊢ σw δw) , coe ((Γ ⊢ Tmₑ δ t ∈_) & (Ty-ₛ∘ₑ σ δ A ⁻¹)) (Γ⊢t∈Aₑ δw tw)


-- Inference inversion
--------------------------------------------------------------------------------

lookup⊢ : ∀ {n Γ} → Γ ⊢ → ∀ x → Γ ⊢ lookup {n} x Γ
lookup⊢ ∙ ()
lookup⊢ (Γw ▶ Aw) zero    = Γ⊢Aₑ wk⊢ Aw
lookup⊢ (Γw ▶ Aw) (suc x) = Γ⊢Aₑ wk⊢ (lookup⊢ Γw x)

Γ⊢t∈? : ∀ {n}{Γ : Con n}{t A} → Γ ⊢ → Γ ⊢ t ∈ A → Γ ⊢ A
Γ⊢t∈? Γw (var x)     = lookup⊢ Γw x
Γ⊢t∈? {Γ = Γ} Γw (app {t} {u} {a} {B} tw uw) with Γ⊢t∈? Γw tw
... | Π aw Bw = Γ⊢Aₛ (idₛ⊢ , coe ((Γ ⊢ u ∈_) & (El & (Tm-idₛ a ⁻¹))) uw) Bw

--------------------------------------------------------------------------------

vz⊢ : ∀ {n}{Γ : Con n}{A} → (Γ ▶ A) ⊢ → (Γ ▶ A) ⊢ var zero ∈ Tyₑ wk A
vz⊢ {n}{Γ}{A} (Γw ▶ Aw) = _⊢_∈_.var zero
