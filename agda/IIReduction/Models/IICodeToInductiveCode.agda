{-# OPTIONS --rewriting --without-K #-}

{- Erasing inductive-inductive codes to mutual inductive "presyntax" codes -}

module Models.IICodeToInductiveCode where

open import Lib
import Inductive.Syntax as I
import InductiveInductive.Syntax as S

infixl 5 _,_
infixl 7 _[_]T
infixl 5 _,s_
infix  6 _∘_
infixl 8 _[_]t

i : Level
i = zero

j : Level
j = zero

Con : Set i
Con = Σ I.Sorts I.Con

Ty  : Con → Set i
Ty (Γ Σ, Δ) = I.Ty Γ  -- erased to constructor type
            ⊎ ⊤       -- erased to sort

Tm : (Γ : Con) → Ty Γ → Set  j
Tm (Γ Σ, Δ) A = A ≡ inj₂ tt → I.Varₛ Γ -- all terms are erased to sorts

Tms : Con → Con → Set j
Tms (Γ Σ, Δ) (Γ' Σ, Δ') = I.Subₛ Γ Γ'

∙ : Con
∙ = I.∙ Σ, I.∙

_,_   : (Γ : Con) → Ty Γ → Con
(Γ Σ, Δ) , inj₁ A = Γ Σ, (Δ I., A)
(Γ Σ, Δ) , inj₂ _ = (Γ I.,U) Σ, (Δ I.,U)

_[_]T : ∀{Γ Δ} → Ty Δ → Tms Γ Δ → Ty Γ
inj₁ A  [ σ ]T = inj₁ (I.Tyₛ σ A)
inj₂ tt [ σ ]T = inj₂ tt

id : ∀{Γ} → Tms Γ Γ
id {Γ} = I.idₛₛ

_∘_   : ∀{Γ Δ Σ} → Tms Δ Σ → Tms Γ Δ → Tms Γ Σ
_∘_ = I._∘ₛ_

ε     : ∀{Γ} → Tms Γ ∙
ε = I.ε

_,s_  : {Γ Δ : Con} (σ : Tms Γ Δ) {A : Ty Δ} → Tm Γ (_[_]T {Γ} {Δ} A σ) → Tms Γ (Δ , A)
_,s_ σ {inj₁ A} t = σ
_,s_ σ {inj₂ _} t = σ I., t refl

π₁ : ∀{Γ Δ}{A : Ty Δ} → Tms Γ (Δ , A) → Tms Γ Δ
π₁ {A = inj₁ A} σ         = σ
π₁ {A = inj₂ _} (σ I., _) = σ

_[_]t : {Γ Δ : Con} {A : Ty Δ} → Tm Δ A → (σ : Tms Γ Δ) → Tm Γ (_[_]T {Γ} {Δ} A σ)
_[_]t {A = inj₁ A}  t σ ()
_[_]t {A = inj₂ tt} t σ p = I.Varₛₛ σ (t refl)

π₂ : {Γ Δ : Con} {A : Ty Δ} (σ : Tms Γ (Δ , A)) → Tm Γ (_[_]T {Γ} {Δ} A (π₁ {Γ} {Δ} {A} σ))
π₂ {A = inj₁ A} σ ()
π₂ {A = inj₂ tt} (σ I., x) p = x

[id]T : {Γ : Con} {A : Ty Γ} → _≡_ {i} {Ty Γ} (_[_]T {Γ} {Γ} A (id {Γ})) A
[id]T {A = inj₁ A} = ap inj₁ (I.Tyₛ-id A)
[id]T {A = inj₂ _} = refl

[][]T : {Γ Δ : Con} {Σ : Con} {A : Ty Σ} {σ : Tms Γ Δ}
        {δ : Tms Δ Σ} →
        _≡_ {i} {Ty Γ} (_[_]T {Γ} {Δ} (_[_]T {Δ} {Σ} A δ) σ)
        (_[_]T {Γ} {Σ} A (_∘_ {Γ} {Δ} {Σ} δ σ))
[][]T {A = inj₁ A} {σ} {δ} = ap inj₁ (I.Tyₛ-∘ δ σ A ⁻¹)
[][]T {A = inj₂ _} {σ} {δ} = refl

idl : {Γ Δ : Con} {σ : Tms Γ Δ} → _≡_ {j} {Tms Γ Δ} (_∘_ {Γ} {Δ} {Δ} (id {Δ}) σ) σ
idl {σ = σ} = I.idlₛ σ

idr : {Γ Δ : Con} {σ : Tms Γ Δ} → _≡_ {j} {Tms Γ Δ} (_∘_ {Γ} {Γ} {Δ} σ (id {Γ})) σ
idr {σ = σ} = I.idrₛ σ

ass   : {Γ Δ : Con} {Σ : Con} {Ω : Con} {σ : Tms Σ Ω} {δ : Tms Δ Σ}
          {ν : Tms Γ Δ} →
          _≡_ {j} {Tms Γ Ω} (_∘_ {Γ} {Δ} {Ω} (_∘_ {Δ} {Σ} {Ω} σ δ) ν)
          (_∘_ {Γ} {Σ} {Ω} σ (_∘_ {Γ} {Δ} {Σ} δ ν))
ass {σ = σ}{δ}{ν} = I.assₛ σ δ ν

,∘  : {Γ Δ : Con} {Σ : Con} {δ : Tms Γ Δ} {σ : Tms Σ Γ} {A : Ty Δ}
        {t : Tm Γ (_[_]T {Γ} {Δ} A δ)} → _≡_ {j} {Tms Σ (Δ , A)} (_∘_
        {Σ} {Γ} {Δ , A} (_,s_ {Γ} {Δ} δ {A} t) σ) (_,s_ {Σ} {Δ} (_∘_
        {Σ} {Γ} {Δ} δ σ) {A} (tr {i} {j} {Ty Σ} (Tm Σ) {_[_]T {Σ} {Γ}
        (_[_]T {Γ} {Δ} A δ) σ} {_[_]T {Σ} {Δ} A (_∘_ {Σ} {Γ} {Δ} δ σ)}
        ([][]T {Σ} {Γ} {Δ} {A} {σ} {δ}) (_[_]t {Σ} {Γ} {_[_]T {Γ} {Δ}
        A δ} t σ)))
,∘ {δ = δ} {σ} {inj₁ A} {t} = refl
,∘ {δ = δ} {σ} {inj₂ _} {t} = refl

π₁β   : {Γ Δ : Con} {A : Ty Δ} {σ : Tms Γ Δ}
        {t : Tm Γ (_[_]T {Γ} {Δ} A σ)} → _≡_ {j} {Tms Γ Δ} (π₁ {Γ} {Δ} {A} (_,s_ {Γ} {Δ} σ {A} t)) σ
π₁β {A = inj₁ _} = refl
π₁β {A = inj₂ _} = refl

πη : {Γ Δ : Con} {A : Ty Δ} {σ : Tms Γ (Δ , A)} →
          _≡_ {j} {Tms Γ (Δ , A)}
          (_,s_ {Γ} {Δ} (π₁ {Γ} {Δ} {A} σ) {A} (π₂ {Γ} {Δ} {A} σ)) σ
πη {A = inj₁ A} = refl
πη {A = inj₂ _} {σ I., _} = refl

εη : {Γ : Con} {σ : Tms Γ ∙} → _≡_ {j} {Tms Γ ∙} σ (ε {Γ})
εη {σ = I.ε} = refl

π₂β : {Γ Δ : Con} {A : Ty Δ} {σ : Tms Γ Δ}
      {t : Tm Γ (_[_]T {Γ} {Δ} A σ)} → _≡_ {j} {Tm Γ (_[_]T {Γ} {Δ} A (π₁
      {Γ} {Δ} {A} (_,s_ {Γ} {Δ} σ {A} t)))} (π₂ {Γ} {Δ} {A} (_,s_ {Γ} {Δ} σ
      {A} t)) (tr {j} {j} {Tms Γ Δ} (λ σ₁ → Tm Γ (_[_]T {Γ} {Δ} A σ₁)) {σ}
      {π₁ {Γ} {Δ} {A} (_,s_ {Γ} {Δ} σ {A} t)} (_⁻¹ {j} {Tms Γ Δ} {π₁ {Γ} {Δ}
      {A} (_,s_ {Γ} {Δ} σ {A} t)} {σ} (π₁β {Γ} {Δ} {A} {σ} {t})) t)
π₂β {A = inj₁ A} = ext λ ()
π₂β {A = inj₂ _} = ext λ {refl → refl}

wk : {Γ : Con} {A : Ty Γ} → Tms (Γ , A) Γ
wk {Γ} {A} = π₁ {Γ , A} {Γ} {A} (id {Γ , A})

vz : {Γ : Con} {A : Ty Γ} → Tm (Γ , A) (_[_]T {Γ , A} {Γ} A (wk {Γ} {A}))
vz {Γ} {A} = π₂ {Γ , A} {Γ} {A} (id {Γ , A})

vs : {Γ : Con} {A B : Ty Γ} → Tm Γ A → Tm (Γ , B) (_[_]T {Γ , B} {Γ} A (wk {Γ} {B}))
vs {Γ} {A} {B} x = _[_]t {Γ , B} {Γ} {A} x (π₁ {Γ , B} {Γ} {B} (id {Γ , B}))

<_> : {Γ : Con} {A : Ty Γ} → Tm Γ A → Tms Γ (Γ , A)
<_> {Γ} {A} t =
  _,s_ {Γ} {Γ} (id {Γ}) {A} (tr {i} {j} {Ty Γ} (Tm Γ) {A} {_[_]T {Γ}
  {Γ} A (id {Γ})} (_⁻¹ {i} {Ty Γ} {_[_]T {Γ} {Γ} A (id {Γ})} {A}
  ([id]T {Γ} {A})) t)

infix 4 <_>

_^_ : {Γ Δ : Con} (σ : Tms Γ Δ) (A : Ty Δ) → Tms (Γ , _[_]T {Γ} {Δ} A σ) (Δ , A)
_^_ {Γ} {Δ} σ A =
  _,s_ {Γ , _[_]T {Γ} {Δ} A σ} {Δ} (_∘_ {Γ , _[_]T {Γ} {Δ} A σ} {Γ}
  {Δ} σ (π₁ {Γ , _[_]T {Γ} {Δ} A σ} {Γ} {_[_]T {Γ} {Δ} A σ} (id {Γ ,
  _[_]T {Γ} {Δ} A σ}))) {A} (tr {i} {j} {Ty (Γ , _[_]T {Γ} {Δ} A σ)}
  (Tm (Γ , _[_]T {Γ} {Δ} A σ)) {_[_]T {Γ , _[_]T {Γ} {Δ} A σ} {Γ}
  (_[_]T {Γ} {Δ} A σ) (π₁ {Γ , _[_]T {Γ} {Δ} A σ} {Γ} {_[_]T {Γ} {Δ} A
  σ} (id {Γ , _[_]T {Γ} {Δ} A σ}))} {_[_]T {Γ , _[_]T {Γ} {Δ} A σ} {Δ}
  A (_∘_ {Γ , _[_]T {Γ} {Δ} A σ} {Γ} {Δ} σ (π₁ {Γ , _[_]T {Γ} {Δ} A σ}
  {Γ} {_[_]T {Γ} {Δ} A σ} (id {Γ , _[_]T {Γ} {Δ} A σ})))} ([][]T {Γ ,
  _[_]T {Γ} {Δ} A σ} {Γ} {Δ} {A} {π₁ {Γ , _[_]T {Γ} {Δ} A σ} {Γ}
  {_[_]T {Γ} {Δ} A σ} (id {Γ , _[_]T {Γ} {Δ} A σ})} {σ}) (π₂ {Γ ,
  _[_]T {Γ} {Δ} A σ} {Γ} {_[_]T {Γ} {Δ} A σ} (id {Γ , _[_]T {Γ} {Δ} A
  σ})))

infixl 5 _^_

U : ∀{Γ} → Ty Γ
U = inj₂ tt

U[] : {Γ Δ : Con} {σ : Tms Γ Δ} → _≡_ {i} {Ty Γ} (_[_]T {Γ} {Δ} (U {Δ}) σ) (U {Γ})
U[] = refl

El : ∀{Γ}(a : Tm Γ (U {Γ})) → Ty Γ
El a = inj₁ (I.sort (a refl))

El[] : {Γ Δ : Con} {σ : Tms Γ Δ} {a : Tm Δ (U {Δ})} →
        _≡_ {i} {Ty Γ} (_[_]T {Γ} {Δ} (El {Δ} a) σ) (El {Γ} (tr {i}
        {j} {Ty Γ} (Tm Γ) {_[_]T {Γ} {Δ} (U {Δ}) σ} {U {Γ}} (U[] {Γ}
        {Δ} {σ}) (_[_]t {Γ} {Δ} {U {Δ}} a σ)))
El[] = refl

Π : {Γ : Con} (a : Tm Γ (U {Γ})) → Ty (Γ , El {Γ} a) → Ty Γ
Π a (inj₁ B) = inj₁ (a refl I.⇒ B)
Π a (inj₂ _) = inj₂ tt

Π[] : {Γ Δ : Con} {σ : Tms Γ Δ} {a : Tm Δ (U {Δ})}
        {B : Ty (Δ , El {Δ} a)} → _≡_ {i} {Ty Γ} (_[_]T {Γ} {Δ} (Π
        {Δ} a B) σ) (Π {Γ} (tr {i} {j} {Ty Γ} (Tm Γ) {_[_]T {Γ} {Δ}
        (U {Δ}) σ} {U {Γ}} (U[] {Γ} {Δ} {σ}) (_[_]t {Γ} {Δ} {U {Δ}}
        a σ)) (tr {i} {i} {Ty Γ} (λ x → Ty (Γ , x)) {_[_]T {Γ} {Δ}
        (El {Δ} a) σ} {El {Γ} (tr {i} {j} {Ty Γ} (Tm Γ) {_[_]T {Γ}
        {Δ} (U {Δ}) σ} {U {Γ}} (U[] {Γ} {Δ} {σ}) (_[_]t {Γ} {Δ} {U
        {Δ}} a σ))} (El[] {Γ} {Δ} {σ} {a}) (_[_]T {Γ , _[_]T {Γ} {Δ}
        (El {Δ} a) σ} {Δ , El {Δ} a} B (_^_ {Γ} {Δ} σ (El {Δ} a)))))
Π[] {σ = σ} {a} {inj₁ B} = ap (λ x → inj₁ (I.Varₛₛ σ (a refl) I.⇒ I.Tyₛ x B)) (I.idrₛ σ ⁻¹)
Π[] {B = inj₂ _} = refl

app : {Γ : Con} {a : Tm Γ (U {Γ})} {B : Ty (Γ , El {Γ} a)} → Tm Γ (Π {Γ} a B) → Tm (Γ , El {Γ} a) B
app {B = inj₁ B} t ()
app {B = inj₂ _} t p = t refl

app[] : {Γ Δ : Con} {σ : Tms Γ Δ} {a : Tm Δ (U {Δ})}
          {B : Ty (Δ , El {Δ} a)} {t : Tm Δ (Π {Δ} a B)} → _≡_ {j}
          {Tm (Γ , El {Γ} (tr {i} {j} {Ty Γ} (Tm Γ) {_[_]T {Γ} {Δ}
          (U {Δ}) σ} {U {Γ}} (U[] {Γ} {Δ} {σ}) (_[_]t {Γ} {Δ} {U
          {Δ}} a σ))) (tr {i} {i} {Ty Γ} (λ z → Ty (Γ , z)) {_[_]T
          {Γ} {Δ} (El {Δ} a) σ} {El {Γ} (tr {i} {j} {Ty Γ} (Tm Γ)
          {_[_]T {Γ} {Δ} (U {Δ}) σ} {U {Γ}} (U[] {Γ} {Δ} {σ}) (_[_]t
          {Γ} {Δ} {U {Δ}} a σ))} (El[] {Γ} {Δ} {σ} {a}) (_[_]T {Γ ,
          _[_]T {Γ} {Δ} (El {Δ} a) σ} {Δ , El {Δ} a} B (_^_ {Γ} {Δ}
          σ (El {Δ} a))))} (tr2 {i} {i} {j} {Ty Γ} {λ z → Ty (Γ ,
          z)} (λ A → Tm (Γ , A)) {_[_]T {Γ} {Δ} (El {Δ} a) σ} {El
          {Γ} (tr {i} {j} {Ty Γ} (Tm Γ) {_[_]T {Γ} {Δ} (U {Δ}) σ} {U
          {Γ}} (U[] {Γ} {Δ} {σ}) (_[_]t {Γ} {Δ} {U {Δ}} a σ))} (El[]
          {Γ} {Δ} {σ} {a}) {_[_]T {Γ , _[_]T {Γ} {Δ} (El {Δ} a) σ}
          {Δ , El {Δ} a} B (_^_ {Γ} {Δ} σ (El {Δ} a))} {tr {i} {i}
          {Ty Γ} (λ z → Ty (Γ , z)) {_[_]T {Γ} {Δ} (El {Δ} a) σ} {El
          {Γ} (tr {i} {j} {Ty Γ} (Tm Γ) {_[_]T {Γ} {Δ} (U {Δ}) σ} {U
          {Γ}} (U[] {Γ} {Δ} {σ}) (_[_]t {Γ} {Δ} {U {Δ}} a σ))} (El[]
          {Γ} {Δ} {σ} {a}) (_[_]T {Γ , _[_]T {Γ} {Δ} (El {Δ} a) σ}
          {Δ , El {Δ} a} B (_^_ {Γ} {Δ} σ (El {Δ} a)))} refl (_[_]t
          {Γ , _[_]T {Γ} {Δ} (El {Δ} a) σ} {Δ , El {Δ} a} {B} (app
          {Δ} {a} {B} t) (_^_ {Γ} {Δ} σ (El {Δ} a)))) (app {Γ} {tr
          {i} {j} {Ty Γ} (Tm Γ) {_[_]T {Γ} {Δ} (U {Δ}) σ} {U {Γ}}
          (U[] {Γ} {Δ} {σ}) (_[_]t {Γ} {Δ} {U {Δ}} a σ)} {tr {i} {i}
          {Ty Γ} (λ x → Ty (Γ , x)) {_[_]T {Γ} {Δ} (El {Δ} a) σ} {El
          {Γ} (tr {i} {j} {Ty Γ} (Tm Γ) {_[_]T {Γ} {Δ} (U {Δ}) σ} {U
          {Γ}} (U[] {Γ} {Δ} {σ}) (_[_]t {Γ} {Δ} {U {Δ}} a σ))} (El[]
          {Γ} {Δ} {σ} {a}) (_[_]T {Γ , _[_]T {Γ} {Δ} (El {Δ} a) σ}
          {Δ , El {Δ} a} B (_^_ {Γ} {Δ} σ (El {Δ} a)))} (tr {i} {j}
          {Ty Γ} (Tm Γ) {_[_]T {Γ} {Δ} (Π {Δ} a B) σ} {Π {Γ} (tr {i}
          {j} {Ty Γ} (Tm Γ) {_[_]T {Γ} {Δ} (U {Δ}) σ} {U {Γ}} (U[]
          {Γ} {Δ} {σ}) (_[_]t {Γ} {Δ} {U {Δ}} a σ)) (tr {i} {i} {Ty
          Γ} (λ x → Ty (Γ , x)) {_[_]T {Γ} {Δ} (El {Δ} a) σ} {El {Γ}
          (tr {i} {j} {Ty Γ} (Tm Γ) {_[_]T {Γ} {Δ} (U {Δ}) σ} {U
          {Γ}} (U[] {Γ} {Δ} {σ}) (_[_]t {Γ} {Δ} {U {Δ}} a σ))} (El[]
          {Γ} {Δ} {σ} {a}) (_[_]T {Γ , _[_]T {Γ} {Δ} (El {Δ} a) σ}
          {Δ , El {Δ} a} B (_^_ {Γ} {Δ} σ (El {Δ} a))))} (Π[] {Γ}
          {Δ} {σ} {a} {B}) (_[_]t {Γ} {Δ} {Π {Δ} a B} t σ)))
app[] {Γ} {σ = σ} {a} {inj₁ B} {t} = ext λ ()
app[] {σ = σ} {a} {inj₂ y} {t} = ap (λ x _ → I.Varₛₛ x (t refl)) (I.idrₛ σ)

-- Recursors & computation (no β for equalities)
--------------------------------------------------------------------------------

postulate
  Conᴿ : S.Con → Con
  Tyᴿ  : ∀ {Γ} → S.Ty Γ → Ty (Conᴿ Γ)
  Tmᴿ  : ∀ {Γ A} → S.Tm Γ A → Tm (Conᴿ Γ) (Tyᴿ A)
  Tmsᴿ : ∀ {Γ Δ} → S.Tms Γ Δ → Tms (Conᴿ Γ) (Conᴿ Δ)

postulate
  ∙ᴿ   : Conᴿ S.∙ ≡ ∙
  ,ᴿ   : ∀ Γ A → Conᴿ (Γ S., A) ≡ Conᴿ Γ , Tyᴿ A
  []Tᴿ : (Γ Δ : S.Con) (A : S.Ty Δ) (σ : S.Tms Γ Δ) →
          _≡_ {i} {Ty (Conᴿ Γ)} (Tyᴿ {Γ} (S._[_]T {Γ} {Δ} A σ))
          (_[_]T {Conᴿ Γ} {Conᴿ Δ} (Tyᴿ {Δ} A) (Tmsᴿ {Γ} {Δ} σ))
{-# REWRITE ∙ᴿ ,ᴿ []Tᴿ #-}

postulate
  []tᴿ : {Γ Δ : S.Con} {A : S.Ty Δ} (t : S.Tm Δ A) (σ : S.Tms Γ Δ) →
           _≡_ {j}
           {Tm (Conᴿ Γ)
            (_[_]T {Conᴿ Γ} {Conᴿ Δ} (Tyᴿ {Δ} A) (Tmsᴿ {Γ} {Δ} σ))}
           (Tmᴿ {Γ} {S._[_]T {Γ} {Δ} A σ} (S._[_]t {Γ} {Δ} {A} t σ))
           (_[_]t {Conᴿ Γ} {Conᴿ Δ} {Tyᴿ {Δ} A} (Tmᴿ {Δ} {A} t)
            (Tmsᴿ {Γ} {Δ} σ))

  idᴿ  : {Γ : S.Con} →
           _≡_ {j} {Tms (Conᴿ Γ) (Conᴿ Γ)} (Tmsᴿ {Γ} {Γ} (S.id {Γ}))
           (id {Conᴿ Γ})

  ∘ᴿ   : {Γ Δ : S.Con} {Σ : S.Con} {σ : S.Tms Δ Σ} {δ : S.Tms Γ Δ} →
           _≡_ {j} {Tms (Conᴿ Γ) (Conᴿ Σ)}
           (Tmsᴿ {Γ} {Σ} (S._∘_ {Γ} {Δ} {Σ} σ δ))
           (_∘_ {Conᴿ Γ} {Conᴿ Δ} {Conᴿ Σ} (Tmsᴿ {Δ} {Σ} σ)
            (Tmsᴿ {Γ} {Δ} δ))

  εᴿ   : {Γ : S.Con} → _≡_ {j} {Tms (Conᴿ Γ) ∙} (Tmsᴿ {Γ} {S.∙} (S.ε {Γ})) (ε {Conᴿ Γ})

  ,sᴿ  : {Γ Δ : S.Con} (σ : S.Tms Γ Δ) {A : S.Ty Δ}
         (t : S.Tm Γ (S._[_]T {Γ} {Δ} A σ)) →
         _≡_ {j} {Tms (Conᴿ Γ) (Conᴿ Δ , Tyᴿ {Δ} A)}
         (Tmsᴿ {Γ} {Δ S., A} (S._,s_ {Γ} {Δ} σ {A} t))
         (_,s_ {Conᴿ Γ} {Conᴿ Δ} (Tmsᴿ {Γ} {Δ} σ) {Tyᴿ {Δ} A}
          (Tmᴿ {Γ} {S._[_]T {Γ} {Δ} A σ} t))

  π₁ᴿ  : {Γ Δ : S.Con} {A : S.Ty Δ} (σ : S.Tms Γ (Δ S., A)) →
          _≡_ {j} {Tms (Conᴿ Γ) (Conᴿ Δ)} (Tmsᴿ {Γ} {Δ} (S.π₁ {Γ} {Δ} {A} σ))
          (π₁ {Conᴿ Γ} {Conᴿ Δ} {Tyᴿ {Δ} A} (Tmsᴿ {Γ} {Δ S., A} σ))

{-# REWRITE []tᴿ idᴿ ∘ᴿ εᴿ ,sᴿ π₁ᴿ #-}

postulate
  π₂ᴿ : {Γ Δ : S.Con} {A : S.Ty Δ} (σ : S.Tms Γ (Δ S., A)) →
          _≡_ {j}
          {Tm (Conᴿ Γ)
           (_[_]T {Conᴿ Γ} {Conᴿ Δ} (Tyᴿ {Δ} A)
            (π₁ {Conᴿ Γ} {Conᴿ Δ} {Tyᴿ {Δ} A} (Tmsᴿ {Γ} {Δ S., A} σ)))}
          (Tmᴿ {Γ} {S._[_]T {Γ} {Δ} A (S.π₁ {Γ} {Δ} {A} σ)}
           (S.π₂ {Γ} {Δ} {A} σ))
          (π₂ {Conᴿ Γ} {Conᴿ Δ} {Tyᴿ {Δ} A} (Tmsᴿ {Γ} {Δ S., A} σ))

  Uᴿ  : {Γ : S.Con} → _≡_ {i} {Ty (Conᴿ Γ)} (Tyᴿ {Γ} (S.U {Γ})) (U {Conᴿ Γ})
{-# REWRITE π₂ᴿ Uᴿ #-}

postulate
  Elᴿ : {Γ : S.Con} {a : S.Tm Γ (S.U {Γ})} → _≡_ {i} {Ty (Conᴿ Γ)} (Tyᴿ {Γ} (S.El {Γ} a))
                                                 (El {Conᴿ Γ} (Tmᴿ {Γ} {S.U {Γ}} a))
{-# REWRITE Elᴿ #-}

postulate
  Πᴿ : {Γ : S.Con} (a : S.Tm Γ (S.U {Γ})) (B : S.Ty (Γ S., S.El {Γ} a)) →
        _≡_ {i} {Ty (Conᴿ Γ)} (Tyᴿ {Γ} (S.Π {Γ} a B))
        (Π {Conᴿ Γ} (Tmᴿ {Γ} {S.U {Γ}} a) (Tyᴿ {Γ S., S.El {Γ} a} B))
{-# REWRITE Πᴿ #-}

postulate
  appᴿ : {Γ : S.Con} {a : S.Tm Γ (S.U {Γ})} {B : S.Ty (Γ S., S.El {Γ} a)}
          (t : S.Tm Γ (S.Π {Γ} a B)) →
          _≡_ {j}
          {Tm (Conᴿ Γ , El {Conᴿ Γ} (Tmᴿ {Γ} {S.U {Γ}} a))
           (Tyᴿ {Γ S., S.El {Γ} a} B)}
          (Tmᴿ {Γ S., S.El {Γ} a} {B} (S.app {Γ} {a} {B} t))
          (app {Conᴿ Γ} {Tmᴿ {Γ} {S.U {Γ}} a} {Tyᴿ {Γ S., S.El {Γ} a} B}
           (Tmᴿ {Γ} {S.Π {Γ} a B} t))
{-# REWRITE appᴿ #-}
