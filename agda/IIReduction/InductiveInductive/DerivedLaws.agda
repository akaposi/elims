{-# OPTIONS --without-K #-}

module InductiveInductive.DerivedLaws where

open import Lib
open import InductiveInductive.Syntax

π₁∘ : ∀{Γ Δ Θ}{A : Ty Δ}{σ : Tms Γ (Δ , A)}{δ : Tms Θ Γ}
     → π₁ σ ∘ δ ≡ π₁ (σ ∘ δ)
π₁∘ {Γ}{Δ}{Θ}{A}{σ}{δ}
  = π₁β ⁻¹ ◾ ap π₁ (,∘ ⁻¹) ◾ ap (λ x → π₁ (x ∘ δ)) πη

wkβ : ∀{Γ Δ}{ρ : Tms Γ Δ}{A : Ty Δ}{t : Tm Γ (A [ ρ ]T)}
      → π₁ id ∘ (ρ ,s t) ≡ ρ
wkβ = π₁∘ ◾ ap π₁ idl ◾ π₁β

wk∘ : ∀{Γ Δ}{A : Ty Δ}{ρ : Tms Γ (Δ , A)}
      → π₁ id ∘ ρ ≡ π₁ ρ
wk∘ = π₁∘ ◾ ap π₁ idl
