
{-# OPTIONS --without-K #-}

open import StrictLib
open import Data.Nat
import Level as L

-- Fragment of CwF with constant and equalizer families
-- Roughly the simplest thing from which (induction <-> initiality) is provable
record IndStructure : Set₂ where
  infixl 5 _▶_ _[_]T
  field
    Con    : Set₁
    Sub    : Con → Con → Set
    Ty     : Con → Set₁
    Tm     : ∀ Γ → Ty Γ → Set
    _▶_    : (Γ : Con) → Ty Γ → Con
    π₁     : ∀ {Γ Δ A} → Sub Γ (Δ ▶ A) → Sub Γ Δ
    _[_]T  : ∀ {Γ Δ} → Ty Δ → Sub Γ Δ → Ty Γ
    π₂     : ∀ {Γ Δ A} → (σ : Sub Γ (Δ ▶ A)) → Tm Γ (A [ π₁ σ ]T)
    idₛ    : ∀ {Γ} → Sub Γ Γ
    [idₛ]T : ∀ {Γ}{A} → A [ idₛ {Γ} ]T ≡ A
    K      : Con → ∀ {Γ} → Ty Γ
    K→     : ∀ {Γ Δ} → Tm Γ (K Δ) → Sub Γ Δ
    Eq     : ∀ {Γ Δ} → Sub Γ Δ → Sub Γ Δ → Ty Γ
    Eq→    : ∀ {Γ Δ σ δ} → Tm Γ (Eq {Γ}{Δ} σ δ) → σ ≡ δ

  Initial : Con → Set₁
  Initial Γ = ∀ Δ → ∃ λ (σ : Sub Γ Δ) → ∀ δ → δ ≡ σ

  Induction : Con → Set₁
  Induction Γ = ∀ A → Tm Γ A

  Initial⇒Induction : ∀ Γ → Initial Γ → Induction Γ
  Initial⇒Induction Γ init A =
    coe ( Tm Γ & (
            (A [_]T) & (init Γ .₂ _ ◾ init Γ .₂ _ ⁻¹)
          ◾ [idₛ]T))
        (π₂ (init (Γ ▶ A) .₁))

  Induction⇒Initial : ∀ Γ → Induction Γ → Initial Γ
  Induction⇒Initial Γ ind Δ =
    K→ (ind (K Δ)) , λ σ → Eq→ (ind (Eq σ (K→ (ind (K Δ)))))

  postulate
    EqS     : ∀ {Γ A} → Tm Γ A → Tm Γ A → Ty Γ
    Refl    : ∀ {Γ A}(t : Tm Γ A) → Tm Γ (EqS t t)
    Reflect : ∀ {Γ A}{t u : Tm Γ A} → Tm Γ (EqS t u) → t ≡ u

  InductionProp : ∀ Γ → (ind ind' : Induction Γ) → ind ≡ ind'
  InductionProp Γ ind ind' = ext (λ A → Reflect (ind (EqS (ind A) (ind' A))))


W-induction : (S : Set)(P : S → Set) → IndStructure
W-induction S P = record
  { Con    = Σ Set λ W → ((s : S) → (P s → W) → W)
  ; Sub    = λ {(W , sup) (W' , sup') → ∃ λ (Wᴹ : W → W') → ∀ s f → Wᴹ (sup s f) ≡ sup' s (Wᴹ ∘ f)}
  ; Ty     = λ {(W , sup) → ∃ λ (Wᶠ : W → Set) → ∀ s f (fᶠ : ∀ p → Wᶠ (f p)) → Wᶠ (sup s f)}
  ; Tm     = λ {(W , sup) (Wᶠ , supᶠ) → ∃ λ (Wˢ : ∀ w → Wᶠ w) → ∀ s f → Wˢ (sup s f) ≡ supᶠ s f (Wˢ ∘ f)}
  ; _▶_    = λ {(W , sup) (Wᶠ , supᶠ) → Σ W Wᶠ , λ s f → (sup s (₁ ∘ f)) , supᶠ _ _ (₂ ∘ f)}
  ; π₁     = λ { {W , sup}{W' , sup'}(Wᴹ , supᴹ) → (₁ ∘ Wᴹ) , λ s f → ₁ & supᴹ s f}
  ; _[_]T  = λ { {W , sup}{W' , sup'}(Wᶠ , supᶠ)(Wᴹ , supᴹ)
                 → (Wᶠ ∘ Wᴹ) , λ s f fᶠ → coe (Wᶠ & (supᴹ s f ⁻¹)) (supᶠ s (Wᴹ ∘ f) fᶠ)}
  ; π₂     = λ { {W , sup}{W' , sup'}{W'' , sup''}(Wᴹ , supᴹ)
                 → ₂ ∘ Wᴹ , λ s f → J (λ y eq → ₂ (Wᴹ (sup s f)) ≡ coe (W'' & (₁ & eq ⁻¹)) (y .₂))
                                       refl (supᴹ s f)}
  ; idₛ    = λ { {W , sup} → id , λ _ _ → refl}
  ; [idₛ]T = λ { {W , sup}{Wᶠ , supᶠ} → refl}
  ; K      = λ {(W , sup){W' , sup'} → (λ _ → W) , λ s _ → sup s}
  ; K→     = id
  ; Eq     = λ { {W , sup}{W' , sup'}(Wᴹ , supᴹ) (Wᴹ' , supᴹ')
                 → (λ w → Wᴹ w ≡ Wᴹ' w) , λ s f fᶠ → supᴹ s f ◾ sup' s & ext fᶠ ◾ supᴹ' s f ⁻¹}
  ; Eq→    = λ { {W , sup} {W' , sup'} {Wᴹ , supᴹ} {Wᴹ' , supᴹ'} (Wᵉ , supᵉ) →
                 ,≡ (ext Wᵉ) (ext λ _ → ext λ _ → UIP _ _)}
  }

Nat-induction : IndStructure
Nat-induction = record
  { Con    = Σ Set λ N → N × (N → N)
  ; Sub    = λ {(N , z , s) (N' , z' , s') → Σ (N → N') λ Nᴹ → (Nᴹ z ≡ z') × (∀ n → Nᴹ (s n) ≡ s' (Nᴹ n))}
  ; Ty     = λ {(N , z , s) → Σ (N → Set) λ Nᶠ → Nᶠ z × (∀ n → Nᶠ n → Nᶠ (s n))}
  ; Tm     = λ {(N , z , s) (Nᶠ , zᶠ , sᶠ) → Σ (∀ n → Nᶠ n) λ Nˢ → (Nˢ z ≡ zᶠ) × (∀ n → Nˢ (s n) ≡ sᶠ n (Nˢ n))}
  ; _▶_    = λ {(N , z , s) (Nᶠ , zᶠ , sᶠ) → Σ N Nᶠ , (z , zᶠ) , λ {(n , nᶠ) → s n , sᶠ n nᶠ}}
  ; π₁     = λ { {N , z , s} {N' , z' , s'} (Nᴹ , zᴹ , sᴹ) → (₁ ∘ Nᴹ) , (₁ & zᴹ) , λ n → ₁ & sᴹ n}
  ; _[_]T  = λ { {N , z , s}{N' , z' , s'} (Nᶠ , zᶠ , sᶠ) (Nᴹ , zᴹ , sᴹ) →
                 (Nᶠ ∘ Nᴹ) , coe (Nᶠ & (zᴹ ⁻¹)) zᶠ , λ n nᶠ → coe (Nᶠ & (sᴹ n ⁻¹)) (sᶠ (Nᴹ n) nᶠ)}
  ; π₂     = λ { {N , z , s} {N' , z' , s'}{Aᶠ , zᶠ , sᶠ}(Nᴹ , refl , sᴹ) →
                 (₂ ∘ Nᴹ) , refl , λ n →
                   J (λ x eq → ₂ x ≡ coe (Aᶠ & (₁ & eq)) (sᶠ (₁ (Nᴹ n)) (₂ (Nᴹ n)))) refl (sᴹ n ⁻¹)
                   ◾ (λ x → coe (Aᶠ & x) (sᶠ (₁ (Nᴹ n)) (₂ (Nᴹ n)))) & (&⁻¹ ₁ (sᴹ n) ⁻¹)}
  ; idₛ    = λ { {N , z , s} → id , refl , λ _ → refl}
  ; [idₛ]T = refl
  ; K      = λ {(N , z , s) {Γ} → (λ _ → N) , z , (λ _ → s)}
  ; K→     = id
  ; Eq     = λ { {N₀ , z₀ , s₀}{N₁ , z₁ , s₁} (Nᴹ₀ , zᴹ₀ , sᴹ₀) (Nᴹ₁ , zᴹ₁ , sᴹ₁)
                  → (λ n → Nᴹ₀ n ≡ Nᴹ₁ n) ,
                    (zᴹ₀ ◾ zᴹ₁ ⁻¹) ,
                    λ n p → sᴹ₀ n ◾ s₁ & p ◾ sᴹ₁ n ⁻¹}
  ; Eq→    = λ { {N₀ , z₀ , s₀}{N₁ , z₁ , s₁}{Nᴹ₀ , zᴹ₀ , sᴹ₀}{Nᴹ₁ , zᴹ₁ , sᴹ₁}(Nˢ , sˢ , zˢ) →
                 ,≡ (ext Nˢ)(,≡ (UIP _ _) (ext (λ _ → UIP _ _)))}
  }


-- S¹-induction : IndStructure
-- S¹-induction = record
--   { Con    = Σ Set λ S¹ → Σ S¹ λ base → base ≡ base
--   ; Sub    = λ {(S¹ , base , loop)(S¹' , base' , loop')
--                 → Σ (S¹ → S¹') λ S¹ᴹ → Σ (S¹ᴹ base ≡ base') λ baseᴹ → (S¹ᴹ & loop) ≡ (baseᴹ ◾ loop' ◾ baseᴹ ⁻¹) }
--   ; Ty     = λ {(S¹ , base , loop) → Σ (S¹ → Set) λ S¹ᶠ → Σ (S¹ᶠ base) λ baseᶠ → coe (S¹ᶠ & loop) baseᶠ ≡ baseᶠ}
--   ; Tm     = λ {(S¹ , base , loop)(S¹ᶠ , baseᶠ , loopᶠ)
--                → Σ (∀ s → S¹ᶠ s) λ S¹ˢ
--                → Σ (S¹ˢ base ≡ baseᶠ) λ baseˢ
--                → apd S¹ˢ loop ≡ (coe (S¹ᶠ & loop) & baseˢ ◾ loopᶠ ◾ baseˢ ⁻¹)}
--   ; _▶_    = λ {(S¹ , base , loop)(S¹ᶠ , baseᶠ , loopᶠ)
--                → Σ S¹ S¹ᶠ ,
--                  (base , baseᶠ) ,
--                  ,≡ loop loopᶠ}
--   ; π₁     = λ { {S¹ , base , loop}{S¹' , base' , loop'}{S¹ᶠ , baseᶠ , loopᶠ}(S¹ᴹ , baseᴹ , loopᴹ)
--                → (₁ ∘ S¹ᴹ) ,
--                  ₁ & baseᴹ ,
--                  {!(₁ &_) & loopᴹ!}}  -- OK
--   ; _[_]T  = {!!}
--   ; π₂     = {!!}
--   ; idₛ    = {!!}
--   ; [idₛ]T = {!!}
--   ; K      = λ {(S¹ , base , loop){Δ} → (λ _ → S¹) , base , {!loop!}} -- OK
--   ; K→     = {!!}
--   ; Eq     = λ { {S¹ , base , loop}{S¹' , base' , loop'}(S¹ᴹ , baseᴹ , loopᴹ)(S¹ᴹ' , baseᴹ' , loopᴹ')
--                   → (λ s → S¹ᴹ s ≡ S¹ᴹ' s) ,
--                     (baseᴹ ◾ baseᴹ' ⁻¹) ,
--                     {!loopᴹ!}}
--   ; Eq→    = {!!}
--   }
