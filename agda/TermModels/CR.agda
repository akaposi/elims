{-# OPTIONS --rewriting --without-K #-}

module CR where

open import Lib hiding (_∘_; id)
open import Level
import Syntax as S

infixl 5 _▶_
infixl 7 _[_]T
infixl 5 _,s_
infix  6 _∘_
infixl 8 _[_]t

i : Level
i = suc (suc zero)

j : Level
j = (suc zero)

Con : Set i
Con = Σ Set₁ λ Γᶜ → Γᶜ → Γᶜ → Set

Ty : Con → Set i
Ty (Γᶜ , Γᴿ) = Σ (Γᶜ → Set₁) λ Aᶜ → {γ₀ γ₁ : Γᶜ} → Γᴿ γ₀ γ₁ → Aᶜ γ₀ → Aᶜ γ₁ → Set

Tms : Con → Con → Set j
Tms (Γᶜ , Γᴿ) (Δᶜ , Δᴿ) = Σ (Γᶜ → Δᶜ) λ σᶜ → {γ₀ γ₁ : Γᶜ}(γᴿ : Γᴿ γ₀ γ₁) → Δᴿ (σᶜ γ₀) (σᶜ γ₁)

Tm : (Γ : Con) → Ty Γ → Set j
Tm (Γᶜ , Γᴿ) (Aᶜ , Aᴿ) = Σ ((γ : Γᶜ) → Aᶜ γ) λ tᶜ → {γ₀ γ₁ : Γᶜ}(γᴿ : Γᴿ γ₀ γ₁) → Aᴿ γᴿ (tᶜ γ₀) (tᶜ γ₁)

∙ : Con
∙ = Lift ⊤ , λ _ _ → ⊤

_▶_ : (Γ : Con) → Ty Γ → Con
(Γᶜ , Γᴿ) ▶ (Aᶜ , Aᴿ) = Σ Γᶜ Aᶜ , λ { (γ₀ , α₀) (γ₁ , α₁) → Σ (Γᴿ γ₀ γ₁) λ γᴿ → Aᴿ γᴿ α₀ α₁}

_[_]T : ∀{Γ Δ} → Ty Δ → Tms Γ Δ → Ty Γ
(Aᶜ , Aᴿ) [ σᶜ , σᴿ ]T = (λ γ → Aᶜ (σᶜ γ)) , λ γᴿ α₀ α₁ → Aᴿ (σᴿ γᴿ) α₀ α₁

id : ∀{Γ} → Tms Γ Γ
id = (λ γ → γ) , λ γᴿ → γᴿ

_∘_ : ∀{Γ Δ Σ} → Tms Δ Σ → Tms Γ Δ → Tms Γ Σ
(σᶜ , σᴿ) ∘ (δᶜ , δᴿ) = (λ γ → σᶜ (δᶜ γ)) , λ γᴿ → σᴿ (δᴿ γᴿ)

ε : ∀{Γ} → Tms Γ ∙
ε = (λ _ → lift tt) , λ _ → tt

_,s_  : ∀{Γ Δ A}(σ : Tms Γ Δ) → Tm Γ (A [ σ ]T) → Tms Γ (Δ ▶ A)
(σᶜ , σᴿ) ,s (tᶜ , tᴿ) = (λ γ → σᶜ γ , tᶜ γ) , λ γᴿ → σᴿ γᴿ , tᴿ γᴿ

π₁ : ∀{Γ Δ}{A : Ty Δ} → Tms Γ (Δ ▶ A) → Tms Γ Δ
π₁ (σᶜ , σᴿ) = (λ γ → ₁ (σᶜ γ)) , λ γᴿ → ₁ (σᴿ γᴿ)

_[_]t : ∀{Γ Δ}{A : Ty Δ} → Tm Δ A → (σ : Tms Γ Δ) → Tm Γ (A [ σ ]T)
(tᶜ , tᴿ) [ (σᶜ , σᴿ) ]t = (λ γ → tᶜ (σᶜ γ)) , λ γᴿ → tᴿ (σᴿ γᴿ)

π₂ : ∀{Γ Δ}{A : Ty Δ}(σ : Tms Γ (Δ ▶ A)) → Tm Γ (A [ π₁ {A = A} σ ]T)
π₂ (σᶜ , σᴿ) = (λ γ → ₂ (σᶜ γ)) , λ γᴿ → ₂ (σᴿ γᴿ)

[id]T : ∀{Γ}{A : Ty Γ} → A [ id ]T ≡ A
[id]T = refl

[][]T : ∀{Γ Δ Σ}{A : Ty Σ}{σ : Tms Γ Δ}{δ : Tms Δ Σ}
        → A [ δ ]T [ σ ]T ≡ A [ _∘_ {Γ}{Δ}{Σ} δ σ ]T
[][]T = refl

idl : ∀{Γ Δ}{δ : Tms Γ Δ} → (_∘_ {Γ}{Δ}{Δ} id δ) ≡ δ
idl = refl

idr : ∀{Γ Δ}{δ : Tms Γ Δ} → (_∘_ {Γ}{Γ}{Δ} δ id) ≡ δ
idr = refl

ass : ∀{Γ Δ Σ Ω}{σ : Tms Σ Ω}{δ : Tms Δ Σ}{ν : Tms Γ Δ}
      → _∘_ {Γ}{Δ}{Ω}(_∘_ {Δ}{Σ}{Ω} σ δ) ν ≡ _∘_ {Γ}{Σ}{Ω} σ (_∘_ {Γ}{Δ}{Σ} δ ν)
ass = refl

,∘ : ∀{Γ Δ Σ}{δ : Tms Γ Δ}{σ : Tms Σ Γ}{A : Ty Δ}{t : Tm Γ (A [ δ ]T)}
      → (_∘_ {Σ}{Γ}{Δ ▶ A} (_,s_ {Γ}{Δ}{A} δ t) σ) ≡
        (_,s_ {Σ}{Δ}{A}(_∘_ {Σ}{Γ}{Δ} δ σ)(tr (Tm Σ) ([][]T {Σ}{Γ}{Δ}{A}{σ}{δ}) (_[_]t {Σ}{Γ}{A [ δ ]T} t σ)))
,∘ = refl

π₁β : ∀{Γ Δ}{A : Ty Δ}{δ : Tms Γ Δ}{a : Tm Γ (A [ δ ]T)}
      → (π₁ {Γ}{Δ}{A}(_,s_ {Γ}{Δ}{A} δ a)) ≡ δ
π₁β = refl

πη : ∀{Γ Δ}{A : Ty Δ}{δ : Tms Γ (Δ ▶ A)}
      → _,s_ {Γ}{Δ}{A}(π₁ {Γ}{Δ}{A} δ)(π₂ {Γ}{Δ}{A} δ) ≡ δ
πη = refl

εη : ∀{Γ}{σ : Tms Γ ∙}
      → σ ≡ ε
εη = refl

π₂β : ∀{Γ Δ}{A : Ty Δ}{δ : Tms Γ Δ}{a : Tm Γ (A [ δ ]T)}
      → tr (λ x → Tm Γ (A [ x ]T)) (π₁β {Γ}{Δ}{A}{δ}{a}) (π₂ {Γ}{Δ}{A}(_,s_ {Γ}{Δ}{A} δ a)) ≡ a
π₂β = refl

wk : ∀{Γ}{A : Ty Γ} → Tms (Γ ▶ A) Γ
wk {Γ}{A} = π₁ {Γ ▶ A}{Γ}{A} id

vz : ∀{Γ}{A : Ty Γ} → Tm (Γ ▶ A) (A [ wk ]T)
vz {Γ}{A} = π₂ {Γ ▶ A}{Γ}{A} id

vs : ∀{Γ}{A B : Ty Γ} → Tm Γ A → Tm (Γ ▶ B) (A [ wk ]T)
vs {Γ}{A}{B} x = _[_]t {Γ ▶ B}{Γ}{A} x (wk {Γ}{B})

<_> : ∀{Γ}{A : Ty Γ} → Tm Γ A → Tms Γ (Γ ▶ A)
<_> {Γ}{A} t = _,s_ {Γ}{Γ}{A} id (tr (Tm Γ) ([id]T {Γ}{A} ⁻¹) t)

infix 4 <_>

_^_ : {Γ Δ : Con}(σ : Tms Γ Δ)(A : Ty Δ) → Tms (Γ ▶ A [ σ ]T) (Δ ▶ A)
_^_ {Γ}{Δ} σ A =
  _,s_ {Γ ▶ A [ σ ]T}{Δ}{A}
       (_∘_ {Γ ▶ A [ σ ]T}{Γ}{Δ} σ wk)
       (tr (Tm (Γ ▶ A [ σ ]T)) ([][]T {Γ ▶ A [ σ ]T}{Γ}{Δ}{A}{wk{Γ}{A [ σ ]T}}{σ}) vz)

infixl 5 _^_

U : ∀{Γ} → Ty Γ
U {Γᶜ , Γᴿ} = (λ _ → Set) , λ _ a₀ a₁ → a₀ → a₁

U[] : ∀{Γ Δ}{σ : Tms Γ Δ} → _[_]T {Γ}{Δ} U σ ≡ U
U[] = refl

El : ∀{Γ}(a : Tm Γ U) → Ty Γ
El (aᶜ , aᴿ) = (λ γ → Lift (aᶜ γ)) , λ { γᴿ (lift x₀) (lift x₁) → aᴿ γᴿ x₀ ≡ x₁ }

El[] : ∀{Γ Δ}{σ : Tms Γ Δ}{a : Tm Δ U}
     → El a [ σ ]T ≡ El (tr (Tm Γ) (U[] {Γ}{Δ}{σ}) (_[_]t {Γ}{Δ}{U} a σ))
El[] = refl

Π : ∀{Γ}(a : Tm Γ U)(B : Ty (Γ ▶ El a)) → Ty Γ
Π (aᶜ , aᴿ) (Bᶜ , Bᴿ)
  = (λ γ → (α : aᶜ γ) → Bᶜ (γ , lift α))
  , λ {γ₀}{γ₁} γᴿ f₀ f₁ → (x₀ : aᶜ γ₀) → Bᴿ (γᴿ , refl) (f₀ x₀) (f₁ (aᴿ γᴿ x₀))

Π[] : ∀{Γ Δ}{σ : Tms Γ Δ}{a : Tm Δ U}{B : Ty (Δ ▶ El a)}
    → (Π a B) [ σ ]T ≡ Π (tr (Tm Γ) (U[] {Γ}{Δ}{σ}) (_[_]t {Γ}{Δ}{U} a σ))
                         (tr (λ x → Ty (Γ ▶ x)) (El[] {σ = σ}{a}) (B [ σ ^ El a ]T))
Π[] = refl

app : ∀{Γ}{a : Tm Γ U}{B : Ty (Γ ▶ El a)} → Tm Γ (Π a B) → Tm (Γ ▶ El a) B
app {Γᶜ , Γᴿ}{aᶜ , aᴿ}{Bᶜ , Bᴿ}(tᶜ , tᴿ)
  = (λ { (γ , lift α) → tᶜ γ α })
  , λ { {γ₀ , lift α₀}{γ₁ , lift α₁}(γᴿ , αᴿ) →  J (λ x z → Bᴿ (γᴿ , z)(tᶜ γ₀ α₀)(tᶜ γ₁ x))(tᴿ γᴿ α₀) αᴿ }

Id : {Γ : Con} (a : Tm Γ (U {Γ})) → Tm Γ (El {Γ} a) → Tm Γ (El {Γ} a) → Tm Γ (U {Γ})
Id {Γᶜ , Γᴿ}(aᶜ , aᴿ)(tᶜ , tᴿ)(uᶜ , uᴿ) =
   (λ γ → lower (tᶜ γ) ≡ lower (uᶜ γ)) , λ γᴿ p → tᴿ γᴿ ⁻¹ ◾ aᴿ γᴿ & p ◾ uᴿ γᴿ

Id[] : {Γ Δ : Con} {σ : Tms Γ Δ} {a : Tm Δ (U {Δ})}
         {t u : Tm Δ (El {Δ} a)} →
         _≡_ {j} {Tm Γ (U {Γ})} (_[_]t {Γ} {Δ} {U {Δ}} (Id {Δ} a t u) σ)
         (Id {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ) (_[_]t {Γ} {Δ} {El {Δ} a} t σ)
          (_[_]t {Γ} {Δ} {El {Δ} a} u σ))
Id[] = refl

LargeId : {Γ : Con} (a : Tm Γ U) → Tm Γ (El a) → Tm Γ (El a) → Ty Γ
LargeId {Γᶜ , Γᴿ}(aᶜ , aᴿ)(tᶜ , tᴿ)(uᶜ , uᴿ) =
  (λ γ → tᶜ γ ≡ uᶜ γ) , λ γᴿ p₀ p₁ →
     (tᴿ γᴿ ⁻¹ ◾ ((λ x → aᴿ γᴿ (lower x)) & p₀) ◾ uᴿ γᴿ) ≡ (lower & p₁)

ΠNI : ∀ {Γ}(A : Set) → (A → Ty Γ) → Ty Γ
ΠNI {Γᶜ , Γᴿ} A B = (λ γ → (α : A) → B α .₁ γ) ,
                   λ γᴿ t₀ t₁ → (α : A) → B α .₂ γᴿ (t₀ α) (t₁ α)

ΠNI[] : ∀ {Γ Δ}{σ : Tms Γ Δ}{A : Set}{B : A → Ty Δ}
        → ΠNI A B [ σ ]T ≡ ΠNI A (λ α → B α [ σ ]T)
ΠNI[] = refl

Πₙᵢ : ∀ {Γ}(A : Set) → (A → Tm Γ U) → Tm Γ U
Πₙᵢ {Γᶜ , Γᴿ} A b =
  (λ γ → (α : A) → b α .₁ γ) , (λ γᴿ f α → b α .₂ γᴿ (f α))

-- Πₙᵢ[] : ∀ {Γ Δ}{σ : Tms Γ Δ}{A : Set}{b : A → Tm Δ U}
--         → Πₙᵢ A b [ σ ]t ≡ Πₙᵢ A (λ α → b α [ σ ]t)
-- Πₙᵢ[] = refl

------------------------------------------------------------

postulate
  Conʳ : S.Con → Con
  Tyʳ  : ∀ {Γ} → S.Ty Γ → Ty (Conʳ Γ)
  Tmʳ  : ∀ {Γ A} → S.Tm Γ A → Tm (Conʳ Γ) (Tyʳ A)
  Tmsʳ : ∀ {Γ Δ} → S.Tms Γ Δ → Tms (Conʳ Γ) (Conʳ Δ)

postulate
  ∙ʳ   : Conʳ S.∙ ≡ ∙
  ,ʳ   : ∀ Γ A → Conʳ (Γ S.▶ A) ≡  Conʳ Γ ▶ Tyʳ A
  []Tʳ : ∀ Γ Δ (A : S.Ty Δ) (σ : S.Tms Γ Δ) → Tyʳ (A S.[ σ ]T) ≡ Tyʳ A [ Tmsʳ σ ]T
{-# REWRITE ∙ʳ ,ʳ []Tʳ #-}

postulate
  idʳ  : ∀ {Γ} → Tmsʳ (S.id {Γ}) ≡ id

  ∘ʳ   : {Γ Δ : S.Con} {Σ : S.Con} (σ : S.Tms Δ Σ) (δ : S.Tms Γ Δ)
         → Tmsʳ (σ S.∘ δ) ≡ _∘_ {Conʳ Γ}{Conʳ Δ}{Conʳ Σ}(Tmsʳ σ) (Tmsʳ δ)

  ,sʳ :
    {Γ Δ : S.Con} (σ : S.Tms Γ Δ) {A : S.Ty Δ}
    (t : S.Tm Γ (S._[_]T {Γ} {Δ} A σ))
    → Tmsʳ {Γ}{Δ S.▶ A}(σ S.,s t) ≡ _,s_ {Conʳ Γ}{Conʳ Δ}{Tyʳ A}(Tmsʳ σ) (Tmʳ t)

  εʳ   : ∀ {Γ} → Tmsʳ (S.ε {Γ}) ≡ ε

  π₁ʳ  :
    {Γ Δ : S.Con} {A : S.Ty Δ} (σ : S.Tms Γ (Δ S.▶ A)) →
    Tmsʳ (S.π₁ σ) ≡ π₁ {Conʳ Γ}{Conʳ Δ}{Tyʳ A} (Tmsʳ σ)
{-# REWRITE idʳ εʳ ∘ʳ ,sʳ π₁ʳ #-}

postulate
  π₂ʳ : {Γ Δ : S.Con} {A : S.Ty Δ} (σ : S.Tms Γ (Δ S.▶ A)) →
    Tmʳ (S.π₂ σ) ≡ π₂ {Conʳ Γ}{Conʳ Δ}{Tyʳ A} (Tmsʳ σ)
{-# REWRITE π₂ʳ #-}

postulate
  []tʳ : ∀{Γ Δ}{A : S.Ty Δ}(t : S.Tm Δ A)(σ : S.Tms Γ Δ) →
         Tmʳ (t S.[ σ ]t)
         ≡ _[_]t {Conʳ Γ}{Conʳ Δ}{Tyʳ A} (Tmʳ t) (Tmsʳ σ)
  Uʳ   : ∀ {Γ} → Tyʳ (S.U {Γ}) ≡ U
{-# REWRITE []tʳ Uʳ #-}

postulate
  Elʳ : ∀ {Γ}{a : S.Tm Γ S.U} → Tyʳ (S.El a) ≡ El (Tmʳ a)
{-# REWRITE Elʳ #-}

postulate
  Πʳ : ∀ {Γ}(a : S.Tm Γ S.U)(B : S.Ty (Γ S.▶ S.El a)) → Tyʳ (S.Π a B) ≡ Π (Tmʳ a) (Tyʳ B)
{-# REWRITE Πʳ #-}

postulate
  appʳ :
    {Γ : S.Con} {a : S.Tm Γ (S.U {Γ})} {B : S.Ty (Γ S.▶ S.El {Γ} a)}
    (t : S.Tm Γ (S.Π {Γ} a B)) →
    Tmʳ (S.app t) ≡ app {Conʳ Γ}{Tmʳ a}{Tyʳ B} (Tmʳ t)
{-# REWRITE appʳ #-}

postulate
  Idʳ : {Γ : S.Con} (a : S.Tm Γ S.U)(t u : S.Tm Γ (S.El a)) →
        Tmʳ (S.Id a t u) ≡ Id {Conʳ Γ}(Tmʳ a)(Tmʳ t)(Tmʳ u)
{-# REWRITE Idʳ #-}

postulate
  LargeIdʳ : {Γ : S.Con} (a : S.Tm Γ S.U)(t u : S.Tm Γ (S.El a)) →
        Tyʳ (S.LargeId a t u) ≡ LargeId {Conʳ Γ}(Tmʳ a)(Tmʳ t)(Tmʳ u)
{-# REWRITE LargeIdʳ #-}

postulate
  ΠNIʳ : ∀ {Γ}(A : Set)(B : A → S.Ty Γ) → S.Ty Γ
       → Tyʳ (S.ΠNI A B) ≡ ΠNI A (λ α → Tyʳ (B α))
{-# REWRITE ΠNIʳ #-}

postulate
  Πₙᵢʳ : ∀ {Γ}(A : Set)(B : A → S.Tm Γ S.U) → S.Tm Γ S.U
       → Tmʳ (S.Πₙᵢ A B) ≡ Πₙᵢ A (λ α → Tmʳ (B α))
{-# REWRITE Πₙᵢʳ #-}
