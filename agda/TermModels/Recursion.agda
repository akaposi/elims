{-# OPTIONS --rewriting #-}

{-
USAGE:
  - copy & paste everything below to a new module
  - remove postulates & add implementation
  - Rename and export stuff as needed
-}

module Recursion where

open import Lib hiding (_∘_; id; Σ)
open import Level

import Syntax as S

infixl 5 _▶_
infixl 7 _[_]T
infixl 5 _,s_
infix  6 _∘_
infixl 8 _[_]t

postulate
  i : Level
  j : Level
  Con : Set i
  Ty : Con → Set i
  Tm : (Γ : Con) → Ty Γ → Set  j
  Tms : Con → Con → Set j

  ∙     : Con
  _▶_   : (Γ : Con) → Ty Γ → Con

  _[_]T : ∀{Γ Δ} → Ty Δ → Tms Γ Δ → Ty Γ

  id    : ∀{Γ} → Tms Γ Γ
  _∘_   : ∀{Γ Δ Σ} → Tms Δ Σ → Tms Γ Δ → Tms Γ Σ
  ε     : ∀{Γ} → Tms Γ ∙
  _,s_  : ∀{Γ Δ}(σ : Tms Γ Δ){A : Ty Δ} → Tm Γ (A [ σ ]T) → Tms Γ (Δ ▶ A)
  π₁    : ∀{Γ Δ}{A : Ty Δ} → Tms Γ (Δ ▶ A) → Tms Γ Δ

  _[_]t : ∀{Γ Δ}{A : Ty Δ} → Tm Δ A → (σ : Tms Γ Δ) → Tm Γ (A [ σ ]T)
  π₂    : ∀{Γ Δ}{A : Ty Δ}(σ : Tms Γ (Δ ▶ A)) → Tm Γ (A [ π₁ σ ]T)

  [id]T : ∀{Γ}{A : Ty Γ} → A [ id ]T ≡ A
  [][]T : ∀{Γ Δ Σ}{A : Ty Σ}{σ : Tms Γ Δ}{δ : Tms Δ Σ}
          → A [ δ ]T [ σ ]T ≡ A [ δ ∘ σ ]T

  idl   : ∀{Γ Δ}{σ : Tms Γ Δ} → (id ∘ σ) ≡ σ
  idr   : ∀{Γ Δ}{σ : Tms Γ Δ} → (σ ∘ id) ≡ σ
  ass   : ∀{Γ Δ Σ Ω}{σ : Tms Σ Ω}{δ : Tms Δ Σ}{ν : Tms Γ Δ}
        → (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν)

{-# REWRITE [][]T [id]T idl idr ass #-}

postulate
  ,∘    : {Γ Δ Σ : Con} {δ : Tms Γ Δ} {σ : Tms Σ Γ} {A : Ty Δ}
            {t : Tm Γ (_[_]T {Γ} {Δ} A δ)} →
            _≡_ {j} {Tms Σ (Δ ▶ A)}
            (_∘_ {Σ} {Γ} {Δ ▶ A} (_,s_ {Γ} {Δ} δ {A} t) σ)
            (_,s_ {Σ} {Δ} (_∘_ {Σ} {Γ} {Δ} δ σ) {A}
             (_[_]t {Σ} {Γ} {_[_]T {Γ} {Δ} A δ} t σ))
  π₁β   : {Γ Δ : Con} {A : Ty Δ} {σ : Tms Γ Δ}
            {t : Tm Γ (_[_]T {Γ} {Δ} A σ)} →
            _≡_ {j} {Tms Γ Δ} (π₁ {Γ} {Δ} {A} (_,s_ {Γ} {Δ} σ {A} t)) σ
  πη    : {Γ Δ : Con} {A : Ty Δ} {σ : Tms Γ (Δ ▶ A)} →
            _≡_ {j} {Tms Γ (Δ ▶ A)}
            (_,s_ {Γ} {Δ} (π₁ {Γ} {Δ} {A} σ) {A} (π₂ {Γ} {Δ} {A} σ)) σ
  εη    : {Γ : Con} {σ : Tms Γ ∙} → _≡_ {j} {Tms Γ ∙} σ (ε {Γ})
{-# REWRITE ,∘ π₁β #-}

postulate
  π₂β   : {Γ Δ : Con} {A : Ty Δ} {σ : Tms Γ Δ}
           {t : Tm Γ (_[_]T {Γ} {Δ} A σ)} →
           _≡_ {j} {Tm Γ (_[_]T {Γ} {Δ} A σ)}
           (π₂ {Γ} {Δ} {A} (_,s_ {Γ} {Δ} σ {A} t)) t
{-#  REWRITE π₂β #-}

postulate
  [id]t : ∀ {Γ}{A}{t : Tm Γ A} → t [ id ]t ≡ t
  [][]t : ∀{Γ Δ Σ}{A : Ty Σ}{σ : Tms Γ Δ}{δ : Tms Δ Σ}{t : Tm Σ A}
          → (t [ δ ]t) [ σ ]t ≡ t [ δ ∘ σ ]t
{-# REWRITE [id]t [][]t #-} -- RULE [][]t NOT FIRING

wk : ∀{Γ}{A : Ty Γ} → Tms (Γ ▶ A) Γ
wk = π₁ id

vz : {Γ : Con} {A : Ty Γ} →
        Tm (Γ ▶ A) (_[_]T {Γ ▶ A} {Γ} A (π₁ {Γ ▶ A} {Γ} {A} (id {Γ ▶ A})))
vz {z} {z₁} = π₂ {z ▶ z₁} {z} {z₁} (id {z ▶ z₁})

vs : {Γ : Con} {A B : Ty Γ} →
       Tm Γ A →
       Tm (Γ ▶ B) (_[_]T {Γ ▶ B} {Γ} A (π₁ {Γ ▶ B} {Γ} {B} (id {Γ ▶ B})))
vs {z} {z₁} {z₂} x =
  _[_]t {z ▶ z₂} {z} {z₁} x (π₁ {z ▶ z₂} {z} {z₂} (id {z ▶ z₂}))

<_> : ∀{Γ}{A : Ty Γ} → Tm Γ A → Tms Γ (Γ ▶ A)
<_> {z} {z₁} t = _,s_ {z} {z} (id {z}) {z₁} t

infix 4 <_>

_^_ : {Γ Δ : Con} (σ : Tms Γ Δ) (A : Ty Δ) → Tms (Γ ▶ _[_]T {Γ} {Δ} A σ) (Δ ▶ A)
_^_ {z} {z₁} σ A =
  _,s_ {z ▶ _[_]T {z} {z₁} A σ} {z₁}
  (_∘_ {z ▶ _[_]T {z} {z₁} A σ} {z} {z₁} σ
   (π₁ {z ▶ _[_]T {z} {z₁} A σ} {z} {_[_]T {z} {z₁} A σ}
    (id {z ▶ _[_]T {z} {z₁} A σ})))
  {A}
  (π₂ {z ▶ _[_]T {z} {z₁} A σ} {z} {_[_]T {z} {z₁} A σ}
   (id {z ▶ _[_]T {z} {z₁} A σ}))

infixl 5 _^_

postulate
  U    : ∀{Γ} → Ty Γ
  U[]  : {Γ Δ : Con} {σ : Tms Γ Δ} → _≡_ {i} {Ty Γ} (_[_]T {Γ} {Δ} (U {Δ}) σ) (U {Γ})
  El   : ∀{Γ}(a : Tm Γ U) → Ty Γ
{-# REWRITE U[] #-}
postulate
  El[] : {Γ Δ : Con} {σ : Tms Γ Δ} {a : Tm Δ (U {Δ})} →
          _≡_ {i} {Ty Γ} (_[_]T {Γ} {Δ} (El {Δ} a) σ)
         (El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ))
{-# REWRITE El[] #-}

postulate
  plzwork : (Γ Δ : Con) (a : Tm Δ (U {Δ})) →
            Ty (Δ ▶ El {Δ} a) →
            (σ : Tms Γ Δ) →
            _≡_ {j}
            {Tm (Γ ▶ El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ))
             (U {Γ ▶ El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)})}
            (_[_]t {Γ ▶ El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)} {Γ} {U {Γ}}
             (_[_]t {Γ} {Δ} {U {Δ}} a σ)
             (π₁ {Γ ▶ El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)} {Γ}
              {El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)}
              (id {Γ ▶ El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)})))
            (_[_]t {Γ ▶ El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)} {Δ} {U {Δ}} a
             (_∘_ {Γ ▶ El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)} {Γ} {Δ} σ
              (π₁ {Γ ▶ El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)} {Γ}
               {El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)}
               (id {Γ ▶ El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)}))))
{-# REWRITE plzwork #-}

postulate
  Π : {Γ : Con} (a : Tm Γ (U {Γ})) → Ty (Γ ▶ El {Γ} a) → Ty Γ

  Π[] : {Γ Δ : Con} {σ : Tms Γ Δ} {a : Tm Δ (U {Δ})}
        {B : Ty (Δ ▶ El {Δ} a)} →
        _≡_ {i} {Ty Γ} (_[_]T {Γ} {Δ} (Π {Δ} a B) σ)
        (Π {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)
         (_[_]T {Γ ▶ El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)} {Δ ▶ El {Δ} a} B
          (_,s_ {Γ ▶ El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)} {Δ}
           (_∘_ {Γ ▶ El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)} {Γ} {Δ} σ
            (π₁ {Γ ▶ El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)} {Γ}
             {El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)}
             (id {Γ ▶ El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)})))
           {El {Δ} a}
           (π₂ {Γ ▶ El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)} {Γ}
            {El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)}
            (id {Γ ▶ El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)})))))
{-# REWRITE Π[] #-}

postulate
  app : {Γ : Con} {a : Tm Γ (U {Γ})} {B : Ty (Γ ▶ El {Γ} a)} →
        Tm Γ (Π {Γ} a B) → Tm (Γ ▶ El {Γ} a) B

  app[] : {Γ Δ : Con} {σ : Tms Γ Δ} {a : Tm Δ (U {Δ})}
           {B : Ty (Δ ▶ El {Δ} a)} {t : Tm Δ (Π {Δ} a B)} →
           _≡_ {j}
           {Tm (Γ ▶ El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ))
            (_[_]T {Γ ▶ El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)} {Δ ▶ El {Δ} a} B
             (_,s_ {Γ ▶ El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)} {Δ}
              (_∘_ {Γ ▶ El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)} {Γ} {Δ} σ
               (π₁ {Γ ▶ El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)} {Γ}
                {El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)}
                (id {Γ ▶ El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)})))
              {El {Δ} a}
              (π₂ {Γ ▶ El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)} {Γ}
               {El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)}
               (id {Γ ▶ El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)}))))}
           (_[_]t {Γ ▶ El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)} {Δ ▶ El {Δ} a} {B}
            (app {Δ} {a} {B} t)
            (_,s_ {Γ ▶ El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)} {Δ}
             (_∘_ {Γ ▶ El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)} {Γ} {Δ} σ
              (π₁ {Γ ▶ El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)} {Γ}
               {El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)}
               (id {Γ ▶ El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)})))
             {El {Δ} a}
             (π₂ {Γ ▶ El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)} {Γ}
              {El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)}
              (id {Γ ▶ El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)}))))
           (app {Γ} {_[_]t {Γ} {Δ} {U {Δ}} a σ}
            {_[_]T {Γ ▶ El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)} {Δ ▶ El {Δ} a} B
             (_,s_ {Γ ▶ El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)} {Δ}
              (_∘_ {Γ ▶ El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)} {Γ} {Δ} σ
               (π₁ {Γ ▶ El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)} {Γ}
                {El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)}
                (id {Γ ▶ El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)})))
              {El {Δ} a}
              (π₂ {Γ ▶ El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)} {Γ}
               {El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)}
               (id {Γ ▶ El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)})))}
            (_[_]t {Γ} {Δ} {Π {Δ} a B} t σ))
{-# REWRITE app[] #-}

_$_ : {Γ : Con} {a : Tm Γ (U {Γ})} {B : Ty (Γ ▶ El {Γ} a)} →
      Tm Γ (Π {Γ} a B) →
      (u : Tm Γ (El {Γ} a)) →
      Tm Γ
      (_[_]T {Γ} {Γ ▶ El {Γ} a} B (_,s_ {Γ} {Γ} (id {Γ}) {El {Γ} a} u))
_$_ {z} {z₁} {z₂} t u =
  _[_]t {z} {z ▶ El {z} z₁} {z₂} (app {z} {z₁} {z₂} t)
  (_,s_ {z} {z} (id {z}) {El {z} z₁} u)

postulate
  Id   : {Γ : Con} (a : Tm Γ (U {Γ})) → Tm Γ (El {Γ} a) → Tm Γ (El {Γ} a) → Tm Γ (U {Γ})
  Id[] : {Γ Δ : Con} {σ : Tms Γ Δ} {a : Tm Δ (U {Δ})}
           {t u : Tm Δ (El {Δ} a)} →
           _≡_ {j} {Tm Γ (U {Γ})} (_[_]t {Γ} {Δ} {U {Δ}} (Id {Δ} a t u) σ)
           (Id {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ) (_[_]t {Γ} {Δ} {El {Δ} a} t σ)
            (_[_]t {Γ} {Δ} {El {Δ} a} u σ))
{-# REWRITE Id[] #-}

-- Recursors & computation (no β for equalities)
--------------------------------------------------------------------------------

postulate
  Conʳ : S.Con → Con
  Tyʳ  : ∀ {Γ} → S.Ty Γ → Ty (Conʳ Γ)
  Tmʳ  : ∀ {Γ A} → S.Tm Γ A → Tm (Conʳ Γ) (Tyʳ A)
  Tmsʳ : ∀ {Γ Δ} → S.Tms Γ Δ → Tms (Conʳ Γ) (Conʳ Δ)

postulate
  ∙ʳ   : Conʳ S.∙ ≡ ∙
  ,ʳ   : ∀ Γ A → Conʳ (Γ S.▶ A) ≡ Conʳ Γ ▶ Tyʳ A
  []Tʳ : (Γ Δ : S.Con) (A : S.Ty Δ) (σ : S.Tms Γ Δ) →
          _≡_ {i} {Ty (Conʳ Γ)} (Tyʳ {Γ} (S._[_]T {Γ} {Δ} A σ))
          (_[_]T {Conʳ Γ} {Conʳ Δ} (Tyʳ {Δ} A) (Tmsʳ {Γ} {Δ} σ))
{-# REWRITE ∙ʳ ,ʳ []Tʳ #-}

postulate
  []tʳ : {Γ Δ : S.Con} {A : S.Ty Δ} (t : S.Tm Δ A) (σ : S.Tms Γ Δ) →
           _≡_ {j}
           {Tm (Conʳ Γ)
            (_[_]T {Conʳ Γ} {Conʳ Δ} (Tyʳ {Δ} A) (Tmsʳ {Γ} {Δ} σ))}
           (Tmʳ {Γ} {S._[_]T {Γ} {Δ} A σ} (S._[_]t {Γ} {Δ} {A} t σ))
           (_[_]t {Conʳ Γ} {Conʳ Δ} {Tyʳ {Δ} A} (Tmʳ {Δ} {A} t)
            (Tmsʳ {Γ} {Δ} σ))

  idʳ  : {Γ : S.Con} →
           _≡_ {j} {Tms (Conʳ Γ) (Conʳ Γ)} (Tmsʳ {Γ} {Γ} (S.id {Γ}))
           (id {Conʳ Γ})

  ∘ʳ   : {Γ Δ : S.Con} {Σ : S.Con} {σ : S.Tms Δ Σ} {δ : S.Tms Γ Δ} →
           _≡_ {j} {Tms (Conʳ Γ) (Conʳ Σ)}
           (Tmsʳ {Γ} {Σ} (S._∘_ {Γ} {Δ} {Σ} σ δ))
           (_∘_ {Conʳ Γ} {Conʳ Δ} {Conʳ Σ} (Tmsʳ {Δ} {Σ} σ)
            (Tmsʳ {Γ} {Δ} δ))

  εʳ   : {Γ : S.Con} → _≡_ {j} {Tms (Conʳ Γ) ∙} (Tmsʳ {Γ} {S.∙} (S.ε {Γ})) (ε {Conʳ Γ})

  ,sʳ  : {Γ Δ : S.Con} (σ : S.Tms Γ Δ) {A : S.Ty Δ}
         (t : S.Tm Γ (S._[_]T {Γ} {Δ} A σ)) →
         _≡_ {j} {Tms (Conʳ Γ) (Conʳ Δ ▶ Tyʳ {Δ} A)}
         (Tmsʳ {Γ} {Δ S.▶ A} (S._,s_ {Γ} {Δ} σ {A} t))
         (_,s_ {Conʳ Γ} {Conʳ Δ} (Tmsʳ {Γ} {Δ} σ) {Tyʳ {Δ} A}
          (Tmʳ {Γ} {S._[_]T {Γ} {Δ} A σ} t))

  π₁ʳ  : {Γ Δ : S.Con} {A : S.Ty Δ} (σ : S.Tms Γ (Δ S.▶ A)) →
          _≡_ {j} {Tms (Conʳ Γ) (Conʳ Δ)} (Tmsʳ {Γ} {Δ} (S.π₁ {Γ} {Δ} {A} σ))
          (π₁ {Conʳ Γ} {Conʳ Δ} {Tyʳ {Δ} A} (Tmsʳ {Γ} {Δ S.▶ A} σ))

{-# REWRITE []tʳ idʳ ∘ʳ εʳ ,sʳ π₁ʳ #-}

postulate
  π₂ʳ : {Γ Δ : S.Con} {A : S.Ty Δ} (σ : S.Tms Γ (Δ S.▶ A)) →
          _≡_ {j}
            {Tm (Conʳ Γ)
           (_[_]T {Conʳ Γ} {Conʳ Δ} (Tyʳ {Δ} A)
            (π₁ {Conʳ Γ} {Conʳ Δ} {Tyʳ {Δ} A} (Tmsʳ {Γ} {Δ S.▶ A} σ)))}
          (Tmʳ {Γ} {S._[_]T {Γ} {Δ} A (S.π₁ {Γ} {Δ} {A} σ)}
           (S.π₂ {Γ} {Δ} {A} σ))
          (π₂ {Conʳ Γ} {Conʳ Δ} {Tyʳ {Δ} A} (Tmsʳ {Γ} {Δ S.▶ A} σ))

  Uʳ  : {Γ : S.Con} → _≡_ {i} {Ty (Conʳ Γ)} (Tyʳ {Γ} (S.U {Γ})) (U {Conʳ Γ})
{-# REWRITE π₂ʳ Uʳ #-}

postulate
  Elʳ : {Γ : S.Con} {a : S.Tm Γ (S.U {Γ})} → _≡_ {i} {Ty (Conʳ Γ)} (Tyʳ {Γ} (S.El {Γ} a))
                                                 (El {Conʳ Γ} (Tmʳ {Γ} {S.U {Γ}} a))
{-# REWRITE Elʳ #-}

postulate
  Πʳ : {Γ : S.Con} (a : S.Tm Γ (S.U {Γ})) (B : S.Ty (Γ S.▶ S.El {Γ} a)) →
        _≡_ {i} {Ty (Conʳ Γ)} (Tyʳ {Γ} (S.Π {Γ} a B))
        (Π {Conʳ Γ} (Tmʳ {Γ} {S.U {Γ}} a) (Tyʳ {Γ S.▶ S.El {Γ} a} B))
{-# REWRITE Πʳ #-}

postulate
  appʳ : {Γ : S.Con} {a : S.Tm Γ (S.U {Γ})} {B : S.Ty (Γ S.▶ S.El {Γ} a)}
          (t : S.Tm Γ (S.Π {Γ} a B)) →
          _≡_ {j}
          {Tm (Conʳ Γ ▶ El {Conʳ Γ} (Tmʳ {Γ} {S.U {Γ}} a))
           (Tyʳ {Γ S.▶ S.El {Γ} a} B)}
          (Tmʳ {Γ S.▶ S.El {Γ} a} {B} (S.app {Γ} {a} {B} t))
          (app {Conʳ Γ} {Tmʳ {Γ} {S.U {Γ}} a} {Tyʳ {Γ S.▶ S.El {Γ} a} B}
           (Tmʳ {Γ} {S.Π {Γ} a B} t))
{-# REWRITE appʳ #-}

postulate
  Idʳ : {Γ : S.Con} (a : S.Tm Γ S.U)(t u : S.Tm Γ (S.El a)) →
        Tmʳ (S.Id a t u) ≡ Id {Conʳ Γ}(Tmʳ a)(Tmʳ t)(Tmʳ u)
{-# REWRITE Idʳ #-}
