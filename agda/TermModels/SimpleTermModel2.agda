{-# OPTIONS --without-K --rewriting #-}

{-
Attempting a term model for InductiveInductive.  We do a nice little
cheat: we use non-dependent elimination on II codes, and do a Σ model
where the first projection is just the syntax, and the rest depends on
the first projections. This way we are spared from the horrors of
dependent elimination. On the other hand, we have to separately prove
that the first projection is indeed the syntax. The setup is a bit
like using unique recursion instead of elimination.
-}

open import Lib hiding (_∘_; id)
open import Level
import Syntax as S
import CR as CR
open import CR using (Conʳ; Tyʳ; Tmʳ; Tmsʳ)

module SimpleTermModel2 (Γ* : S.Con) where

-- using equality reflection and large equality

infixl 5 _▶_
infixl 7 _[_]T
infixl 5 _,s_
infix  6 _∘_
infixl 8 _[_]t

i : Level
i = suc zero

j : Level
j = suc zero

Con : Set i
Con = Σ S.Con λ Γ → S.Tms Γ* Γ → Conʳ Γ .₁

Ty : Con → Set i
Ty (Γ , Γᵀ) = Σ (S.Ty Γ) λ A → (σ* : S.Tms Γ* Γ)(t : S.Tm Γ* (A S.[ σ* ]T))
                              → Tyʳ A .₁ (Γᵀ σ*)

Tm : (Γ : Con) → Ty Γ → Set  j
Tm (Γ , Γᵀ) (A , Aᵀ) =
  Σ (S.Tm Γ A) λ t → (σ* : S.Tms Γ* Γ) → Aᵀ σ* (t S.[ σ* ]t) ≡ Tmʳ t .₁ (Γᵀ σ*)

Tms : Con → Con → Set j
Tms (Γ , Γᵀ) (Δ , Δᵀ) = Σ (S.Tms Γ Δ) λ σ → (σ* : S.Tms Γ* Γ)
                        → Δᵀ (σ S.∘ σ*) ≡ Tmsʳ σ .₁ (Γᵀ σ*)

∙ : Con
∙ = S.∙ , (λ _ → lift tt)

_▶_   : (Γ : Con) → Ty Γ → Con
(Γ , Γᵀ) ▶ (A , Aᵀ) = (Γ S.▶ A) , (λ σ* → Γᵀ (S.π₁ σ*) , Aᵀ (S.π₁ σ*) (S.π₂ σ*))

_[_]T : ∀{Γ Δ} → Ty Δ → Tms Γ Δ → Ty Γ
_[_]T {Γ , Γᵀ}{Δ , Δᵀ}(A , Aᵀ) (σ , σᵀ) =
  (A S.[ σ ]T) , λ σ* t → tr (Tyʳ A .₁) (σᵀ σ*) (Aᵀ (σ S.∘ σ*) t)

id : ∀{Γ} → Tms Γ Γ
id {Γ , Γᵀ} = S.id , λ σ* → refl

_∘_   : ∀{Γ Δ Σ} → Tms Δ Σ → Tms Γ Δ → Tms Γ Σ
_∘_ {Γ , Γᵀ}{Δ , Δᵀ}{Ξ , Ξᵀ}(σ , σᵀ)(δ , δᵀ) =
  (σ S.∘ δ) , (λ σ* → σᵀ (δ S.∘ σ*) ◾ (Tmsʳ σ .₁) & δᵀ σ*)

ε     : ∀{Γ} → Tms Γ ∙
ε {Γ , Γᵀ} = S.ε , (λ σ* → refl)

_,s_  : ∀{Γ Δ}(σ : Tms Γ Δ){A : Ty Δ} → Tm Γ (A [ σ ]T) → Tms Γ (Δ ▶ A)
_,s_ {Γ , Γᵀ}{Δ , Δᵀ}(σ , σᵀ){A , Aᵀ}(t , tᵀ) =
  (σ S.,s t) , λ σ* → ,≡ (σᵀ σ*)
    (J (λ _ eq → coe ((λ v → ₁ (Tyʳ A) v) & eq) (Aᵀ (σ S.∘ σ*) (t S.[ σ* ]t))
               ≡ tr (Tyʳ A .₁) eq (Aᵀ (σ S.∘ σ*) (t S.[ σ* ]t)))
       refl (σᵀ σ*) ◾ tᵀ σ*)

π₁    : ∀{Γ Δ}{A : Ty Δ} → Tms Γ (Δ ▶ A) → Tms Γ Δ
π₁ {Γ , Γᵀ}{Δ , Δᵀ}{A , Aᵀ}(σ , σᵀ) =
  (S.π₁ σ) , (λ σ* → {!₁ & σᵀ σ*!})

_[_]t : ∀{Γ Δ}{A : Ty Δ} → Tm Δ A → (σ : Tms Γ Δ) → Tm Γ (A [ σ ]T)
_[_]t {Γ , Γᵀ}{Δ , Δᵀ}{A , Aᵀ}(t , tᵀ)(σ , σᵀ) =
   (t S.[ σ ]t) , (λ σ* → J (λ σσ* eq → tr (Tyʳ A .₁) eq (Aᵀ (σ S.∘ σ*) (t S.[ σ S.∘ σ* ]t)) ≡
      ₁ (Tmʳ t) σσ*) (tᵀ (σ S.∘ σ*)) (σᵀ σ*))

π₂ : {Γ Δ : Con} {A : Ty Δ} (σ : Tms Γ (Δ ▶ A)) → Tm Γ (_[_]T {Γ} {Δ} A (π₁ {Γ} {Δ} {A} σ))
π₂ {Γ , Γᵀ}{Δ , Δᵀ}{A , Aᵀ}(σ , σᵀ) =
  (S.π₂ σ) , λ σ* → {!S.π₁β!}

[id]T : ∀{Γ}{A : Ty Γ} → A [ id ]T ≡ A
[id]T {Γ , Γᵀ}{A , Aᵀ} = refl

[][]T : {Γ Δ Σ : Con} {A : Ty Σ} {σ : Tms Γ Δ} {δ : Tms Δ Σ} →
  _≡_ {i} {Ty Γ} (_[_]T {Γ} {Σ} A (_∘_ {Γ} {Δ} {Σ} δ σ))
  (_[_]T {Γ} {Σ} A (_∘_ {Γ} {Δ} {Σ} δ σ))
[][]T {Γ , Γᵀ}{Δ , Δᵀ}{Σ , Σᵀ}{A , Aᵀ}{σ , σᵀ}{δ , δᵀ} = refl

idl   : {Γ Δ : Con} {σ : Tms Γ Δ} → _≡_ {j} {Tms Γ Δ} σ σ
idl = refl

idr   : {Γ Δ : Con} {σ : Tms Γ Δ} → _≡_ {j} {Tms Γ Δ} σ σ
idr = refl

ass   : {Γ Δ Σ Ω : Con} {σ : Tms Σ Ω} {δ : Tms Δ Σ} {ν : Tms Γ Δ} →
  _≡_ {j} {Tms Γ Ω} (_∘_ {Γ} {Σ} {Ω} σ (_∘_ {Γ} {Δ} {Σ} δ ν))
  (_∘_ {Γ} {Σ} {Ω} σ (_∘_ {Γ} {Δ} {Σ} δ ν))
ass = refl

-- {-# REWRITE [][]T [id]T idl idr ass #-}

-- ,∘    : {Γ Δ Σ : Con} {δ : Tms Γ Δ} {σ : Tms Σ Γ} {A : Ty Δ}
--             {t : Tm Γ (_[_]T {Γ} {Δ} A δ)} →
--             _≡_ {j} {Tms Σ (Δ ▶ A)}
--             (_∘_ {Σ} {Γ} {Δ ▶ A} (_,s_ {Γ} {Δ} δ {A} t) σ)
--             (_,s_ {Σ} {Δ} (_∘_ {Σ} {Γ} {Δ} δ σ) {A}
--              (_[_]t {Σ} {Γ} {_[_]T {Γ} {Δ} A δ} t σ))
-- ,∘ = ?

--   π₁β   : {Γ Δ : Con} {A : Ty Δ} {σ : Tms Γ Δ}
--             {t : Tm Γ (_[_]T {Γ} {Δ} A σ)} →
--             _≡_ {j} {Tms Γ Δ} (π₁ {Γ} {Δ} {A} (_,s_ {Γ} {Δ} σ {A} t)) σ
--   πη    : {Γ Δ : Con} {A : Ty Δ} {σ : Tms Γ (Δ ▶ A)} →
--             _≡_ {j} {Tms Γ (Δ ▶ A)}
--             (_,s_ {Γ} {Δ} (π₁ {Γ} {Δ} {A} σ) {A} (π₂ {Γ} {Δ} {A} σ)) σ
--   εη    : {Γ : Con} {σ : Tms Γ ∙} → _≡_ {j} {Tms Γ ∙} σ (ε {Γ})
-- {-# REWRITE ,∘ π₁β #-}

-- postulate
--   π₂β   : {Γ Δ : Con} {A : Ty Δ} {σ : Tms Γ Δ}
--            {t : Tm Γ (_[_]T {Γ} {Δ} A σ)} →
--            _≡_ {j} {Tm Γ (_[_]T {Γ} {Δ} A σ)}
--            (π₂ {Γ} {Δ} {A} (_,s_ {Γ} {Δ} σ {A} t)) t
-- {-#  REWRITE π₂β #-}

-- postulate
--   [id]t : ∀ {Γ}{A}{t : Tm Γ A} → t [ id ]t ≡ t
--   [][]t : ∀{Γ Δ Σ}{A : Ty Σ}{σ : Tms Γ Δ}{δ : Tms Δ Σ}{t : Tm Σ A}
--           → (t [ δ ]t) [ σ ]t ≡ t [ δ ∘ σ ]t
-- {-# REWRITE [id]t [][]t #-} -- RULE [][]t NOT FIRING

-- wk : ∀{Γ}{A : Ty Γ} → Tms (Γ ▶ A) Γ
-- wk = π₁ id

-- vz : {Γ : Con} {A : Ty Γ} →
--         Tm (Γ ▶ A) (_[_]T {Γ ▶ A} {Γ} A (π₁ {Γ ▶ A} {Γ} {A} (id {Γ ▶ A})))
-- vz {z} {z₁} = π₂ {z ▶ z₁} {z} {z₁} (id {z ▶ z₁})

-- vs : {Γ : Con} {A B : Ty Γ} →
--        Tm Γ A →
--        Tm (Γ ▶ B) (_[_]T {Γ ▶ B} {Γ} A (π₁ {Γ ▶ B} {Γ} {B} (id {Γ ▶ B})))
-- vs {z} {z₁} {z₂} x =
--   _[_]t {z ▶ z₂} {z} {z₁} x (π₁ {z ▶ z₂} {z} {z₂} (id {z ▶ z₂}))

-- <_> : ∀{Γ}{A : Ty Γ} → Tm Γ A → Tms Γ (Γ ▶ A)
-- <_> {z} {z₁} t = _,s_ {z} {z} (id {z}) {z₁} t

-- infix 4 <_>

-- _^_ : {Γ Δ : Con} (σ : Tms Γ Δ) (A : Ty Δ) → Tms (Γ ▶ _[_]T {Γ} {Δ} A σ) (Δ ▶ A)
-- _^_ {z} {z₁} σ A =
--   _,s_ {z ▶ _[_]T {z} {z₁} A σ} {z₁}
--   (_∘_ {z ▶ _[_]T {z} {z₁} A σ} {z} {z₁} σ
--    (π₁ {z ▶ _[_]T {z} {z₁} A σ} {z} {_[_]T {z} {z₁} A σ}
--     (id {z ▶ _[_]T {z} {z₁} A σ})))
--   {A}
--   (π₂ {z ▶ _[_]T {z} {z₁} A σ} {z} {_[_]T {z} {z₁} A σ}
--    (id {z ▶ _[_]T {z} {z₁} A σ}))

-- infixl 5 _^_


-- Term quotients
------------------------------------------------------------



U : ∀{Γ} → Ty Γ
U {Γ , Γᵀ} = S.U , λ σ* t → S.Tm Γ* (S.El t)

U[]  : {Γ Δ : Con} {σ : Tms Γ Δ} → _≡_ {i} {Ty Γ} (_[_]T {Γ} {Δ} (U {Δ}) σ) (U {Γ})
U[] {Γ , Γᵀ}{Δ , Δᵀ}{σ , σᵀ} =
  ,≡ refl (ext λ σ* → J (λ _ eq → (λ t → tr (λ _ → Set) eq (S.Tm Γ* (S.El t)))
                                ≡ (λ t → S.Tm Γ* (S.El t)))
                         refl (σᵀ σ*))

El   : ∀{Γ}(a : Tm Γ U) → Ty Γ
El {Γ , Γᵀ}(a , aᵀ) = (S.El a) , (λ σ* t → lift (coe (aᵀ σ*) t))

-- {-# REWRITE U[] #-}

-- El[] : {Γ Δ : Con} {σ : Tms Γ Δ} {a : Tm Δ (U {Δ})} → El a [ σ ]T ≡ {!El (a [ σ ]T)!}
         --  _≡_ {i} {Ty Γ} (_[_]T {Γ} {Δ} (El {Δ} a) σ)
         -- (El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ))
-- El[] = {!!}
-- {-# REWRITE El[] #-}

-- postulate
--   plzwork : (Γ Δ : Con) (a : Tm Δ (U {Δ})) →
--             Ty (Δ ▶ El {Δ} a) →
--             (σ : Tms Γ Δ) →
--             _≡_ {j}
--             {Tm (Γ ▶ El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ))
--              (U {Γ ▶ El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)})}
--             (_[_]t {Γ ▶ El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)} {Γ} {U {Γ}}
--              (_[_]t {Γ} {Δ} {U {Δ}} a σ)
--              (π₁ {Γ ▶ El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)} {Γ}
--               {El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)}
--               (id {Γ ▶ El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)})))
--             (_[_]t {Γ ▶ El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)} {Δ} {U {Δ}} a
--              (_∘_ {Γ ▶ El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)} {Γ} {Δ} σ
--               (π₁ {Γ ▶ El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)} {Γ}
--                {El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)}
--                (id {Γ ▶ El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)}))))
-- {-# REWRITE plzwork #-}

Π : {Γ : Con} (a : Tm Γ (U {Γ})) → Ty (Γ ▶ El {Γ} a) → Ty Γ
Π {Γ , Γᵀ}(a , aᵀ)(B , Bᵀ) =
  S.Π a B , λ σ* t α →
      let p = aᵀ σ*
          q = Bᵀ (σ* S.,s coe (p ⁻¹) α) (coe {!!} (S._$_ t (coe (p ⁻¹) α)))
      in coe ((λ x → Tyʳ B .₁ (Γᵀ σ* , lift x)) & J (λ _ eq → ∀ α → coe eq (coe (eq ⁻¹) α) ≡ α) (λ α → refl) (aᵀ σ*) α) q


--   Π[] : {Γ Δ : Con} {σ : Tms Γ Δ} {a : Tm Δ (U {Δ})}
--         {B : Ty (Δ ▶ El {Δ} a)} →
--         _≡_ {i} {Ty Γ} (_[_]T {Γ} {Δ} (Π {Δ} a B) σ)
--         (Π {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)
--          (_[_]T {Γ ▶ El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)} {Δ ▶ El {Δ} a} B
--           (_,s_ {Γ ▶ El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)} {Δ}
--            (_∘_ {Γ ▶ El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)} {Γ} {Δ} σ
--             (π₁ {Γ ▶ El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)} {Γ}
--              {El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)}
--              (id {Γ ▶ El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)})))
--            {El {Δ} a}
--            (π₂ {Γ ▶ El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)} {Γ}
--             {El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)}
--             (id {Γ ▶ El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)})))))
-- {-# REWRITE Π[] #-}


app : {Γ : Con} {a : Tm Γ (U {Γ})} {B : Ty (Γ ▶ El {Γ} a)} →
      Tm Γ (Π {Γ} a B) → Tm (Γ ▶ El {Γ} a) B
app {Γ , Γᵀ}{a , aᵀ}{B , Bᵀ} (t , tᵀ) =
  (S.app t) , λ σ* → let foo = tᵀ (S.π₁ σ*)
                         bar = S.π₂ σ*
                         baz = Bᵀ σ* (S.app t S.[ σ* ]t)
                         p   = aᵀ (S.π₁ σ*)
                     in {!!} ◾ apd (λ f → f (coe p bar)) foo

Id  : {Γ : Con} (a : Tm Γ (U {Γ})) → Tm Γ (El {Γ} a) → Tm Γ (El {Γ} a) → Tm Γ (U {Γ})
Id {Γ , Γᵀ}(a , aᵀ)(t , tᵀ)(u , uᵀ) =
  (S.Id a t u) , λ σ* → let foo = (tᵀ σ* ⁻¹ ◾ lift & (coe (aᵀ σ*) & {!!})) ◾ uᵀ σ* in {!!} -- kinda iffy

LargeId : ∀ {Γ}(a : Tm Γ U) → Tm Γ (El a) → Tm Γ (El a) → Ty Γ
LargeId {Γ , Γᵀ}(A , Aᵀ)(t , tᵀ)(u , uᵀ) =
  (S.LargeId A t u) , λ σ* e
    → tᵀ σ* ⁻¹ ◾ ap lift (ap (coe (Aᵀ σ*)) (S.LargeReflection e)) ◾ uᵀ σ*

ΠNI : ∀ {Γ}(A : Set) → (A → Ty Γ) → Ty Γ
ΠNI {Γ , Γᵀ} A B =
  (S.ΠNI A λ α → B α .₁) ,
  λ σ* t α → B α .₂ σ* {!S.appₙᵢ t α!}  -- we have no α : A

Πₙᵢ : ∀ {Γ}(A : Set) → (A → Tm Γ U) → Tm Γ U
Πₙᵢ {Γ , Γᵀ} A b =
  S.Πₙᵢ A (λ α → b α .₁) ,
  λ σ* → {!S.Tm Γ* (S.El (S.Πₙᵢ A (λ α → b α .₁ S.[ σ* ]t)))!}    -- we have no α : A


        -- tr (λ x → S.Tm Γ*
        --       (S.El (S.Id (a S.[ σ* ]t) (t S.[ σ* ]t) (u S.[ σ* ]t)))
        --       ≡ (x ≡ lower (₁ (Tmʳ u) (Γᵀ σ*))))
        --    (lower & tᵀ σ*)
        -- (tr (λ x → S.Tm Γ*
        --        (S.El (S.Id (a S.[ σ* ]t) (t S.[ σ* ]t) (u S.[ σ* ]t)))
        --        ≡ (coe (aᵀ σ*) (t S.[ σ* ]t) ≡ x))
        --     (lower & uᵀ σ*)
        --     {!!})

-- lower & tᵀ σ*

    -- J (λ _ eq →
    --   (uᵀ : lift (coe eq (u S.[ σ* ]t)) ≡ Tmʳ u .₁ (Γᵀ σ*))
    --   (tᵀ : ₂ (El (a , aᵀ)) σ* (t S.[ σ* ]t) ≡ Tmʳ t .₁ (Γᵀ σ*))
    --   →

    --   S.Tm Γ* (S.El (S.Id (a S.[ σ* ]t) (t S.[ σ* ]t) (u S.[ σ* ]t)))
    --   ≡ (lower (₁ (Tmʳ t) (Γᵀ σ*)) ≡ lower (₁ (Tmʳ u) (Γᵀ σ*))))


-- Id[] : {Γ Δ : Con} {σ : Tms Γ Δ} {a : Tm Δ (U {Δ})}
--            {t u : Tm Δ (El {Δ} a)} →
--            _≡_ {j} {Tm Γ (U {Γ})} (_[_]t {Γ} {Δ} {U {Δ}} (Id {Δ} a t u) σ)
--            (Id {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ) (_[_]t {Γ} {Δ} {El {Δ} a} t σ)
--             (_[_]t {Γ} {Δ} {El {Δ} a} u σ))


--   app[] : {Γ Δ : Con} {σ : Tms Γ Δ} {a : Tm Δ (U {Δ})}
--            {B : Ty (Δ ▶ El {Δ} a)} {t : Tm Δ (Π {Δ} a B)} →
--            _≡_ {j}
--            {Tm (Γ ▶ El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ))
--             (_[_]T {Γ ▶ El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)} {Δ ▶ El {Δ} a} B
--              (_,s_ {Γ ▶ El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)} {Δ}
--               (_∘_ {Γ ▶ El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)} {Γ} {Δ} σ
--                (π₁ {Γ ▶ El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)} {Γ}
--                 {El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)}
--                 (id {Γ ▶ El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)})))
--               {El {Δ} a}
--               (π₂ {Γ ▶ El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)} {Γ}
--                {El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)}
--                (id {Γ ▶ El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)}))))}
--            (_[_]t {Γ ▶ El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)} {Δ ▶ El {Δ} a} {B}
--             (app {Δ} {a} {B} t)
--             (_,s_ {Γ ▶ El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)} {Δ}
--              (_∘_ {Γ ▶ El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)} {Γ} {Δ} σ
--               (π₁ {Γ ▶ El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)} {Γ}
--                {El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)}
--                (id {Γ ▶ El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)})))
--              {El {Δ} a}
--              (π₂ {Γ ▶ El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)} {Γ}
--               {El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)}
--               (id {Γ ▶ El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)}))))
--            (app {Γ} {_[_]t {Γ} {Δ} {U {Δ}} a σ}
--             {_[_]T {Γ ▶ El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)} {Δ ▶ El {Δ} a} B
--              (_,s_ {Γ ▶ El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)} {Δ}
--               (_∘_ {Γ ▶ El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)} {Γ} {Δ} σ
--                (π₁ {Γ ▶ El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)} {Γ}
--                 {El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)}
--                 (id {Γ ▶ El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)})))
--               {El {Δ} a}
--               (π₂ {Γ ▶ El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)} {Γ}
--                {El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)}
--                (id {Γ ▶ El {Γ} (_[_]t {Γ} {Δ} {U {Δ}} a σ)})))}
--             (_[_]t {Γ} {Δ} {Π {Δ} a B} t σ))
-- {-# REWRITE app[] #-}

-- _$_ : {Γ : Con} {a : Tm Γ (U {Γ})} {B : Ty (Γ ▶ El {Γ} a)} →
--       Tm Γ (Π {Γ} a B) →
--       (u : Tm Γ (El {Γ} a)) →
--       Tm Γ
--       (_[_]T {Γ} {Γ ▶ El {Γ} a} B (_,s_ {Γ} {Γ} (id {Γ}) {El {Γ} a} u))
-- _$_ {z} {z₁} {z₂} t u =
--   _[_]t {z} {z ▶ El {z} z₁} {z₂} (app {z} {z₁} {z₂} t)
--   (_,s_ {z} {z} (id {z}) {El {z} z₁} u)


-- -- Recursors & computation (no β for equalities)
-- --------------------------------------------------------------------------------

-- postulate
--   Conᴿ : S.Con → Con
--   Tyᴿ  : ∀ {Γ} → S.Ty Γ → Ty (Conᴿ Γ)
--   Tmᴿ  : ∀ {Γ A} → S.Tm Γ A → Tm (Conᴿ Γ) (Tyᴿ A)
--   Tmsᴿ : ∀ {Γ Δ} → S.Tms Γ Δ → Tms (Conᴿ Γ) (Conᴿ Δ)

-- postulate
--   ∙ᴿ   : Conᴿ S.∙ ≡ ∙
--   ,ᴿ   : ∀ Γ A → Conᴿ (Γ S.▶ A) ≡ Conᴿ Γ ▶ Tyᴿ A
--   []Tᴿ : (Γ Δ : S.Con) (A : S.Ty Δ) (σ : S.Tms Γ Δ) →
--           _≡_ {i} {Ty (Conᴿ Γ)} (Tyᴿ {Γ} (S._[_]T {Γ} {Δ} A σ))
--           (_[_]T {Conᴿ Γ} {Conᴿ Δ} (Tyᴿ {Δ} A) (Tmsᴿ {Γ} {Δ} σ))
-- {-# REWRITE ∙ᴿ ,ᴿ []Tᴿ #-}

-- postulate
--   []tᴿ : {Γ Δ : S.Con} {A : S.Ty Δ} (t : S.Tm Δ A) (σ : S.Tms Γ Δ) →
--            _≡_ {j}
--            {Tm (Conᴿ Γ)
--             (_[_]T {Conᴿ Γ} {Conᴿ Δ} (Tyᴿ {Δ} A) (Tmsᴿ {Γ} {Δ} σ))}
--            (Tmᴿ {Γ} {S._[_]T {Γ} {Δ} A σ} (S._[_]t {Γ} {Δ} {A} t σ))
--            (_[_]t {Conᴿ Γ} {Conᴿ Δ} {Tyᴿ {Δ} A} (Tmᴿ {Δ} {A} t)
--             (Tmsᴿ {Γ} {Δ} σ))

--   idᴿ  : {Γ : S.Con} →
--            _≡_ {j} {Tms (Conᴿ Γ) (Conᴿ Γ)} (Tmsᴿ {Γ} {Γ} (S.id {Γ}))
--            (id {Conᴿ Γ})

--   ∘ᴿ   : {Γ Δ : S.Con} {Σ : S.Con} {σ : S.Tms Δ Σ} {δ : S.Tms Γ Δ} →
--            _≡_ {j} {Tms (Conᴿ Γ) (Conᴿ Σ)}
--            (Tmsᴿ {Γ} {Σ} (S._∘_ {Γ} {Δ} {Σ} σ δ))
--            (_∘_ {Conᴿ Γ} {Conᴿ Δ} {Conᴿ Σ} (Tmsᴿ {Δ} {Σ} σ)
--             (Tmsᴿ {Γ} {Δ} δ))

--   εᴿ   : {Γ : S.Con} → _≡_ {j} {Tms (Conᴿ Γ) ∙} (Tmsᴿ {Γ} {S.∙} (S.ε {Γ})) (ε {Conᴿ Γ})

--   ,sᴿ  : {Γ Δ : S.Con} (σ : S.Tms Γ Δ) {A : S.Ty Δ}
--          (t : S.Tm Γ (S._[_]T {Γ} {Δ} A σ)) →
--          _≡_ {j} {Tms (Conᴿ Γ) (Conᴿ Δ ▶ Tyᴿ {Δ} A)}
--          (Tmsᴿ {Γ} {Δ S.▶ A} (S._,s_ {Γ} {Δ} σ {A} t))
--          (_,s_ {Conᴿ Γ} {Conᴿ Δ} (Tmsᴿ {Γ} {Δ} σ) {Tyᴿ {Δ} A}
--           (Tmᴿ {Γ} {S._[_]T {Γ} {Δ} A σ} t))

--   π₁ᴿ  : {Γ Δ : S.Con} {A : S.Ty Δ} (σ : S.Tms Γ (Δ S.▶ A)) →
--           _≡_ {j} {Tms (Conᴿ Γ) (Conᴿ Δ)} (Tmsᴿ {Γ} {Δ} (S.π₁ {Γ} {Δ} {A} σ))
--           (π₁ {Conᴿ Γ} {Conᴿ Δ} {Tyᴿ {Δ} A} (Tmsᴿ {Γ} {Δ S.▶ A} σ))

-- {-# REWRITE []tᴿ idᴿ ∘ᴿ εᴿ ,sᴿ π₁ᴿ #-}

-- postulate
--   π₂ᴿ : {Γ Δ : S.Con} {A : S.Ty Δ} (σ : S.Tms Γ (Δ S.▶ A)) →
--           _≡_ {j}
--           {Tm (Conᴿ Γ)
--            (_[_]T {Conᴿ Γ} {Conᴿ Δ} (Tyᴿ {Δ} A)
--             (π₁ {Conᴿ Γ} {Conᴿ Δ} {Tyᴿ {Δ} A} (Tmsᴿ {Γ} {Δ S.▶ A} σ)))}
--           (Tmᴿ {Γ} {S._[_]T {Γ} {Δ} A (S.π₁ {Γ} {Δ} {A} σ)}
--            (S.π₂ {Γ} {Δ} {A} σ))
--           (π₂ {Conᴿ Γ} {Conᴿ Δ} {Tyᴿ {Δ} A} (Tmsᴿ {Γ} {Δ S.▶ A} σ))

--   Uᴿ  : {Γ : S.Con} → _≡_ {i} {Ty (Conᴿ Γ)} (Tyᴿ {Γ} (S.U {Γ})) (U {Conᴿ Γ})
-- {-# REWRITE π₂ᴿ Uᴿ #-}

-- postulate
--   Elᴿ : {Γ : S.Con} {a : S.Tm Γ (S.U {Γ})} → _≡_ {i} {Ty (Conᴿ Γ)} (Tyᴿ {Γ} (S.El {Γ} a))
--                                                  (El {Conᴿ Γ} (Tmᴿ {Γ} {S.U {Γ}} a))
-- {-# REWRITE Elᴿ #-}

-- postulate
--   Πᴿ : {Γ : S.Con} (a : S.Tm Γ (S.U {Γ})) (B : S.Ty (Γ S.▶ S.El {Γ} a)) →
--         _≡_ {i} {Ty (Conᴿ Γ)} (Tyᴿ {Γ} (S.Π {Γ} a B))
--         (Π {Conᴿ Γ} (Tmᴿ {Γ} {S.U {Γ}} a) (Tyᴿ {Γ S.▶ S.El {Γ} a} B))
-- {-# REWRITE Πᴿ #-}

-- postulate
--   appᴿ : {Γ : S.Con} {a : S.Tm Γ (S.U {Γ})} {B : S.Ty (Γ S.▶ S.El {Γ} a)}
--           (t : S.Tm Γ (S.Π {Γ} a B)) →
--           _≡_ {j}
--           {Tm (Conᴿ Γ ▶ El {Conᴿ Γ} (Tmᴿ {Γ} {S.U {Γ}} a))
--            (Tyᴿ {Γ S.▶ S.El {Γ} a} B)}
--           (Tmᴿ {Γ S.▶ S.El {Γ} a} {B} (S.app {Γ} {a} {B} t))
--           (app {Conᴿ Γ} {Tmᴿ {Γ} {S.U {Γ}} a} {Tyᴿ {Γ S.▶ S.El {Γ} a} B}
--            (Tmᴿ {Γ} {S.Π {Γ} a B} t))
-- {-# REWRITE appᴿ #-}
