{-# OPTIONS --rewriting #-}

module Syntax where

open import Lib hiding (_∘_; id)

infixl 5 _▶_
infixl 7 _[_]T
infixl 5 _,s_
infix  6 _∘_
infixl 8 _[_]t

postulate
  Con : Set
  Ty  : Con → Set
  Tm  : (Γ : Con) → Ty Γ → Set
  Tms : Con → Con → Set

  ∙     : Con
  _▶_   : (Γ : Con) → Ty Γ → Con

  _[_]T : ∀{Γ Δ} → Ty Δ → Tms Γ Δ → Ty Γ

  id    : ∀{Γ} → Tms Γ Γ
  _∘_   : ∀{Γ Δ Σ} → Tms Δ Σ → Tms Γ Δ → Tms Γ Σ
  ε     : ∀{Γ} → Tms Γ ∙
  _,s_  : ∀{Γ Δ}(σ : Tms Γ Δ){A : Ty Δ} → Tm Γ (A [ σ ]T) → Tms Γ (Δ ▶ A)
  π₁    : ∀{Γ Δ}{A : Ty Δ} → Tms Γ (Δ ▶ A) → Tms Γ Δ

  _[_]t : ∀{Γ Δ}{A : Ty Δ} → Tm Δ A → (σ : Tms Γ Δ) → Tm Γ (A [ σ ]T)
  π₂    : ∀{Γ Δ}{A : Ty Δ}(σ : Tms Γ (Δ ▶ A)) → Tm Γ (A [ π₁ σ ]T)

  [id]T : ∀{Γ}{A : Ty Γ} → A [ id ]T ≡ A
  [][]T : ∀{Γ Δ Σ}{A : Ty Σ}{σ : Tms Γ Δ}{δ : Tms Δ Σ}
          → A [ δ ]T [ σ ]T ≡ A [ δ ∘ σ ]T

  idl   : ∀{Γ Δ}{σ : Tms Γ Δ} → (id ∘ σ) ≡ σ
  idr   : ∀{Γ Δ}{σ : Tms Γ Δ} → (σ ∘ id) ≡ σ
  ass   : ∀{Γ Δ Σ Ω}{σ : Tms Σ Ω}{δ : Tms Δ Σ}{ν : Tms Γ Δ}
        → (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν)

{-# REWRITE [][]T [id]T idl idr ass #-}

postulate
  ,∘    : ∀{Γ Δ Σ}{δ : Tms Γ Δ}{σ : Tms Σ Γ}{A : Ty Δ }{t : Tm Γ (A [ δ ]T)}
        → ((_,s_ δ {A = A} t) ∘ σ) ≡ ((δ ∘ σ) ,s t [ σ ]t )
  π₁∘   : ∀{Γ Δ Σ}{A : Ty Σ}{σ : Tms Δ (Σ ▶ A)}{δ : Tms Γ Δ} → π₁ σ ∘ δ ≡ π₁ (σ ∘ δ)

  π₁β   : ∀{Γ Δ}{A : Ty Δ}{σ : Tms Γ Δ}{t : Tm Γ (A [ σ ]T)}
        → (π₁ {A = A}(σ ,s t)) ≡ σ
  πη    : ∀{Γ Δ}{A : Ty Δ}{σ : Tms Γ (Δ ▶ A)}
        → (π₁ σ ,s π₂ σ) ≡ σ
  εη    : ∀{Γ}{σ : Tms Γ ∙}
        → σ ≡ ε
{-# REWRITE ,∘ π₁∘ π₁β #-}

postulate
  π₂β     : ∀{Γ Δ}{A : Ty Δ }{σ : Tms Γ Δ}{t : Tm Γ (A [ σ ]T)}
        → π₂ {A = A}(σ ,s t) ≡ t
  π₂∘Ne   : ∀{Γ Δ Σ}{A : Ty Σ}{σ : Tms Δ (Σ ▶ A)}{δ : Tms Γ Δ}
        → π₂ σ [ δ ]t ≡ π₂ (σ ∘ δ)
{-# REWRITE π₂β π₂∘Ne #-}

wk : ∀{Γ}{A : Ty Γ} → Tms (Γ ▶ A) Γ
wk = π₁ id

vz : ∀{Γ}{A : Ty Γ} → Tm (Γ ▶ A) (A [ wk ]T)
vz = π₂ id

vs : ∀{Γ}{A B : Ty Γ} → Tm Γ A → Tm (Γ ▶ B) (A [ wk ]T)
vs x = x [ wk ]t

<_> : ∀{Γ}{A : Ty Γ} → Tm Γ A → Tms Γ (Γ ▶ A)
< t > = id ,s t

infix 4 <_>

_^_ : ∀ {Γ Δ : Con}(σ : Tms Γ Δ)(A : Ty Δ) → Tms (Γ ▶ A [ σ ]T) (Δ ▶ A)
_^_ {Γ} {Δ} σ A = σ ∘ wk ,s vz {Γ}{A [ σ ]T}

infixl 5 _^_

postulate
  U    : ∀{Γ} → Ty Γ
  U[]  : ∀{Γ Δ}{σ : Tms Γ Δ} → (U [ σ ]T) ≡ U
  El   : ∀{Γ}(a : Tm Γ U) → Ty Γ
{-# REWRITE U[] #-}
postulate
  El[] : ∀{Γ Δ}{σ : Tms Γ Δ}{a : Tm Δ U}
       → (El a [ σ ]T) ≡ (El (a [ σ ]t))
{-# REWRITE El[] #-}

postulate
  [id]t : ∀ {Γ}{A}{t : Tm Γ A} → t [ id ]t ≡ t
{-# REWRITE [id]t #-}

postulate
  [][]tU  : ∀ {Γ Δ Σ : Con}{σ : Tms Γ Δ} {δ : Tms Δ Σ}(t : Tm Σ U)
          → t [ δ ]t [ σ ]t ≡ t [ δ ∘ σ ]t
{-# REWRITE [][]tU #-}

postulate
  [][]tEl : ∀ {Γ Δ Σ : Con}{σ : Tms Γ Δ} {δ : Tms Δ Σ}{a : Tm Σ U}(t : Tm Σ (El a))
          → t [ δ ]t [ σ ]t ≡ t [ δ ∘ σ ]t
  [][]tNe : {Γ Δ : Con} {Σ₁ : Con} {A : Ty Σ₁} {σ : Tms Γ Δ} {δ : Tms Δ Σ₁}
           {t : Tm Σ₁ A} →
           t [ δ ]t [ σ ]t ≡ t [ δ ∘ σ ]t
{-# REWRITE [][]tEl [][]tNe #-}

postulate
  vz<>Elσ : ∀ {Γ Δ}{a}{t : Tm Γ (El a)}{σ : Tms Δ Γ}
        → vz [ < t [ σ ]t > ]t ≡ t [ σ ]t
{-# REWRITE vz<>Elσ #-}

postulate
  π₂∘U  : ∀{Γ Δ Σ}{σ : Tms Δ (Σ ▶ U)}{δ : Tms Γ Δ}
        → _[_]t {Γ}{Δ}{U}(π₂ σ) δ ≡ π₂ (σ ∘ δ)
{-# REWRITE π₂∘U #-}
postulate
  π₂∘El : ∀{Γ Δ Σ}{a}{σ : Tms Δ (Σ ▶ El a)}{δ : Tms Γ Δ}
        → _[_]t {Γ}{Δ}{El (a [ π₁ σ ]t)} (π₂ σ) δ ≡ π₂ (σ ∘ δ)
{-# REWRITE π₂∘El #-}

postulate
  Π : ∀{Γ}(a : Tm Γ U)(B : Ty (Γ ▶ El a)) → Ty Γ

postulate
  Π[] : ∀{Γ Δ}{σ : Tms Γ Δ}{a : Tm Δ U}{B : Ty (Δ ▶ El a)}
      → (Π a B) [ σ ]T ≡ Π (a [ σ ]t) (B [ σ ^ El a ]T)
{-# REWRITE Π[] #-}

postulate
  app : ∀{Γ}{a : Tm Γ U}{B : Ty (Γ ▶ El a)} → Tm Γ (Π a B) → Tm (Γ ▶ El a) B

  app[] : ∀{Γ Δ}{σ : Tms Γ Δ}{a : Tm Δ U}{B : Ty (Δ ▶ El a)}{t : Tm Δ (Π a B)}
          → app t [ σ ^ El a ]t ≡ app (t [ σ ]t)
{-# REWRITE app[] #-}

_$_ : ∀ {Γ}{a : Tm Γ U}{B : Ty (Γ ▶ El a)}(t : Tm Γ (Π a B))(u : Tm Γ (El a))
      → Tm Γ (B [ < u > ]T)
t $ u = (app t) [ < u > ]t

postulate
  Id   : ∀ {Γ}(a : Tm Γ U) → Tm Γ (El a) → Tm Γ (El a) → Tm Γ U
  Id[] : ∀ {Γ Δ}{σ : Tms Γ Δ}{a : Tm Δ U}{t u : Tm Δ (El a)}
         → Id a t u [ σ ]t ≡ Id (a [ σ ]t) (t [ σ ]t) (u [ σ ]t)
{-# REWRITE Id[] #-}

postulate
  Transp :
    ∀ {Γ}{a : Tm Γ U}(P : Ty (Γ ▶ El a))
      {t u : Tm Γ (El a)}
      (pt : Tm Γ (P [ < t > ]T))
      (eq : Tm Γ (El (Id a t u)))
    → Tm Γ (P [ < u > ]T)

  Transp[] :
    ∀ {Γ Δ}{σ : Tms Γ Δ}
      {a : Tm Δ U}(P : Ty (Δ ▶ El a))
      {t u : Tm Δ (El a)}
      (pt : Tm Δ (P [ id ,s t ]T))
      (eq : Tm Δ (El (Id a t u)))
    → Transp P pt eq [ σ ]t ≡
      Transp (P [ σ ^ El a ]T) (pt [ σ ]t) (eq [ σ ]t)

postulate
  Reflection : ∀ {Γ a}{t u : Tm Γ (El a)} → Tm Γ (El (Id a t u)) → t ≡ u

postulate
  LargeId : ∀ {Γ}(a : Tm Γ U) → Tm Γ (El a) → Tm Γ (El a) → Ty Γ
  LargeId[] : ∀ {Γ Δ}{σ : Tms Γ Δ}{a : Tm Δ U}{t u : Tm Δ (El a)}
    → LargeId a t u [ σ ]T ≡ LargeId (a [ σ ]t) (t [ σ ]t) (u [ σ ]t)

{-# REWRITE LargeId[] #-}

-- postulate
--   LargeRefl : ∀ {Γ}{a : Tm Γ U}(t : Tm Γ (El a)) → Tm Γ (LargeId a t t)
--   LargeRefl[]

postulate
  LargeReflection :
    ∀ {Γ a}{t u : Tm Γ (El a)} → Tm Γ (LargeId a t u) → t ≡ u

postulate
  ΠNI : ∀ {Γ}(A : Set) → (A → Ty Γ) → Ty Γ
  ΠNI[] : ∀ {Γ Δ}{σ : Tms Γ Δ}{A : Set}{B : A → Ty Δ}
          → ΠNI A B [ σ ]T ≡ ΠNI A (λ α → B α [ σ ]T)
{-# REWRITE ΠNI[] #-}

postulate
  Πₙᵢ : ∀ {Γ}(A : Set) → (A → Tm Γ U) → Tm Γ U

  Πₙᵢ[] : ∀ {Γ Δ}{σ : Tms Γ Δ}{A : Set}{b : A → Tm Δ U}
          → Πₙᵢ A b [ σ ]t ≡ Πₙᵢ A (λ α → b α [ σ ]t)

-- ;tr (Tm Γ) U[] (Πₙᵢ A b [ σ ]t) ≡ Πₙᵢ A (λ α → tr (Tm Γ) U[] (b α [ σ ]t))
{-# REWRITE Πₙᵢ[] #-}
