{-# OPTIONS --without-K #-}

module TermModelExperiments.MutualInductive where

{-
Term model for simple mutual inductive types. This is a practice
project for the inductive-inductive case.

It seems to me that as soon as we can specify inductive-inductive types, we already
have the all initial algebras, because codes must be able to refer to arbitrary expressions
made of previous constructors, and "expressions made out of constructors" is exactly
the syntax/initial algebra. So, we should be able to use Tm to construct the term model.

For QIIT-s, the term model should quotient "Tm Γ"-s the following way:
(t : Tm Γ (El A)) and (u : Tm Γ (El A)) are equal if (Tm Γ (El (Id A t u)))
is inhabited.

  data Tm/= (Γ : Con)(A : Tm Γ U) : Set where
    [_]  : Tm Γ (El A) → Tm/= Γ (El A)
    quot : {t u : Tm Γ (El A)} → Tm Γ (El (Id A t u)) → [ t ] ≡ [ u ]

For example, we can give the term model for the interval as follows (in semiformal syntax):

ICode : Con
ICode = (I : U, left : El I, right : El I, seg : Id I left right)

ITermAlg : (I : Set, left : I, right : I, seg : left ≡ right)
ITermAlg = (Tm/= ICode I, [left], [right], quot seg)

-}

open import Lib renaming (zero to lzero; suc to lsuc)

--------------------------------------------------------------------------------

data Sorts : Set where
  ∙   : Sorts
  _,U : Sorts → Sorts

data Varₛ : Sorts → Set where
  vz  : ∀ {n} → Varₛ (n ,U)
  vs  : ∀ {n} → Varₛ n → Varₛ (n ,U)

data Ty (n : Sorts) : Set where
  sort : Varₛ n → Ty n
  _⇒_  : Varₛ n → Ty n → Ty n

data Con (n : Sorts) : Set where
  ∙    : Con n
  _,_  : Con n → Ty n → Con n

ΣCon : Set
ΣCon = Σ Sorts Con

infixl 4 _,_
infixr 5 _⇒_
infixl 4 _,U

-- Algebras
--------------------------------------------------------------------------------

Sortsᶜ : Sorts → Set₁
Sortsᶜ ∙      = Lift ⊤
Sortsᶜ (n ,U) = Sortsᶜ n × Set

Varₛᶜ : ∀ {n} → Varₛ n → Sortsᶜ n → Set
Varₛᶜ vz     nᶜ = ₂ nᶜ
Varₛᶜ (vs x) nᶜ = Varₛᶜ x (₁ nᶜ)

Tyᶜ : ∀ {n} → Ty n → Sortsᶜ n → Set
Tyᶜ (sort x) nᶜ = Varₛᶜ x nᶜ
Tyᶜ (x ⇒ A)  nᶜ = Varₛᶜ x nᶜ → Tyᶜ A nᶜ

Conᶜ : ∀ {n} → Con n → Sortsᶜ n → Set
Conᶜ ∙       nᶜ = ⊤
Conᶜ (Γ , A) nᶜ = Conᶜ Γ nᶜ × Tyᶜ A nᶜ

ΣConᶜ : ΣCon → Set₁
ΣConᶜ (n Σ, Γ) = Σ (Sortsᶜ n) (Conᶜ Γ)

-- Morphisms
--------------------------------------------------------------------------------

Sortsᴹ : (n : Sorts) → Sortsᶜ n → Sortsᶜ n → Set₁
Sortsᴹ ∙      ν₀         ν₁         = Lift ⊤
Sortsᴹ (n ,U) (ν₀ Σ, A₀) (ν₁ Σ, A₁) = Sortsᴹ n ν₀ ν₁ × (A₀ → A₁)

Varₛᴹ : ∀{n}(x : Varₛ n) → ∀ {ν₀ ν₁} → Sortsᴹ n ν₀ ν₁ → Varₛᶜ x ν₀ → Varₛᶜ x ν₁
Varₛᴹ vz     νᴹ = ₂ νᴹ
Varₛᴹ (vs x) νᴹ = Varₛᴹ x (₁ νᴹ)

Tyᴹ : ∀ {n}(A : Ty n) → ∀ {ν₀ ν₁} → Sortsᴹ n ν₀ ν₁ → Tyᶜ A ν₀ → Tyᶜ A ν₁ → Set
Tyᴹ (sort x) νᴹ t₀ t₁ = Varₛᴹ x νᴹ t₀ ≡ t₁
Tyᴹ (x ⇒ B)  νᴹ t₀ t₁ = (α : Varₛᶜ x _) → Tyᴹ B νᴹ (t₀ α) (t₁ (Varₛᴹ x νᴹ α))

Conᴹ : ∀ {n} (Γ : Con n) → ∀ {ν₀ ν₁} → Sortsᴹ n ν₀ ν₁ → Conᶜ Γ ν₀ → Conᶜ Γ ν₁ → Set
Conᴹ ∙       νᴹ γ₀         γ₁         = ⊤
Conᴹ (Γ , A) νᴹ (γ₀ Σ, t₀) (γ₁ Σ, t₁) = Conᴹ Γ νᴹ γ₀ γ₁ × Tyᴹ A νᴹ t₀ t₁

ΣConᴹ : (sig : ΣCon) → ΣConᶜ sig → ΣConᶜ sig → Set₁
ΣConᴹ (n Σ, Γ) (ν₀ Σ, γ₀) (ν₁ Σ, γ₁) = Σ (Sortsᴹ n ν₀ ν₁) λ ν₂ → Conᴹ Γ ν₂ γ₀ γ₁

--------------------------------------------------------------------------------

data Var {n} : Con n → Ty n → Set where
  vz : ∀ {Γ A} → Var (Γ , A) A
  vs : ∀ {Γ A B} → Var Γ A → Var (Γ , B) A

data Tm {n} (Γ : Con n) : Ty n → Set where
  var : ∀ {A} → Var Γ A → Tm Γ A
  app : ∀ {x B} → Tm Γ (x ⇒ B) → Tm Γ (sort x) → Tm Γ B

Varᶜ : ∀ {n Γ A} → Var {n} Γ A → ∀ ν → Conᶜ Γ ν → Tyᶜ A ν
Varᶜ vz     ν γ = ₂ γ
Varᶜ (vs x) ν γ = Varᶜ x ν (₁ γ)

Tmᶜ : ∀ {n Γ A} → Tm {n} Γ A → ∀ ν → Conᶜ Γ ν → Tyᶜ A ν
Tmᶜ (var x)   ν γ = Varᶜ x ν γ
Tmᶜ (app t u) ν γ = Tmᶜ t ν γ (Tmᶜ u ν γ)


-- Embedding calculus
--------------------------------------------------------------------------------

data OPEₛ : Sorts → Sorts → Set where
  ∙    : OPEₛ ∙ ∙
  drop : ∀ {Γ Δ} → OPEₛ Γ Δ → OPEₛ (Γ ,U) Δ
  keep : ∀ {Γ Δ} → OPEₛ Γ Δ → OPEₛ (Γ ,U) (Δ ,U)

idₛ : ∀ {Γ} → OPEₛ Γ Γ
idₛ {∙}    = ∙
idₛ {Γ ,U} = keep idₛ

infixr 6 _∘ₛ_
_∘ₛ_ : ∀ {Γ Δ Σ : Sorts} → OPEₛ Δ Σ → OPEₛ Γ Δ → OPEₛ Γ Σ
σ      ∘ₛ ∙      = σ
σ      ∘ₛ drop δ = drop (σ ∘ₛ δ)
drop σ ∘ₛ keep δ = drop (σ ∘ₛ δ)
keep σ ∘ₛ keep δ = keep (σ ∘ₛ δ)

OPEₛᶜ : ∀ {n m} → OPEₛ n m → Sortsᶜ n → Sortsᶜ m
OPEₛᶜ ∙ ν = lift tt
OPEₛᶜ (drop σ) ν = OPEₛᶜ σ (₁ ν)
OPEₛᶜ (keep σ) ν = OPEₛᶜ σ (₁ ν) Σ, ₂ ν

OPEₛᶜ-id : ∀ {Γ} ν → OPEₛᶜ (idₛ {Γ}) ν ≡ ν
OPEₛᶜ-id {∙}    ν = refl
OPEₛᶜ-id {Γ ,U} ν = ap (_Σ, _) (OPEₛᶜ-id {Γ} (₁ ν))

OPEₛᶜ-∘ : ∀ {n m k} (σ : OPEₛ m k)(δ : OPEₛ n m) ν → OPEₛᶜ (σ ∘ₛ δ) ν ≡ OPEₛᶜ σ (OPEₛᶜ δ ν)
OPEₛᶜ-∘ σ        ∙        ν = refl
OPEₛᶜ-∘ σ        (drop δ) ν = OPEₛᶜ-∘ σ δ (₁ ν)
OPEₛᶜ-∘ (drop σ) (keep δ) ν = OPEₛᶜ-∘ σ δ (₁ ν)
OPEₛᶜ-∘ (keep σ) (keep δ) ν = ap (_Σ, _) (OPEₛᶜ-∘ σ δ (₁ ν))

Varₛₑ : ∀ {n m} → OPEₛ n m → Varₛ m → Varₛ n
Varₛₑ ∙        x      = x
Varₛₑ (drop σ) x      = vs (Varₛₑ σ x)
Varₛₑ (keep σ) vz     = vz
Varₛₑ (keep σ) (vs x) = vs (Varₛₑ σ x)

Varₛₑ-id : ∀ {n}(x : Varₛ n) → Varₛₑ idₛ x ≡ x
Varₛₑ-id vz     = refl
Varₛₑ-id (vs x) = ap vs (Varₛₑ-id x)

Varₛₑ-∘ :
  ∀ {Γ Δ Σ}(σ : OPEₛ Δ Σ)(δ : OPEₛ Γ Δ)(x : Varₛ Σ) → Varₛₑ (σ ∘ₛ δ) x ≡ Varₛₑ δ (Varₛₑ σ x)
Varₛₑ-∘ σ        ∙        x      = refl
Varₛₑ-∘ σ        (drop δ) x      = ap vs (Varₛₑ-∘ σ δ x)
Varₛₑ-∘ (drop σ) (keep δ) x      = ap vs (Varₛₑ-∘ σ δ x)
Varₛₑ-∘ (keep σ) (keep δ) vz     = refl
Varₛₑ-∘ (keep σ) (keep δ) (vs x) = ap vs (Varₛₑ-∘ σ δ x)

Varₛₑᶜ : ∀ {n m}(σ : OPEₛ n m)(x : Varₛ m) ν → Varₛᶜ (Varₛₑ σ x) ν ≡ Varₛᶜ x (OPEₛᶜ σ ν)
Varₛₑᶜ ∙        x      ν = refl
Varₛₑᶜ (drop σ) x      ν = Varₛₑᶜ σ x (₁ ν)
Varₛₑᶜ (keep σ) vz     ν = refl
Varₛₑᶜ (keep σ) (vs x) ν = Varₛₑᶜ σ x (₁ ν)

data OPE {n} : Con n → Con n → Set where
  ∙    : OPE ∙ ∙
  drop : ∀ {Γ Δ A} → OPE Γ Δ → OPE (Γ , A) Δ
  keep : ∀ {Γ Δ A} → OPE Γ Δ → OPE (Γ , A) (Δ , A)

id : ∀ {n Γ} → OPE {n} Γ Γ
id {Γ = ∙}     = ∙
id {Γ = Γ , A} = keep id

infixr 6 _∘_
_∘_ : ∀ {n Γ Δ Σ} → OPE {n} Δ Σ → OPE Γ Δ → OPE Γ Σ
σ      ∘ ∙      = σ
σ      ∘ drop δ = drop (σ ∘ δ)
drop σ ∘ keep δ = drop (σ ∘ δ)
keep σ ∘ keep δ = keep (σ ∘ δ)

Varₑ : ∀{n}{Γ Δ : Con n}{A} → OPE Γ Δ → Var Δ A → Var Γ A
Varₑ ∙        x      = x
Varₑ (drop σ) x      = vs (Varₑ σ x)
Varₑ (keep σ) vz     = vz
Varₑ (keep σ) (vs x) = vs (Varₑ σ x)



module _ (n : Sorts)(Γ : Con n) where

  -- Term algebra
  --------------------------------------------------------------------------------

  Sortsᴬ : ∀ m → OPEₛ n m → Sortsᶜ m
  Sortsᴬ ∙      σ = lift tt
  Sortsᴬ (m ,U) σ = Sortsᴬ m (drop idₛ ∘ₛ σ) Σ, Tm Γ (sort (Varₛₑ σ vz))

  SortsAlg : Sortsᶜ n
  SortsAlg = Sortsᴬ n idₛ

  Varₛᴬ : ∀ m σ x → Varₛᶜ x (Sortsᴬ m σ) ≡ Tm Γ (sort (Varₛₑ σ x))
  Varₛᴬ (m ,U) σ vz     = refl
  Varₛᴬ (m ,U) σ (vs x) = Varₛᴬ m (drop idₛ ∘ₛ σ) x
                        ◾ ap (λ x → Tm Γ (sort x))
                            ( Varₛₑ-∘ (drop idₛ) σ x
                            ◾ ap (λ x → Varₛₑ σ (vs x)) (Varₛₑ-id x))

  Tyᴬ : (A : Ty n) → Tm Γ A → Tyᶜ A SortsAlg
  Tyᴬ (sort x) t = coe (ap (λ x → Tm Γ (sort x)) (Varₛₑ-id x ⁻¹) ◾ Varₛᴬ n idₛ x ⁻¹) t
  Tyᴬ (x ⇒ B)  t =
    λ u → Tyᴬ B (app t (coe (Varₛᴬ n idₛ x ◾ ap (λ x → Tm Γ (sort x))
                            (Varₛₑ-id x)) u))

  Conᴬ : (Δ : Con n) → OPE Γ Δ → Conᶜ Δ SortsAlg
  Conᴬ ∙       σ = tt
  Conᴬ (Δ , A) σ = Conᴬ Δ (drop id ∘ σ) Σ, Tyᴬ A (var (Varₑ σ vz))

  module _ (ν : Sortsᶜ n)(γ : Conᶜ Γ ν) where

    Varₛᴿ : ∀ m (σ : OPEₛ n m) x → Tm Γ (sort (Varₛₑ σ x)) → Varₛᶜ (Varₛₑ σ x) ν
    Varₛᴿ _      σ vz     t = Tmᶜ t ν γ
    Varₛᴿ (m ,U) σ (vs x) t = coe (ap (λ x → Varₛᶜ x ν){Varₛₑ (drop idₛ ∘ₛ σ) x}{Varₛₑ σ (vs x)} {!!}) (Varₛᴿ m (drop idₛ ∘ₛ σ) x (coe (ap (λ x → Tm Γ (sort x)) {!!}) t))

    Sortsᴿ : ∀ m (σ : OPEₛ n m) → Sortsᴹ m (Sortsᴬ m σ) (OPEₛᶜ σ ν)
    Sortsᴿ ∙      σ = lift tt
    Sortsᴿ (m ,U) σ = tr (Sortsᴹ m (Sortsᴬ m (drop idₛ ∘ₛ σ)))
                         (OPEₛᶜ-∘ (drop idₛ) σ ν ◾ OPEₛᶜ-id _)
                         (Sortsᴿ m (drop idₛ ∘ₛ σ))
                   Σ, (λ t → coe (   ap (λ x → Varₛᶜ x ν) (Varₛₑ-id _)
                                  ◾ Varₛₑᶜ σ vz ν)
                                 (Varₛᴿ n idₛ (Varₛₑ σ vz)
                                   (tr (λ x → Tm Γ (sort x))
                                       (Varₛₑ-id _ ⁻¹)
                                       t)))

  -- Weak initiality TODO
  --------------------------------------------------------------------------------

-- TermAlgebra : (code : ΣCon) → ΣConᶜ code
-- TermAlgebra (n Σ, Γ) = Sortsᴬ n Γ n idₛ Σ, Conᴬ n Γ Γ id

-- private
--   EvenOdd : ΣCon
--   EvenOdd = (∙ ,U ,U) Σ, (∙ , sort (vs vz) , vs vz ⇒ sort vz , vz ⇒ sort (vs vz))

--   test : TermAlgebra EvenOdd ≡
--         ((_ Σ,
--           Tm (₂ EvenOdd) (sort (vs vz))) Σ,
--           Tm (₂ EvenOdd) (sort vz)) Σ,
--         ((_ Σ,
--           var (vs (vs vz))) Σ,
--           app (var (vs vz))) Σ,
--           app (var vz)
--   test = refl
