{-# OPTIONS --without-K #-}

{-
Constructing (weakly) initial term models for single-sorted
inductive types

See I.agda for further notes.
-}

module OldExperiments.SimpleInductive where

open import Lib hiding (id; _∘_)
open import Level renaming (zero to lzero; suc to lsuc)

data Ty : Set where
  Sort    : Ty
  Sort⇒_  : Ty → Ty

data Con : Set where
  ∙    : Con
  _▶_  : Con → Ty → Con

infixl 4 _▶_
infixr 5 Sort⇒_

-- Algebras
--------------------------------------------------------------------------------

Sortᶜ : Set₁
Sortᶜ = Set

Tyᶜ : Ty → Sortᶜ → Set
Tyᶜ Sort      X = X
Tyᶜ (Sort⇒ B) X = X → Tyᶜ B X

Conᶜ : Con → Sortᶜ → Set
Conᶜ ∙       X = ⊤
Conᶜ (Γ ▶ A) X = Conᶜ Γ X × Tyᶜ A X

ΣConᶜ : Con → Set₁
ΣConᶜ Γ = Σ Sortᶜ (Conᶜ Γ)

-- Morphisms
--------------------------------------------------------------------------------

Tyᴹ : (A : Ty) → ∀ {X₀ X₁} → (X₀ → X₁) → Tyᶜ A X₀ → Tyᶜ A X₁ → Set
Tyᴹ Sort       Xᴹ t₀ t₁ = Xᴹ t₀ ≡ t₁
Tyᴹ (Sort⇒ B)  Xᴹ t₀ t₁ = ∀ x → Tyᴹ B Xᴹ (t₀ x) (t₁ (Xᴹ x))

Conᴹ : (Γ : Con) → ∀ {X₀ X₁} → (X₀ → X₁) → Conᶜ Γ X₀ → Conᶜ Γ X₁ → Set
Conᴹ ∙       Xᴹ γ₀         γ₁         = ⊤
Conᴹ (Γ ▶ A) Xᴹ (γ₀ , t₀) (γ₁ , t₁) = Conᴹ Γ Xᴹ γ₀ γ₁ × Tyᴹ A Xᴹ t₀ t₁

ΣConᴹ : (Γ : Con) → ΣConᶜ Γ → ΣConᶜ Γ → Set
ΣConᴹ Γ (X₀ , γ₀) (X₁ , γ₁) = Σ (X₀ → X₁) λ Xᴹ → Conᴹ Γ Xᴹ γ₀ γ₁

-- Term algebra & some substitution calculus
-----------------------------------------------------------------------------------

data Var : Con → Ty → Set where
  vz : ∀ {Γ A} → Var (Γ ▶ A) A
  vs : ∀ {Γ A B} → Var Γ A → Var (Γ ▶ B) A

data Tm (Γ : Con) : Ty → Set where
  var : ∀ {A} → Var Γ A → Tm Γ A
  app : ∀ {B} → Tm Γ (Sort⇒ B) → Tm Γ Sort → Tm Γ B

Varᶜ : ∀ {Γ A} → Var Γ A → ∀ X → Conᶜ Γ X → Tyᶜ A X
Varᶜ vz     X γ = ₂ γ
Varᶜ (vs x) X γ = Varᶜ x X (₁ γ)

Tmᶜ : ∀ {Γ A} → Tm Γ A → ∀ X → Conᶜ Γ X → Tyᶜ A X
Tmᶜ (var x)   X γ = Varᶜ x X γ
Tmᶜ (app t u) X γ = Tmᶜ t X γ (Tmᶜ u X γ)

data OPE : Con → Con → Set where
  ∙    : OPE ∙ ∙
  drop : ∀ {Γ Δ A} → OPE Γ Δ → OPE (Γ ▶ A) Δ
  keep : ∀ {Γ Δ A} → OPE Γ Δ → OPE (Γ ▶ A) (Δ ▶ A)

id : ∀ {Γ} → OPE Γ Γ
id {∙}     = ∙
id {Γ ▶ A} = keep id

infixr 6 _∘_
_∘_ : ∀ {Γ Δ Σ} → OPE Δ Σ → OPE Γ Δ → OPE Γ Σ
σ      ∘ ∙      = σ
σ      ∘ drop δ = drop (σ ∘ δ)
drop σ ∘ keep δ = drop (σ ∘ δ)
keep σ ∘ keep δ = keep (σ ∘ δ)

OPEᶜ : ∀ {Γ Δ} → OPE Γ Δ → ∀ X → Conᶜ Γ X → Conᶜ Δ X
OPEᶜ ∙        X γ = γ
OPEᶜ (drop σ) X γ = OPEᶜ σ X (₁ γ)
OPEᶜ (keep σ) X γ = OPEᶜ σ X (₁ γ) , ₂ γ

OPEᶜ-id : ∀ {Γ} X γ → OPEᶜ (id {Γ}) X γ ≡ γ
OPEᶜ-id {∙}     X γ        = refl
OPEᶜ-id {Γ ▶ x} X (γ , t) = (_, t) & (OPEᶜ-id X γ)

OPEᶜ-∘ : ∀ {Γ Δ Σ} (σ : OPE Δ Σ)(δ : OPE Γ Δ) X γ → OPEᶜ (σ ∘ δ) X γ ≡ OPEᶜ σ X (OPEᶜ δ X γ)
OPEᶜ-∘ σ        ∙        X γ = refl
OPEᶜ-∘ σ        (drop δ) X γ = OPEᶜ-∘ σ δ X (₁ γ)
OPEᶜ-∘ (drop σ) (keep δ) X γ = OPEᶜ-∘ σ δ X (₁ γ)
OPEᶜ-∘ (keep σ) (keep δ) X γ = (_, _) & (OPEᶜ-∘ σ δ X (₁ γ))

Varₑ : ∀ {Γ Δ A} → OPE Γ Δ → Var Δ A → Var Γ A
Varₑ ∙        x      = x
Varₑ (drop σ) x      = vs (Varₑ σ x)
Varₑ (keep σ) vz     = vz
Varₑ (keep σ) (vs x) = vs (Varₑ σ x)

Varₑᶜ :
  ∀ {Γ Δ A}(σ : OPE Γ Δ)(x : Var Δ A) X γ → Varᶜ (Varₑ σ x) X γ ≡ Varᶜ x X (OPEᶜ σ X γ)
Varₑᶜ ∙        x      X γ = refl
Varₑᶜ (drop σ) x      X γ = Varₑᶜ σ x X (₁ γ)
Varₑᶜ (keep σ) vz     X γ = refl
Varₑᶜ (keep σ) (vs x) X γ = Varₑᶜ σ x X (₁ γ)

module _ (Γ : Con) where

  Sortᴬ : Sortᶜ
  Sortᴬ = Tm Γ Sort

  Tyᴬ : (A : Ty) → Tm Γ A → Tyᶜ A Sortᴬ
  Tyᴬ Sort      t = t
  Tyᴬ (Sort⇒ B) t = λ u → Tyᴬ B (app t u)

  Conᴬ : (Δ : Con) → OPE Γ Δ → Conᶜ Δ Sortᴬ
  Conᴬ ∙       σ = tt
  Conᴬ (Δ ▶ A) σ = Conᴬ Δ (drop id ∘ σ) , Tyᴬ A (var (Varₑ σ vz))

  module _ (X : Sortᶜ)(γ : Conᶜ Γ X) where

    Sortᴿ : ∀ {A} → Tm Γ A → Tyᶜ A X
    Sortᴿ t = Tmᶜ t X γ

    Tyᴿ : (A : Ty)(t : Tm Γ A) → Tyᴹ A Sortᴿ (Tyᴬ A t) (Tmᶜ t X γ)
    Tyᴿ Sort      t = refl
    Tyᴿ (Sort⇒ A) t = λ u → Tyᴿ A (app t u)

    Conᴿ : (Δ : Con)(σ : OPE Γ Δ) → Conᴹ Δ Sortᴿ (Conᴬ Δ σ) (OPEᶜ σ X γ)
    Conᴿ ∙       σ = tt
    Conᴿ (Δ ▶ A) σ = tr (Conᴹ _ _ _)
                        (OPEᶜ-∘ (drop id) σ X γ ◾ OPEᶜ-id X _)
                        (Conᴿ Δ (drop id ∘ σ))
                   , tr (Tyᴹ A Sortᴿ _)
                        (Varₑᶜ σ vz X γ)
                        (Tyᴿ A (var (Varₑ σ vz)))

Syntax : ∀ Γ → ΣConᶜ Γ
Syntax Γ = Tm Γ Sort , Conᴬ Γ Γ id

Rec : ∀ Γ (Xγ : ΣConᶜ Γ) → ΣConᴹ Γ (Syntax Γ) Xγ
Rec Γ (X , γ) = Sortᴿ Γ X γ , tr (Conᴹ Γ (Sortᴿ Γ X γ) (Conᴬ Γ Γ id))
                                 (OPEᶜ-id X γ)
                                 (Conᴿ Γ X γ Γ id)

-- Example
--------------------------------------------------------------------------------

Nat : Con
Nat = ∙ ▶ Sort ▶ Sort⇒ Sort

test : Rec Nat ≡ λ {(X , γ) → (λ t → Tmᶜ t X γ) , (_ , refl) , λ _ → refl}
test = refl
