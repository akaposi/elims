
{- Π type substitution #-}

open import Level

module Monolithic.PiSub {α : Level} where

open import StrictLib hiding (id; _∘_)
open import Monolithic.Pi {α} public

-- doesn't check in 30 minutes
-- Π[] : {Γ Δ : Con}{σ : Sub Γ Δ}{a : Tm {γ = suc α}{α} Δ U}{B : Ty (Δ ▶ El a)}
--       → Π a B [ σ ]T ≡ Π (coe (Tm Γ & U[] σ) (a [ σ ]t))
--                          (coe ((λ x → Ty (Γ ▶ x)) & El[]) (B [ σ ^ El a ]T))
-- Π[] = {!!}
