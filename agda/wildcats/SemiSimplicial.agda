{-# OPTIONS --without-K #-}

open import Lib
open import WildCat
import Level as L

------------------------------------------------------------

open import Data.Nat

data M : ℕ → ℕ → Set where
  start : M zero zero
  keep : ∀ {n m} → M n m → M (suc n) (suc m)
  drop : ∀ {n m} → M n m → M (suc n) m

id : {n : ℕ} → M n n
id {zero} = start
id {suc n} = keep (id {n})

_∘_ : {i j k : ℕ} → M j k → M i j → M i k
f ∘ start = f
keep f ∘ keep g = keep (f ∘ g)
drop f ∘ keep g = drop (f ∘ g)
f ∘ drop g = drop (f ∘ g)

idl : {i j : ℕ} (f : M i j) → (id ∘ f) ≡ f
idl start = refl
idl (keep f) = ap keep (idl f)
idl (drop f) = ap drop (idl f)

idr : {i j : ℕ} (f : M i j) → (f ∘ id) ≡ f
idr start = refl
idr (keep f) = ap keep (idr f)
idr (drop f) = ap drop (idr f)

ass : {i j k l : ℕ} (f : M k l) (g : M j k) (h : M i j) → (f ∘ (g ∘ h)) ≡ ((f ∘ g) ∘ h)
ass f g start = refl
ass (keep f) (keep g) (keep h) = ap keep (ass f g h)
ass (drop f) (keep g) (keep h) = ap drop (ass f g h)
ass f (drop g) (keep h) = ap drop (ass f g h)
ass f g (drop h) = ap drop (ass f g h)

SemiSimpl : Cat L.zero L.zero
SemiSimpl = mkCat
  ℕ
  (λ i j → M (suc i) (suc j))
  id
  _∘_
  idl
  idr
  ass
  {!!}

2-0 : M 2 1
2-0 = drop (keep start)

2-1 : M 2 1
2-1 = keep (drop start)

3-0 : M 3 1
3-0 = drop (drop (keep start))
3-1 : M 3 1
3-1 = drop (keep (drop start))
3-2 : M 3 1
3-2 = keep (drop (drop start))

3-01 : M 3 2
3-01 = drop (keep (keep start))
3-02 : M 3 2
3-02 = keep (drop (keep start))
3-12 : M 3 2
3-12 = keep (keep (drop start))

4-012 : M 4 3
4-012 = drop (keep (keep (keep start)))

------------------------------------------------------------

_ᵒᵖ : {i j : L.Level} → Cat i j → Cat i j
mkCat O M id ∘ idl idr ass pent ᵒᵖ = mkCat
  O
  (λ x y → M y x)
  id
  (λ g f → ∘ f g)
  (λ f → idr f)
  (λ f → idl f)
  (λ f g h → ass h g f ⁻¹)
  (λ f g h i → {!pent i h g f!})

------------------------------------------------------------

Type : Cat (L.suc L.zero) L.zero
Type = mkCat
  Set
  (λ A B → A → B)
  (λ x → x)
  (λ g f x → g (f x))
  (λ _ → refl)
  (λ _ → refl)
  (λ _ _ _ → refl)
  (λ _ _ _ _ → refl)

------------------------------------------------------------

module _ (A : Functor SemiSimpl Type) where

  open Functor A

  A₀ : Set
  A₀ = Oᴿ 0

  A₁ : A₀ → A₀ → Set
  A₁ x₀ x₁ = Σ (Oᴿ 1) λ x₀₁ → (Mᴿ 2-0 x₀₁ ≡ x₀) × (Mᴿ 2-1 x₀₁ ≡ x₁)
  
  A₂ : {x₀ x₁ x₂ : A₀} → A₁ x₀ x₁ → A₁ x₀ x₂ → A₁ x₁ x₂ → Set
  A₂ {x₀}{x₁}{x₂} x₀₁ x₀₂ x₁₂ = Σ (Oᴿ 2) λ x₀₁₂ → (Mᴿ 3-01 x₀₁₂ ≡ proj₁ x₀₁) × (Mᴿ 3-02 x₀₁₂ ≡ proj₁ x₀₂) × (Mᴿ 3-12 x₀₁₂ ≡ proj₁ x₁₂) × (Mᴿ 3-0 x₀₁₂ ≡ x₀) × {!!}

  A₃ : {x₀ x₁ x₂ x₃ : A₀}
       {x₀₁ : A₁ x₀ x₁}{x₀₂ : A₁ x₀ x₂}{x₀₃ : A₁ x₀ x₃}
       {x₁₂ : A₁ x₁ x₂}{x₁₃ : A₁ x₁ x₃}{x₂₃ : A₁ x₂ x₃}
       → A₂ {x₀}{x₁}{x₂} x₀₁ x₀₂ x₁₂
       → A₂ {x₀}{x₁}{x₃} x₀₁ x₀₃ x₁₃
       → A₂ {x₀}{x₂}{x₃} x₀₂ x₀₃ x₂₃
       → A₂ {x₁}{x₂}{x₃} x₁₂ x₁₃ x₂₃
       → Set
  A₃ {x₀}{x₁}{x₂}{x₃}{x₀₁}{x₀₂}{x₀₃}{x₁₂}{x₁₃}{x₂₃} x₀₁₂ x₀₁₃ x₀₂₃ x₁₂₃
    = Σ (Oᴿ 3) λ x₀₁₂₃ → Mᴿ 4-012 x₀₁₂₃ ≡ proj₁ x₀₁₂ -- × ...

  -- ...
{-
  A' : ℕ → Set
  A' 0 = Oᴿ 0
  A' 1 = Σ (Oᴿ 0 × Oᴿ 0) λ { (x₀ Σ, x₁) → Σ (Oᴿ 1) λ x₀₁ → (Mᴿ 2-0 x₀₁ ≡ x₀) × (Mᴿ 2-1 x₀₁ ≡ x₁) }
  A' 2 = Σ (A' 1 × A' 1 × A' 1) λ { (x₀₁ Σ, x₀₂ Σ, x₁₂) → Σ (Oᴿ 2) λ x₀₁₂ → {!!} }
  A' _ = {!Oᴿ 1!}
  
--   A' (suc n) = Σ (A' n) λ A'n → Σ (Oᴿ (suc n)) λ x → (f : M (suc (suc n)) (suc n)) → Mᴿ f x ≡ {!!}
-}

record SSType : Set₁ where
  constructor mkSSType
  field
    Oᴿ   : ℕ → Set
    Mᴿ   : ∀ {i j} → M (suc i) (suc j) → (Oᴿ i) → (Oᴿ j)
    idᴿ  : ∀ {i} → Mᴿ (id {suc i}) ≡ λ x → x
    ∘ᴿ   : ∀ {i j k}(f : M (suc j)(suc k))(g : M (suc i)(suc j)) → Mᴿ (f ∘ g) ≡ λ x → (Mᴿ f (Mᴿ g x))
    idlᴿ : ∀ {i j}(f : M (suc i)(suc j)) → ap Mᴿ (idl f) ≡ ∘ᴿ id f ◾ ap (λ z → λ x → z (Mᴿ f x)) idᴿ ◾ refl
    idrᴿ : ∀ {i j}(f : M (suc i)(suc j)) → ap Mᴿ (idr f) ≡ ∘ᴿ f id ◾ ap (λ z → λ x → Mᴿ f (z x)) (idᴿ {i}) ◾ refl
    assᴿ : ∀ {i j k l}(f : M (suc k)(suc l))(g : M (suc j)(suc k))(h : M (suc i)(suc j))
           → ap Mᴿ (ass f g h) ≡
               (∘ᴿ f (g ∘ h) ◾ ap (λ z → λ x → (Mᴿ f (z x))) (∘ᴿ g h))
             ◾ refl
             ◾ (∘ᴿ (f ∘ g) h ◾ ap (λ z → λ x → z (Mᴿ h x)) (∘ᴿ f g)) ⁻¹
{-
  pentᴿ : ...
  pentᴿ = ...
-}


record TT : Set₁ where
  constructor mkTT
  field
    Oᴿ   : Set → Set
    Mᴿ   : ∀ {i j} → (i → j) → (Oᴿ i) → (Oᴿ j)
    idᴿ  : ∀ {i} → Mᴿ {i}(λ x → x) ≡ λ x → x
    ∘ᴿ   : ∀ {i j k}(f : j → k)(g : i → j) → Mᴿ (λ x → f (g x)) ≡ λ x → (Mᴿ f (Mᴿ g x))
    idlᴿ : ∀ {i j}(f : i → j) → ap Mᴿ refl ≡ ∘ᴿ (λ x → x) f ◾ ap (λ z → λ x → z (Mᴿ f x)) (idᴿ {j}) ◾ refl
{-
    idrᴿ : ∀ {i j}(f : M C i j) → ap Mᴿ (idr C f) ≡ ∘ᴿ f (id C) ◾ ap (∘ D (Mᴿ f)) idᴿ ◾ idr D (Mᴿ f)
    assᴿ : ∀ {i j k l}(f : M C k l)(g : M C j k)(h : M C i j)
           → ap Mᴿ (ass C f g h) ≡
               (∘ᴿ f (∘ C g h) ◾ ap (∘ D (Mᴿ f)) (∘ᴿ g h))
             ◾ ass D (Mᴿ f) (Mᴿ g) (Mᴿ h)
             ◾ (∘ᴿ (∘ C f g) h ◾ ap (λ x → ∘ D x (Mᴿ h)) (∘ᴿ f g)) ⁻¹

-}
