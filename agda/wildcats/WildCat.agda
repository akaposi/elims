{-# OPTIONS --rewriting --without-K #-}

open import Lib
open import Level

record Cat (i j : Level) : Set (suc (i ⊔ j)) where
  constructor mkCat
  field
    O   : Set i
    M   : O → O → Set j
    id  : ∀ {i} → M i i
    ∘   : ∀ {i j k} → M j k → M i j → M i k
    idl : ∀ {i j}(f : M i j) → ∘ id f ≡ f
    idr : ∀ {i j}(f : M i j) → ∘ f id ≡ f
    ass : ∀ {i j k l}(f : M k l)(g : M j k)(h : M i j)
          → (∘ f (∘ g h)) ≡ ∘ (∘ f g) h
    pent : ∀{i j k l m}(f : M l m)(g : M k l)(h : M j k)(i : M i j)
         → ass f g (∘ h i) ◾ ass (∘ f g) h i
         ≡ ap (∘ f) (ass g h i) ◾ ass f (∘ g h) i ◾ ap (λ z → ∘ z i) (ass f g h)
open Cat

record Functor {i j i' j' : Level} (C : Cat i j)(D : Cat i' j') : Set (i ⊔ j ⊔ i' ⊔ j') where
  constructor mkFunctor
  field
    Oᴿ   : O C → O D
    Mᴿ   : ∀ {i j} → M C i j → M D (Oᴿ i) (Oᴿ j)
    idᴿ  : ∀ {i} → Mᴿ (id C {i}) ≡ id D {Oᴿ i}
    ∘ᴿ   : ∀ {i j k}(f : M C j k)(g : M C i j) → Mᴿ (∘ C f g) ≡ ∘ D (Mᴿ f) (Mᴿ g)
    idlᴿ : ∀ {i j}(f : M C i j) → ap Mᴿ (idl C f) ≡ ∘ᴿ (id C) f ◾ ap (λ x → ∘ D x (Mᴿ f)) idᴿ ◾ idl D (Mᴿ f)
    idrᴿ : ∀ {i j}(f : M C i j) → ap Mᴿ (idr C f) ≡ ∘ᴿ f (id C) ◾ ap (∘ D (Mᴿ f)) idᴿ ◾ idr D (Mᴿ f)
    assᴿ : ∀ {i j k l}(f : M C k l)(g : M C j k)(h : M C i j)
           → ap Mᴿ (ass C f g h) ≡
               (∘ᴿ f (∘ C g h) ◾ ap (∘ D (Mᴿ f)) (∘ᴿ g h))
             ◾ ass D (Mᴿ f) (Mᴿ g) (Mᴿ h)
             ◾ (∘ᴿ (∘ C f g) h ◾ ap (λ x → ∘ D x (Mᴿ h)) (∘ᴿ f g)) ⁻¹
open Functor

Id : ∀ {i j}(C : Cat i j) → Functor C C
Id C =
  record
    { Oᴿ   = λ i → i
    ; Mᴿ   = λ f → f
    ; idᴿ  = refl
    ; ∘ᴿ   = λ _ _ → refl
    ; idlᴿ = λ f → ap-id (idl C f)
    ; idrᴿ = λ f → ap-id (idr C f)
    ; assᴿ = λ f g h → ◾refl _ ◾ ap-id (ass C f g h)
    }

-- tr2'↓ :
--   ∀ {α β} {A : Set α} (P : A → A → Set β)
--     {a a' : A}(p : a ≡ a')(x : P a a)
--   → tr (λ x → P x x) p x ≡ tr2' P p p x
-- tr2'↓ =P refl x = refl

-- Functor≡ :
--   ∀ {i j}{C D : Cat i j}
--     {Oᴿ₀ Oᴿ₁ : O C → O D}(Oᴿ₂ : ∀ i → Oᴿ₀ i ≡ Oᴿ₁ i)

--     {Mᴿ₀ : ∀ {i j} → M C i j → M D (Oᴿ₀ i) (Oᴿ₀ j)}
--       {Mᴿ₁ : ∀ {i j} → M C i j → M D (Oᴿ₁ i) (Oᴿ₁ j)}
--         (Mᴿ₂ : ∀ {i j} (f : M C i j) →  tr2' (M D) (Oᴿ₂ i) (Oᴿ₂ j) (Mᴿ₀ f) ≡ Mᴿ₁ f)

--     {idᴿ₀ : ∀ {i} → Mᴿ₀ (id C {i}) ≡ id D {Oᴿ₀ i}}
--       {idᴿ₁ : ∀ {i} → Mᴿ₁ (id C {i}) ≡ id D {Oᴿ₁ i}}
--         (idᴿ₂ : ∀ {i} →
--            tr2 (λ x y → y ≡ id D {x}) (Oᴿ₂ i)
--            (tr2'↓ (M D) (Oᴿ₂ i) (Mᴿ₀ (id C)) ◾ Mᴿ₂ (id C {i})) (idᴿ₀ {i}) ≡ idᴿ₁ {i})

--     {∘ᴿ₀ : ∀ {i j k}(f : M C j k)(g : M C i j) → Mᴿ₀ (∘ C f g) ≡ ∘ D (Mᴿ₀ f) (Mᴿ₀ g)}
--       {∘ᴿ₁ : ∀ {i j k}(f : M C j k)(g : M C i j) → Mᴿ₁ (∘ C f g) ≡ ∘ D (Mᴿ₁ f) (Mᴿ₁ g)}
--         (∘ᴿ₂ : ∀ {i j k} f g → {!∘ᴿ₀ {i}{j} f g!} ≡ ∘ᴿ₁ {i}{j}{k} f g)

--   → mkFunctor Oᴿ₀ Mᴿ₀ idᴿ₀ {!!} {!!} {!!} {!!}
--     ≡
--     mkFunctor Oᴿ₁ Mᴿ₁ idᴿ₁ {!!} {!!} {!!} {!!}
-- Functor≡ = {!!}

Comp : ∀ {i j}{C D E : Cat i j} → Functor D E → Functor C D → Functor C E
Comp {i}{j}{C} {D} {E} F G =
  record
    { Oᴿ   = λ i → Oᴿ F (Oᴿ G i)
    ; Mᴿ   = λ f → Mᴿ F (Mᴿ G f)
    ; idᴿ  = λ {i} → ap (Mᴿ F) (idᴿ G {i}) ◾ idᴿ F
    ; ∘ᴿ   = λ f g → ap (Mᴿ F) (∘ᴿ G f g) ◾ ∘ᴿ F _ _
    ; idlᴿ = λ f →
          ap (ap (Mᴿ F)) (idlᴿ G f)
        ◾ ap (ap (Mᴿ F) (∘ᴿ G (id C) f) ◾_)
            (ap (ap (λ x → Mᴿ F (∘ D x (Mᴿ G f))) (idᴿ G) ◾_) (idlᴿ F (Mᴿ G f))
          ◾ ap (_◾ ap (λ x → ∘ E x (Mᴿ F (Mᴿ G f))) (idᴿ F) ◾ idl E (Mᴿ F (Mᴿ G f)))
               (J (λ idD idᴿG →
                          ap (λ x → Mᴿ F (∘ D x (Mᴿ G f))) (idᴿG) ◾ ∘ᴿ F (idD) (Mᴿ G f)
                        ≡ ∘ᴿ F (Mᴿ G (id C)) (Mᴿ G f) ◾ ap (λ x → ∘ E (Mᴿ F x) (Mᴿ F (Mᴿ G f))) (idᴿG))
                    refl
                    (idᴿ G)))
    ; idrᴿ = λ f →
        ap (ap (Mᴿ F)) (idrᴿ G f)
      ◾ ap (ap (Mᴿ F) (∘ᴿ G f (id C)) ◾_)
            (ap (ap (λ x → Mᴿ F (∘ D (Mᴿ G f) x)) (idᴿ G) ◾_) (idrᴿ F (Mᴿ G f))
          ◾ ap (_◾ ap (∘ E (Mᴿ F (Mᴿ G f))) (idᴿ F) ◾ idr E (Mᴿ F (Mᴿ G f)))
               (J (λ idD idᴿG →
                        ap (λ x → Mᴿ F (∘ D (Mᴿ G f) x)) idᴿG ◾ ∘ᴿ F (Mᴿ G f) idD
                      ≡ ∘ᴿ F (Mᴿ G f) (Mᴿ G (id C)) ◾ ap (λ x → ∘ E (Mᴿ F (Mᴿ G f)) (Mᴿ F x)) idᴿG)
                    refl
                    (idᴿ G)))
    ; assᴿ = λ f g h →
         ap (ap (Mᴿ F)) (assᴿ G f g h)
       ◾ ap (λ x → ap (Mᴿ F) (∘ᴿ G f (∘ C g h)) ◾
                    ap (λ x → Mᴿ F (∘ D (Mᴿ G f) x)) (∘ᴿ G g h) ◾
                    x ◾
                    ap (Mᴿ F) (ap (λ x → ∘ D x (Mᴿ G h)) (∘ᴿ G f g) ⁻¹) ◾
                    ap (Mᴿ F) (∘ᴿ G (∘ C f g) h ⁻¹))
             (assᴿ F (Mᴿ G f) (Mᴿ G g) (Mᴿ G h))
       ◾ ap (λ x → ap (Mᴿ F) (∘ᴿ G f (∘ C g h)) ◾ x ◾ ap (Mᴿ F) (∘ᴿ G (∘ C f g) h ⁻¹))
               (wedge (ap (λ x → Mᴿ F (∘ D (Mᴿ G f) x)) (∘ᴿ G g h) ◾
                       ∘ᴿ F (Mᴿ G f) (∘ D (Mᴿ G g) (Mᴿ G h)) ◾
                       ap (∘ E (Mᴿ F (Mᴿ G f))) (∘ᴿ F (Mᴿ G g) (Mᴿ G h)))
                      (∘ᴿ F (Mᴿ G f) (Mᴿ G (∘ C g h)) ◾
                       ap (λ x → ∘ E (Mᴿ F (Mᴿ G f)) (Mᴿ F x)) (∘ᴿ G g h) ◾
                       ap (∘ E (Mᴿ F (Mᴿ G f))) (∘ᴿ F (Mᴿ G g) (Mᴿ G h)))
                      (ass E (Mᴿ F (Mᴿ G f)) (Mᴿ F (Mᴿ G g)) (Mᴿ F (Mᴿ G h)))
                      (ap (λ x → ∘ E x (Mᴿ F (Mᴿ G h))) (∘ᴿ F (Mᴿ G f) (Mᴿ G g) ⁻¹) ◾
                       ∘ᴿ F (∘ D (Mᴿ G f) (Mᴿ G g)) (Mᴿ G h) ⁻¹ ◾
                       ap (λ x → Mᴿ F (∘ D x (Mᴿ G h))) (∘ᴿ G f g ⁻¹))
                      (ap (λ x → ∘ E x (Mᴿ F (Mᴿ G h))) (∘ᴿ F (Mᴿ G f) (Mᴿ G g) ⁻¹) ◾
                       ap (λ x → ∘ E (Mᴿ F x) (Mᴿ F (Mᴿ G h))) (∘ᴿ G f g ⁻¹) ◾
                       ∘ᴿ F (Mᴿ G (∘ C f g)) (Mᴿ G h) ⁻¹)
                      (ap (_◾ ap (∘ E (Mᴿ F (Mᴿ G f))) (∘ᴿ F (Mᴿ G g) (Mᴿ G h)))
                         (J (λ y p →
                                   ap (λ x → Mᴿ F (∘ D (Mᴿ G f) x)) p ◾ ∘ᴿ F (Mᴿ G f) y
                                 ≡ ∘ᴿ F (Mᴿ G f) (Mᴿ G (∘ C g h)) ◾ ap (λ x → ∘ E (Mᴿ F (Mᴿ G f)) (Mᴿ F x)) p)
                              refl
                              (∘ᴿ G g h)))
                      (ap (ap (λ x → ∘ E x (Mᴿ F (Mᴿ G h))) (∘ᴿ F (Mᴿ G f) (Mᴿ G g) ⁻¹) ◾_)
                        ((∘ᴿ F (∘ D (Mᴿ G f) (Mᴿ G g)) (Mᴿ G h) ⁻¹ ◾ ap (λ x → Mᴿ F (∘ D x (Mᴿ G h))) (∘ᴿ G f g ⁻¹)
                         ≡ ap (λ x → ∘ E (Mᴿ F x) (Mᴿ F (Mᴿ G h))) (∘ᴿ G f g ⁻¹) ◾ ∘ᴿ F (Mᴿ G (∘ C f g)) (Mᴿ G h) ⁻¹)
                         ∋ J (λ y p → ∘ᴿ F y (Mᴿ G h) ⁻¹ ◾
                                       ap (λ x → Mᴿ F (∘ D x (Mᴿ G h))) (p ⁻¹)
                                       ≡
                                       ap (λ x → ∘ E (Mᴿ F x) (Mᴿ F (Mᴿ G h))) (p ⁻¹) ◾
                                       ∘ᴿ F (Mᴿ G (∘ C f g)) (Mᴿ G h) ⁻¹)
                              refl
                              (∘ᴿ G f g))))
    }

-- Idl : ∀ {i j}(C D : Cat i j)(F : Functor C D) → Comp (Id _) F ≡ F
-- Idl C D F = Functor≡
--   (λ i → refl)
--   (λ {i}{j} f → {!!})
--   {!!}
--   {!!}
