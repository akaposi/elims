{-# OPTIONS --without-K #-}

open import Lib


record PreCatᴿ : Set₁ where
  infixr 5 _∘₀_ _∘₁_

  field
    C₀   : Set
    c₀   : C₀ → C₀ → Set
    id₀  : ∀ {i} → c₀ i i
    _∘₀_ : ∀ {i j k} → c₀ j k → c₀ i j → c₀ i k
    idl₀ : ∀ {i j} (f : c₀ i j) → id₀ ∘₀ f ≡ f
    idr₀ : ∀ {i j} (f : c₀ i j) → f ∘₀ id₀ ≡ f
    ass₀ : ∀ {i j k l} (f : c₀ k l) (g : c₀ j k) (h : c₀ i j) → f ∘₀ (g ∘₀ h) ≡ (f ∘₀ g) ∘₀ h

    C₁   : Set
    c₁   : C₁ → C₁ → Set
    id₁  : ∀ {i} → c₁ i i
    _∘₁_ : ∀ {i j k} → c₁ j k → c₁ i j → c₁ i k
    idl₁ : ∀ {i j} (f : c₁ i j) → id₁ ∘₁ f ≡ f
    idr₁ : ∀ {i j} (f : c₁ i j) → f ∘₁ id₁ ≡ f
    ass₁ : ∀ {i j k l} (f : c₁ k l) (g : c₁ j k) (h : c₁ i j) → f ∘₁ (g ∘₁ h) ≡ (f ∘₁ g) ∘₁ h

    Cᵣ   : C₀ → C₁
    cᵣ   : ∀ {i j} → c₀ i j → c₁ (Cᵣ i) (Cᵣ j)
    idᵣ  : ∀ {i} → cᵣ (id₀ {i}) ≡ id₁ {Cᵣ i}
    ∘ᵣ   : ∀ {i j k}(f : c₀ j k)(g : c₀ i j) → cᵣ (f ∘₀ g) ≡ cᵣ f ∘₁ cᵣ g

    idlᵣ : ∀ {i j} (f₀ : c₀ i j) →

             --   tr (λ x → cᵣ (id₀ ∘₀ f₀) ≡ x ∘₁ cᵣ f₀) idᵣ (∘ᵣ id₀ f₀) ⁻¹
             -- ◾ (∘ᵣ id₀ f₀ ◾ ap (_∘₁ cᵣ f₀) idᵣ) ⁻¹
               (∘ᵣ id₀ f₀ ◾ ap (_∘₁ cᵣ f₀) idᵣ) ⁻¹
             ◾ ap cᵣ (idl₀ f₀)
             ≡
             idl₁ (cᵣ f₀)

    idrᵣ : ∀ {i j} (f₀ : c₀ i j) →

             --   tr (λ x → cᵣ (f₀ ∘₀ id₀) ≡ cᵣ f₀ ∘₁ x) idᵣ (∘ᵣ f₀ id₀) ⁻¹
             -- ◾ ap cᵣ (idr₀ f₀)
               (∘ᵣ f₀ id₀ ◾ ap (cᵣ f₀ ∘₁_) idᵣ) ⁻¹
             ◾ ap cᵣ (idr₀ f₀)
             ≡
             idr₁ (cᵣ f₀)

    assᵣ : ∀ {i j k l} (f₀ : c₀ k l) (g₀ : c₀ j k) (h₀ : c₀ i j) →

             --   tr (λ x → cᵣ (f₀ ∘₀ g₀ ∘₀ h₀) ≡ cᵣ f₀ ∘₁ x) (∘ᵣ g₀ h₀) (∘ᵣ f₀ (g₀ ∘₀ h₀)) ⁻¹
             -- ◾ ap cᵣ (ass₀ f₀ g₀ h₀)
             -- ◾ tr (λ x → cᵣ ((f₀ ∘₀ g₀) ∘₀ h₀) ≡ x ∘₁ cᵣ h₀) (∘ᵣ f₀ g₀) (∘ᵣ (f₀ ∘₀ g₀) h₀)
               (∘ᵣ f₀ (g₀ ∘₀ h₀) ◾ ap (cᵣ f₀ ∘₁_) (∘ᵣ g₀ h₀)) ⁻¹
             ◾ ap cᵣ (ass₀ f₀ g₀ h₀)
             ◾ ∘ᵣ (f₀ ∘₀ g₀) h₀ ◾ ap (_∘₁ cᵣ h₀) (∘ᵣ f₀ g₀)
             ≡
             ass₁ (cᵣ f₀) (cᵣ g₀) (cᵣ h₀)

  -- examples for coherence
  postulate
    i j k l : C₀
    f       : c₀ i j
    g       : c₀ j k
    h       : c₀ k l

  p1 : cᵣ (id₀ ∘₀ f) ≡ cᵣ f
  p1 = ap cᵣ (idl₀ f)

  p2 : cᵣ (id₀ ∘₀ f) ≡ cᵣ f
  p2 = ∘ᵣ id₀ f ◾ ap (_∘₁ cᵣ f) idᵣ ◾ idl₁ (cᵣ f)

  p12 : p1 ≡ p2
  p12 = cancelₗ⁻¹ (∘ᵣ id₀ f ◾ ap (_∘₁ cᵣ f) idᵣ) _ _ (idlᵣ f)
      ◾ ass≡ (∘ᵣ id₀ f) (ap (_∘₁ cᵣ f) idᵣ) (idl₁ (cᵣ f))

  p3 : cᵣ (h ∘₀ g ∘₀ f) ≡ cᵣ ((h ∘₀ g) ∘₀ f)
  p3 = ap cᵣ (ass₀ h g f)

  p4 : cᵣ (h ∘₀ g ∘₀ f) ≡ cᵣ ((h ∘₀ g) ∘₀ f)
  p4 = ∘ᵣ h (g ∘₀ f)
     ◾ ap (cᵣ h ∘₁_) (∘ᵣ g f)
     ◾ ass₁ (cᵣ h) (cᵣ g) (cᵣ f)
     ◾ ap (_∘₁ cᵣ f) (∘ᵣ h g ⁻¹)
     ◾ ∘ᵣ (h ∘₀ g) f ⁻¹

  p34 : p3 ≡ p4
  p34 =
       cancelᵣ (ap cᵣ (ass₀ h g f)) _ _
         (cancelₗ⁻¹ (∘ᵣ h (g ∘₀ f) ◾ ap (_∘₁_ (cᵣ h)) (∘ᵣ g f)) _ _ (assᵣ h g f))
     ◾ ap (((∘ᵣ h (g ∘₀ f) ◾ ap (_∘₁_ (cᵣ h)) (∘ᵣ g f)) ◾ ass₁ (cᵣ h) (cᵣ g) (cᵣ f)) ◾_)
            (◾⁻¹ (∘ᵣ (h ∘₀ g) f) (ap (_∘₁ cᵣ f) (∘ᵣ h g))
          ◾ ap (_◾ ∘ᵣ (h ∘₀ g) f ⁻¹) (ap⁻¹ (_∘₁ cᵣ f) (∘ᵣ h g) ⁻¹))
     ◾ ass≡
         (∘ᵣ h (g ∘₀ f) ◾ ap (_∘₁_ (cᵣ h)) (∘ᵣ g f))
         (ass₁ (cᵣ h) (cᵣ g) (cᵣ f))
         (ap (_∘₁ cᵣ f) (∘ᵣ h g ⁻¹) ◾ ∘ᵣ (h ∘₀ g) f ⁻¹)
     ◾ ass≡
         (∘ᵣ h (g ∘₀ f))
         (ap (_∘₁_ (cᵣ h)) (∘ᵣ g f))
         (ass₁ (cᵣ h) (cᵣ g) (cᵣ f) ◾ ap (_∘₁ cᵣ f) (∘ᵣ h g ⁻¹) ◾ ∘ᵣ (h ∘₀ g) f ⁻¹)
