{-# OPTIONS --without-K --rewriting #-}

module Lib where

open import Relation.Binary.PropositionalEquality using (_≡_;refl) public

infixr 5 _◾_
infix 6 _⁻¹

infix 3 _∋_
_∋_ : ∀ {α}(A : Set α) → A → A
A ∋ a = a

_◾_ : ∀ {a} {A : Set a}{x y z : A} → x ≡ y → y ≡ z → x ≡ z
_◾_ refl eq = eq

_⁻¹ : ∀ {a} {A : Set a}{x y : A} → x ≡ y → y ≡ x
_⁻¹ refl = refl

ap : ∀ {a b} {A : Set a} {B : Set b}
       (f : A → B) {x y} → x ≡ y → f x ≡ f y
ap f refl = refl

ap-◾ : ∀ {α β}{A : Set α}{B : Set β}(f : A → B){x y z : A}(p : x ≡ y)(q : y ≡ z) → (ap f (p ◾ q)) ≡ (ap f p ◾ ap f q)
ap-◾ f refl refl = refl

◾refl : ∀ {α}{A : Set α}{x y : A}(p : x ≡ y) → (p ◾ refl) ≡ p
◾refl refl = refl

J : ∀{α β}{A : Set α}{t : A}(P : ∀ u → t ≡ u → Set β) → P t refl → ∀ {u} eq → P u eq
J P pr refl = pr

ap-id : ∀ {α}{A : Set α}{x y : A}(p : x ≡ y) → ap (λ x → x) p ≡ p
ap-id refl = refl

ap-∘ : ∀ {α β γ}{A : Set α}{B : Set β}{C : Set γ}
         {x y : A}(f : B → C)(g : A → B)(p : x ≡ y) → ap f (ap g p) ≡ ap (λ x → f (g x)) p
ap-∘ f g refl = refl

tr : ∀ {α β}{A : Set α}(P : A → Set β){x y} → x ≡ y → P x → P y
tr P refl px = px

tr2 :
  ∀ {i j k}{A : Set i}{B : A → Set j}(C : ∀ a → B a → Set k)
    {a₀ : A}{a₁ : A}(a₂ : a₀ ≡ a₁)
    {b₀ : B a₀}{b₁ : B a₁}(b₂ : tr B a₂ b₀ ≡ b₁)
  → C a₀ b₀ → C a₁ b₁
tr2 {B = B} C {a₀}{.a₀} refl refl c₀ = c₀

tr2' : ∀ {α β γ}{A : Set α}{B : Set β}(P : A → B → Set γ){a a' b b'} → a ≡ a' → b ≡ b' → P a b → P a' b'
tr2' P refl refl x = x

_⊗_ :
  ∀ {α β}{A : Set α}{B : Set β}
    {f g : A → B}(p : f ≡ g){a a' : A}(q : a ≡ a')
  → f a ≡ g a'
refl ⊗ refl = refl
infixl 8 _⊗_

module _ {α}{A : Set α} where
  reflᵣ : {x y : A}(p : x ≡ y) → p ◾ refl ≡ p
  reflᵣ refl = refl

  invᵣ : {x y : A}(p : x ≡ y) → p ◾ p ⁻¹ ≡ refl
  invᵣ refl = refl

  invₗ : {x y : A}(p : x ≡ y) → p ⁻¹ ◾ p ≡ refl
  invₗ refl = refl

  ◾-ass : {a b c d : A}(p : a ≡ b)(q : b ≡ c) (r : c ≡ d) → (p ◾ q) ◾ r ≡ p ◾ q ◾ r
  ◾-ass refl _ _ = refl

  transferₗ⁻¹ : {a b c : A}(p : b ≡ a)(q : b ≡ c)(r : a ≡ c) → p ⁻¹ ◾ q ≡ r → q ≡ p ◾ r
  transferₗ⁻¹ refl q r s = s

  transferₗ : {a b c : A}(p : a ≡ b)(q : b ≡ c)(r : a ≡ c) → p ◾ q ≡ r → q ≡ p ⁻¹ ◾ r
  transferₗ refl q r s = s

  transferᵣ : {a b c : A}(p : a ≡ b)(q : b ≡ c)(r : a ≡ c) → p ◾ q ≡ r → p ≡ r ◾ q ⁻¹
  transferᵣ refl refl .refl refl = refl

  transferᵣ⁻¹ : {a b c : A}(p : a ≡ b)(q : c ≡ b)(r : a ≡ c) → p ◾ q ⁻¹ ≡ r → p ≡ r ◾ q
  transferᵣ⁻¹ refl refl .refl refl = refl

  ◾⁻¹ : ∀ {a b c : A}(p : a ≡ b)(q : b ≡ c) → (p ◾ q) ⁻¹ ≡ q ⁻¹ ◾ p ⁻¹
  ◾⁻¹ refl refl = refl

  ap⁻¹ : ∀ {β}{B : Set β}{x y : A}(f : A → B)(p : x ≡ y) → ap f p ⁻¹ ≡ ap f (p ⁻¹)
  ap⁻¹ f refl = refl

  wedge : {a b c d : A}(p p' : a ≡ b)(q : b ≡ c)(r r' : c ≡ d) → p ≡ p' → r ≡ r' → p ◾ q ◾ r ≡ p' ◾ q ◾ r'
  wedge p .p refl r .r refl refl = refl

--------------------------------------------------------------------------------

{-# BUILTIN REWRITE _≡_ #-}
{-# REWRITE ◾-ass ◾⁻¹ reflᵣ ap-id ap-◾ ap-∘ ap⁻¹ #-}

open import Agda.Primitive

record Σ {i j} (A : Set i) (B : A → Set j) : Set (i ⊔ j) where
  constructor _Σ,_
  field
    proj₁ : A
    proj₂ : B proj₁
infixr 5 _Σ,_

Σ,= : ∀{ℓ ℓ'}{A : Set ℓ}{B : A → Set ℓ'}{a a' : A}{b : B a}{b' : B a'}
     (p : a ≡ a') → tr B p b ≡ b' → _≡_ {A = Σ A B} (a Σ, b) (a' Σ, b')
Σ,= refl refl = refl

open Σ public

_×_ : ∀{i j} → Set i → Set j → Set (i ⊔ j)
A × B = Σ A λ _ → B
infixr 4 _×_

