
module Lib where

open import Relation.Binary.PropositionalEquality using (_≡_; refl) public
open import Level

infix 3 _∋_
_∋_ : ∀ {α}(A : Set α) → A → A
A ∋ a = a

_∘_ : ∀ {a b c}
        {A : Set a} {B : A → Set b} {C : {x : A} → B x → Set c} →
        (∀ {x} (y : B x) → C y) → (g : (x : A) → B x) →
        ((x : A) → C (g x))
f ∘ g = λ x → f (g x)

_◾_ : ∀{i}{A : Set i}{x y z : A} → x ≡ y → y ≡ z → x ≡ z
refl ◾ q = q
infixr 4 _◾_

_⁻¹ : ∀{i}{A : Set i}{x y : A} → x ≡ y → y ≡ x
refl ⁻¹ = refl
infix 6 _⁻¹

coe : ∀{i}{A B : Set i} → A ≡ B → A → B
coe refl a = a

coe◾ : ∀ {α}{A B C : Set α}(p : A ≡ B)(q : B ≡ C)(a : A) → coe (p ◾ q) a ≡ coe q (coe p a)
coe◾ refl refl a = refl

coe≡≡ : ∀ {α}{A B : Set α}{p₀ p₁ : A ≡ B}(p₂ : p₀ ≡ p₁){a₀ a₁}(a₂ : a₀ ≡ a₁) → coe p₀ a₀ ≡ coe p₁ a₁
coe≡≡ refl refl = refl

◾refl : ∀ {α}{A : Set α}{x y : A}(p : x ≡ y) → (p ◾ refl) ≡ p
◾refl refl = refl

_&_ :
  ∀{i j}{A : Set i}{B : Set j}(f : A → B){a₀ a₁ : A}(a₂ : a₀ ≡ a₁)
  → f a₀ ≡ f a₁
f & refl = refl
infixl 9 _&_

_⊗_ :
  ∀ {α β}{A : Set α}{B : Set β}
    {f g : A → B}(p : f ≡ g){a a' : A}(q : a ≡ a')
  → f a ≡ g a'
refl ⊗ refl = refl
infixl 8 _⊗_

apd : ∀{i j}{A : Set i}{B : A → Set j}(f : (x : A) → B x){a₀ a₁ : A}(a₂ : a₀ ≡ a₁)
    → coe (B & a₂) (f a₀) ≡ f a₁
apd f refl = refl

J :
  ∀{α β}{A : Set α} {x : A} (P : {y : A} → x ≡ y → Set β)
  → P refl → {y : A} → (w : x ≡ y) → P w
J P pr refl = pr

record Σ {i j} (A : Set i) (B : A → Set j) : Set (i ⊔ j) where
  constructor _,_
  field
    proj₁ : A
    proj₂ : B proj₁
infixr 5 _,_

∃ : ∀ {a b} {A : Set a} → (A → Set b) → Set (a ⊔ b)
∃ = Σ _

∃₂ : ∀ {a b c} {A : Set a} {B : A → Set b}
     (C : (x : A) → B x → Set c) → Set (a ⊔ b ⊔ c)
∃₂ C = ∃ λ a → ∃ λ b → C a b

,_ : ∀ {a b} {A : Set a} {B : A → Set b} {x} → B x → ∃ B
, y = _ , y

open Σ public

_×_ : ∀{i j} → Set i → Set j → Set (i ⊔ j)
A × B = Σ A λ _ → B
infixr 4 _×_

,Σ≡ : ∀{i j}{A : Set i}{B : A → Set j}{a a' : A}{b : B a}{b' : B a'}
     (p : a ≡ a') → coe (B & p) b ≡ b' → (Σ A B ∋ (a , b)) ≡ (a' , b')
,Σ≡ refl refl = refl

record ⊤ : Set where
  constructor tt

data ⊥ : Set where

⊥-elim : ∀{i}{A : Set i} → ⊥ → A
⊥-elim ()

data _⊎_ (A B : Set) : Set where
  inl : A → A ⊎ B
  inr : B → A ⊎ B
infixr 1 _⊎_

either : {A B C : Set} → (A → C) → (B → C) → A ⊎ B → C
either f g (inl x) = f x
either f g (inr x) = g x

ind⊎ : {A B : Set}(P : A ⊎ B → Set) → ((a : A) → P (inl a)) → ((b : B) → P (inr b))
     → (w : A ⊎ B) → P w
ind⊎ P ca cb (inl a) = ca a
ind⊎ P ca cb (inr b) = cb b

record Reveal_·_is_ {a b} {A : Set a} {B : A → Set b}
                    (f : (x : A) → B x) (x : A) (y : B x) :
                    Set (a ⊔ b) where
  constructor pack
  field eq : f x ≡ y

inspect : ∀ {a b} {A : Set a} {B : A → Set b}
          (f : (x : A) → B x) (x : A) → Reveal f · x is f x
inspect f x = pack refl

postulate
  ext  : ∀{i j}{A : Set i}{B : A → Set j}{f g : (x : A) → B x}
          → ((x : A) → f x  ≡ g x) → _≡_ f g

  exti : ∀{i j}{A : Set i}{B : A → Set j}{f g : {x : A} → B x}
          → ((x : A) → f {x} ≡ g {x}) → _≡_ {A = {x : A} → B x} f g

Π≡≡ :
  ∀ {α β}{A : Set α}{B₀ B₁ : A → Set β}
  → (B₂ : ∀ a → B₀ a ≡ B₁ a)
  → (∀ a → B₀ a) ≡ (∀ a → B₁ a)
Π≡≡ B₂ = (λ B → ∀ a → B a) & ext B₂

Πi≡≡ :
  ∀ {α β}{A : Set α}{B₀ B₁ : A → Set β}
  → (B₂ : ∀ a → B₀ a ≡ B₁ a)
  → (∀ {a} → B₀ a) ≡ (∀ {a} → B₁ a)
Πi≡≡ B₂ = (λ B → ∀ {a} → B a) & ext B₂

coeΠ :
 ∀ {α β γ}{A : Set α}{B : Set β}(C : A → B → Set γ)
   {b b' : B}(p : b ≡ b')(f : ∀ a → C a b)
 →  coe ((λ x → ∀ a → C a x) & p) f ≡ (λ a → coe (C a & p) (f a))
coeΠ C refl f = refl

coeΠi :
 ∀ {α β γ}{A : Set α}{B : Set β}(C : A → B → Set γ)
   {b b' : B}(p : b ≡ b')(f : ∀ {a} → C a b)
 →  (λ {a} → coe ((λ x → ∀ {a} → C a x) & p) f {a}) ≡ (λ {a} → coe (C a & p) (f {a}))
coeΠi C refl f = refl


-- HEq
--------------------------------------------------------------------------------
record _≅_ {α}{A B : Set α}(a : A)(b : B) : Set (suc α) where
  constructor con
  field
    ty : A ≡ B
    tm : coe ty a ≡ b
open _≅_ public

infix 5 _~
pattern _~ p = record {ty = refl; tm = p}
pattern refl̃ = refl ~

uncoe : ∀ {α}{A B : Set α}(p : B ≡ A) (b : B) → coe p b ≅ b
uncoe refl a = refl̃

infix 6 _⁻¹̃
_⁻¹̃ : ∀ {α}{A B : Set α}{a : A}{b : B} → a ≅ b → b ≅ a
refl̃ ⁻¹̃ = refl̃

infixr 5 _◾̃_
_◾̃_ : ∀ {α}{A B C : Set α}{a : A}{b : B}{c : C} → a ≅ b → b ≅ c → a ≅ c
refl̃ ◾̃ q = q

ap̃ :
  ∀ {α β}{A : Set α}{B : A → Set β}
  (f : ∀ a → B a){a₀ a₁ : A}(a₂ : a₀ ≡ a₁) → f a₀ ≅ f a₁
ap̃ f refl = refl̃

ap2̃ :
  ∀ {α β γ}
  {A : Set α}{B : A → Set β}{C : ∀ a → B a → Set γ}
  (f : ∀ a → (b : B a) → C a b)
  {a₀ a₁ : A}(a₂ : a₀ ≡ a₁){b₀ : B a₀}{b₁ : B a₁}(b₂ : b₀ ≅ b₁) → f a₀ b₀ ≅ f a₁ b₁
ap2̃ f refl refl̃ = refl̃

ap3̃ :
  ∀ {α β γ δ}
  {A : Set α}{B : A → Set β}{C : ∀ a (b : B a) → Set γ}{D : ∀ a (b : B a)(c : C a b) → Set δ}
  (f : ∀ a b c → D a b c)
  {a₀ a₁ : A}(a₂ : a₀ ≡ a₁)
  {b₀ : B a₀}{b₁ : B a₁}(b₂ : b₀ ≅ b₁)
  {c₀ : C a₀ b₀}{c₁ : C a₁ b₁}(c₂ : c₀ ≅ c₁)
  → f a₀ b₀ c₀ ≅ f a₁ b₁ c₁
ap3̃ f refl refl̃ refl̃ = refl̃

ap4̃ :
  ∀ {α β γ δ ε}
  {A : Set α}{B : A → Set β}{C : ∀ a (b : B a) → Set γ}
    {D : ∀ a b (c : C a b) → Set δ}{E : ∀ a b c (d : D a b c) → Set ε}
  (f : ∀ a b c d → E a b c d)
  {a₀ a₁ : A}                        (a₂ : a₀ ≡ a₁)
  {b₀ : B a₀}      {b₁ : B a₁}       (b₂ : b₀ ≅ b₁)
  {c₀ : C a₀ b₀}   {c₁ : C a₁ b₁}    (c₂ : c₀ ≅ c₁)
  {d₀ : D a₀ b₀ c₀}{d₁ : D a₁ b₁ c₁} (d₂ : d₀ ≅ d₁)
  → f a₀ b₀ c₀ d₀ ≅ f a₁ b₁ c₁ d₁
ap4̃ f refl refl̃ refl̃ refl̃ = refl̃

uñ : ∀ {α}{A : Set α}{a b : A} → a ≅ b → a ≡ b
uñ refl̃ = refl

ap2 :
  ∀{α β γ}{A : Set α}{B : A → Set β}{C : Set γ}
  (f : ∀ a → B a → C)
  {a₀ a₁ : A}(a₂ : a₀ ≡ a₁)
  {b₀ : B a₀}{b₁ : B a₁}(b₂ : b₀ ≅ b₁)
  → f a₀ b₀ ≡ f a₁ b₁
ap2 f refl refl̃ = refl

ap3 :
  ∀ {α β γ δ}
  {A : Set α}{B : A → Set β}{C : ∀ a (b : B a) → Set γ}{D : Set δ}
  (f : ∀ a b (c : C a b) → D)
  {a₀ a₁ : A}(a₂ : a₀ ≡ a₁)
  {b₀ : B a₀}{b₁ : B a₁}(b₂ : b₀ ≅ b₁)
  {c₀ : C a₀ b₀}{c₁ : C a₁ b₁}(c₂ : c₀ ≅ c₁)
  → f a₀ b₀ c₀ ≡ f a₁ b₁ c₁
ap3 f refl refl̃ refl̃ = refl

ap3' :
  ∀ {α β γ δ}
  {A : Set α}{B : A → Set β}{C : ∀ a (b : B a) → Set γ}{D : Set δ}
  (f : ∀ a b (c : C a b) → D)
  {a₀ a₁ : A}(a₂ : a₀ ≡ a₁)
  {b₀ : B a₀}{b₁ : B a₁}(b₂ : b₀ ≅ b₁)
  {c₀ : C a₀ b₀}{c₁ : C a₁ b₁}(c₂ : c₀ ≅ c₁)
  → f a₀ b₀ c₀ ≡ f a₁ b₁ c₁
ap3' f refl refl̃ refl̃ = refl

ap4 :
  ∀ {α β γ δ ε}
  {A : Set α}{B : A → Set β}{C : ∀ a (b : B a) → Set γ}
    {D : ∀ a b (c : C a b) → Set δ}{E : Set ε}
  (f : ∀ a b c (d : D a b c) → E)
  {a₀ a₁ : A}                        (a₂ : a₀ ≡ a₁)
  {b₀ : B a₀}      {b₁ : B a₁}       (b₂ : b₀ ≅ b₁)
  {c₀ : C a₀ b₀}   {c₁ : C a₁ b₁}    (c₂ : c₀ ≅ c₁)
  {d₀ : D a₀ b₀ c₀}{d₁ : D a₁ b₁ c₁} (d₂ : d₀ ≅ d₁)
  → f a₀ b₀ c₀ d₀ ≡ f a₁ b₁ c₁ d₁
ap4 f refl refl̃ refl̃ refl̃ = refl

ext̃ :
  ∀ {α β}
    {A : Set α}
    {B₀ B₁ : A → Set β}
    {f₀ : ∀ a → B₀ a}{f₁ : ∀ a → B₁ a}
  → (∀ a → f₀ a ≅ f₁ a)
  → f₀ ≅ f₁
ext̃ {A = A} {B₀} {B₁} {f₀} {f₁} f₂ =
  J (λ {B₁} (B₂ : B₀ ≡ B₁) →
          {f₀ : ∀ a → B₀ a}{f₁ : ∀ a → B₁ a}
        → (∀ a → f₀ a ≅ f₁ a)
        → f₀ ≅ f₁)
     (λ {f₀}{f₁} f₂ → ext (λ a → uñ (f₂ a)) ~)
     (ext (λ a → f₂ a .ty)) f₂

ext̃' :
  ∀ {α β}
    {A₀ A₁ : Set α}
    {B₀ : A₀ → Set β}{B₁ : A₁ → Set β}
    {f₀ : ∀ a → B₀ a}{f₁ : ∀ a → B₁ a}
  → A₀ ≡ A₁
  → (∀ a₀ a₁ (a₂ : a₀ ≅ a₁) → f₀ a₀ ≅ f₁ a₁)
  → f₀ ≅ f₁
ext̃' {A₀ = A} {.A} {B₀} {B₁} {f₀} {f₁} refl f₂ = ext̃ (λ a → f₂ a a refl̃)

extĩ :
  ∀ {α β}
    {A : Set α}
    {B₀ B₁ : A → Set β}
    {f₀ : ∀ {a} → B₀ a}{f₁ : ∀ {a} → B₁ a}
  → (∀ a → f₀ {a} ≅ f₁ {a})
  → (λ {a} → f₀ {a}) ≅ (λ {a} → f₁ {a})
extĩ {A = A} {B₀} {B₁} {f₀} {f₁} f₂ =
  J (λ {B₁} (B₂ : B₀ ≡ B₁) → {f₀ : ∀ {a} → B₀ a}{f₁ : ∀ {a} → B₁ a}
      → (∀ a → f₀ {a} ≅ f₁ {a})
      → (λ {a} → f₀ {a}) ≅ (λ {a} → f₁ {a}))
    (λ {f₀}{f₁} f₂ → exti (λ a → uñ (f₂ a)) ~)
    (ext (λ a → f₂ a .ty)) f₂

extĩ' :
  ∀ {α β}
    {A₀ A₁ : Set α}
    {B₀ : A₀ → Set β}{B₁ : A₁ → Set β}
    {f₀ : ∀ {a} → B₀ a}{f₁ : ∀ {a} → B₁ a}
  → A₀ ≡ A₁
  → (∀ a₀ a₁ (a₂ : a₀ ≅ a₁) → f₀ {a₀} ≅ f₁ {a₁})
  → (λ {a} → f₀ {a}) ≅ (λ {a} → f₁ {a})
extĩ' {A₀ = A}{A₁ = .A} {B₀} {B₁} {f₀} {f₁} refl f₂ = extĩ (λ a → f₂ a a refl̃ )
