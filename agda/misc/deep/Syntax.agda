
{-# OPTIONS --without-K --rewriting #-}

open import Lib hiding (_,_; _∘_)

{-
  Assuming axiom K, what's the most convenient way?
  First goal: formalize J[] in a sane way
-}

postulate
  _⊢>_ : ∀ {α}{A : Set α} → A → A → Set
{-# BUILTIN REWRITE _⊢>_ #-}

infixl 5 _,_
infixl 7 _[_]T
infixl 5 _,s_
infix  6 _∘_
infixl 8 _[_]t

postulate
  Con : Set
  Ty  : Con → Set
  Tm  : ∀ Γ → Ty Γ → Set
  Tms : Con → Con → Set

Ty≡ : ∀{Γ₀ Γ₁} → Γ₀ ≡ Γ₁ → Ty Γ₀ ≡ Ty Γ₁
Ty≡ refl = refl

Tm≡≡ : ∀ {Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁){A₀ A₁} → coe (Ty≡ Γ₂) A₀ ≡ A₁ → Tm Γ₀ A₀ ≡ Tm Γ₁ A₁
Tm≡≡ refl refl = refl

TmΓ≡ : ∀ {Γ}{A₀ A₁ : Ty Γ} → A₀ ≡ A₁ → Tm Γ A₀ ≡ Tm Γ A₁
TmΓ≡ = Tm≡≡ refl

Tms≡≡ : ∀ {Γ₀ Γ₁}(_ : Γ₀ ≡ Γ₁){Δ₀ Δ₁} → Δ₀ ≡ Δ₁ → Tms Γ₀ Δ₀ ≡ Tms Γ₁ Δ₁
Tms≡≡ refl refl = refl

Tms≡≡-≡≡ :
  ∀ {Γ₀ Γ₁}{Γ₂₀ Γ₂₁ : Γ₀ ≡ Γ₁}(Γ₂₂ : Γ₂₀ ≡ Γ₂₁){Δ₀ Δ₁}{Δ₂₀ Δ₂₁ : Δ₀ ≡ Δ₁}(Δ₂₂ : Δ₂₀ ≡ Δ₂₁)
  → Tms≡≡ Γ₂₀ Δ₂₀ ≡ Tms≡≡ Γ₂₁ Δ₂₁
Tms≡≡-≡≡ refl refl = refl

Tms≡≡◾ :
  ∀ {Γ₀ Γ₁ Γ₂ Δ₀ Δ₁ Δ₂}(Γ₀₁ : Γ₀ ≡ Γ₁)(Δ₀₁ : Δ₀ ≡ Δ₁)(Γ₁₂ : Γ₁ ≡ Γ₂)(Δ₁₂ : Δ₁ ≡ Δ₂)
  → (Tms≡≡ Γ₀₁ Δ₀₁ ◾ Tms≡≡ Γ₁₂ Δ₁₂) ≡ Tms≡≡ (Γ₀₁ ◾ Γ₁₂) (Δ₀₁ ◾ Δ₁₂)
Tms≡≡◾ refl refl r s = refl

postulate
  •     : Con
  _,_   : (Γ : Con) → Ty Γ → Con

  _[_]T : ∀{Γ Δ} → Ty Δ → Tms Γ Δ → Ty Γ

  id    : ∀{Γ} → Tms Γ Γ
  _∘_   : ∀{Γ Δ Σ} → Tms Δ Σ → Tms Γ Δ → Tms Γ Σ
  ε     : ∀{Γ} → Tms Γ •
  _,s_  : ∀{Γ Δ}(σ : Tms Γ Δ){A : Ty Δ} → Tm Γ (A [ σ ]T) → Tms Γ (Δ , A)
  π₁    : ∀{Γ Δ}{A : Ty Δ} → Tms Γ (Δ , A) →  Tms Γ Δ
  _[_]t : ∀{Γ Δ}{A : Ty Δ} → Tm Δ A → (σ : Tms Γ Δ) → Tm Γ (A [ σ ]T)
  π₂    : ∀{Γ Δ}{A : Ty Δ}(σ : Tms Γ (Δ , A)) → Tm Γ (A [ π₁ σ ]T)

coe[]T :
  ∀ {Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁){Δ}{A : Ty Δ}{σ : Tms Γ₀ Δ}
  → coe (Ty≡ Γ₂) (A [ σ ]T) ≡ A [ coe (Tms≡≡ Γ₂ refl) σ ]T
coe[]T refl = refl

≡[≡]T :
  ∀ {Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁){Δ₀ Δ₁}(Δ₂ : Δ₀ ≡ Δ₁){A₀ A₁}(A₂ : coe (Ty≡ Δ₂) A₀ ≡ A₁)
  {σ₀ σ₁}(σ₂ : coe (Tms≡≡ Γ₂ Δ₂) σ₀ ≡ σ₁)
  → coe (Ty≡ Γ₂) (A₀ [ σ₀ ]T) ≡ A₁ [ σ₁ ]T
≡[≡]T refl refl refl refl = refl

A[≡]T :
  ∀ {Γ Δ}{A : Ty Δ}{σ₀ σ₁ : Tms Γ Δ}(σ₂ : σ₀ ≡ σ₁)
  → A [ σ₀ ]T ≡ A [ σ₁ ]T
A[≡]T = ≡[≡]T refl refl refl

≡[σ]T :
  ∀ {Γ Δ}{A₀ A₁ : Ty Δ}(A₂ : A₀ ≡ A₁){σ : Tms Γ Δ} → A₀ [ σ ]T ≡ A₁ [ σ ]T
≡[σ]T p = ≡[≡]T refl refl p refl

≡,≡ : ∀ {Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁){A₀ A₁}(A₂ : coe (Ty≡ Γ₂) A₀ ≡ A₁) → (Γ₀ , A₀) ≡ (Γ₁ , A₁)
≡,≡ refl refl = refl

Γ,≡ : ∀ {Γ}{A₀ A₁ : Ty Γ}(A₂ : A₀ ≡ A₁) → (Γ , A₀) ≡ (Γ , A₁)
Γ,≡ = ≡,≡ refl

≡,A : ∀ {Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁){A} → Γ₀ , A ≡ Γ₁ , coe (Ty≡ Γ₂) A
≡,A p = ≡,≡ p refl

coe∘ :
  ∀ {Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁){Δ}{Σ₀ Σ₁}(Σ₂ : Σ₀ ≡ Σ₁)(σ : Tms Δ Σ₀)(δ : Tms Γ₀ Δ)
  → coe (Tms≡≡ Γ₂ Σ₂) (σ ∘ δ) ≡ coe (Tms≡≡ refl Σ₂) σ ∘ coe (Tms≡≡ Γ₂ refl) δ
coe∘ refl refl σ δ = refl

postulate
  [id]T : ∀{Γ}{A : Ty Γ} → A [ id ]T ≡ A
  [][]T : ∀{Γ Δ Σ}{A : Ty Σ}{σ : Tms Γ Δ}{δ : Tms Δ Σ}
          → A [ δ ]T [ σ ]T ≡ A [ δ ∘ σ ]T
  idl   : ∀{Γ Δ}{δ : Tms Γ Δ} → id ∘ δ ≡ δ
  idr   : ∀{Γ Δ}{δ : Tms Γ Δ} → δ ∘ id ≡ δ
  ass   : ∀{Γ Δ Σ Ω}{σ : Tms Σ Ω}{δ : Tms Δ Σ}{ν : Tms Γ Δ}
        → (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν)
  ,∘    : ∀{Γ Δ Σ}{δ : Tms Γ Δ}{σ : Tms Σ Γ}{A : Ty Δ}{a : Tm Γ (A [ δ ]T)}
        → (δ ,s a) ∘ σ ≡ (δ ∘ σ) ,s coe (TmΓ≡ [][]T) (a [ σ ]t)
  π₁β   : ∀{Γ Δ}{A : Ty Δ}{δ : Tms Γ Δ}{a : Tm Γ (A [ δ ]T)}
        → π₁ (δ ,s a) ≡ δ
  ,η    : ∀{Γ Δ}{A : Ty Δ}{δ : Tms Γ (Δ , A)}
        → (π₁ δ ,s π₂ δ) ≡ δ
  εη    : ∀{Γ}{σ : Tms Γ •}
        → σ ≡ ε
  π₂β   : ∀{Γ Δ}{A : Ty Δ}{δ : Tms Γ Δ}{a : Tm Γ (A [ δ ]T)}
        → π₂ (δ ,s a) ≡ coe (TmΓ≡ (A[≡]T (π₁β ⁻¹))) a

wk : ∀{Γ}{A : Ty Γ} → Tms (Γ , A) Γ
wk = π₁ id

vz : ∀{Γ}{A : Ty Γ} → Tm (Γ , A) (A [ wk ]T)
vz = π₂ id

vs : ∀{Γ}{A B : Ty Γ} → Tm Γ A → Tm (Γ , B) (A [ wk ]T)
vs x = x [ wk ]t

<_> : ∀{Γ}{A : Ty Γ} → Tm Γ A → Tms Γ (Γ , A)
< t > = id ,s coe (TmΓ≡ ([id]T ⁻¹)) t

infix 4 <_>

_^_ : {Γ Δ : Con}(σ : Tms Γ Δ)(A : Ty Δ) → Tms (Γ , A [ σ ]T) (Δ , A)
σ ^ A = (σ ∘ wk) ,s coe (TmΓ≡ [][]T) vz

infixl 5 _^_

coe,s :
  ∀ {Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁){Δ₀ Δ₁}(Δ₂ : Δ₀ ≡ Δ₁){σ : Tms Γ₀ Δ₀}{A₀ A₁}(A₂ : coe (Ty≡ Δ₂) A₀ ≡ A₁)
    {t : Tm Γ₀ (A₀ [ σ ]T)}
  → coe (Tms≡≡ Γ₂ (≡,≡ Δ₂ A₂)) (σ ,s t) ≡
    coe (Tms≡≡ Γ₂ Δ₂) σ ,s
    coe (Tm≡≡ Γ₂ (
      coe[]T Γ₂ ◾ ≡[≡]T refl Δ₂ A₂ (coe◾ (Tms≡≡ Γ₂ refl) (Tms≡≡ refl Δ₂) σ ⁻¹
                ◾ coe≡≡ {p₀ = Tms≡≡ Γ₂ refl ◾ Tms≡≡ refl Δ₂}{Tms≡≡ Γ₂ Δ₂}
                    (Tms≡≡◾ Γ₂ refl refl Δ₂ ◾ Tms≡≡-≡≡ (◾refl Γ₂) refl) refl)))
      t
coe,s refl refl refl = refl

-- coeid₁ :
--   ∀ {Γ₀ Γ₁ Γ₂}(Γ₀₁ : Γ₀ ≡ Γ₁)(Γ₀₂ : Γ₁ ≡ Γ₂) → coe (Tms≡≡ Γ₀₁ Γ₀₂) (id {Γ₀}) ≡ {!!}
-- coeid₁ p q = {!!}

≡,s≡ :
  ∀ {Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁){Δ₀ Δ₁ : Con}(Δ₂ : Δ₀ ≡ Δ₁)
  {σ₀ σ₁}(σ₂ : coe (Tms≡≡ Γ₂ Δ₂) σ₀ ≡ σ₁){A₀ A₁}(A₂ : coe (Ty≡ Δ₂) A₀ ≡ A₁)
  {t₀ t₁}(t₂ : coe (Tm≡≡ Γ₂ (≡[≡]T Γ₂ Δ₂ A₂ σ₂)) t₀ ≡ t₁)
  → coe (Tms≡≡ Γ₂ (≡,≡ Δ₂ A₂)) (σ₀ ,s t₀) ≡ (σ₁ ,s t₁)
≡,s≡ refl refl refl refl refl = refl

coeπ₁ :
  ∀ {Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁){Δ₀ Δ₁}(Δ₂ : Δ₀ ≡ Δ₁){A} σ
  → coe (Tms≡≡ Γ₂ Δ₂) (π₁ {Γ₀}{Δ₀}{A} σ) ≡ π₁ (coe (Tms≡≡ Γ₂ (≡,≡ Δ₂ refl)) σ)
coeπ₁ refl refl σ = refl

π₁∘ :
  ∀ {Γ Δ Σ A}{σ : Tms Γ (Δ , A)}{δ : Tms Σ Γ}
  → π₁ σ ∘ δ ≡ π₁ (σ ∘ δ)
π₁∘ {Γ}{Δ}{Σ}{A}{σ}{δ} = π₁β ⁻¹ ◾ π₁ & ,∘ ⁻¹ ◾ (λ σ → π₁ (σ ∘ δ)) & ,η

coe[]t :
  ∀ {Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁){Δ}{A₀ A₁ : Ty Δ}(A₂ : A₀ ≡ A₁)
    {σ₀ σ₁}(σ₂ : coe (Tms≡≡ Γ₂ refl) σ₀ ≡ σ₁)
    (t : Tm Δ A₀)
  → coe (Tm≡≡ Γ₂ (≡[≡]T Γ₂ refl A₂ σ₂)) (t [ σ₀ ]t) ≡ coe (TmΓ≡ A₂) t [ σ₁ ]t
coe[]t refl refl refl t = refl

postulate
  U    : ∀{Γ} → Ty Γ
  U[]  : ∀{Γ Δ}{σ : Tms Γ Δ} → (U [ σ ]T) ≡ U
  El   : ∀{Γ}(Â : Tm Γ U) → Ty Γ
  El[] : ∀{Γ Δ}{σ : Tms Γ Δ}{Â : Tm Δ U}
       → (El Â [ σ ]T) ≡ (El (coe (TmΓ≡ U[]) (Â [ σ ]t)))
  Π : ∀{Γ}(a : Tm Γ U)(B : Ty (Γ , El a)) → Ty Γ

  Π[] : ∀{Γ Δ}{σ : Tms Γ Δ}{a : Tm Δ U}{B : Ty (Δ , El a)}
      → (Π a B) [ σ ]T ≡ Π (coe (TmΓ≡ U[]) (a [ σ ]t))
                           (coe (Ty≡ (Γ,≡ El[])) (B [ σ ^ El a ]T))

foo :
    ∀{Γ Δ}{a : Tm Δ U}{u : Tm Δ (El a)}{σ : Tms Γ Δ}
    → coe ((λ x → Tm Γ (El a [ x ]T)) & idl ⁻¹) (u [ σ ]t)
    ≡ coe (TmΓ≡ [][]T) (coe (TmΓ≡ ([id]T ⁻¹)) u [ σ ]t)
foo {Γ}{Δ}{a}{u}{σ} = {!coe[]t refl !}

postulate
  app :
    ∀{Γ}{a : Tm Γ U}{B : Ty (Γ , El a)}(t : Tm Γ (Π a B))(u : Tm Γ (El a))
    → Tm Γ (B [ < u > ]T)

  app[] :
    ∀{Γ Δ}{a : Tm Δ U}{B : Ty (Δ , El a)}{t : Tm Δ (Π a B)}{u : Tm Δ (El a)}
     {σ : Tms Γ Δ}
    → app t u [ σ ]t ≡
        coe
          (TmΓ≡ (
              {!coe ((λ x → Tm Γ (El a [ x ]T)) & idl ⁻¹) (u [ σ ]t)!}
            -- ◾ (B [_]T) & {!!}
            ◾ (B [_]T) & ,∘ ⁻¹
            ◾ [][]T ⁻¹))

        -- coe (TmΓ≡
        --     (≡[σ]T (coe[]T (Γ,≡ El[]))
        --   ◾ [][]T
        --   ◾ A[≡]T
        --       ((_∘ (id ,s coe (Tm≡≡ refl ([id]T ⁻¹)) (coe (Tm≡≡ refl El[]) (u [ σ ]t)))) &
        --         coe,s (≡,≡ refl El[]) refl refl
        --     ◾ ,∘
        --     ◾
        --        ≡,s≡ {Γ}{Γ} refl {Δ}{Δ} refl
        --        {coe (Tms≡≡ (≡,≡ refl El[]) refl) (σ ∘ wk) ∘
        --         (id ,s coe (Tm≡≡ refl ([id]T ⁻¹)) (coe (Tm≡≡ refl El[]) (u [ σ ]t)))}
        --        {id ∘ σ}
        --        ((_∘ (id ,s coe (Tm≡≡ refl ([id]T ⁻¹)) (coe (Tm≡≡ refl El[]) (u [ σ ]t))))
        --              & coe∘ (≡,≡ refl El[]) refl σ wk
        --          ◾ ass
        --          ◾ (σ ∘_) &
        --            {!!}
        --          ◾ idr
        --          ◾ idl ⁻¹)
        --        {El a}
        --        {El a}
        --        refl
        --        {!!})
        --   ◾ A[≡]T ,∘ ⁻¹
        --   ◾ [][]T ⁻¹))

          (app (coe (TmΓ≡ Π[])  (t [ σ ]t))
               (coe (TmΓ≡ El[]) (u [ σ ]t)))

-- ((σ ∘ π₁ id) ∘ (id ,s u [ σ ])) ≡ σ
-- σ ∘ (π₁ id ∘ (id ,s u[σ]))
-- σ ∘ (

--   app : ∀{Γ}{a : Tm Γ U}{B : Ty (Γ , El a)} → Tm Γ (Π a B) → Tm (Γ , El a) B

-- postulate
--   app[] :
--     ∀{Γ Δ}{a : Tm Δ U}{B : Ty (Δ , El a)}{t : Tm Δ (Π a B)}{σ : Tms Γ (Δ , El a)}
--     → (app t) [ σ ]t ≡ {!!} -- (app (coe {!!} (t [ π₁ σ ]t))) [ {!< π₂ σ >!} ]t

--------------------------------------------------------------------------------


-- coe (Ty≡ (Γ,≡ El[]))
--       (B [ σ ∘ π₁ id ,s coe (TmΓ≡ [][]T) (π₂ id) ]T)
--       [ id ,s coe (TmΓ≡ ([id]T ⁻¹)) (coe (TmΓ≡ El[]) (u [ σ ]t)) ]T
--       ≡
--       B [ id ∘ σ ,s coe (TmΓ≡ [][]T) (coe (TmΓ≡ ([id]T ⁻¹)) u [ σ ]t) ]T
