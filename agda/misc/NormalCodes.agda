
module NormalCodes where

open import StrictLib

infixl 5 _▶_
infixr 5 _ₛ∘ₑ_ _∘ₑ_ _ₑ∘ₛ_ _∘ₛ_

data Con : Set
data Ty  : Con → Set
data Tm  : (Γ : Con) → Ty Γ → Set
data Var : (Γ : Con) → Ty Γ → Set
data OPE : Con → Con → Set
data Sub : Con → Con → Set

{-# TERMINATING #-}
Tyₑ     : ∀ {Γ Δ} → OPE Γ Δ → Ty Δ → Ty Γ
idₑ     : ∀ {Γ} → OPE Γ Γ
Ty-idₑ  : ∀ {Γ} A → Tyₑ (idₑ {Γ}) A ≡ A
Varₑ    : ∀ {Γ Δ A}(σ : OPE Γ Δ) → Var Δ A → Var Γ (Tyₑ σ A)
Var-idₑ : ∀ {Γ A}(x : Var Γ A) → coe (Var Γ & Ty-idₑ A) (Varₑ idₑ x) ≡ x
Tmₑ     : ∀ {Γ Δ A}(σ : OPE Γ Δ) → Tm Δ A → Tm Γ (Tyₑ σ A)
Tm-idₑ  : ∀ {Γ A}(t : Tm Γ A) → coe (Tm Γ & Ty-idₑ A) (Tmₑ idₑ t) ≡ t
Tyₛ     : ∀ {Γ Δ} → Sub Γ Δ → Ty Δ → Ty Γ
idₛ     : ∀ {Γ} → Sub Γ Γ
Ty-idₛ  : ∀ {Γ} A → Tyₛ (idₛ {Γ}) A ≡ A
_ₛ∘ₑ_   : ∀ {Γ Δ Σ} → Sub Δ Σ → OPE Γ Δ → Sub Γ Σ
_ₑ∘ₛ_   : ∀ {Γ Δ Σ} → OPE Δ Σ → Sub Γ Δ → Sub Γ Σ
Ty-ₛ∘ₑ  : ∀ {Γ Δ Σ} (σ : Sub Δ Σ) (δ : OPE Γ Δ)(A : Ty Σ)
          → Tyₛ (σ ₛ∘ₑ δ) A ≡ Tyₑ δ (Tyₛ σ A)
Ty-ₑ∘ₛ  : ∀ {Γ Δ Σ} (σ : OPE Δ Σ) (δ : Sub Γ Δ)(A : Ty Σ)
          → Tyₛ (σ ₑ∘ₛ δ) A ≡ Tyₛ δ (Tyₑ σ A)

Tmₛ     : ∀ {Γ Δ A} → (σ : Sub Γ Δ) → Tm Δ A  → Tm Γ (Tyₛ σ A)
Varₛ    : ∀ {Γ Δ A} → (σ : Sub Γ Δ) → Var Δ A → Tm Γ (Tyₛ σ A)
Tm-idₛ  : ∀ {Γ A} (t : Tm Γ A) → coe (Tm Γ & Ty-idₛ A) (Tmₛ (idₛ {Γ}) t) ≡ t

_∘ₑ_    : ∀ {Γ Δ Σ} → OPE Δ Σ → OPE Γ Δ → OPE Γ Σ
_∘ₛ_    : ∀ {Γ Δ Σ} → Sub Δ Σ → Sub Γ Δ → Sub Γ Σ
Ty-∘ₑ   : ∀ {Γ Δ Σ}(σ : OPE Δ Σ)(δ : OPE Γ Δ) A → Tyₑ (σ ∘ₑ δ) A ≡ Tyₑ δ (Tyₑ σ A)
Ty-∘ₛ   : ∀ {Γ Δ Σ}(σ : Sub Δ Σ)(δ : Sub Γ Δ) A → Tyₛ (σ ∘ₛ δ) A ≡ Tyₛ δ (Tyₛ σ A)

idrₑ    : ∀ {Γ Δ}(σ : OPE Γ Δ) → σ ∘ₑ idₑ ≡ σ
idrₛ    : ∀ {Γ Δ}(σ : Sub Γ Δ) → σ ∘ₛ idₛ ≡ σ
idlₑ    : ∀ {Γ Δ}(σ : OPE Γ Δ) → idₑ ∘ₑ σ ≡ σ
idlₛ    : ∀ {Γ Δ}(σ : Sub Γ Δ) → idₛ ∘ₛ σ ≡ σ
idlₑₛ   : ∀ {Γ Δ}(σ : Sub Γ Δ) → idₑ ₑ∘ₛ σ ≡ σ

data Con where
  ∙ : Con
  _▶_ : (Γ : Con) → Ty Γ → Con

data OPE where
  ∙    : OPE ∙ ∙
  drop : ∀ {Γ Δ A} → OPE Γ Δ → OPE (Γ ▶ A) Δ
  keep : ∀ {Γ Δ A}(σ : OPE Γ Δ) → OPE (Γ ▶ Tyₑ σ A) (Δ ▶ A)

data Sub where
  ∙    : ∀ {Γ} → Sub Γ ∙
  _,s_ : ∀ {Γ Δ}(σ : Sub Γ Δ){A : Ty Δ} → Tm Γ (Tyₛ σ A) → Sub Γ (Δ ▶ A)

data Ty where
  U  : ∀ {Γ} → Ty Γ
  El : ∀ {Γ} → Tm Γ U → Ty Γ
  Π  : ∀ {Γ}(a : Tm Γ U) → Ty (Γ ▶ El a) → Ty Γ

data Var where
  vz : ∀ {Γ : Con}{A : Ty Γ} → Var (Γ ▶ A) (Tyₑ (drop idₑ) A)
  vs : ∀ {Γ A B} → Var Γ A → Var (Γ ▶ B) (Tyₑ (drop idₑ) A)

data Tm where
  var : ∀ {Γ A} → Var Γ A → Tm Γ A
  app :
    ∀ {Γ a B} → Tm Γ (Π a B)
              → (u : Tm Γ (El a))
              → Tm Γ (Tyₛ (idₛ ,s coe (Tm Γ & (Ty-idₛ (El a) ⁻¹)) u) B)

Tyₑ σ U        = U
Tyₑ σ (El a)   = El (Tmₑ σ a)
Tyₑ σ (Π a A)  = Π (Tmₑ σ a) (Tyₑ (keep σ) A)

idₑ {∙}        = ∙
idₑ {Γ ▶ A}    = coe ((λ x → OPE (Γ ▶ x) (Γ ▶ A)) & Ty-idₑ A) (keep idₑ)

wk : ∀ {Γ A} → OPE (Γ ▶ A) Γ
wk {Γ}{A} = drop idₑ

dropₛ : ∀ {Γ Δ A}(σ : Sub Γ Δ) → Sub (Γ ▶ Tyₛ σ A) Δ
dropₛ σ = σ ₛ∘ₑ wk

keepₛ : ∀ {Γ Δ A}(σ : Sub Γ Δ) → Sub (Γ ▶ Tyₛ σ A) (Δ ▶ A)
keepₛ {Γ} {Δ} {A} σ = dropₛ σ ,s
                      coe (Tm (Γ ▶ Tyₛ σ A) & (Ty-ₛ∘ₑ σ wk A ⁻¹)) (var (vz{A = Tyₛ σ A}))

idₛ {∙}     = ∙
idₛ {Γ ▶ A} = (idₛ ₛ∘ₑ wk) ,s
              coe (Tm (Γ ▶ A) & (Tyₑ wk & (Ty-idₛ A ⁻¹) ◾ Ty-ₛ∘ₑ idₛ wk A ⁻¹))
                  (var (vz {A = A}))

Ty-idₑ U       = refl
Ty-idₑ (El a)  = El & Tm-idₑ a
Ty-idₑ {Γ} (Π a A) = ap2̃ Π
  (Tm-idₑ a)
  (ap2̃̃ (λ a σ → Tyₑ {Γ = Γ ▶ El a} σ A) (Tm-idₑ a) (uncoe _ _ ⁻¹̃)
     ◾̃ (Ty-idₑ A ~))

Tyₛ σ U       = U
Tyₛ σ (El a)  = El (Tmₛ σ a)
Tyₛ {Γ} {Δ} σ (Π a B) = Π (Tmₛ σ a) (Tyₛ (keepₛ σ) B)

Ty-idₛ {Γ} U       = refl
Ty-idₛ {Γ} (El a)  = El & Tm-idₛ a
Ty-idₛ {Γ} (Π a A) = ap2̃ Π (Tm-idₛ a) (ap2̃̃ (λ a σ → Tyₛ {Γ = Γ ▶ El a} σ A)
                           (Tm-idₛ a)
                           {!!}
                         ◾̃ (Ty-idₛ A ~))

σ ∘ₑ ∙      = σ
σ ∘ₑ drop δ = drop (σ ∘ₑ δ)
drop σ ∘ₑ keep δ = drop (σ ∘ₑ δ)
keep {Γ} {Δ} {A} σ ∘ₑ keep {Σ} δ =
  coe ((λ x → OPE (Σ ▶ x) (Δ ▶ A)) & Ty-∘ₑ σ δ A) (keep {A = A} (σ ∘ₑ δ))

Varₑ ∙ ()
Varₑ {Δ = Δ} {A₁} (drop {Γ} {A = A} σ) x =
  coe (Var (Γ ▶ A) & (Ty-∘ₑ σ wk A₁ ⁻¹ ◾ (λ x → Tyₑ x A₁) & (drop & idrₑ _)))
      (vs {B = A} (Varₑ σ x))
Varₑ (keep {Γ} {Δ} {A} σ) vz =
  coe (Var (Γ ▶ Tyₑ σ A) & ((Ty-∘ₑ σ wk A ⁻¹
             ◾ (λ x → Tyₑ (drop x) A) & (idrₑ σ ◾ idlₑ σ ⁻¹))
             ◾ Ty-∘ₑ wk (keep σ) A))
      (vz {A = Tyₑ σ A})
Varₑ (keep {Γ} {Δ} {A} σ) (vs {A = A₁} x) =
  coe (Var (Γ ▶ Tyₑ σ A) & ((Ty-∘ₑ σ wk A₁ ⁻¹
      ◾ (λ x → Tyₑ (drop x) A₁) & (idrₑ σ ◾ idlₑ σ ⁻¹)) ◾ Ty-∘ₑ wk (keep σ) A₁))
      (vs {B = Tyₑ σ A} (Varₑ σ x))

Var-idₑ (vz {Γ} {A})       =
  uñ (uncoe (Var (Γ ▶ A) & Ty-idₑ (Tyₑ (drop idₑ) A)) _ ◾̃
      ((Varₑ (coe ((λ x → OPE (Γ ▶ x) (Γ ▶ A)) & Ty-idₑ A) (keep idₑ)) vz ≃ Varₑ (keep idₑ) (vz {A = A})) ∋ {!!}) ◾̃ uncoe
      (Var (Γ ▶ Tyₑ idₑ A) &
       ((Ty-∘ₑ idₑ wk A ⁻¹ ◾
         (λ x → Tyₑ (drop x) A) & (idrₑ idₑ ◾ idlₑ idₑ ⁻¹))
        ◾ Ty-∘ₑ wk (keep idₑ) A))
      vz ◾̃ ap̃̃ (λ A → vz {A = A}) (Ty-idₑ A))

Var-idₑ (vs {Γ} {A} {B} x) =
  uñ (uncoe (Var (Γ ▶ B) & Ty-idₑ (Tyₑ (drop idₑ) A)) _ ◾̃
      {!!})

Tmₑ σ (var x)   = var (Varₑ σ x)
Tmₑ {Γ}{Δ} σ (app {_}{a}{B} t u) =
  coe (Tm Γ &
           (Ty-ₑ∘ₛ (keep σ) (idₛ ,s coe (Tm Γ & (El & Tm-idₛ (Tmₑ σ a) ⁻¹)) (Tmₑ σ u)) B ⁻¹
         ◾ (λ x → Tyₛ x B) & {!!}
         ◾ Ty-ₛ∘ₑ (idₛ ,s coe (Tm Δ & (El & Tm-idₛ a ⁻¹)) u) σ B))
       (app (Tmₑ σ t) (Tmₑ σ u))

Tm-idₑ {Γ} {A} (var x) = uñ (uncoe (Tm Γ & Ty-idₑ A) (var (Varₑ idₑ x)) ◾̃ ap2̃̃ (λ A₁ → var {A = A₁}) (Ty-idₑ A) (uncoe (Var Γ & Ty-idₑ A) (Varₑ idₑ x) ⁻¹̃)) ◾ var & Var-idₑ x
Tm-idₑ {Γ} (app {a = a} {B} t u) = {!!}

∙        ₛ∘ₑ δ = ∙
_ₛ∘ₑ_ {Γ} {Δ} (_,s_ {Δ = Δ₁} σ {A} t) δ =
  (σ ₛ∘ₑ δ) ,s coe (Tm Γ & (Ty-ₛ∘ₑ σ δ A ⁻¹)) (Tmₑ δ t)

∙      ₑ∘ₛ ∙        = ∙
drop σ ₑ∘ₛ (δ ,s t) = σ ₑ∘ₛ δ
_ₑ∘ₛ_ {Γ} (keep {A = A} σ) (δ ,s t) =
  (σ ₑ∘ₛ δ) ,s coe (Tm Γ & (Ty-ₑ∘ₛ σ δ A ⁻¹)) t

Varₛ {Δ} (σ ,s t) (vz {Γ} {A}) =
  coe (Tm Δ & ((λ x → Tyₛ x A) & (idlₑₛ σ ⁻¹) ◾ Ty-ₑ∘ₛ wk (σ ,s t) A)) t
Varₛ {Γ} (_,s_ {Δ = Δ} σ {A} t) (vs {A = B} x) =
  coe (Tm Γ & ((λ x → Tyₛ x B) & (idlₑₛ σ ⁻¹) ◾ Ty-ₑ∘ₛ wk (σ ,s t) B)) (Varₛ σ x)

Tmₛ σ (var x)   = Varₛ σ x
Tmₛ {Γ} {Δ} σ (app {a = a} {B} t u) =
   coe (Tm Γ & (Ty-∘ₛ (keepₛ σ) (idₛ ,s coe (Tm Γ & (El & Tm-idₛ (Tmₛ σ a) ⁻¹)) (Tmₛ σ u)) B ⁻¹
             ◾ (λ x → Tyₛ x B) & {!!}
             ◾ Ty-∘ₛ (idₛ ,s coe (Tm Δ & (El & Tm-idₛ a ⁻¹)) u) σ B))
       (app (Tmₛ σ t) (Tmₛ σ u))

∙        ∘ₛ δ = ∙
_∘ₛ_ {Γ} {Δ} (_,s_ {Δ = Σ} σ {A} t) δ = (σ ∘ₛ δ) ,s coe (Tm Γ & (Ty-∘ₛ σ δ A ⁻¹)) (Tmₛ δ t)

Ty-ₛ∘ₑ = {!!}

Tm-idₛ = {!!}

Ty-∘ₑ = {!!}

idrₑ = {!!}
idlₑ = {!!}

Ty-ₑ∘ₛ = {!!}
Ty-∘ₛ = {!!}

idlₑₛ = {!!}
idrₛ = {!!}
idlₛ = {!!}
