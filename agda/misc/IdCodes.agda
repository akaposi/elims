
open import Lib hiding (_,_; _∘_)

-- postulate
--   _⊢>_ : ∀ {α}{A : Set α} → A → A → Set
-- {-# BUILTIN REWRITE _⊢>_ #-}

infixl 5 _,_
infixl 7 _[_]T
infixl 5 _,s_
infix  6 _∘_
infixl 8 _[_]t

postulate
  Con   : Set
  Ty    : Con → Set
  Tm    : ∀ Γ → Ty Γ → Set
  Tms   : Con → Con → Set
  •     : Con
  _,_   : (Γ : Con) → Ty Γ → Con

  _[_]T : ∀{Γ Δ} → Ty Δ → Tms Γ Δ → Ty Γ

  id    : ∀{Γ} → Tms Γ Γ
  _∘_   : ∀{Γ Δ Σ} → Tms Δ Σ → Tms Γ Δ → Tms Γ Σ
  ε     : ∀{Γ} → Tms Γ •
  _,s_  : ∀{Γ Δ}(σ : Tms Γ Δ){A : Ty Δ} → Tm Γ (A [ σ ]T) → Tms Γ (Δ , A)
  π₁    : ∀{Γ Δ}{A : Ty Δ} → Tms Γ (Δ , A) →  Tms Γ Δ
  _[_]t : ∀{Γ Δ}{A : Ty Δ} → Tm Δ A → (σ : Tms Γ Δ) → Tm Γ (A [ σ ]T)
  π₂    : ∀{Γ Δ}{A : Ty Δ}(σ : Tms Γ (Δ , A)) → Tm Γ (A [ π₁ σ ]T)
  [id]T : ∀{Γ}{A : Ty Γ} → A [ id ]T ≡ A
  [][]T : ∀{Γ Δ Σ}{A : Ty Σ}{σ : Tms Γ Δ}{δ : Tms Δ Σ}
          → A [ δ ]T [ σ ]T ≡ A [ δ ∘ σ ]T
  idl   : ∀{Γ Δ}{δ : Tms Γ Δ} → id ∘ δ ≡ δ
  idr   : ∀{Γ Δ}{δ : Tms Γ Δ} → δ ∘ id ≡ δ
  ass   : ∀{Γ Δ Σ Ω}{σ : Tms Σ Ω}{δ : Tms Δ Σ}{ν : Tms Γ Δ}
        → (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν)
  ,∘    : ∀{Γ Δ Σ}{δ : Tms Γ Δ}{σ : Tms Σ Γ}{A : Ty Δ}{a : Tm Γ (A [ δ ]T)}
        → (δ ,s a) ∘ σ ≡ (δ ∘ σ) ,s coe (Tm _ & [][]T) (a [ σ ]t)
  π₁β   : ∀{Γ Δ}{A : Ty Δ}{δ : Tms Γ Δ}{a : Tm Γ (A [ δ ]T)}
        → π₁ (δ ,s a) ≡ δ
  ,η    : ∀{Γ Δ}{A : Ty Δ}{δ : Tms Γ (Δ , A)}
        → (π₁ δ ,s π₂ δ) ≡ δ
  εη    : ∀{Γ}{σ : Tms Γ •}
        → σ ≡ ε
  π₂β   : ∀{Γ Δ}{A : Ty Δ}{δ : Tms Γ Δ}{a : Tm Γ (A [ δ ]T)}
        → π₂ (δ ,s a) ≡ coe (Tm _ & ((_ [_]T) & (π₁β ⁻¹))) a

wk : ∀{Γ}{A : Ty Γ} → Tms (Γ , A) Γ
wk = π₁ id

vz : ∀{Γ}{A : Ty Γ} → Tm (Γ , A) (A [ wk ]T)
vz = π₂ id

vs : ∀{Γ}{A B : Ty Γ} → Tm Γ A → Tm (Γ , B) (A [ wk ]T)
vs x = x [ wk ]t

<_> : ∀{Γ}{A : Ty Γ} → Tm Γ A → Tms Γ (Γ , A)
< t > = id ,s coe (Tm _ & ([id]T ⁻¹)) t

infix 4 <_>

_^_ : {Γ Δ : Con}(σ : Tms Γ Δ)(A : Ty Δ) → Tms (Γ , A [ σ ]T) (Δ , A)
σ ^ A = (σ ∘ wk) ,s coe (Tm _ & [][]T) vz

infixl 5 _^_

postulate
  U    : ∀{Γ} → Ty Γ
  U[]  : ∀{Γ Δ}{σ : Tms Γ Δ} → (U [ σ ]T) ≡ U
  El   : ∀{Γ}(Â : Tm Γ U) → Ty Γ
  El[] : ∀{Γ Δ}{σ : Tms Γ Δ}{Â : Tm Δ U}
         → (El Â [ σ ]T) ≡ (El (coe (Tm _ & U[]) (Â [ σ ]t)))
  Π    : ∀{Γ}(a : Tm Γ U)(B : Ty (Γ , El a)) → Ty Γ
  Π[]  : ∀{Γ Δ}{σ : Tms Γ Δ}{a : Tm Δ U}{B : Ty (Δ , El a)}
         → (Π a B) [ σ ]T ≡ Π (coe (Tm _ & U[]) (a [ σ ]t))
                              (coe (Ty & (_,_ Γ & El[])) (B [ σ ^ El a ]T))
--   app :
--     ∀{Γ}{a : Tm Γ U}{B : Ty (Γ , El a)}(t : Tm Γ (Π a B))(u : Tm Γ (El a))
--     → Tm Γ (B [ < u > ]T)

-- appLem :
--   ∀{Γ Δ}{a : Tm Δ U}{B : Ty (Δ , El a)}{t : Tm Δ (Π a B)}{u : Tm Δ (El a)}
--    {σ : Tms Γ Δ}
--    → Tm Γ
--       (coe (Ty & (_,_ Γ & El[]))
--        (B [ σ ∘ π₁ id ,s coe (Tm (Γ , El a [ σ ]T) & [][]T) (π₂ id) ]T)
--        [ id ,s coe (Tm Γ & ([id]T ⁻¹)) (coe (Tm Γ & El[]) (u [ σ ]t)) ]T)
--       ≡ Tm Γ (B [ id ,s coe (Tm Δ & ([id]T ⁻¹)) u ]T [ σ ]T)
-- appLem {Γ}{Δ}{a}{B}{t}{u}{σ} =
--   Tm Γ &
--       (uñ (ap3̃ (λ (A : Ty Γ)(B : Ty (Γ , A))(σ : Tms Γ (Γ , A)) → B [ σ ]T)
--         (El[] ⁻¹)
--         (uncoe (Ty & (_,_ Γ & El[])) _)
--         {id ,s coe (Tm Γ & ([id]T ⁻¹)) (coe (Tm Γ & El[]) (u [ σ ]t))}
--         {id ,s coe (Tm Γ & ([id]T ⁻¹)) (u [ σ ]t)}
--         {!!})
--     ◾ [][]T
--     ◾ (B [_]T) &
--         ({!!}
--       ◾ ,∘ ⁻¹)
--     ◾ [][]T ⁻¹)

-- postulate
--   app[] :
--     ∀{Γ Δ}{a : Tm Δ U}{B : Ty (Δ , El a)}{t : Tm Δ (Π a B)}{u : Tm Δ (El a)}
--      {σ : Tms Γ Δ}
--     → app t u [ σ ]t ≡
--         coe (appLem {Γ}{Δ}{a}{B}{t}{u}{σ})
--           (app (coe (Tm _ & Π[])  (t [ σ ]t))
--                (coe (Tm _ & El[]) (u [ σ ]t)))

postulate
  Id   : {Γ : Con}(a : Tm Γ U)(t u : Tm Γ (El a)) → Tm Γ U
  Refl : ∀ {Γ}(a : Tm Γ U)(t : Tm Γ (El a)) → Tm Γ (El (Id a t t))
  IdTr   :
    ∀ {Γ}(a : Tm Γ U)(t u : Tm Γ (El a))(P : Ty (Γ , El a))
         (eq : Tm Γ (El (Id a t u)))
         (Pt : Tm Γ (P [ < t > ]T)) → Tm Γ (P [ < u > ]T)

  -- IdContr :
  --   ∀ {Γ}(a : Tm Γ U)(t u : Tm Γ (El a))(P : Ty (Γ , El a))
  --     (eq : Tm Γ (El (Id a t u)))
  --   → Tm Γ (El (Id (Id a t u)
  --       (coe {!!}
  --         (IdTr a t u
  --              (El (Id (coe (Tm _ & U[]) (a [ wk ]t))
  --              (coe (Tm _ & El[]) (t [ wk ]t))
  --              (coe (Tm _ & El[]) vz)))
  --              eq
  --              (coe {!!} (Refl a t))))
  --       eq))

  IdTrβ :
    ∀ {Γ}(a : Tm Γ U)(t : Tm Γ (El a))(P : Ty (Γ , El a))
      (Pt : Tm Γ (P [ < t > ]T))
    → IdTr a t t P (Refl a t) Pt ≡ Pt

IdLem :
 ∀ {Γ}
   (a : Tm Γ U)
   (t u : Tm Γ (El a))
 → Id a u t ≡
   coe (Tm Γ & U[])
   (Id (coe (Tm (Γ , El a) & U[]) (a [ wk ]t))
    (coe (Tm (Γ , El a) & El[]) (t [ wk ]t))
    (coe (Tm (Γ , El a) & El[]) vz)
    [ id ,s coe (Tm Γ & [id]T ⁻¹) u ]t)
IdLem {Γ} a t u = uñ
    ({!!}
  ◾̃ uncoe (Tm Γ & U[]) _ ⁻¹̃)

postulate
  IdJ :
    ∀ {Γ}
       (a : Tm Γ U)
       (t : Tm Γ (El a))
       (P : Ty (Γ , El a
                  , El (Id (coe (Tm _ & U[])  (a [ wk ]t))
                           (coe (Tm _ & El[]) (t [ wk ]t))
                           (coe (Tm _ & El[]) vz))))
       (pr : Tm Γ (P [ id ,s coe (Tm _ & [id]T ⁻¹) t
                          ,s coe (Tm _ & (El & {!!} ◾ El[] ⁻¹)) (Refl a t) ]T))
       (u : Tm Γ (El a))
       (p : Tm Γ (El (Id a u t)))
       → Tm Γ (P [ id ,s coe (Tm _ & [id]T ⁻¹) u
                      ,s coe (Tm _ & (El & {!!} ◾ El[] ⁻¹)) p ]T)
