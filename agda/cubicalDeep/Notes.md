
## Goals:

- Nicely normalized goal types
- Fast typechecking
- Control over how we print stuff (show pragma?)
- Principled lemmas

## Non-priority:

- Modularity

## Notes
