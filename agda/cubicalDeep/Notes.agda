{-# OPTIONS --rewriting #-}

open import Lib
open import Syntax
import Models.C as C renaming (Conᴿ to Con; Tyᴿ to Ty; Tmsᴿ to Tms; Tmᴿ to Tm)

open import Lib
open import Syntax


-- Shorthands
------------------------------------------------------------

zU : ∀ {Γ} → Tm (Γ , U) U
zU = tr (Tm _) (U[] _) vz

infixl 3 sU_
sU_ : ∀ {Γ A} → Tm Γ U → Tm (Γ , A) U
sU_ t = tr (Tm _) (U[] _) (vs t)

zEl : ∀ {Γ a} → Tm (Γ , El a) (El (sU a))
zEl = tr (Tm _) (El[] _) vz

infixl 3 sEl_
sEl_ : ∀ {Γ a B} → Tm Γ (El a) → Tm (Γ , B) (El (sU a))
sEl_ t = tr (Tm _) (El[] _) (vs t)

-- zNU : ∀ {Γ A} → Tm (Γ , ΠNI A (λ _ → U)) (ΠNI A (λ _ → U))
-- zNU = tr (Tm _) (ΠNI[] ◾ ap (ΠNI _) (ext λ _ → U[])) vz

-- infixl 3 sNU_
-- sNU_ : ∀ {Γ A C} → Tm Γ (ΠNI A (λ _ → U)) → Tm (Γ , C) (ΠNI A (λ _ → U))
-- sNU_ t = tr (Tm _) (ΠNI[] ◾ ap (ΠNI _) (ext λ _ → U[])) (vs t)

-- zId : ∀ {Γ a t u} → Tm (Γ , El (Id a t u)) (El (Id (sU a) (sEl t) (sEl u)))
-- zId = tr (Tm _) (El[] wk ◾ ap El Id[]) vz

-- infixl 3 sId_
-- sId_ : ∀ {Γ a t u B} → Tm Γ (El (Id a t u)) → Tm (Γ , B) (El (Id (sU a) (sEl t) (sEl u)))
-- sId_ t = tr (Tm _) (El[] _ ◾ ap El Id[]) (vs t)

-- zΠU : ∀ {Γ a} → Tm (Γ , Π a U) (Π (sU a) U)
-- zΠU {Γ}{a} =
--   tr (Tm _)
--      (Π[] ◾ ap (Π _) (ap (tr (λ x → Ty (Γ , Π a U , x)) (El[] _)) (U[] _)
--           ◾ J (λ el[] → tr (λ x → Ty (Γ , Π a U , x)) el[] U ≡ U) refl (El[] _)))
--       vz

-- infixl 3 sΠU_
-- sΠU_ : ∀ {Γ a B} → Tm Γ (Π a U) → Tm (Γ , B) (Π (sU a) U)
-- sΠU_ {Γ}{a}{B} t =
--   tr (Tm _)
--      (Π[] ◾ ap (Π _) (ap (tr (λ x → Ty (Γ , B , x)) El[]) U[]
--           ◾ J (λ el[] → tr (λ x → Ty (Γ , B , x)) el[] U ≡ U) refl El[] ))
--      (vs t)

-- infixl 7 _$U_
-- _$U_ : ∀ {Γ a} → Tm Γ (Π a U) → Tm Γ (El a) → Tm Γ U
-- t $U a = tr (Tm _) U[] (t $ a)



Lift' = Lift {lzero}{lsuc lzero}

unit : Con
unit = ∙ , U , El zU

nat : Con
nat = ∙ , U , El zU , Π (sU zU) (El (sU sU zU))



-- -- unitᶜ : Set₁
-- -- unitᶜ = C.Con unit

-- -- itworks : unitᶜ ≡  Σ (Lift' ⊤ × Set) (λ γ → Lift' (proj₂ γ))
-- -- itworks = refl
-- -- infixr 4 _◾'_
-- -- _◾'_ : ∀ {i}{A : Set i}{x y z : A} → x ≡ y → y ≡ z → x ≡ z
-- -- _◾'_ {x = x} {y} {z} p q = tr (λ z → x ≡ z) q p

-- -- postulate
-- --   p1 : ∀ {i j}{A : Set i}{B : Set j}(f : A → B)(x y z : A)
-- --        (p : x ≡ y)(q : y ≡ z)(i : I)
-- --        → f ((p ◾ q) $ i) ≡ (ap f p ◾ ap f q) $ i

-- --   p2 :
-- --     ∀ {i j}{A : Set i}{B : A → Set j}
-- --       (f : ∀ a → B a)(x y z : A)(p : x ≡ y)(q : y ≡ z)(i : I)
-- --       → f ((p ◾ q) $ i) ≡ coe (p1 B _ _ _ p q i ⁻¹)
-- --           {!!}

-- -- coercion goes inside function application
-- -- function application goes inside composition

-- test : Con
-- test = ∙
--      , U
--      , U
--      , El (tr (Tm _) (ap (_[ wk ]T) (U[] _) ◾ U[] _) (vs vz))

-- -- foo

-- -- Σ (Σ (Σ (Lift ⊤) (λ γ → Set)) (λ γ → Set))
-- -- (λ γ →
-- --    Lift
-- --    (coe (λ i → C.Ty (((λ i₁ → U[] $ i₁ [ π₁ id ]T) ◾ U[]) $ i) γ)
-- --     (proj₂ (proj₁ γ))))

-- -- nat : Con
-- -- nat = ∙
-- --     , U
-- --     , El (tr (Tm _) U[] vz)
-- --     , Π (tr (Tm _) (ap (_[ wk ]T) U[] ◾ U[]) (vs vz))
-- --         (El (tr (Tm _) (ap (λ x → x [ wk ]T [ wk ]T) U[] ◾ ap (_[ wk ]T) U[] ◾ U[])
-- --         (vs (vs vz))))

-- -- natᶜ : Set₁
-- -- natᶜ = C.Con nat

-- -- itworks :
-- --   natᶜ ≡ Σ (Σ (Lift' ⊤ × Set)
-- --         (λ γ → Lift' (proj₂ γ)))
-- --         (λ γ → proj₂ (proj₁ γ) → Lift' (proj₂ (proj₁ γ)))
-- -- itworks = refl
