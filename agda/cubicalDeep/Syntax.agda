{-# OPTIONS --rewriting #-}

open import Lib

infixl 5 _,_
infixl 7 _[_]T
infixl 5 _,s_
infix  6 _∘_
infixl 8 _[_]t

postulate
  Con : Set
  Ty  : Con → Set
  Tm  : ∀ Γ → Ty Γ → Set
  Tms : Con → Con → Set

  ∙     : Con
  _,_   : (Γ : Con) → Ty Γ → Con

  _[_]T : ∀{Γ Δ} → Ty Δ → Tms Γ Δ → Ty Γ

  coe-[]T : (Γ : I → Con) → ∀ {Δ}{A : Ty Δ}(σ : Tms (Γ ₀) Δ)
          → coe (⟨ i ⟩ Ty (Γ  i)) (A [ σ ]T) ↦ A [ coe (⟨ i ⟩ Tms (Γ i) Δ) σ ]T

  id : ∀{Γ} → Tms Γ Γ

  coe-id : (Γ : I → Con) → coe (⟨ i ⟩ Tms (Γ i) (Γ i)) (id {Γ ₀}) ↦ id

  _∘_   : ∀{Γ Δ Σ} → Tms Δ Σ → Tms Γ Δ → Tms Γ Σ

  coe-∘ : (Γ Σ : I → Con)(Δ : Con)(σ : Tms Δ (Σ ₀))(δ : Tms (Γ ₀) Δ)
          → coe (⟨ i ⟩ Tms (Γ i) (Σ i)) (σ ∘ δ) ↦ coe (⟨ i ⟩ Tms Δ (Σ i)) σ ∘ coe (⟨ i ⟩ Tms (Γ i) Δ) δ

  ε     : ∀{Γ} → Tms Γ ∙

  coe-ε : (Γ : I → Con) → coe (⟨ i ⟩ Tms (Γ i) ∙) ε ↦ ε

  _,s_  : ∀{Γ Δ}(σ : Tms Γ Δ){A : Ty Δ} → Tm Γ (A [ σ ]T) → Tms Γ (Δ , A)

  coe-,s : (Γ : I → Con)(Δ : I → Con)(A : ∀ i → Ty (Δ i))
           (σ : Tms (Γ ₀) (Δ ₀))(t : Tm (Γ ₀) (A ₀ [ σ ]T))
           → coe (⟨ i ⟩ Tms (Γ i) (Δ i , A i)) (σ ,s t) ↦
           coe (⟨ i ⟩ Tms (Γ i) (Δ i)) σ ,s
           coe (⟨ i ⟩ Tm (Γ i) (A i [ coe (⟨ j ⟩ Tms (Γ (j ∧ i)) (Δ (j ∧ i))) σ ]T)) t

  π₁    : ∀{Γ Δ}{A : Ty Δ} → Tms Γ (Δ , A) → Tms Γ Δ

  coe-π₁ : (Γ Δ : I → Con)(A : Ty (Δ ₀))(σ : Tms (Γ ₀) (Δ ₀ , A))
           → coe (⟨ i ⟩ Tms (Γ i) (Δ i)) (π₁ σ) ↦
             π₁ (coe (⟨ i ⟩ Tms (Γ i) (Δ i , coe (⟨ j ⟩ Ty (Δ (i ∧ j))) A)) σ)

  _[_]t : ∀{Γ Δ}{A : Ty Δ} → Tm Δ A → (σ : Tms Γ Δ) → Tm Γ (A [ σ ]T)

  coe-[]t : (Γ Δ : I → Con)(A : ∀ i → Ty (Δ i))(t : Tm (Δ ₀) (A ₀))(σ : ∀ i → Tms (Γ i) (Δ i))
            → coe (⟨ i ⟩ Tm (Γ i) (A i [ σ i ]T)) (t [ σ ₀ ]t) ↦
              coe (⟨ i ⟩ Tm (Δ i) (A i)) t [ σ ₁ ]t

  π₂    : ∀{Γ Δ}{A : Ty Δ}(σ : Tms Γ (Δ , A)) → Tm Γ (A [ π₁ σ ]T)

  coe-π₂ : (Γ Δ : I → Con)(A : ∀ i → Ty (Δ i))(σ : ∀ i → Tms (Γ i) (Δ i , A i))
           → coe (⟨ i ⟩ Tm (Γ i) (A i [ π₁ (σ i) ]T)) (π₂ (σ ₀)) ↦
             π₂ (σ ₁)

{-# REWRITE coe-[]T coe-id coe-∘ coe-ε coe-,s coe-π₁ coe-[]t coe-π₂ #-}

postulate

  [id]T : ∀{Γ}{A : Ty Γ} → A [ id ]T ≡ A
  [][]T : ∀{Γ Δ Σ}{A : Ty Σ}(σ : Tms Γ Δ)(δ : Tms Δ Σ)
          → A [ δ ]T [ σ ]T ≡ A [ δ ∘ σ ]T

  idl   : ∀{Γ Δ}{σ : Tms Γ Δ} → (id ∘ σ) ≡ σ
  idr   : ∀{Γ Δ}{σ : Tms Γ Δ} → (σ ∘ id) ≡ σ
  ass   : ∀{Γ Δ Σ Ω}{σ : Tms Σ Ω}{δ : Tms Δ Σ}{ν : Tms Γ Δ}
        → (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν)

  ,∘    : ∀{Γ Δ Σ}{δ : Tms Γ Δ}{σ : Tms Σ Γ}{A : Ty Δ}{a : Tm Γ (A [ δ ]T)}
        → ((δ ,s a) ∘ σ) ≡ ((δ ∘ σ) ,s tr (Tm Σ) ([][]T _ _) (a [ σ ]t))
  π₁β   : ∀{Γ Δ}{A : Ty Δ}{σ : Tms Γ Δ}{a : Tm Γ (A [ σ ]T)}
        → (π₁ (σ ,s a)) ≡ σ
  πη    : ∀{Γ Δ}{A : Ty Δ}{σ : Tms Γ (Δ , A)}
        → (π₁ σ ,s π₂ σ) ≡ σ
  εη    : ∀{Γ}{σ : Tms Γ ∙}
        → σ ≡ ε
  π₂β   : ∀{Γ Δ}{A : Ty Δ}{σ : Tms Γ Δ}{a : Tm Γ (A [ σ ]T)}
        → π₂ (σ ,s a) ≡ tr (λ σ → Tm Γ (A [ σ ]T)) (π₁β ⁻¹) a

wk : ∀{Γ}{A : Ty Γ} → Tms (Γ , A) Γ
wk = π₁ id

vz : ∀{Γ}{A : Ty Γ} → Tm (Γ , A) (A [ wk ]T)
vz = π₂ id

vs : ∀{Γ}{A B : Ty Γ} → Tm Γ A → Tm (Γ , B) (A [ wk ]T)
vs x = x [ wk ]t

<_> : ∀{Γ}{A : Ty Γ} → Tm Γ A → Tms Γ (Γ , A)
< t > = id ,s tr (Tm _) ([id]T ⁻¹) t

infix 4 <_>

_^_ : {Γ Δ : Con}(σ : Tms Γ Δ)(A : Ty Δ) → Tms (Γ , A [ σ ]T) (Δ , A)
σ ^ A = (σ ∘ wk) ,s tr (Tm _) ([][]T _ _) vz

infixl 5 _^_

postulate
  U     : ∀{Γ} → Ty Γ
  coe-U : (Γ : I → Con) → coe (⟨ i ⟩ Ty (Γ i)) U ↦ U
  U[]   : ∀{Γ Δ}(σ : Tms Γ Δ) → (U [ σ ]T) ≡ U

  El     : ∀{Γ}(a : Tm Γ U) → Ty Γ
  coe-El : (Γ : I → Con)(a : Tm (Γ ₀) U) → coe (⟨ i ⟩ Ty (Γ i)) (El a) ↦ El (coe (⟨ i ⟩ Tm (Γ i) U) a)
  El[]   : ∀{Γ Δ}(σ : Tms Γ Δ){a : Tm Δ U}
              → (El a [ σ ]T) ≡ (El (tr (Tm Γ) (U[] _) (a [ σ ]t)))

  -- coe-El[] : ∀{Γ : I → Con}{Δ : I → Con}{σ : ∀ i → Tms (Γ i) (Δ i)}{a : ∀ i → Tm (Δ i) U}
  --            → coe (⟨ i ⟩ (El (a i) [ σ i ]T ≡ El (tr (Tm (Γ i)) U[] (a i [ σ i ]t))))
  --                  (El[] {σ = σ ₀}{a ₀}) ≡ El[] {σ = σ ₁}{a ₁}

  Π      : ∀{Γ}(a : Tm Γ U)(B : Ty (Γ , El a)) → Ty Γ
  coe-Π'  : (Γ : I → Con)(a : Tm (Γ ₀) U)(B : Ty (Γ ₀ , El a))
          → coe (⟨ i ⟩ Ty (Γ i)) (Π a B) ↦
          Π (coe (⟨ i ⟩ Tm (Γ i) U) a) (coe (⟨ i ⟩ Ty (Γ i , El (coe (⟨ j ⟩ (Tm (Γ (i ∧ j))) U) a))) B)

  Π[] : ∀{Γ Δ}{σ : Tms Γ Δ}{a : Tm Δ U}{B : Ty (Δ , El a)}
      → (Π a B) [ σ ]T ≡ Π (tr (Tm Γ) (U[] _) (a [ σ ]t))
                           (tr (λ x → Ty (Γ , x)) (El[] _) (B [ σ ^ El a ]T))

  app : ∀{Γ}{a : Tm Γ U}{B : Ty (Γ , El a)} → Tm Γ (Π a B) → Tm (Γ , El a) B

  coe-app : (Γ : I → Con)(a : ∀ i → Tm (Γ i) U)(B : ∀ i → Ty (Γ i , El (a i)))
            (t : Tm (Γ ₀) (Π (a ₀) (B ₀)))
            → coe (⟨ i ⟩ Tm (Γ i , El (a i)) (B i)) (app t) ↦ app (coe (⟨ i ⟩ Tm (Γ i) (Π (a i) (B i))) t)

  lam : ∀{Γ}{a : Tm Γ U}{B : Ty (Γ , El a)} → Tm (Γ , El a) B → Tm Γ (Π a B)

  coe-lam : (Γ : I → Con)(a : ∀ i → Tm (Γ i) U)(B : ∀ i → Ty (Γ i , El (a i)))
            (t : Tm (Γ ₀ , El (a ₀)) (B ₀))
            → coe (⟨ i ⟩ Tm (Γ i) (Π (a i) (B i))) (lam t) ↦
            lam (coe (⟨ i ⟩ Tm (Γ i , El (a i)) (B i)) t)

  lam[] : ∀{Γ Δ}{σ : Tms Γ Δ}{a : Tm Δ U}{B : Ty (Δ , El a)}{t : Tm (Δ , El a) B}
    → tr (Tm Γ) Π[] (lam t [ σ ]t) ≡
        (lam (coe
               (⟨ i ⟩ Tm (Γ , (El[] σ {a}) $ i)
                         (coe (⟨ j ⟩ Ty (Γ , (El[] σ {a}) $ * i ₀ j)) (B [ σ ^ El a ]T)))
               (t [ σ ^ El a ]t)))

  Πβ : ∀{Γ}{a : Tm Γ U}{B : Ty (Γ , El a)}{t : Tm (Γ , El a) B} → app (lam t) ≡ t
  Πη : ∀{Γ}{a : Tm Γ U}{B : Ty (Γ , El a)}{t : Tm Γ (Π a B)}    → lam (app t) ≡ t

{-# REWRITE coe-U coe-El coe-Π' coe-app coe-lam #-}

--------------------------------------------------------------------------------

-- potential computations:
-- 1. ap f (p ◾ q) ↦ ap f p ◾ ap f q
-- 2. coe rules for β-η equations


-- Not enough coherence for [id]t this either

-- [id]t : ∀ {Γ A}{t : Tm Γ A} → tr (Tm Γ) [id]T (t [ id ]t) ≡ t
-- [id]t {Γ}{A} {t} = {!π₂β !}

-- t [ id ]t
-- π₂ (id ,s t [ id ]t)
-- π₂ (id ∘ id ,s t [ id ]t))
-- π₂ ((id ,s t) ∘ id)
-- π₂ (id ,s t)
-- t

postulate
  [id]t : ∀ {Γ A}{t : Tm Γ A} → tr (Tm Γ) [id]T (t [ id ]t) ≡ t

  [][]t : ∀ {Γ Δ Σ A}{t : Tm Σ A}{σ : Tms Δ Σ}{δ : Tms Γ Δ}
          → tr (Tm Γ) ([][]T _ _) (t [ σ ]t [ δ ]t) ≡ t [ σ ∘ δ ]t

  U[]-[id]T : ∀ {Γ} → [id]T ≡ (U[] (id {Γ}))

π₁∘ : ∀{Γ Δ Σ}{A : Ty Δ}{δ : Tms Γ (Δ , A)}{ν : Tms Σ Γ}
     → π₁ δ ∘ ν ≡ π₁ (δ ∘ ν)
π₁∘ {Γ}{Δ}{Σ}{A}{δ}{ν}
  = (π₁β ⁻¹ ◾ ap π₁ (,∘ ⁻¹)) ◾ ap (λ x → π₁ (x ∘ ν)) πη

wkβ : ∀{Γ Δ A}{σ : Tms Γ Δ}{t : Tm Γ (A [ σ ]T)}
      → wk ∘ (σ ,s t) ≡ σ
wkβ = π₁∘ ◾ ap π₁ idl ◾ π₁β

-- Transp
--------------------------------------------------------------------------------

_^El_ :
  {Γ Δ : Con}(σ : Tms Γ Δ)(a : Tm Δ U)
  → Tms (Γ , El (tr (Tm Γ) (U[] _) (a [ σ ]t))) (Δ , El a)
σ ^El a = σ ∘ wk ,s tr (Tm _) (ap (_[ wk ]T) (El[] _ ⁻¹) ◾ ([][]T _ _)) vz

infixl 5 _^El_

-- postulate
--   Id   : ∀ {Γ}(a : Tm Γ U) → Tm Γ (El a) → Tm Γ (El a) → Tm Γ U
--   Id[] : ∀ {Γ Δ}{σ : Tms Γ Δ}{a : Tm Δ U}{t u : Tm Δ (El a)}
--          → tr (Tm Γ) (U[] _) (Id a t u [ σ ]t) ≡
--              (Id (tr (Tm Γ) (U[] _)  (a [ σ ]t))
--                  (tr (Tm Γ) (El[] _) (t [ σ ]t))
--                  (tr (Tm Γ) (El[] _) (u [ σ ]t)))

--   Refl : ∀ {Γ}(a : Tm Γ U)(t : Tm Γ (El a)) → Tm Γ (El (Id a t t))

--   Refl[] : ∀ {Γ Δ}{σ : Tms Γ Δ}{a : Tm Δ U}{t : Tm Δ (El a)}
--            → tr (Tm Γ) (El[] σ ◾ ap El (Id[] {σ = σ}))
--              (Refl a t [ σ ]t) ≡
--                (Refl
--                  (tr (Tm Γ) (U[] _) (a [ σ ]t))
--                  (tr (Tm Γ) (El[] _) (t [ σ ]t)))

--   Transp :
--     ∀ {Γ}{a : Tm Γ U}(P : Ty (Γ , El a))
--       {t u : Tm Γ (El a)}
--       (pt : Tm Γ (P [ < t > ]T))
--       (eq : Tm Γ (El (Id a t u)))
--     → Tm Γ (P [ < u > ]T)

--   Transp[] :
--     ∀ {Γ Δ}{σ : Tms Γ Δ}
--       {a : Tm Δ U}(P : Ty (Δ , El a))
--       {t u : Tm Δ (El a)}
--       (pt : Tm Δ (P [ < t > ]T))
--       (eq : Tm Δ (El (Id a t u)))
--     → tr (Tm Γ) {!!} (Transp P pt eq [ σ ]t) ≡
--       Transp
--         (P [ σ ^El a ]T)
--         (tr (Tm Γ) ([][]T _ _ ◾ ap (P [_]T) (,∘ ◾ {!⟨ i ⟩ idl {σ = σ} $ i ,s tr (Tm Γ) ([][]T σ id) (tr (Tm Δ) ([id]T ⁻¹) t [ σ ]t)!} ◾ ,∘ ⁻¹) ◾ [][]T _ _ ⁻¹) (pt [ σ ]t))
--         (tr (Tm Γ) ((El[] σ ◾ ap El (Id[] {σ = σ}))) (eq [ σ ]t))

--   Transpβ :
--     ∀ {Γ}{a : Tm Γ U}(P : Ty (Γ , El a))
--       {t : Tm Γ (El a)}
--       (pt : Tm Γ (P [ < t > ]T))
--     → Transp P pt (Refl a t) ≡ pt

-- J
--------------------------------------------------------------------------------

-- lem3 :
--   {Γ : Con}(a : Tm Γ U)(u : Tm Γ (El a))
--   →
--   (tr (Tm Γ) (U[] _)
--    (tr (Tm (Γ , El a)) (U[] _) (a [ wk ]t) [ id ,s tr (Tm Γ) ([id]T ⁻¹) u
--     ]t))
--   ≡
--   tr (Tm Γ) (U[] _)
--     (tr (Tm Γ) ([][]T _ _)
--       (a [ wk {A = El a} ]t [ id ,s tr (Tm Γ) ([id]T ⁻¹) u ]t))
-- lem3 {Γ} a u = {!      (tr (Tm (Γ , El a)) U[] (a [ wk ]t) [ id ,s tr (Tm Γ) ([id]T ⁻¹) u
--        ]t)!}

-- Tm Γ (U [ id ,s tr (Tm Γ) ([id]T ⁻¹) u ]T)
-- Tm Γ (U [ wk ∘ (id ,s tr (Tm Γ) ([id]T ⁻¹) u) ]T)

-- lem2 : {Γ : Con}(a : Tm Γ U)(u : Tm Γ (El a))
--       →
--       tr (Tm Γ) (U[] _)
--         (tr (Tm Γ) ([][]T _ _)
--           (a [ wk {A = El a} ]t [ id ,s tr (Tm Γ) ([id]T ⁻¹) u ]t))
--       ≡ a
-- lem2 {Γ} a u =
--     ap (tr (Tm Γ) (U[] _)) ([][]t {σ = wk}{id ,s coe (ap (Tm Γ) ([id]T ⁻¹)) u})
--   ◾ ap (λ x → tr (Tm Γ) (U[] _) (a [ x ]t)) (wkβ {σ = id}{coe (ap (Tm Γ) ([id]T ⁻¹)) u})
--   ◾ tr (λ x → tr (Tm Γ) x (a [ id ]t) ≡ a) U[]-[id]T [id]t

-- lem1 : {Γ : Con}(a : Tm Γ U)(u : Tm Γ (El a))
--       →
--       (tr (Tm Γ) (U[] _)
--        (tr (Tm (Γ , El a)) (U[] _) (a [ wk ]t) [ id ,s tr (Tm Γ) ([id]T ⁻¹) u
--         ]t))
--       ≡ a
-- lem1 {Γ} a u =
--   let foo = a [ wk {A = El a} ]t [ id ,s coe (ap (Tm Γ) ([id]T ⁻¹)) u ]t
--       bar = tr (Tm Γ) ([][]T _ _ ◾ U[] _) foo
--   in {!!}

postulate
  Id   : ∀ {Γ}(a : Tm Γ U) → Tm Γ (El a) → Tm Γ (El a) → Tm Γ U
  Id[] : ∀ {Γ Δ}{σ : Tms Γ Δ}{a : Tm Δ U}{t u : Tm Δ (El a)}
         → tr (Tm Γ) (U[] _) (Id a t u [ σ ]t) ≡
             (Id (tr (Tm Γ) (U[] _) (a [ σ ]t))
                 (tr (Tm Γ) (El[] _) (t [ σ ]t))
                 (tr (Tm Γ) (El[] _) (u [ σ ]t)))

  Refl : ∀ {Γ}(a : Tm Γ U)(t : Tm Γ (El a)) → Tm Γ (El (Id a t t))

  Refl[] : ∀ {Γ Δ}{σ : Tms Γ Δ}{a : Tm Δ U}{t : Tm Δ (El a)}
           → tr (Tm Γ) (El[] σ ◾ ap El (Id[] {σ = σ}))
             (Refl a t [ σ ]t) ≡
               (Refl
                 (tr (Tm Γ) (U[] _) (a [ σ ]t))
                 (tr (Tm Γ) (El[] _) (t [ σ ]t)))

  -- diag :
  --   ∀ {Γ}
  --     {a : Tm Γ U}
  --     {t : Tm Γ (El a)}
  --     (P : Ty (Γ , El a , El (Id (tr (Tm (Γ , El a)) (U[] _) (a [ wk ]t))
  --                                (tr (Tm _) (El[] _) vz)
  --                                (tr (Tm _) (El[] _) (t [ wk ]t)))))
  --     {u : Tm Γ (El a)}
  --     (p : Tm Γ (El (Id a t u)))
  --   →
  --     El (Id a t u)
  --     ≡ El
  --      (Id (tr (Tm (Γ , El a)) (U[] wk) (a [ wk ]t))
  --       (tr (Tm (Γ , El a)) (El[] wk) vz)
  --       (tr (Tm (Γ , El a)) (El[] wk) (t [ wk ]t)))
  --      [ id ,s tr (Tm Γ) ([id]T ⁻¹) u ]T

  -- IdJ :
  --   ∀ {Γ}
  --     (a : Tm Γ U)
  --     (t : Tm Γ (El a))
  --     (P : Ty (Γ , El a , El (Id (tr (Tm (Γ , El a)) (U[] _) (a [ wk ]t))
  --                                (tr (Tm _) (El[] _) vz)
  --                                (tr (Tm _) (El[] _) (t [ wk ]t)))))
  --     (pr : Tm Γ (P [ id ,s tr (Tm Γ) ([id]T ⁻¹) t
  --                        ,s tr (Tm Γ) (diag P (Refl a t)) (Refl a t) ]T))
  --     (u : Tm Γ (El a))
  --     (p : Tm Γ (El (Id a t u)))
  --   → Tm Γ (P [ id ,s tr (Tm Γ) ([id]T ⁻¹) u ,s tr (Tm Γ) (diag P p) p ]T)

  -- IdJ[] :
  --   ∀ {Γ Δ}(σ : Tms Γ Δ)
  --     (a : Tm Δ U)
  --     (t : Tm Δ (El a))
  --     (P : Ty (Δ , El a , El (Id (tr (Tm (Δ , El a)) (U[] _) (a [ wk ]t))
  --                                (tr (Tm _) (El[] _) vz)
  --                                (tr (Tm _) (El[] _) (t [ wk ]t)))))
  --     (pr : Tm Δ (P [ id ,s tr (Tm Δ) ([id]T ⁻¹) t
  --                        ,s tr (Tm Δ) (diag P (Refl a t)) (Refl a t) ]T))
  --     (u : Tm Δ (El a))
  --     (p : Tm Δ (El (Id a t u)))
  --   → tr (Tm Γ) {!!} (IdJ a t P pr u p [ σ ]t)
  --     ≡ IdJ (tr (Tm Γ) (U[] _) (a [ σ ]t))
  --           (tr (Tm Γ) (El[] _) (t [ σ ]t))
  --           (P [ (σ ∘ wk) ∘ wk ,s tr (Tm _) (ap (_[ wk ]T) (ap (_[ wk ]T) (El[] _ ⁻¹) ◾ [][]T wk σ) ◾ [][]T wk (σ ∘ wk)) (vs vz) ,s tr (Tm _) {!!} vz ]T)
  --           {!!}
  --           (tr (Tm Γ) (El[] _) (u [ σ ]t))
  --           (tr (Tm Γ) (El[] _ ◾ ap El (Id[] {σ = σ})) (p [ σ ]t))
