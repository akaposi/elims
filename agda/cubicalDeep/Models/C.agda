{-# OPTIONS --rewriting #-}

module Models.C where

open import Agda.Primitive
open import Lib
import Syntax as S

private
  infixl 5 _,_
  infixl 7 _[_]T
  infixl 5 _,s_
  infix  6 _∘_
  infixl 8 _[_]t

  i : Level
  i = lsuc (lsuc lzero)

  j : Level
  j = (lsuc lzero)

  Con : Set i
  Con = Set₁

  Ty : Con → Set i
  Ty Γ = Γ → Set₁

  Tm : ∀ Γ → Ty Γ → Set j
  Tm Γ A = (γ : Γ) → A γ

  Tms : Con → Con → Set j
  Tms Γ Δ = Γ → Δ

  ∙ : Con
  ∙ = Lift ⊤

  _,_ : (Γ : Con) → Ty Γ → Con
  _,_ = Σ

  _[_]T : ∀{Γ Δ} → Ty Δ → Tms Γ Δ → Ty Γ
  A [ σ ]T = λ γ → A (σ γ)

  id : ∀{Γ} → Tms Γ Γ
  id = λ γ → γ

  _∘_ : ∀{Γ Δ Σ} → Tms Δ Σ → Tms Γ Δ → Tms Γ Σ
  σ ∘ δ = λ γ → σ (δ γ)

  ε : ∀{Γ} → Tms Γ ∙
  ε = λ _ → lift tt

  _,s_  : ∀{Γ Δ A}(σ : Tms Γ Δ) → Tm Γ (A [ σ ]T) → Tms Γ (Δ , A)
  σ ,s t = λ γ → (σ γ) Σ, (t γ)

  π₁ : ∀{Γ Δ}{A : Ty Δ} → Tms Γ (Δ , A) → Tms Γ Δ
  π₁ σ = λ γ → proj₁ (σ γ)

  _[_]t : ∀{Γ Δ}{A : Ty Δ} → Tm Δ A → (σ : Tms Γ Δ) → Tm Γ (A [ σ ]T)
  t [ σ ]t = λ γ → t (σ γ)

  π₂ : ∀{Γ Δ}{A : Ty Δ}(σ : Tms Γ (Δ , A)) → Tm Γ (A [ π₁ σ ]T)
  π₂ = λ σ γ → proj₂ (σ γ)

  [id]T : ∀{Γ}{A : Ty Γ} → A [ id ]T ≡ A
  [id]T = refl

  [][]T : ∀{Γ Δ Σ}{A : Ty Σ}{σ : Tms Γ Δ}{δ : Tms Δ Σ}
          → A [ δ ]T [ σ ]T ≡ A [ δ ∘ σ ]T
  [][]T = refl

  idl : ∀{Γ Δ}{δ : Tms Γ Δ} → (id ∘ δ) ≡ δ
  idl = refl

  idr : ∀{Γ Δ}{δ : Tms Γ Δ} → (δ ∘ id) ≡ δ
  idr = refl

  ass : ∀{Γ Δ Σ Ω}{σ : Tms Σ Ω}{δ : Tms Δ Σ}{ν : Tms Γ Δ}
        → (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν)
  ass = refl

  ,∘ : ∀{Γ Δ Σ}{δ : Tms Γ Δ}{σ : Tms Σ Γ}{A : Ty Δ}{a : Tm Γ (A [ δ ]T)}
        → ((_,s_ {Γ}{Δ}{A} δ a) ∘ σ) ≡
          ((δ ∘ σ) ,s tr (Tm Σ) ([][]T {Σ}{Γ}{Δ}{A}{σ}{δ}) (_[_]t {Σ}{Γ}{A [ δ ]T} a σ))
  ,∘ = refl

  π₁β : ∀{Γ Δ}{A : Ty Δ}{δ : Tms Γ Δ}{a : Tm Γ (A [ δ ]T)}
        → (π₁ (_,s_ {Γ}{Δ}{A} δ a)) ≡ δ
  π₁β = refl

  πη : ∀{Γ Δ}{A : Ty Δ}{δ : Tms Γ (Δ , A)}
        → (π₁ δ ,s π₂ δ) ≡ δ
  πη = refl

  εη : ∀{Γ}{σ : Tms Γ ∙}
        → σ ≡ ε
  εη = refl

  π₂β : ∀{Γ Δ}{A : Ty Δ}{δ : Tms Γ Δ}{a : Tm Γ (A [ δ ]T)}
        → tr (λ x → Tm Γ (A [ x ]T)) (π₁β {Γ}{Δ}{A}{δ}{a}) (π₂ (_,s_ {Γ}{Δ}{A} δ a)) ≡ a
  π₂β = refl

  wk : ∀{Γ}{A : Ty Γ} → Tms (Γ , A) Γ
  wk = π₁ id

  vz : ∀{Γ}{A : Ty Γ} → Tm (Γ , A) (A [ wk ]T)
  vz = π₂ id

  vs : ∀{Γ}{A B : Ty Γ} → Tm Γ A → Tm (Γ , B) (A [ wk ]T)
  vs x = x [ wk ]t

  <_> : ∀{Γ}{A : Ty Γ} → Tm Γ A → Tms Γ (Γ , A)
  < t > = id ,s tr (Tm _) ([id]T ⁻¹) t

  infix 4 <_>

  _^_ : {Γ Δ : Con}(σ : Tms Γ Δ)(A : Ty Δ) → Tms (Γ , A [ σ ]T) (Δ , A)
  _^_ {Γ}{Δ} σ A =
    (σ ∘ wk) ,s
    tr (Tm (Γ , A [ σ ]T)) ([][]T {Γ , A [ σ ]T}{Γ}{Δ}{A}{wk{Γ}{A [ σ ]T}}{σ}) vz

  infixl 5 _^_

  U : ∀{Γ} → Ty Γ
  U = λ γ → Set

  U[] : ∀{Γ Δ}{σ : Tms Γ Δ} → (U [ σ ]T) ≡ U
  U[] = refl

  El : ∀{Γ}(a : Tm Γ U) → Ty Γ
  El a = λ γ → Lift (a γ)

  El[] : ∀{Γ Δ}{σ : Tms Γ Δ}{a : Tm Δ U}
       → (El a [ σ ]T) ≡ (El (tr (Tm Γ) (U[] {σ = σ}) (a [ σ ]t)))
  El[] = refl

  Π : ∀{Γ}(a : Tm Γ U)(B : Ty (Γ , El a)) → Ty Γ
  Π a B = λ γ → (α : a γ) → B (γ Σ, lift α)

  Π[] : ∀{Γ Δ}{σ : Tms Γ Δ}{a : Tm Δ U}{B : Ty (Δ , El a)}
      → (Π a B) [ σ ]T ≡ Π (tr (Tm Γ) (U[] {σ = σ}) (a [ σ ]t))
                           (tr (λ x → Ty (Γ , x)) (El[] {σ = σ}{a}) (B [ σ ^ El a ]T))
  Π[] = refl

  app : ∀{Γ}{a : Tm Γ U}{B : Ty (Γ , El a)} → Tm Γ (Π a B) → Tm (Γ , El a) B
  app t = λ γ → t (proj₁ γ) (proj₂ γ .lower)

  lam : ∀{Γ}{a : Tm Γ U}{B : Ty (Γ , El a)} → Tm (Γ , El a) B → Tm Γ (Π a B)
  lam t = λ γ α → t (γ Σ, lift α)

  lam[] :
    {Γ Δ : Con} {σ : Tms Γ Δ} {a : Tm Δ (U {Δ})} {B : Ty (Δ , El {Δ}
    a)} {t : Tm (Δ , El {Δ} a) B} → _≡_ {_} {Tm Γ (_[_]T {Γ} {Δ} (Π
    {Δ} a B) σ)} (_[_]t {Γ} {Δ} {Π {Δ} a B} (lam {Δ} {a} {B} t) σ) (tr
    {_} {_} {Ty Γ} (Tm Γ) {Π {Γ} (tr {_} {_} {Ty Γ} (Tm Γ) {_[_]T {Γ}
    {Δ} (U {Δ}) σ} {U {Γ}} (U[] {Γ} {Δ} {σ}) (_[_]t {Γ} {Δ} {U {Δ}} a
    σ)) (tr {_} {_} {Ty Γ} (λ x → Ty (Γ , x)) {_[_]T {Γ} {Δ} (El {Δ}
    a) σ} {El {Γ} (tr {_} {_} {Ty Γ} (Tm Γ) {_[_]T {Γ} {Δ} (U {Δ}) σ}
    {U {Γ}} (U[] {Γ} {Δ} {σ}) (_[_]t {Γ} {Δ} {U {Δ}} a σ))} (El[] {Γ}
    {Δ} {σ} {a}) (_[_]T {Γ , _[_]T {Γ} {Δ} (El {Δ} a) σ} {Δ , El {Δ}
    a} B (_^_ {Γ} {Δ} σ (El {Δ} a))))} {_[_]T {Γ} {Δ} (Π {Δ} a B) σ}
    (_⁻¹ {_} {Ty Γ} {_[_]T {Γ} {Δ} (Π {Δ} a B) σ} {Π {Γ} (tr {_} {_}
    {Ty Γ} (Tm Γ) {_[_]T {Γ} {Δ} (U {Δ}) σ} {U {Γ}} (U[] {Γ} {Δ} {σ})
    (_[_]t {Γ} {Δ} {U {Δ}} a σ)) (tr {_} {_} {Ty Γ} (λ x → Ty (Γ , x))
    {_[_]T {Γ} {Δ} (El {Δ} a) σ} {El {Γ} (tr {_} {_} {Ty Γ} (Tm Γ)
    {_[_]T {Γ} {Δ} (U {Δ}) σ} {U {Γ}} (U[] {Γ} {Δ} {σ}) (_[_]t {Γ} {Δ}
    {U {Δ}} a σ))} (El[] {Γ} {Δ} {σ} {a}) (_[_]T {Γ , _[_]T {Γ} {Δ}
    (El {Δ} a) σ} {Δ , El {Δ} a} B (_^_ {Γ} {Δ} σ (El {Δ} a))))} (Π[]
    {Γ} {Δ} {σ} {a} {B})) (lam {Γ} {tr {_} {_} {Ty Γ} (Tm Γ) {_[_]T
    {Γ} {Δ} (U {Δ}) σ} {U {Γ}} (U[] {Γ} {Δ} {σ}) (_[_]t {Γ} {Δ} {U
    {Δ}} a σ)} {coe {_} {Ty (Γ , _[_]T {Γ} {Δ} (El {Δ} a) σ)} {Ty (Γ ,
    El {Γ} (tr {_} {_} {Ty Γ} (Tm Γ) {_[_]T {Γ} {Δ} (U {Δ}) σ} {U {Γ}}
    (U[] {Γ} {Δ} {σ}) (_[_]t {Γ} {Δ} {U {Δ}} a σ)))} (⟨ j ⟩ Ty (Γ ,
    _$_ {_} {Ty Γ} {_[_]T {Γ} {Δ} (El {Δ} a) σ} {El {Γ} (tr {_} {_}
    {Ty Γ} (Tm Γ) {_[_]T {Γ} {Δ} (U {Δ}) σ} {U {Γ}} (U[] {Γ} {Δ} {σ})
    (_[_]t {Γ} {Δ} {U {Δ}} a σ))} (El[] {Γ} {Δ} {σ} {a}) j)) (_[_]T {Γ
    , _[_]T {Γ} {Δ} (El {Δ} a) σ} {Δ , El {Δ} a} B (_^_ {Γ} {Δ} σ (El
    {Δ} a)))} (coe {_} {Tm (Γ , _[_]T {Γ} {Δ} (El {Δ} a) σ) (_[_]T {Γ
    , _[_]T {Γ} {Δ} (El {Δ} a) σ} {Δ , El {Δ} a} B (_^_ {Γ} {Δ} σ (El
    {Δ} a)))} {Tm (Γ , El {Γ} (tr {_} {_} {Ty Γ} (Tm Γ) {_[_]T {Γ} {Δ}
    (U {Δ}) σ} {U {Γ}} (U[] {Γ} {Δ} {σ}) (_[_]t {Γ} {Δ} {U {Δ}} a σ)))
    (coe {_} {Ty (Γ , _[_]T {Γ} {Δ} (El {Δ} a) σ)} {Ty (Γ , El {Γ} (tr
    {_} {_} {Ty Γ} (Tm Γ) {_[_]T {Γ} {Δ} (U {Δ}) σ} {U {Γ}} (U[] {Γ}
    {Δ} {σ}) (_[_]t {Γ} {Δ} {U {Δ}} a σ)))} (⟨ j ⟩ Ty (Γ , _$_ {_} {Ty
    Γ} {_[_]T {Γ} {Δ} (El {Δ} a) σ} {El {Γ} (tr {_} {_} {Ty Γ} (Tm Γ)
    {_[_]T {Γ} {Δ} (U {Δ}) σ} {U {Γ}} (U[] {Γ} {Δ} {σ}) (_[_]t {Γ} {Δ}
    {U {Δ}} a σ))} (El[] {Γ} {Δ} {σ} {a}) j)) (_[_]T {Γ , _[_]T {Γ}
    {Δ} (El {Δ} a) σ} {Δ , El {Δ} a} B (_^_ {Γ} {Δ} σ (El {Δ} a))))}
    (⟨ i ⟩ Tm (Γ , _$_ {_} {Ty Γ} {_[_]T {Γ} {Δ} (El {Δ} a) σ} {El {Γ}
    (tr {_} {_} {Ty Γ} (Tm Γ) {_[_]T {Γ} {Δ} (U {Δ}) σ} {U {Γ}} (U[]
    {Γ} {Δ} {σ}) (_[_]t {Γ} {Δ} {U {Δ}} a σ))} (El[] {Γ} {Δ} {σ} {a})
    i) (coe {_} {Ty (Γ , _[_]T {Γ} {Δ} (El {Δ} a) σ)} {Ty (Γ , _$_ {_}
    {Ty Γ} {_[_]T {Γ} {Δ} (El {Δ} a) σ} {El {Γ} (tr {_} {_} {Ty Γ} (Tm
    Γ) {_[_]T {Γ} {Δ} (U {Δ}) σ} {U {Γ}} (U[] {Γ} {Δ} {σ}) (_[_]t {Γ}
    {Δ} {U {Δ}} a σ))} (El[] {Γ} {Δ} {σ} {a}) i)} (⟨ j ⟩ Ty (Γ , _$_
    {_} {Ty Γ} {_[_]T {Γ} {Δ} (El {Δ} a) σ} {El {Γ} (tr {_} {_} {Ty Γ}
    (Tm Γ) {_[_]T {Γ} {Δ} (U {Δ}) σ} {U {Γ}} (U[] {Γ} {Δ} {σ}) (_[_]t
    {Γ} {Δ} {U {Δ}} a σ))} (El[] {Γ} {Δ} {σ} {a}) (* i ₀ j))) (_[_]T
    {Γ , _[_]T {Γ} {Δ} (El {Δ} a) σ} {Δ , El {Δ} a} B (_^_ {Γ} {Δ} σ
    (El {Δ} a))))) (_[_]t {Γ , _[_]T {Γ} {Δ} (El {Δ} a) σ} {Δ , El {Δ}
    a} {B} t (_^_ {Γ} {Δ} σ (El {Δ} a))))))
  lam[] = refl

  Πβ : ∀{Γ}{a : Tm Γ U}{B : Ty (Γ , El a)}{t : Tm (Γ , El a) B} → app (lam t) ≡ t
  Πβ = refl

  Πη : ∀{Γ}{a : Tm Γ U}{B : Ty (Γ , El a)}{t : Tm Γ (Π a B)} → lam (app t) ≡ t
  Πη = refl

-- We skip path beta rules since UIP makes them redundant
--------------------------------------------------------------------------------

postulate
  Conᴿ : S.Con → Con
  Tyᴿ  : ∀ {Γ} → S.Ty Γ → Ty (Conᴿ Γ)
  Tmᴿ  : ∀ {Γ A} → S.Tm Γ A → Tm (Conᴿ Γ) (Tyᴿ A)
  Tmsᴿ : ∀ {Γ Δ} → S.Tms Γ Δ → Tms (Conᴿ Γ) (Conᴿ Δ)

postulate
  ∙ᴿ   : Conᴿ S.∙ ↦ ∙
  ,ᴿ   : ∀ Γ A → Conᴿ (Γ S., A) ↦ Conᴿ Γ , Tyᴿ A
  []Tᴿ : ∀ Γ Δ (A : S.Ty Δ) (σ : S.Tms Γ Δ) → Tyᴿ (A S.[ σ ]T) ↦ Tyᴿ A [ Tmsᴿ σ ]T
{-# REWRITE ∙ᴿ ,ᴿ []Tᴿ #-}

postulate
  idᴿ  : ∀ {Γ} → Tmsᴿ (S.id {Γ}) ↦ id
  ∘ᴿ   : ∀ {Γ Δ Σ}(σ : S.Tms Δ Σ)(δ : S.Tms Γ Δ) → Tmsᴿ (σ S.∘ δ) ↦ Tmsᴿ σ ∘ Tmsᴿ δ
  ,sᴿ  : ∀ {Γ Δ}(σ : S.Tms Γ Δ){A : S.Ty Δ}(t : S.Tm Γ (A S.[ σ ]T)) → Tmsᴿ (σ S.,s t) ↦ Tmsᴿ σ ,s Tmᴿ t
  εᴿ   : ∀ {Γ} → Tmsᴿ (S.ε {Γ}) ↦ ε
  π₁ᴿ  : ∀ {Γ Δ}{A : S.Ty Δ}(σ : S.Tms Γ (Δ S., A)) → Tmsᴿ (S.π₁ σ) ↦ π₁ (Tmsᴿ σ)
{-# REWRITE idᴿ ∘ᴿ ,sᴿ εᴿ π₁ᴿ #-}

postulate
  []tᴿ   : ∀{Γ Δ}{A : S.Ty Δ}(t : S.Tm Δ A)(σ : S.Tms Γ Δ) → Tmᴿ (t S.[ σ ]t) ↦ Tmᴿ t [ Tmsᴿ σ ]t
  π₂ᴿ    : ∀{Γ Δ}{A : S.Ty Δ}(σ : S.Tms Γ (Δ S., A)) → Tmᴿ (S.π₂ σ) ↦ π₂ (Tmsᴿ σ)
  Uᴿ     : ∀ {Γ} → Tyᴿ (S.U {Γ}) ↦ U
{-# REWRITE []tᴿ π₂ᴿ Uᴿ #-}

postulate
  Elᴿ : ∀ {Γ}{a : S.Tm Γ S.U} → Tyᴿ (S.El a) ↦ El (Tmᴿ a)
{-# REWRITE Elᴿ #-}

postulate
  Πᴿ : ∀ {Γ}(a : S.Tm Γ S.U)(B : S.Ty (Γ S., S.El a)) → Tyᴿ (S.Π a B) ↦ Π (Tmᴿ a) (Tyᴿ B)
{-# REWRITE Πᴿ #-}

postulate
  appᴿ : ∀ {Γ}{a : S.Tm Γ S.U}{B : S.Ty (Γ S., S.El a)}(t : S.Tm Γ (S.Π a B))
         → Tmᴿ (S.app t) ↦ app (Tmᴿ t)
  lamᴿ : ∀ {Γ}{a : S.Tm Γ S.U}{B : S.Ty (Γ S., S.El a)}(t : S.Tm (Γ S., S.El a) B)
         → Tmᴿ (S.lam t) ↦ lam (Tmᴿ t)
{-# REWRITE appᴿ lamᴿ #-}

--------------------------------------------------------------------------------

postulate
  U[]ᴿ :
    (Γ Δ : S.Con)(σ : S.Tms Γ Δ)(i : I)
    → Tyᴿ (S.U[]{Γ}{Δ} σ $ i) ↦ U[] {Conᴿ Γ}{Conᴿ Δ}{Tmsᴿ σ} $ i

  El[]ᴿ :
    (Γ Δ : S.Con)(σ : S.Tms Γ Δ)(a : S.Tm Δ S.U) (i : I)
    → Tyᴿ (S.El[] σ {a = a} $ i) ↦ El[] {σ = Tmsᴿ σ}{a = Tmᴿ a} $ i

  ◾-Tyᴿ :
    ∀ {Γ}(A B C : S.Ty Γ)
      (p : A ≡ B)(q : B ≡ C)(γ : Conᴿ Γ)(i : I)
      → Tyᴿ {Γ} ((p ◾ q) $ i) γ ↦ ((ap Tyᴿ p ◾ ap Tyᴿ q) $ i) γ
{-# REWRITE U[]ᴿ El[]ᴿ ◾-Tyᴿ #-}

--------------------------------------------------------------------------------

postulate
  coe-Tm :
    (Γ : I → S.Con)(A : ∀ i → S.Ty (Γ i))
    (t : S.Tm (Γ ₀) (A ₀))
    → Tmᴿ (coe (⟨ i ⟩ S.Tm (Γ i) (A i)) t) ↦ coe (⟨ i ⟩ Tm (Conᴿ (Γ i)) (Tyᴿ (A i))) (Tmᴿ t)

  coe-Ty :
    (Γ : I → S.Con)
    (A : S.Ty (Γ ₀))
    → Tyᴿ (coe (⟨ i ⟩ S.Ty (Γ i)) A) ↦ coe (⟨ i ⟩ Ty (Conᴿ (Γ i))) (Tyᴿ A)

  coe-Tms :
    (Γ Δ : I → S.Con)
    (σ : S.Tms (Γ ₀) (Δ ₀))
    → Tmsᴿ (coe (⟨ i ⟩ S.Tms (Γ i) (Δ i)) σ) ↦
      coe (⟨ i ⟩ Tms (Conᴿ (Γ i)) (Conᴿ (Δ i))) (Tmsᴿ σ)

{-# REWRITE coe-Tm coe-Ty coe-Tms #-}
