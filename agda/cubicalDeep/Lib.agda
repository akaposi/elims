
{-# OPTIONS --rewriting #-}

open import Agda.Primitive public

-- equality
--------------------------------------------------------------------------------

postulate _↦_ : ∀ {i}{A : Set i} → A → A → Set
{-# BUILTIN REWRITE _↦_ #-}
infix 3 _↦_

postulate
  I : Set
  ₀ ₁ : I
  * : I → I → I → I

infix 3 _≡_
data _≡_ {i}{A : Set i} : A → A → Set i where
  path : (f : I → A) → f ₀ ≡ f ₁

syntax path (λ i → t) = ⟨ i ⟩ t

_$_ : ∀ {i}{A : Set i}{x y : A} → x ≡ y → I → A
path f $ i = f i
infixl 8 _$_

{-# DISPLAY path f = f #-}

refl : ∀ {i}{A : Set i}{a : A} → a ≡ a
refl {a = a} = ⟨ _ ⟩ a

_∧_ : I → I → I
i ∧ j = * i ₀ j

_∨_ : I → I → I
i ∨ j = * i j ₁

infixl 4 _∧_
infixl 3 _∨_

~ : I → I
~ i = * i ₁ ₀

postulate
  $-₀ : ∀ {i}{A : Set i}{x y : A}(p : x ≡ y) → p $ ₀ ↦ x
  $-₁ : ∀ {i}{A : Set i}{x y : A}(p : x ≡ y) → p $ ₁ ↦ y
{-# REWRITE $-₀ $-₁ #-}

postulate
  *₀₀     : ∀ p   → * p ₀ ₀ ↦ ₀
  *₀₁     : ∀ p   → * p ₀ ₁ ↦ p
  *₁₁     : ∀ p   → * p ₁ ₁ ↦ ₁
  *left   : ∀ q r → * ₀ q r ↦ q
  *right  : ∀ q r → * ₁ q r ↦ r
  path-η  : ∀ i (A : Set i) (S T : A) (Q : S ≡ T) → ⟨ i ⟩ (Q $ i) ↦ Q
{-#  REWRITE *₀₀ *₀₁ *₁₁ *left *right #-}
{-# REWRITE path-η #-}

infixr 5 _◾_
postulate
  _◾_ : ∀{i}{A : Set i}{x y z : A} → x ≡ y → y ≡ z → x ≡ z
  coe : ∀{i}{A B : Set i} → A ≡ B → A → B
  reflexive-coe : ∀{i}(A : Set i)(p : A ≡ A)(a : A) → coe p a ↦ a
  regularity    : ∀{i}(A : Set i)(a : A) → coe (⟨ _ ⟩ A) a ↦ a
-- {-# REWRITE reflexive-coe #-}
{-# REWRITE regularity #-}

postulate
  coe-Π :
    -- NOTE: Agda doesn't accept universe polymorphism for both A and B (bug)
    ∀ α (A : I → Set) (B : (i : I) → A i → Set α)(f : (a : A ₀) → B ₀ a)
    → coe (⟨ i ⟩ ((a : A i) → B i a)) f
      ↦
      (λ a → coe (⟨ i ⟩ B i (coe (⟨ j ⟩ A (* j ₁ i)) a )) (f (coe (⟨ i ⟩ A (~ i)) a)))

  coe-≡ :
      ∀ α (A : I → Set α)(x y : ∀ i → A i)(p : x ₀ ≡ y ₀)
    → coe (⟨ i ⟩ (_≡_ {_}{A i} (x i) (y i))) p ↦
       ⟨ i ⟩ coe (⟨ j ⟩ (A (* i ₁ j))) (x (~ i))
     ◾ ⟨ i ⟩ coe (path A) (p $ i)
     ◾ ⟨ i ⟩ coe (⟨ j ⟩ (A (i ∨ j))) (y i)

  coe-◾ :
    ∀ {α}(A B C : Set α)(p : A ≡ B)(q : B ≡ C)(a : A)
    → coe (p ◾ q) a ↦ coe q (coe p a)

  refl◾p :
    ∀ {α}(A : Set α)(x y : A)(p : x ≡ y)
      → (⟨ _ ⟩ x) ◾ p ↦ p

  p◾q◾r :
    ∀ {α}(A : Set α)(a b c d : A)(p : a ≡ b)(q : b ≡ c)(r : c ≡ d)
    → ((p ◾ q) ◾ r) ↦ p ◾ (q ◾ r)

{-# REWRITE coe-Π coe-◾  #-}
{-# REWRITE coe-≡ #-}
{-# REWRITE refl◾p #-}
{-# REWRITE p◾q◾r #-}

infix 6 _⁻¹
_⁻¹ : ∀ {i}{A : Set i}{x y : A} → x ≡ y → y ≡ x
_⁻¹ p = ⟨ i ⟩ (p $ ~ i)

ap : ∀ {α β}{A : Set α}{B : Set β}(f : A → B){x y} → x ≡ y → f x ≡ f y
ap f p = ⟨ i ⟩ f (p $ i)

tr : ∀ {i j}{A : Set i}(B : A → Set j){a₀ : A}{a₁ : A}(a₂ : a₀ ≡ a₁) → B a₀ → B a₁
tr B p x = coe (⟨ i ⟩ B (p $ i)) x

apd :
  ∀ {α β}{A : Set α}{B : A → Set β}(f : ∀ a → B a){x y}
  → (p : x ≡ y) → tr B p (f x) ≡ f y
apd {B = B} f {x} {y} p = ⟨ i ⟩ coe (⟨ j ⟩ B (p $ * i j ₁)) (f (p $ i))

ext  : ∀{i j}{A : Set i}{B : A → Set j}{f g : (x : A) → B x}
       → ((x : A) → f x ≡ g x) → f ≡ g
ext h = ⟨ i ⟩ λ x → h x $ i

exti : ∀{i j}{A : Set i}{B : A → Set j}{f g : {x : A} → B x}
       → ((x : A) → f {x} ≡ g {x}) → _≡_ {A = {x : A} → B x} f g
exti h = ⟨ i ⟩ λ {x} → h x $ i

J : ∀ {i j}{A : Set i}{a : A}(P : ∀ a' → a ≡ a' → Set j) → P a refl → ∀ {a'} (p : a ≡ a') → P a' p
J P refl* p = coe (⟨ i ⟩ P (p $ i) (⟨ j ⟩ (p $ * i ₀ j))) refl*

--------------------------------------------------------------------------------

record Σ {α β}(A : Set α) (B : A → Set β) : Set (α ⊔ β) where
  constructor _Σ,_
  field
    proj₁ : A
    proj₂ : B proj₁
open Σ public
infixr 5 _Σ,_

Σ,= : ∀{ℓ ℓ'}{A : Set ℓ}{B : A → Set ℓ'}{a a' : A}{b : B a}{b' : B a'}
     (p : a ≡ a') → tr B p b ≡ b' → _≡_ {A = Σ A B} (a Σ, b) (a' Σ, b')
Σ,= {A = A}{B}{a}{a'}{b}{b'} p q
  = J (λ a' p → {b' : B a'} → tr B p b ≡ b' → _≡_ {A = Σ A B} (a Σ, b) (a' Σ, b')) (ap (a Σ,_)) p q

_×_ : ∀ {α β} → Set α → Set β → Set (α ⊔ β)
A × B = Σ A (λ _ → B)
infixr 5 _×_

postulate
  coe-Σ :
    ∀ α β (A : I → Set α)(B : ∀ i → A i → Set β)(p : Σ (A ₀) (B ₀))
    → coe (⟨ i ⟩ (Σ (A i) (B i))) p
    ↦ ((coe (path A) (proj₁ p)) Σ,
       coe (⟨ i ⟩ B i (coe (⟨ j ⟩ A (j ∧ i)) (proj₁ p))) (proj₂ p))
{-# REWRITE coe-Σ #-}

--------------------------------------------------------------------------------

record ⊤ : Set where
  constructor tt
open ⊤ public

data ⊥ : Set where

⊥-elim : ∀{i}{A : Set i} → ⊥ → A
⊥-elim ()

record Lift {a ℓ} (A : Set a) : Set (a ⊔ ℓ) where
  constructor lift
  field lower : A

open Lift public

postulate
  coe-Lift : ∀ α β (A₀ A₁ : Set α)(p : A₀ ≡ A₁)(l : Lift {α}{β} A₀)
             → coe (ap Lift p) l ↦ lift (coe p (lower l))
{-# REWRITE coe-Lift #-}
