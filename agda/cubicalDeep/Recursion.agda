{-# OPTIONS --rewriting #-}

{-
USAGE:
  - copy & paste everything below to a new module
  - remove postulates & add implementation
  - Rename and export stuff as needed
-}

module Recursion where

open import Agda.Primitive

open import Lib
import Syntax as S

infixl 5 _,_
infixl 7 _[_]T
infixl 5 _,s_
infix  6 _∘_
infixl 8 _[_]t

postulate
  i : Level

  j : Level

  Con : Set i

  Ty : Con → Set i

  Tm : ∀ Γ → Ty Γ → Set j

  Tms : Con → Con → Set j

  ∙ : Con

  _,_ : (Γ : Con) → Ty Γ → Con

  _[_]T : ∀{Γ Δ} → Ty Δ → Tms Γ Δ → Ty Γ

  id : ∀{Γ} → Tms Γ Γ

  _∘_ : ∀{Γ Δ Σ} → Tms Δ Σ → Tms Γ Δ → Tms Γ Σ

  ε : ∀{Γ} → Tms Γ ∙

  _,s_  : ∀{Γ Δ A}(σ : Tms Γ Δ) → Tm Γ (A [ σ ]T) → Tms Γ (Δ , A)

  π₁ : ∀{Γ Δ}{A : Ty Δ} → Tms Γ (Δ , A) →  Tms Γ Δ

  _[_]t : ∀{Γ Δ}{A : Ty Δ} → Tm Δ A → (σ : Tms Γ Δ) → Tm Γ (A [ σ ]T)

  π₂ : ∀{Γ Δ}{A : Ty Δ}(σ : Tms Γ (Δ , A)) → Tm Γ (A [ π₁ σ ]T)

  [id]T : ∀{Γ}{A : Ty Γ} → A [ id ]T ≡ A

  [][]T : ∀{Γ Δ Σ}{A : Ty Σ}{σ : Tms Γ Δ}{δ : Tms Δ Σ}
          → A [ δ ]T [ σ ]T ≡ A [ δ ∘ σ ]T

  idl : ∀{Γ Δ}{δ : Tms Γ Δ} → (id ∘ δ) ≡ δ

  idr : ∀{Γ Δ}{δ : Tms Γ Δ} → (δ ∘ id) ≡ δ

  ass : ∀{Γ Δ Σ Ω}{σ : Tms Σ Ω}{δ : Tms Δ Σ}{ν : Tms Γ Δ}
        → (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν)

  ,∘ : ∀{Γ Δ Σ}{δ : Tms Γ Δ}{σ : Tms Σ Γ}{A : Ty Δ}{a : Tm Γ (A [ δ ]T)}
        → ((_,s_ {Γ}{Δ}{A} δ a) ∘ σ) ≡
          ((δ ∘ σ) ,s tr (Tm Σ) ([][]T {Σ}{Γ}{Δ}{A}{σ}{δ}) (_[_]t {Σ}{Γ}{A [ δ ]T} a σ))

  π₁β : ∀{Γ Δ}{A : Ty Δ}{δ : Tms Γ Δ}{a : Tm Γ (A [ δ ]T)}
        → (π₁ (_,s_ {Γ}{Δ}{A} δ a)) ≡ δ

  πη : ∀{Γ Δ}{A : Ty Δ}{δ : Tms Γ (Δ , A)}
        → (π₁ δ ,s π₂ δ) ≡ δ

  εη : ∀{Γ}{σ : Tms Γ ∙}
        → σ ≡ ε

  π₂β : ∀{Γ Δ}{A : Ty Δ}{δ : Tms Γ Δ}{a : Tm Γ (A [ δ ]T)}
        → tr (λ x → Tm Γ (A [ x ]T)) (π₁β {Γ}{Δ}{A}{δ}{a}) (π₂ (_,s_ {Γ}{Δ}{A} δ a)) ≡ a

wk : ∀{Γ}{A : Ty Γ} → Tms (Γ , A) Γ
wk = π₁ id

vz : ∀{Γ}{A : Ty Γ} → Tm (Γ , A) (A [ wk ]T)
vz = π₂ id

vs : ∀{Γ}{A B : Ty Γ} → Tm Γ A → Tm (Γ , B) (A [ wk ]T)
vs x = x [ wk ]t

<_> : ∀{Γ}{A : Ty Γ} → Tm Γ A → Tms Γ (Γ , A)
< t > = id ,s tr (Tm _) ([id]T ⁻¹) t

infix 4 <_>

_^_ : {Γ Δ : Con}(σ : Tms Γ Δ)(A : Ty Δ) → Tms (Γ , A [ σ ]T) (Δ , A)
_^_ {Γ}{Δ} σ A = (σ ∘ wk) ,s tr (Tm (Γ , A [ σ ]T)) ([][]T {Γ , A [ σ ]T}{Γ}{Δ}{A}{wk{Γ}{A [ σ ]T}}{σ}) vz

infixl 5 _^_

postulate
  U : ∀{Γ} → Ty Γ

  U[] : ∀{Γ Δ}{σ : Tms Γ Δ} → (U [ σ ]T) ≡ U

  El : ∀{Γ}(a : Tm Γ U) → Ty Γ

  El[] : ∀{Γ Δ}{σ : Tms Γ Δ}{a : Tm Δ U}
       → (El a [ σ ]T) ≡ (El (tr (Tm Γ) (U[] {σ = σ}) (a [ σ ]t)))

  Π : ∀{Γ}(a : Tm Γ U)(B : Ty (Γ , El a)) → Ty Γ

  Π[] : ∀{Γ Δ}{σ : Tms Γ Δ}{a : Tm Δ U}{B : Ty (Δ , El a)}
      → (Π a B) [ σ ]T ≡
        Π (tr (Tm Γ) (U[] {σ = σ}) (a [ σ ]t))
          (tr (λ x → Ty (Γ , x)) (El[] {σ = σ}{a}) (B [ σ ^ El a ]T))

  app : ∀{Γ}{a : Tm Γ U}{B : Ty (Γ , El a)} → Tm Γ (Π a B) → Tm (Γ , El a) B

  lam : ∀{Γ}{a : Tm Γ U}{B : Ty (Γ , El a)} → Tm (Γ , El a) B → Tm Γ (Π a B)

  lam[] : ∀{Γ Δ}{σ : Tms Γ Δ}{a : Tm Δ U}{B : Ty (Δ , El a)}{t : Tm (Δ , El a) B}
    → lam t [ σ ]t ≡ tr (Tm Γ) (Π[] ⁻¹)
        (lam (coe
               (⟨ i ⟩ Tm (Γ , El[] {σ = σ}{a} $ i)
                         (coe (⟨ j ⟩ Ty (Γ , El[] {σ = σ}{a} $ * i ₀ j)) (B [ σ ^ El a ]T)))
               (t [ σ ^ El a ]t)))

  Πβ : ∀{Γ}{a : Tm Γ U}{B : Ty (Γ , El a)}{t : Tm (Γ , El a) B} → app (lam t) ≡ t

  Πη : ∀{Γ}{a : Tm Γ U}{B : Ty (Γ , El a)}{t : Tm Γ (Π a B)} → lam (app t) ≡ t

--------------------------------------------------------------------------------

postulate
  Conᴿ : S.Con → Con
  Tyᴿ  : ∀ {Γ} → S.Ty Γ → Ty (Conᴿ Γ)
  Tmᴿ  : ∀ {Γ A} → S.Tm Γ A → Tm (Conᴿ Γ) (Tyᴿ A)
  Tmsᴿ : ∀ {Γ Δ} → S.Tms Γ Δ → Tms (Conᴿ Γ) (Conᴿ Δ)

postulate
  ∙ᴿ   : Conᴿ S.∙ ↦ ∙
  ,ᴿ   : ∀ Γ A → Conᴿ (Γ S., A) ↦ Conᴿ Γ , Tyᴿ A
  []Tᴿ : ∀ Γ Δ (A : S.Ty Δ) (σ : S.Tms Γ Δ) → Tyᴿ (A S.[ σ ]T) ↦ Tyᴿ A [ Tmsᴿ σ ]T
{-# REWRITE ∙ᴿ ,ᴿ []Tᴿ #-}

postulate
  idᴿ  : ∀ {Γ} → Tmsᴿ (S.id {Γ}) ↦ id
  ∘ᴿ   : ∀ {Γ Δ Σ}(σ : S.Tms Δ Σ)(δ : S.Tms Γ Δ) → Tmsᴿ (σ S.∘ δ) ↦ Tmsᴿ σ ∘ Tmsᴿ δ
  ,sᴿ  : ∀ {Γ Δ}(σ : S.Tms Γ Δ){A : S.Ty Δ}(t : S.Tm Γ (A S.[ σ ]T)) → Tmsᴿ (σ S.,s t) ↦ Tmsᴿ σ ,s Tmᴿ t
  εᴿ   : ∀ {Γ} → Tmsᴿ (S.ε {Γ}) ↦ ε
  π₁ᴿ  : ∀ {Γ Δ}{A : S.Ty Δ}(σ : S.Tms Γ (Δ S., A)) → Tmsᴿ (S.π₁ σ) ↦ π₁ (Tmsᴿ σ)
{-# REWRITE idᴿ ∘ᴿ ,sᴿ εᴿ π₁ᴿ #-}

postulate
  []tᴿ   : ∀{Γ Δ}{A : S.Ty Δ}(t : S.Tm Δ A)(σ : S.Tms Γ Δ) → Tmᴿ (t S.[ σ ]t) ↦ Tmᴿ t [ Tmsᴿ σ ]t
  π₂ᴿ    : ∀{Γ Δ}{A : S.Ty Δ}(σ : S.Tms Γ (Δ S., A)) → Tmᴿ (S.π₂ σ) ↦ π₂ (Tmsᴿ σ)
  Uᴿ     : ∀ {Γ} → Tyᴿ (S.U {Γ}) ↦ U
{-# REWRITE []tᴿ π₂ᴿ Uᴿ #-}

postulate
  Elᴿ : ∀ {Γ}{a : S.Tm Γ S.U} → Tyᴿ (S.El a) ↦ El (Tmᴿ a)
{-# REWRITE Elᴿ #-}

postulate
  Πᴿ : ∀ {Γ}(a : S.Tm Γ S.U)(B : S.Ty (Γ S., S.El a)) → Tyᴿ (S.Π a B) ↦ Π (Tmᴿ a) (Tyᴿ B)
{-# REWRITE Πᴿ #-}

postulate
  appᴿ : ∀ {Γ}{a : S.Tm Γ S.U}{B : S.Ty (Γ S., S.El a)}(t : S.Tm Γ (S.Π a B))
         → Tmᴿ (S.app t) ↦ app (Tmᴿ t)
  lamᴿ : ∀ {Γ}{a : S.Tm Γ S.U}{B : S.Ty (Γ S., S.El a)}(t : S.Tm (Γ S., S.El a) B)
         → Tmᴿ (S.lam t) ↦ lam (Tmᴿ t)
{-# REWRITE appᴿ lamᴿ #-}

--------------------------------------------------------------------------------

postulate
  U[]ᴿ :
    (Γ Δ : S.Con)(σ : S.Tms Γ Δ)(i : I)
    → Tyᴿ (S.U[]{Γ}{Δ} σ $ i) ↦ U[] {Conᴿ Γ}{Conᴿ Δ}{Tmsᴿ σ} $ i

  El[]ᴿ :
    (Γ Δ : S.Con)(σ : S.Tms Γ Δ)(a : S.Tm Δ S.U) (i : I)
    → Tyᴿ (S.El[] σ {a = a} $ i) ↦ El[] {σ = Tmsᴿ σ}{a = Tmᴿ a} $ i

-- postulate
--   [id]Tᴿ : {!S.[id]T!}

  -- [id]T : ∀{Γ}{A : Ty Γ} → A [ id ]T ≡ A

  -- [][]T : ∀{Γ Δ Σ}{A : Ty Σ}{σ : Tms Γ Δ}{δ : Tms Δ Σ}
  --         → A [ δ ]T [ σ ]T ≡ A [ δ ∘ σ ]T

  -- idl : ∀{Γ Δ}{δ : Tms Γ Δ} → (id ∘ δ) ≡ δ

  -- idr : ∀{Γ Δ}{δ : Tms Γ Δ} → (δ ∘ id) ≡ δ

  -- ass : ∀{Γ Δ Σ Ω}{σ : Tms Σ Ω}{δ : Tms Δ Σ}{ν : Tms Γ Δ}
  --       → (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν)

--------------------------------------------------------------------------------

postulate
  coe-Tm :
    (Γ : I → S.Con)(A : ∀ i → S.Ty (Γ i))
    (t : S.Tm (Γ ₀) (A ₀))
    → Tmᴿ (coe (⟨ i ⟩ S.Tm (Γ i) (A i)) t) ↦ coe (⟨ i ⟩ Tm (Conᴿ (Γ i)) (Tyᴿ (A i))) (Tmᴿ t)

  coe-Ty :
    (Γ : I → S.Con)
    (A : S.Ty (Γ ₀))
    → Tyᴿ (coe (⟨ i ⟩ S.Ty (Γ i)) A) ↦ coe (⟨ i ⟩ Ty (Conᴿ (Γ i))) (Tyᴿ A)

  coe-Tms :
    (Γ Δ : I → S.Con)
    (σ : S.Tms (Γ ₀) (Δ ₀))
    → Tmsᴿ (coe (⟨ i ⟩ S.Tms (Γ i) (Δ i)) σ) ↦
      coe (⟨ i ⟩ Tms (Conᴿ (Γ i)) (Conᴿ (Δ i))) (Tmsᴿ σ)

{-# REWRITE coe-Tm coe-Ty coe-Tms #-}
