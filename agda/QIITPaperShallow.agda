{-# OPTIONS --without-K #-}

{-
Shallow formalization for the paper "Codes for Quotient Inductive Inductive Types"
  * In each case, we abstract over inductive hypotheses (inductive _ᶜ, _ᴹ, _ᴱ results).
    Since all of these results are in the metatheory, we do not consider the syntax of
    the theory of codes here at all, and _ᶜ is implicit.
  * Dependency on Γ contexts is also implicit.
-}

-- Metatheory
--------------------------------------------------------------------------------

open import Relation.Binary.PropositionalEquality
open import Data.Product

J : ∀ {α β}(A : Set α)(a : A)(B : ∀ b → a ≡ b → Set β) → B a refl → ∀ {b} p → B b p
J _ _ B pr refl = pr

_◾_ = trans
infixr 5 _◾_
ap = cong
_⁻¹ = sym

happly : ∀ {A : Set}{B : A → Set}{f g : ∀ a → B a} → f ≡ g → ∀ a → f a ≡ g a
happly p a = ap (λ f → f a) p

isEquiv : {A B : Set}(f : A → B) → Set
isEquiv {A}{B} f = ∃₂ λ (g h : B → A) → (∀ b → f (g b) ≡ b) × (∀ b → g (f b) ≡ b)

-- strong function extensionality (needed for small inductive βᴱ, ηᴱ)
postulate
  funexts : ∀ {A B} f g → isEquiv (happly {A}{B}{f}{g})

funext : {A : Set}{B : A → Set}{f g : ∀ a → B a} → (∀ a → f a ≡ g a) → f ≡ g
funext {A}{B}{f}{g} = proj₁ (funexts f g)

inv⁻¹ : ∀ {A : Set}{t u : A}(eq : t ≡ u) → (eq ⁻¹ ◾ eq) ≡ refl
inv⁻¹ eq = J _ _ (λ u eq → (eq ⁻¹ ◾ eq) ≡ refl) refl eq

transp : ∀ {α β}{A : Set α}{a b : A}(B : A → Set β) → a ≡ b → B a → B b
transp B refl ba = ba

apd : ∀ {α β}{A : Set α}{B : A → Set β}(f : ∀ a → B a){x y : A}(p : x ≡ y) → transp B p (f x) ≡ f y
apd {B = B} f {x} {y} p = J _ x (λ y p → transp B p (f x) ≡ f y) refl p

Π : (A : Set)(B : A → Set) → Set
Π A B = (x : A) → B x

Πₙᵢ  = Π
Πₙᵢₛ = Π

--------------------------------------------------------------------------------
-- Induction methods
--------------------------------------------------------------------------------

Setᴹ : Set → Set₁
Setᴹ A = A → Set

Uᴹ : Set → Set₁
Uᴹ A = A → Set

Elᴹ : ∀{a}(aᴹ : Uᴹ a) → Setᴹ a -- Elᴹ is left implicit from now on
Elᴹ aᴹ x = aᴹ x

-- inductive functions

Πᴹ : ∀ {a}(aᴹ : Uᴹ a){B : a → Set}(Bᴹ : ∀ {x} (xₘ : aᴹ x) → Setᴹ (B x)) → Setᴹ (Π a B)
Πᴹ {a} aᴹ {B} Bᴹ f = (x : a)(xₘ : aᴹ x) → Bᴹ xₘ (f x)

appᴹ :
  ∀ {a}(aᴹ : Uᴹ a){B : a → Set}(Bᴹ : ∀ {x}(xₘ : aᴹ x) → Setᴹ (B x))
    {t : Π a B}(tᴹ : Πᴹ aᴹ Bᴹ t){u : a} (uᴹ : aᴹ u)
  → Bᴹ uᴹ (t u)
appᴹ {a} aᴹ {B} Bᴹ {t} tᴹ {u} uᴹ = tᴹ u uᴹ

lamᴹ :
  ∀ {a}(aᴹ : Uᴹ a){B : a → Set}(Bᴹ : ∀ {x}(xₘ : aᴹ x) → Setᴹ (B x))
    {t : Π a B}(tᴹ : ∀ {x}(xₘ : aᴹ x) → Bᴹ xₘ (t x))
    → Πᴹ aᴹ Bᴹ t
lamᴹ {a} aᴹ {B} Bᴹ {t} tᴹ = λ x xₘ → tᴹ xₘ

Πβᴹ :
  ∀{a}(aᴹ : Uᴹ a){B : a → Set}(Bᴹ : ∀ {x}(xₘ :  aᴹ x) → Setᴹ (B x))
  {t : Π a B}(tᴹ : ∀ {x}(xₘ :  aᴹ x) → Bᴹ xₘ (t x))
  {u : a} (uᴹ : aᴹ u)
  → appᴹ aᴹ Bᴹ (lamᴹ aᴹ Bᴹ tᴹ) uᴹ ≡ tᴹ uᴹ
Πβᴹ _ _ _ _ = refl

Πηᴹ :
  ∀ {a}(aᴹ : Uᴹ a){B : a → Set}(Bᴹ : ∀ {x}(xₘ :  aᴹ x) → Setᴹ (B x))
    {t : Π a B}(tᴹ : Πᴹ aᴹ Bᴹ t)
  → lamᴹ aᴹ Bᴹ (appᴹ aᴹ Bᴹ tᴹ) ≡ tᴹ
Πηᴹ _ _ _ = refl

-- non-inductive functions

Πₙᵢᴹ : (A : Set){B : A → Set}(Bᴹ : ∀ a → Setᴹ (B a)) → Setᴹ (Π A B)
Πₙᵢᴹ A {B} Bᴹ f = ∀ a → Bᴹ a (f a)

appₙᵢᴹ :
  ∀ (A : Set)
    {B : A → Set}  (Bᴹ : ∀ a → Setᴹ (B a))
    {t : Πₙᵢ A B}  (tᴹ : Πₙᵢᴹ A Bᴹ t)
    (u : A)
  → Bᴹ u (t u)
appₙᵢᴹ A {B} Bᴹ {t} tᴹ u = tᴹ u

lamₙᵢᴹ :
  ∀ (A : Set)
    {B : A → Set} (Bᴹ : ∀ a → Setᴹ (B a))
    {t : Πₙᵢ A B}(tᴹ : ∀ a → Bᴹ a (t a))
  → Πₙᵢᴹ A Bᴹ t
lamₙᵢᴹ A {B} Bᴹ {t} tᴹ = λ a → tᴹ a

Πₙᵢβᴹ :
  ∀ {A : Set}
    {B : A → Set}  (Bᴹ : ∀ a → Setᴹ (B a))
    {t : Πₙᵢ A B}(tᴹ : ∀ a → Bᴹ a (t a))
    (u : A)
  → appₙᵢᴹ A Bᴹ (lamₙᵢᴹ A Bᴹ tᴹ) u ≡ tᴹ u
Πₙᵢβᴹ _ _ _  = refl

Πₙᵢηᴹ :
  ∀ (A : Set)
    {B : A → Set}  (Bᴹ : ∀ a → Setᴹ (B a))
    {t : Πₙᵢ A B}  (tᴹ : Πₙᵢᴹ A Bᴹ t)
  → lamₙᵢᴹ A Bᴹ (appₙᵢᴹ A Bᴹ tᴹ) ≡ tᴹ
Πₙᵢηᴹ _ _ _ = refl

-- small non-inductive functions (exactly the same as non-inductive for ᴹ)

Πₙᵢₛᴹ : (A : Set){b : A → Set}(Bᴹ : ∀ a → Uᴹ (b a)) → Uᴹ (Πₙᵢₛ A b)
Πₙᵢₛᴹ A {B} bᴹ = λ f → ∀ a → bᴹ a (f a)

appₙᵢₛᴹ :
  ∀ (A : Set)
    {b : A → Set}  (bᴹ : ∀ a → Uᴹ (b a))
    {t : Πₙᵢₛ A b} (tᴹ : Πₙᵢₛᴹ A bᴹ t)
    (u : A)
  → bᴹ u (t u)
appₙᵢₛᴹ A {B} _ {t} tᴹ u = tᴹ u

lamₙᵢₛᴹ :
  ∀ (A : Set)
    {b : A → Set} (bᴹ : ∀ a → Uᴹ (b a))
    {t : Πₙᵢₛ A b}(tᴹ : ∀ a → bᴹ a (t a))
  → Πₙᵢₛᴹ A bᴹ t
lamₙᵢₛᴹ A {B} _ {t} tᴹ = λ a → tᴹ a

Πₙᵢₛβᴹ :
  ∀ {A : Set}
    {b : A → Set}  (bᴹ : ∀ a → Uᴹ (b a))
    {t : Πₙᵢₛ A b}(tᴹ : ∀ a → bᴹ a (t a))
    (u : A)
  → appₙᵢₛᴹ A bᴹ (lamₙᵢₛᴹ A bᴹ tᴹ) u ≡ tᴹ u
Πₙᵢₛβᴹ _ _ _  = refl

Πₙᵢₛηᴹ :
  ∀ (A : Set)
    {b : A → Set}   (bᴹ : ∀ a → Setᴹ (b a))
    {t : Πₙᵢₛ A b}  (tᴹ : Πₙᵢₛᴹ A bᴹ t)
  → lamₙᵢₛᴹ A bᴹ (appₙᵢₛᴹ A bᴹ tᴹ) ≡ tᴹ
Πₙᵢₛηᴹ _ _ _ = refl


-- Identity

≡ᴹ :
  {a : Set}    (aᴹ : Uᴹ a)
  {t : a}      (tᴹ :  aᴹ t)
  {u : a}      (uᴹ :  aᴹ u)
  → Uᴹ (t ≡ u)
≡ᴹ {a} aᴹ {t} tᴹ {u} uᴹ = λ z → transp aᴹ z tᴹ ≡ uᴹ

reflᴹ :
  {a : Set}  (aᴹ : Uᴹ a)
  {t : a}    (tᴹ :  aᴹ t)
  → (≡ᴹ aᴹ tᴹ tᴹ) refl
reflᴹ {a} aᴹ {t} tᴹ = refl

Jᴹ :
  {a  : Set}                  (aᴹ  : Uᴹ a)
  {t  : a}                    (tᴹ  : aᴹ t)
  {P  : ∀ x (z : t ≡ x) → Set}(Pᴹ  : ∀ {x} (xₘ : aᴹ x) {z} zₘ → Setᴹ (P x z))
  {pr : P t refl}             (prᴹ : Pᴹ tᴹ (reflᴹ aᴹ tᴹ) pr)
  {u  : a}                    (uᴹ  : aᴹ u)
  {eq : t ≡ u}                (eqᴹ : (≡ᴹ aᴹ tᴹ uᴹ) eq)
  → Pᴹ uᴹ eqᴹ (J a t P pr eq)
Jᴹ {a} aᴹ {t} tᴹ {P} Pᴹ {pr} prᴹ {u} uᴹ {eq} eqᴹ =
  J (aᴹ u) (transp aᴹ eq tᴹ)
     (λ xᴹ zᴹ → Pᴹ {u} xᴹ {eq} zᴹ (J a t P pr eq))
     (J a t
        (λ x z → Pᴹ {x} (transp aᴹ z tᴹ) {z} refl (J a t P pr z))
        prᴹ eq)
     eqᴹ

≡βᴹ :
  {a  : Set}                   (aᴹ  : Uᴹ a)
  {t  : a}                     (tᴹ  : aᴹ t)
  {P  : ∀ x (z : t ≡ x) → Set} (Pᴹ  : ∀ {x} (xₘ :  aᴹ x) {z} zₘ → Setᴹ (P x z))
  {pr : P t refl}              (prᴹ : Pᴹ tᴹ (reflᴹ aᴹ tᴹ) pr)
  → Jᴹ aᴹ tᴹ Pᴹ prᴹ tᴹ (reflᴹ aᴹ tᴹ) ≡ prᴹ
≡βᴹ _ _ _ _ = refl


-- Elimination
--------------------------------------------------------------------------------

Setᴱ : {A : Set}(Aᴹ : Setᴹ A) → Set₁
Setᴱ {A} Aᴹ = (x : A) → Aᴹ x → Set

Uᴱ : (A : Set) → Setᴹ A → Set
Uᴱ a aₘ = (x : a) → aₘ x

Elᴱ : {a : Set}(aᴹ : Uᴹ a)(aᴱ : Uᴱ a aᴹ) → Setᴱ ( aᴹ)
Elᴱ {a} aᴹ aᴱ x xₘ = aᴱ x ≡ xₘ

-- inductive functions

Πᴱ : {a : Set}    (aᴹ : Uᴹ a)                          (aᴱ : Uᴱ a aᴹ)
     {B : a → Set}(Bᴹ : ∀ {x}(xₘ :  aᴹ x) → Setᴹ (B x))(Bᴱ : ∀ {x} xₘ (xₑ : Elᴱ aᴹ aᴱ x xₘ) → Setᴱ (Bᴹ xₘ))
     → Setᴱ (Πᴹ aᴹ Bᴹ)
Πᴱ {a} aᴹ aᴱ {B} Bᴹ Bᴱ f fₘ = (x : a) → Bᴱ {x} (aᴱ x) refl (f x) (fₘ x (aᴱ x))

appᴱ :
  {a : Set}    (aᴹ : Uᴹ a)                          (aᴱ : Uᴱ a aᴹ)
  {B : a → Set}(Bᴹ : ∀ {x}(xₘ :  aᴹ x) → Setᴹ (B x))(Bᴱ : ∀ {x} xₘ (xₑ : Elᴱ aᴹ aᴱ x xₘ) → Setᴱ (Bᴹ xₘ))
  {t : Π a B}  (tᴹ : Πᴹ aᴹ Bᴹ t)                    (tᴱ : Πᴱ aᴹ aᴱ Bᴹ Bᴱ t tᴹ)
  {u : a}      (uᴹ :  aᴹ u)                         (uᴱ : Elᴱ aᴹ aᴱ u uᴹ)
  → Bᴱ uᴹ uᴱ (t u) (tᴹ u uᴹ)
appᴱ {a} aᴹ aᴱ {B} Bᴹ Bᴱ {t} tᴹ tᴱ {u} uᴹ uᴱ =
  J (aᴹ u) (aᴱ u) (λ xᴹ xᴱ → Bᴱ {u} xᴹ xᴱ (t u) (tᴹ u xᴹ)) (tᴱ u) {uᴹ} uᴱ

lamᴱ :
  {a : Set}      (aᴹ : Uᴹ a)                           (aᴱ : Uᴱ a aᴹ)
  {B : a → Set}  (Bᴹ : ∀ {x}(xₘ :  aᴹ x) → Setᴹ (B x)) (Bᴱ : ∀ {x} xₘ (xₑ : Elᴱ aᴹ aᴱ x xₘ) → Setᴱ (Bᴹ xₘ))
  {t : ∀ x → B x}(tᴹ : ∀ {x}(xₘ :  aᴹ x) → Bᴹ xₘ (t x))(tᴱ : ∀ {x} xₘ (xₑ : Elᴱ aᴹ aᴱ x xₘ) → Bᴱ xₘ xₑ (t x) (tᴹ xₘ))
  → Πᴱ aᴹ aᴱ Bᴹ Bᴱ t (λ x xₘ → tᴹ xₘ)
lamᴱ {a} aᴹ aᴱ {B} Bᴹ Bᴱ {t} tᴹ tᴱ = λ x → tᴱ (aᴱ x) refl

Πβᴱ :
  {a : Set}       (aᴹ : Uᴹ a)                           (aᴱ : Uᴱ a aᴹ)
  {B : a → Set}   (Bᴹ : ∀ {x}(xₘ :  aᴹ x) → Setᴹ (B x)) (Bᴱ : ∀ {x} xₘ (xₑ : Elᴱ aᴹ aᴱ x xₘ) → Setᴱ (Bᴹ xₘ))
  {t : ∀ x → B x} (tᴹ : ∀ {x}(xₘ :  aᴹ x) → Bᴹ xₘ (t x))(tᴱ : ∀ {x} xₘ (xₑ : Elᴱ aᴹ aᴱ x xₘ) → Bᴱ xₘ xₑ (t x) (tᴹ xₘ))
  {u : a}         (uᴹ :  aᴹ u)                          (uᴱ : Elᴱ aᴹ aᴱ u uᴹ)
  → appᴱ aᴹ aᴱ Bᴹ Bᴱ (lamᴹ aᴹ Bᴹ tᴹ) (lamᴱ aᴹ aᴱ Bᴹ Bᴱ tᴹ tᴱ) uᴹ uᴱ ≡ tᴱ uᴹ uᴱ
Πβᴱ aᴹ aᴱ Bᴹ Bᴱ {t} tᴹ tᴱ {u} uᴹ uᴱ =
  J _ (aᴱ u)
     (λ uᴹ uᴱ → appᴱ aᴹ aᴱ Bᴹ Bᴱ (lamᴹ aᴹ Bᴹ tᴹ) (lamᴱ aᴹ aᴱ Bᴹ Bᴱ tᴹ tᴱ) uᴹ uᴱ ≡ tᴱ uᴹ uᴱ)
     refl uᴱ

Πηᴱ :
  {a : Set}    (aᴹ : Uᴹ a)                          (aᴱ : Uᴱ a aᴹ)
  {B : a → Set}(Bᴹ : ∀ {x}(xₘ :  aᴹ x) → Setᴹ (B x))(Bᴱ : ∀ {x} xₘ (xₑ : Elᴱ aᴹ aᴱ x xₘ) → Setᴱ (Bᴹ xₘ))
  {t : Π a B}  (tᴹ : Πᴹ aᴹ Bᴹ t)                    (tᴱ : Πᴱ aᴹ aᴱ Bᴹ Bᴱ t tᴹ)
  → lamᴱ aᴹ aᴱ Bᴹ Bᴱ (appᴹ aᴹ Bᴹ tᴹ) (appᴱ aᴹ aᴱ Bᴹ Bᴱ tᴹ tᴱ) ≡ tᴱ
Πηᴱ _ _ _ _ _ _ = refl

-- non-inductive functions

Πₙᵢᴱ :
  (A : Set)
  {B : A → Set}   (Bᴹ : ∀ a → Setᴹ (B a))   (Bᴱ : ∀ a → Setᴱ (Bᴹ a))
  → Setᴱ (Πₙᵢᴹ A Bᴹ)
Πₙᵢᴱ A {B} Bᴹ Bᴱ f fᴹ = ∀ a → Bᴱ a (f a) (fᴹ a)

appₙᵢᴱ :
  ∀ (A : Set)
    {B : A → Set}  (Bᴹ : ∀ a → Setᴹ (B a)) (Bᴱ : ∀ a → Setᴱ (Bᴹ a))
    {t : Πₙᵢ A B}  (tᴹ : Πₙᵢᴹ A Bᴹ t)      (tᴱ : Πₙᵢᴱ A Bᴹ Bᴱ t tᴹ)
    (a : A)
  → Bᴱ a (t a) (tᴹ a)
appₙᵢᴱ A {B} Bᴹ Bᴱ {t} tᴹ tᴱ a = tᴱ a

lamₙᵢᴱ :
  ∀ (A : Set)
    {B : A → Set} (Bᴹ : ∀ a → Setᴹ (B a)) (Bᴱ : ∀ a → Setᴱ (Bᴹ a))
    {t : Πₙᵢ A B} (tᴹ : ∀ a → Bᴹ a (t a)) (tᴱ : ∀ a → Bᴱ a (t a) (tᴹ a))
  → Πₙᵢᴱ A Bᴹ Bᴱ t tᴹ
lamₙᵢᴱ A {B} Bᴹ Bᴱ {t} tᴹ tᴱ = λ a → tᴱ a

Πₙᵢᴱβ :
  ∀ (A : Set)
    {B : A → Set}  (Bᴹ : ∀ a → Setᴹ (B a)) (Bᴱ : ∀ a → Setᴱ (Bᴹ a))
    {t : Πₙᵢ A B} (tᴹ : ∀ a → Bᴹ a (t a)) (tᴱ : ∀ a → Bᴱ a (t a) (tᴹ a))
    (a : A)
  → appₙᵢᴱ A Bᴹ Bᴱ (lamₙᵢᴹ A Bᴹ tᴹ) (lamₙᵢᴱ A Bᴹ Bᴱ tᴹ tᴱ) a ≡ tᴱ a
Πₙᵢᴱβ A {B} Bᴹ Bᴱ {t} tᴹ tᴱ a = refl

Πₙᵢηᴱ :
  ∀ (A : Set)
    {B : A → Set}  (Bᴹ : ∀ a → Setᴹ (B a)) (Bᴱ : ∀ a → Setᴱ (Bᴹ a))
    {t : Πₙᵢ A B}  (tᴹ : Πₙᵢᴹ A Bᴹ t)      (tᴱ : Πₙᵢᴱ A Bᴹ Bᴱ t tᴹ)
  → lamₙᵢᴱ A Bᴹ Bᴱ (appₙᵢᴹ A Bᴹ tᴹ) (appₙᵢᴱ A Bᴹ Bᴱ tᴹ tᴱ) ≡ tᴱ
Πₙᵢηᴱ _ _ _ _ _ = refl

-- small non-inductive functions
Πₙᵢₛᴱ :
  (A : Set)
  {b : A → Set} (bᴹ : ∀ a → Uᴹ (b a)) (bᴱ : ∀ a → Uᴱ (b a) (bᴹ a))
  → Uᴱ (Πₙᵢₛ A b) (Πₙᵢₛᴹ A bᴹ)
Πₙᵢₛᴱ A {b} bᴹ bᴱ = λ f a → bᴱ a (f a)

appₙᵢₛᴱ :
  (A : Set)
  {b : A → Set}  (bᴹ : ∀ a → Uᴹ (b a)) (bᴱ : ∀ a → Uᴱ (b a) (bᴹ a))
  {t : Πₙᵢₛ A b} (tᴹ : Πₙᵢₛᴹ A bᴹ t)   (tᴱ : Elᴱ (Πₙᵢₛᴹ A bᴹ) (Πₙᵢₛᴱ A bᴹ bᴱ) t tᴹ)
  (a : A)
  → Elᴱ (bᴹ a) (bᴱ a) (t a) (tᴹ a)
appₙᵢₛᴱ A {b} bᴹ bᴱ {t} tᴹ tᴱ a = ap (λ f → f a) tᴱ

lamₙᵢₛᴱ :
  ∀ (A : Set)
    {b : A → Set} (bᴹ : ∀ a → Uᴹ (b a))   (bᴱ : ∀ a → Uᴱ (b a) (bᴹ a))
    {t : Πₙᵢₛ A b}(tᴹ : ∀ a → bᴹ a (t a)) (tᴱ : ∀ a → Elᴱ (bᴹ a) (bᴱ a) (t a) (tᴹ a))
  → Elᴱ (Πₙᵢₛᴹ A bᴹ) (Πₙᵢₛᴱ A bᴹ bᴱ) t tᴹ
lamₙᵢₛᴱ A {b} bᴹ bᴱ {t} tᴹ tᴱ = funext (λ a → tᴱ a)

Πₙᵢₛβᴱ :
  ∀ (A : Set)
    {b : A → Set} (bᴹ : ∀ a → Uᴹ (b a))   (bᴱ : ∀ a → Uᴱ (b a) (bᴹ a))
    {t : Πₙᵢₛ A b}(tᴹ : ∀ a → bᴹ a (t a)) (tᴱ : ∀ a → Elᴱ (bᴹ a) (bᴱ a) (t a) (tᴹ a))
    (u : A)
  → appₙᵢₛᴱ A bᴹ bᴱ (lamₙᵢₛᴹ A bᴹ tᴹ) (lamₙᵢₛᴱ A bᴹ bᴱ tᴹ tᴱ) u ≡ tᴱ u
Πₙᵢₛβᴱ A {b} bᴹ bᴱ {t} tᴹ tᴱ u =
  let (p , q , r , s) = funexts (λ a → bᴱ a (t a)) tᴹ
  in happly (r tᴱ) u

Πₙᵢₛηᴱ :
  (A : Set)
  {b : A → Set}  (bᴹ : ∀ a → Uᴹ (b a)) (bᴱ : ∀ a → Uᴱ (b a) (bᴹ a))
  {t : Πₙᵢₛ A b} (tᴹ : Πₙᵢₛᴹ A bᴹ t)   (tᴱ : Elᴱ (Πₙᵢₛᴹ A bᴹ) (Πₙᵢₛᴱ A bᴹ bᴱ) t tᴹ)
  → lamₙᵢₛᴱ A bᴹ bᴱ (appₙᵢₛᴹ A bᴹ tᴹ) (appₙᵢₛᴱ A bᴹ bᴱ tᴹ tᴱ) ≡ tᴱ
Πₙᵢₛηᴱ A {b} bᴹ bᴱ {t} tᴹ tᴱ =
  let (p , q , r , s) = funexts (λ a → bᴱ a (t a)) tᴹ
  in s tᴱ

-- Identity

≡ᴱ :
  {a : Set}(aᴹ : Uᴹ a)    (aᴱ : Uᴱ a aᴹ)
  {t : a}  (tᴹ : Elᴹ aᴹ t)(tᴱ : Elᴱ aᴹ aᴱ t tᴹ)
  {u : a}  (uᴹ : Elᴹ aᴹ u)(uᴱ : Elᴱ aᴹ aᴱ u uᴹ)
  → Uᴱ (t ≡ u) (≡ᴹ aᴹ tᴹ uᴹ)
≡ᴱ {a} aᴹ aᴱ {t} tᴹ tᴱ {u} uᴹ uᴱ =
  λ e → transp (λ xᴹ → transp aᴹ e xᴹ ≡ uᴹ) tᴱ (transp (λ yᴹ → transp aᴹ e (aᴱ t) ≡ yᴹ) uᴱ (apd aᴱ e))

reflᴱ :
  {a : Set}(aᴹ : Uᴹ a)    (aᴱ : Uᴱ a aᴹ)
  {t : a}  (tᴹ : Elᴹ aᴹ t)(tᴱ : Elᴱ aᴹ aᴱ t tᴹ)
  → Elᴱ (≡ᴹ aᴹ tᴹ tᴹ) (≡ᴱ aᴹ aᴱ tᴹ tᴱ tᴹ tᴱ) refl (reflᴹ aᴹ tᴹ)
reflᴱ {a} aᴹ aᴱ {t} tᴹ tᴱ
  = J (aᴹ t)
       (aᴱ t)
       (λ xᴹ xᴱ →
         transp (λ yᴹ → ≡ᴹ aᴹ yᴹ xᴹ refl) xᴱ
         (transp (λ yᴹ → ≡ᴹ aᴹ (aᴱ t) yᴹ refl) xᴱ refl) ≡ reflᴹ aᴹ xᴹ)
       refl
       {tᴹ} tᴱ

Jᴱ :
  {a : Set}(aᴹ  : Uᴹ a) (aᴱ  : Uᴱ a aᴹ)
  {t : a}  (tᴹ  : aᴹ t) (tᴱ  : Elᴱ aᴹ aᴱ t tᴹ)

  {P : (x : a)(z : t ≡ x) → Set}
    (Pᴹ : ∀{x}(xᴹ : aᴹ x){z}(zᴹ : (≡ᴹ aᴹ tᴹ xᴹ) z) → Setᴹ (P x z))
      (Pᴱ : ∀{x} xᴹ (xᴱ : Elᴱ aᴹ aᴱ x xᴹ){z} zᴹ (zᴱ : Elᴱ (≡ᴹ aᴹ tᴹ xᴹ) (≡ᴱ aᴹ aᴱ tᴹ tᴱ xᴹ xᴱ) z zᴹ)
            → Setᴱ {P x z} (Pᴹ xᴹ zᴹ))

  {pr : P t refl}
    (prᴹ : Pᴹ tᴹ (reflᴹ aᴹ tᴹ) pr)
      (prᴱ : Pᴱ tᴹ tᴱ (reflᴹ aᴹ tᴹ) (reflᴱ aᴹ aᴱ tᴹ tᴱ) pr prᴹ)

  {u : a}(uᴹ : aᴹ u)(uᴱ : Elᴱ aᴹ aᴱ u uᴹ)

  {eq : t ≡ u}
    (eqᴹ : (≡ᴹ aᴹ tᴹ uᴹ) eq)
      (eqᴱ : Elᴱ (≡ᴹ aᴹ tᴹ uᴹ) (≡ᴱ aᴹ aᴱ tᴹ tᴱ uᴹ uᴱ) eq eqᴹ)

  → Pᴱ uᴹ uᴱ eqᴹ eqᴱ (J a t P pr eq) (Jᴹ aᴹ tᴹ Pᴹ prᴹ uᴹ eqᴹ)
Jᴱ {a} aᴹ aᴱ {t} tᴹ tᴱ {P} Pᴹ Pᴱ {pr} prᴹ prᴱ {u} uᴹ uᴱ {eq} eqᴹ eqᴱ =
  J (transp aᴹ eq tᴹ ≡ uᴹ)
    (≡ᴱ aᴹ aᴱ tᴹ tᴱ uᴹ uᴱ eq)
    (λ eqᴹ eqᴱ → Pᴱ uᴹ uᴱ eqᴹ eqᴱ (J a t P pr eq) (Jᴹ aᴹ tᴹ Pᴹ prᴹ uᴹ eqᴹ))
    (J (aᴹ u) (aᴱ u)
       (λ uᴹ uᴱ → Pᴱ uᴹ uᴱ (≡ᴱ aᴹ aᴱ tᴹ tᴱ uᴹ uᴱ eq) (refl {x = ≡ᴱ aᴹ aᴱ tᴹ tᴱ uᴹ uᴱ eq}) (J a t P pr eq)
                     (Jᴹ aᴹ tᴹ Pᴹ prᴹ uᴹ (≡ᴱ aᴹ aᴱ tᴹ tᴱ uᴹ uᴱ eq)))
       (J a t
          (λ u eq → Pᴱ (aᴱ u) refl (≡ᴱ aᴹ aᴱ tᴹ tᴱ (aᴱ u) refl eq) refl (J a t P pr eq)
                       (Jᴹ aᴹ tᴹ Pᴹ prᴹ (aᴱ u) (≡ᴱ aᴹ aᴱ tᴹ tᴱ (aᴱ u) refl eq)))
          (J _ (aᴱ t) (λ tᴹ tᴱ →
                (Pᴹ  : {x : a} (xᴹ :  aᴹ x) {z : t ≡ x} →
                       (≡ᴹ aᴹ tᴹ xᴹ) z → Setᴹ (P x z))
                (Pᴱ  : {x : a} (xᴹ : aᴹ x) (xᴱ : Elᴱ aᴹ aᴱ x xᴹ)
                      {z : t ≡ x} (zᴹ : ≡ᴹ aᴹ tᴹ xᴹ z) →
                      Elᴱ (≡ᴹ aᴹ tᴹ xᴹ) (≡ᴱ aᴹ aᴱ tᴹ tᴱ xᴹ xᴱ) z zᴹ → Setᴱ (Pᴹ xᴹ zᴹ))
                (prᴹ : Pᴹ tᴹ (reflᴹ aᴹ tᴹ) pr)
                (prᴱ : Pᴱ tᴹ tᴱ (reflᴹ aᴹ tᴹ) (reflᴱ aᴹ aᴱ tᴹ tᴱ) pr prᴹ)
                →
                  Pᴱ (aᴱ t) refl (≡ᴱ aᴹ aᴱ tᴹ tᴱ (aᴱ t) refl refl)
                   refl pr (Jᴹ aᴹ tᴹ Pᴹ prᴹ (aᴱ t) (≡ᴱ aᴹ aᴱ tᴹ tᴱ (aᴱ t) refl refl)))
                (λ Pᴹ Pᴱ prᴹ prᴱ → prᴱ)
                tᴱ Pᴹ Pᴱ prᴹ prᴱ)
          eq)
       uᴱ)
    eqᴱ

Jᴱβ :
  {a : Set} (aᴹ  : Uᴹ a)  (aᴱ  : Uᴱ a aᴹ)
  {t : a}   (tᴹ  :  aᴹ t) (tᴱ  : Elᴱ aᴹ aᴱ t tᴹ)

  {P : (x : a)(z : t ≡ x) → Set}
    (Pᴹ : ∀{x}(xᴹ : aᴹ x){z}(zᴹ : (≡ᴹ aᴹ tᴹ xᴹ) z) → Setᴹ (P x z))
      (Pᴱ : ∀{x} xᴹ (xᴱ : Elᴱ aᴹ aᴱ x xᴹ){z} zᴹ (zᴱ : Elᴱ (≡ᴹ aᴹ tᴹ xᴹ) (≡ᴱ aᴹ aᴱ tᴹ tᴱ xᴹ xᴱ) z zᴹ)
            → Setᴱ {P x z} (Pᴹ xᴹ zᴹ))

  {pr : P t refl}
    (prᴹ : Pᴹ tᴹ (reflᴹ aᴹ tᴹ) pr)
      (prᴱ : Pᴱ tᴹ tᴱ (reflᴹ aᴹ tᴹ) (reflᴱ aᴹ aᴱ tᴹ tᴱ) pr prᴹ)

  → Jᴱ {a} aᴹ aᴱ {t} tᴹ tᴱ {P} Pᴹ Pᴱ {pr} prᴹ prᴱ {t} tᴹ tᴱ {refl} (reflᴹ aᴹ tᴹ) (reflᴱ aᴹ aᴱ tᴹ tᴱ)
  ≡ prᴱ
Jᴱβ {a} aᴹ aᴱ {t} .(aᴱ t) refl {P} Pᴹ Pᴱ {pr} prᴹ prᴱ = refl
