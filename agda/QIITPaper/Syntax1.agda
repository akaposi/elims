module Syntax1 where

-- without rewrite rules

open import Lib
open import JM

infixl 5 _▷_
infixl 7 _[_]T
infixl 5 _,_
infix  6 _∘_
infixl 8 _[_]t
infix 4 <_>
infixl 5 _^_

------------------------------------------------------------
-- substitution calculus
------------------------------------------------------------

postulate
  Con : Set
  Ty : Con → Set
  Tms : Con → Con → Set
  Tm : (Γ : Con) → Ty Γ → Set

  ∙ : Con
  _▷_ : (Γ : Con) → Ty Γ → Con
  
  _[_]T : ∀{Γ Δ} → Ty Δ → Tms Γ Δ → Ty Γ

  id : ∀{Γ} → Tms Γ Γ
  _∘_ : ∀{Γ Θ Δ} → Tms Θ Δ → Tms Γ Θ → Tms Γ Δ
  ε : ∀{Γ} → Tms Γ ∙
  _,_  : ∀{Γ Δ A}(σ : Tms Γ Δ) → Tm Γ (A [ σ ]T) → Tms Γ (Δ ▷ A)
  π₁ : ∀{Γ Δ}{A : Ty Δ} → Tms Γ (Δ ▷ A) → Tms Γ Δ

  π₂ : ∀{Γ Δ}{A : Ty Δ}(σ : Tms Γ (Δ ▷ A)) → Tm Γ (A [ π₁ {A = A} σ ]T)
  _[_]t : ∀{Γ Δ}{A : Ty Δ} → Tm Δ A → (σ : Tms Γ Δ) → Tm Γ (A [ σ ]T)

  [id]T : ∀{Γ}{A : Ty Γ} → A [ id ]T ≡ A
  [][]T : ∀{Γ Θ Δ}{A : Ty Δ}{σ : Tms Θ Δ}{δ : Tms Γ Θ} → A [ σ ]T [ δ ]T ≡ A [ _∘_ {Γ}{Θ}{Δ} σ δ ]T

  idl : ∀{Γ Δ}{σ : Tms Γ Δ} → (_∘_ {Γ}{Δ}{Δ} id σ) ≡ σ
  idr : ∀{Γ Δ}{σ : Tms Γ Δ} → (_∘_ {Γ}{Γ}{Δ} σ id) ≡ σ
  ass : ∀{Γ Δ Σ Ω}{σ : Tms Σ Ω}{δ : Tms Δ Σ}{ν : Tms Γ Δ} →
    _∘_ {Γ}{Δ}{Ω}(_∘_ {Δ}{Σ}{Ω} σ δ) ν ≡ _∘_ {Γ}{Σ}{Ω} σ (_∘_ {Γ}{Δ}{Σ} δ ν)
  ▷β₁ : ∀{Γ Δ}{A : Ty Δ}{σ : Tms Γ Δ}{t : Tm Γ (A [ σ ]T)} →
    (π₁ (_,_ {Γ}{Δ}{A} σ t)) ≡ σ
  ▷β₂ : ∀{Γ Δ}{A : Ty Δ}{σ : Tms Γ Δ}{t : Tm Γ (A [ σ ]T)} →
     tr (Tm Γ) (ap (A [_]T) (▷β₁ {Γ}{Δ}{A}{σ})) (π₂ (_,_ {Γ}{Δ}{A} σ t)) ≡ t
  ▷η : ∀{Γ Δ}{A : Ty Δ}{δ : Tms Γ (Δ ▷ A)} →
    _,_ {Γ}{Δ}{A}(π₁ {Γ}{Δ}{A} δ)(π₂ {Γ}{Δ}{A} δ) ≡ δ
  ,∘ : ∀{Γ Θ Δ}{σ : Tms Θ Δ}{δ : Tms Γ Θ}{A : Ty Δ}{t : Tm Θ (A [ σ ]T)} →
    (_∘_ {Γ}{Θ}{Δ ▷ A} (_,_ {Θ}{Δ}{A} σ t) δ) ≡
    (_,_ {Γ}{Δ}{A}(_∘_ {Γ}{Θ}{Δ} σ δ) (tr (Tm Γ) ([][]T {Γ}{Θ}{Δ}{A}{σ}{δ}) (_[_]t {Γ}{Θ}{A [ σ ]T} t δ)))
  ∙ηid  : id {∙} ≡ ε {∙}
  ∙η∘   : {Γ Δ : Con}{σ : Tms Γ Δ} → ε ∘ σ ≡ ε
  ∙ηπ₁  : {Γ : Con}{A : Ty ∙}{σ : Tms Γ (∙ ▷ A)} → π₁ σ ≡ ε
  ∙η : ∀{Γ}{σ : Tms Γ ∙} → σ ≡ ε

-- defined stuff

wk : ∀{Γ}{A : Ty Γ} → Tms (Γ ▷ A) Γ
wk = π₁ id

vz : ∀{Γ}{A : Ty Γ} → Tm (Γ ▷ A) (A [ wk ]T)
vz = π₂ id

vs : ∀{Γ}{A B : Ty Γ} → Tm Γ A → Tm (Γ ▷ B) (A [ wk ]T) 
vs x = x [ wk ]t

<_> : ∀{Γ}{A : Ty Γ} → Tm Γ A → Tms Γ (Γ ▷ A)
< t > = id , tr (Tm _) ([id]T ⁻¹) t

_^_ : {Γ Δ : Con}(σ : Tms Γ Δ)(A : Ty Δ) → Tms (Γ ▷ A [ σ ]T) (Δ ▷ A)
σ ^ A = (σ ∘ wk) , tr (Tm _) [][]T vz

------------------------------------------------------------
-- universe
------------------------------------------------------------

postulate
  U : ∀{Γ} → Ty Γ
  U[] : ∀{Γ Δ}{σ : Tms Γ Δ} → _[_]T {Γ}{Δ} U σ ≡ U

postulate
  El : ∀{Γ}(a : Tm Γ U) → Ty Γ
  El[] : ∀{Γ Δ}{σ : Tms Γ Δ}{a : Tm Δ U} →
    (El a [ σ ]T) ≡ El (tr (Tm Γ) U[] (a [ σ ]t))

------------------------------------------------------------
-- function space with small codomain
------------------------------------------------------------

postulate
  Π : ∀{Γ}(a : Tm Γ U)(B : Ty (Γ ▷ El a)) → Ty Γ
  Π[] : ∀{Γ Δ}{σ : Tms Γ Δ}{a : Tm Δ U}{B : Ty (Δ ▷ El a)} →
    (Π a B) [ σ ]T ≡
    Π (tr (Tm Γ) U[] (a [ σ ]t)) (B [ tr (λ z → Tms (Γ ▷ z) (Δ ▷ El a)) El[] (σ ^ El a) ]T)

Tm▷= : {Γ : Con}{A₀ A₁ : Ty Γ}(A₂ : A₀ ≡ A₁){Δ : Con}{B : Ty Δ}{σ : Tms (Γ ▷ A₀) Δ} →
  Tm (Γ ▷ A₀) (B [ σ ]T) ≡ Tm (Γ ▷ A₁) (B [ tr (λ z → Tms (Γ ▷ z) Δ) A₂ σ ]T)
Tm▷= refl = refl

postulate
  app : ∀{Γ}{a : Tm Γ U}{B : Ty (Γ ▷ El a)} → Tm Γ (Π a B) → Tm (Γ ▷ El a) B
  app[] : ∀{Γ}{a : Tm Γ U}{B : Ty (Γ ▷ El a)}{t : Tm Γ (Π a B)}{Θ : Con}{σ : Tms Θ Γ} →
    coe (Tm▷= El[]) (app t [ σ ^ El a ]t) ≡ app (tr (Tm Θ) Π[] (t [ σ ]t))

_$_ : ∀{Γ}{a : Tm Γ U}{B : Ty (Γ ▷ El a)} → Tm Γ (Π a B) → (u : Tm Γ (El a)) → Tm Γ (B [ < u > ]T)
t $ u = app t [ < u > ]t

------------------------------------------------------------
-- identity type
------------------------------------------------------------

postulate
  Id : ∀{Γ}(a : Tm Γ U)(t u : Tm Γ (El a)) → Ty Γ
  reflect : ∀{Γ a}{t u : Tm Γ (El a)} → Tm Γ (Id a t u) → t ≡ u
  Id[] : ∀{Γ}{a : Tm Γ U}{t u : Tm Γ (El a)}{Θ : Con}{σ : Tms Θ Γ} →
    Id a t u [ σ ]T ≡
    Id (tr (Tm Θ) U[] (a [ σ ]t)) (tr (Tm Θ) El[] (t [ σ ]t)) (tr (Tm Θ) El[] (u [ σ ]t))

transp : ∀{Γ}{a : Tm Γ U}(P : Ty (Γ ▷ El a)){t u : Tm Γ (El a)}(e : Tm Γ (Id a t u)) →
  Tm Γ (P [ < t > ]T) → Tm Γ (P [ < u > ]T)
transp {Γ} P e = tr (λ z → Tm Γ (P [ < z > ]T)) (reflect e)

------------------------------------------------------------
-- function space with metatheoretic domain
------------------------------------------------------------

postulate
  Π' : ∀{Γ}(T : Set)(B : T → Ty Γ) → Ty Γ
  Π'[] : ∀{Γ}{T : Set}{B : T → Ty Γ}{Θ : Con}{σ : Tms Θ Γ} → (Π' T B) [ σ ]T ≡ Π' T (λ α → B α [ σ ]T)

  app' : ∀{Γ}{T : Set}{B : T → Ty Γ}(t : Tm Γ (Π' T B))(α : T) → Tm Γ (B α)
  app'[] : ∀{Γ}{T : Set}{B : T → Ty Γ}{t : Tm Γ (Π' T B)}{α : T}{Θ : Con}{σ : Tms Θ Γ} →
    _[_]t {A = B α} (app' {B = B} t α) σ ≡
    app' {B = λ α → B α [ σ ]T} (tr (Tm Θ) Π'[] (t [ σ ]t)) α

------------------------------------------------------------
-- function space with metatheoretic domain and small codomain
------------------------------------------------------------

postulate
  Π'' : ∀{Γ}(T : Set)(b : T → Tm Γ U) → Tm Γ U
  Π''[] : ∀{Γ}{T : Set}{b : T → Tm Γ U}{Θ : Con}{σ : Tms Θ Γ} →
    tr (Tm Θ) U[] (_[_]t {A = U} (Π'' T b) σ) ≡
    Π'' T (λ α → tr (Tm Θ) U[] (_[_]t {A = U} (b α) σ))

  app'' : ∀{Γ}{T : Set}{b : T → Tm Γ U}(t : Tm Γ (El (Π'' T b)))(α : T) → Tm Γ (El (b α))
  app''[] : ∀{Γ}{T : Set}{b : T → Tm Γ U}{t : Tm Γ (El (Π'' T b))}{α : T}{Θ : Con}{σ : Tms Θ Γ} →
    tr (Tm Θ) El[] (_[_]t {A = El (b α)} (app'' {b = b} t α) σ) ≡
    app'' (tr (λ z → Tm Θ (El z)) Π''[] (tr (Tm Θ) (El[]) (t [ σ ]t))) α
