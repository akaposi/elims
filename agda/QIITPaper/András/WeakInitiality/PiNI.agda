
{-# OPTIONS --rewriting #-}

module WeakInitiality.PiNI where

open import StrictLib hiding (id; _∘_)
import Syntax as S
import AM as AM
open import AM using (Conʳ; Tyʳ; Tmʳ; Subʳ)

open import WeakInitiality.CwFOps

ΠNI : ∀ {Γ}(A : Set) → (A → Ty Γ) → Ty Γ
ΠNI {Γ , Γᶜ} A B =
    (S.ΠNI A (λ α → B α .₁))
  , λ σ* t →
     (λ α → B α .₂ σ* (S.appNI t α) .₁) ,
     λ α → B α .₂ σ* (S.appNI t α) .₂

coe-cod :
  ∀ {α β γ}{A : Set α}{B : Set β}
    {b₀ b₁ : B}
    (b₂ : b₀ ≡ b₁)
    (C : A → B → Set γ)
    (f₀ : ∀ a → C a b₀)
    a
    → coe ((λ b → ∀ a → C a b) & b₂) f₀ a ≡ coe (C a & b₂) (f₀ a)
coe-cod {A = A}{B}{b₀}{b₁} refl C f₀ a =
  refl

ΠNI[] :
  {Γ Δ : Con} {σ : Sub Γ Δ} {A : _} {B : A → Ty Δ} →
  _≡_ {_} {Ty Γ} (_[_]T {Γ} {Δ} (ΠNI {Δ} A B) σ)
  (ΠNI {Γ} A (λ α → _[_]T {Γ} {Δ} (B α) σ))
ΠNI[] {Γ , Γᶜ}{Δ , Δᶜ}{σ}{A}{B} =
  ,≡ refl (ext λ σ* → ext λ t →
    ,≡ (ext λ α →
      coe-cod
        (₂ σ σ* .₁)
        (λ α₁ γ → ₁ (Tyʳ (B α₁ .₁)) γ)
        (λ α₁ → B α₁ .₂ (₁ σ S.∘ σ*) (S.appNI t α₁) .₁) α
      ◾ coe-coe _ _ (B α .₂ (₁ σ S.∘ σ*) (S.appNI t α) .₁))
       (ext λ α → {!!})) -- trivial modulo coe
      -- goal:
      -- B α .₂ (₁ σ S.∘ σ*) (S.appNI t α) .₂ =
      -- B α .₂ (₁ σ S.∘ σ*) (S.appNI t α) .₂

appNI :  ∀ {Γ}{A}{B : A → Ty Γ} → Tm Γ (ΠNI A B) → (∀ α → Tm Γ (B α))
appNI {Γ , Γᶜ}{A}{B}(t , tᶜ) α =
  (S.appNI t α) ,
  λ σ* →
    (λ f → f α) & tᶜ σ* .₁ ,
    {!? ◾
    ((λ f → f α) & tᶜ σ* .₂)!}
    -- bug? doesn't refine (but kinda trivial)
