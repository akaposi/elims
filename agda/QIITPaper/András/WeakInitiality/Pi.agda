{-# OPTIONS --rewriting #-}

module WeakInitiality.Pi where

open import StrictLib hiding (id; _∘_)
import Syntax as S
import AM as AM
open import AM using (Conʳ; Tyʳ; Tmʳ; Subʳ)

open import WeakInitiality.CwFOps
open import WeakInitiality.ElU

{-# REWRITE coecoe⁻¹ #-}
{-# REWRITE coecoe⁻¹' #-}

Π : ∀{Γ}(a : Tm Γ U)(B : Ty (Γ ▶ El a)) → Ty Γ
Π {Γ , Γᶜ}(a , aᶜ)(B , Bᶜ) =
  (S.Π a B) ,
  (λ σ* t →
    (λ α → let α' = coe (aᶜ σ* .₁ ⁻¹) α
           in (Bᶜ (σ* S.,s α')
               (coe cheat -- TODO (possibly with more rewrites)
               (S.app t S.[ S.< α' > ]t)) .₁))
    ,
    λ α → let α' = coe (aᶜ σ* .₁ ⁻¹) α
              p1 : ₁ (Tmʳ (coe (aᶜ σ* .₁ ⁻¹) α)) γ* ≡ lift (₂ (Tmʳ a) (Γᶜ σ* .₂) α)
              p1 = J (λ _ eq → ∀ α → ₁ (Tmʳ (coe (eq ⁻¹) α)) γ* ≡
                            lift (coe ((λ x → x → ₁ (Tmʳ a) (₁ (Subʳ σ*) γ*)) & eq)
                            (λ t₁ → lower (Tmʳ t₁ .₁ γ*)) α))
                      (λ _ → refl) (aᶜ σ* .₁) α
                   ◾ lift & happly (aᶜ σ* .₂) α
              p2 : Tmʳ t .₁ γ* (lower (₁ (Tmʳ (coe (aᶜ σ* .₁ ⁻¹) α)) γ*))
                   ≃ Tmʳ t .₁ γ* (₂ (Tmʳ a) (Γᶜ σ* .₂) α)
              p2 = ap̃̃ (Tmʳ t .₁ γ*) (lower & p1)
              p3 : Tmʳ {Γ*}{B S.[ σ* S.,s coe (aᶜ σ* .₁ ⁻¹) α ]T}
                   (coe cheat (S.app t S.[ S.id S.,s coe (aᶜ σ* .₁ ⁻¹) α ]t)) .₁ γ*
                   ≃ Tmʳ t .₁ γ* (₂ (Tmʳ a) (Γᶜ σ* .₂) α)
              p3 = {!!} ◾̃ p2 -- TODO (should disappear alongside cheat as refl)
          in
          coe
            (uñ
              (ap5̃̃ (λ x y → Tyʳ B .₂ {x}{y})
                refl
                (,≡ refl p1 ~)
                (ap3̃̃ (λ B → _,_{B = B})
                   (ext λ γᴹ → (₂ (Tmʳ a) γᴹ α ≡_) & (lower & p1))
                   refl̃
                   (UIP̃' _ _ refl (lower & p1)))
                refl̃
                {!p3!} -- bug (should accept p3)
                ))
          (Bᶜ (σ* S.,s α') (coe cheat (S.app t S.[ S.< α' > ]t)) .₂)
          )

app : ∀{Γ}{a : Tm Γ U}{B : Ty (Γ ▶ El a)} → Tm Γ (Π a B) → Tm (Γ ▶ El a) B
app {Γ , Γᶜ}{a , aᶜ}{B , Bᶜ} (t , tᶜ) =
  (S.app t) ,
  (λ σ* →
    (let α = S.π₂ σ*
         α' : ₁ (Tmʳ a) (Γᶜ (S.π₁ σ*) .₁)
         α' = coe (aᶜ (S.π₁ σ*) .₁) α
         p1 = ap̃̃ (λ f → f α') (tᶜ (S.π₁ σ*) .₁)
     in uñ (ap2̃̃ (λ x y → Bᶜ x y .₁)
             (S.πη ⁻¹)
             -- same cheat as before
             ((cheat ◾̃ ((λ x → x S.[ S.id S.,s S.π₂ σ* ]t) & S.app[] {σ = S.π₁ σ*}{t = t} ~))
                ◾̃ uncoe cheat (S.app (t S.[ S.π₁ σ* ]t) S.[ S.id S.,s S.π₂ σ* ]t) ⁻¹̃)
          ◾̃ p1)
    ) ,
    {!!})
       -- this should check but doesn't:

    -- uñ (uncoe
    --   ((λ x → Tyʳ B .₂ (Γᶜ (S.π₁ σ*) .₂ , (happly (aᶜ (S.π₁ σ*) .₂)
    --       (coe (aᶜ (S.π₁ σ*) .₁) (S.π₂ σ*)) ⁻¹ ◾ J (λ z eq → coe ((λ
    --       x₁ → x₁ → ₁ (Tmʳ a) (₁ (₁ (Subʳ σ*) γ*))) & eq) (λ t₁ →
    --       lower (Tmʳ t₁ .₁ γ*)) (coe eq (S.π₂ σ*)) ≡ lower (Tmʳ (S.π₂
    --       σ*) .₁ γ*)) refl (aᶜ (S.π₁ σ*) .₁))) x (₁ (Tmʳ t) (₁ (₁
    --       (Subʳ σ*) γ*)) (lower (₂ (₁ (Subʳ σ*) γ*))))) & uñ (ap2̃̃ (λ x
    --       y → Bᶜ x y .₁) (S.πη ⁻¹) ((cheat ◾̃ ((λ x → x S.[ S.id S.,s
    --       S.π₂ σ* ]t) & S.app[] ~)) ◾̃ uncoe cheat (S.app (t S.[ S.π₁
    --       σ* ]t) S.[ S.id S.,s S.π₂ σ* ]t) ⁻¹̃) ◾̃ ap̃̃ (λ f → f (coe (aᶜ
    --       (S.π₁ σ*) .₁) (S.π₂ σ*))) (tᶜ (S.π₁ σ*) .₁))) (Bᶜ σ* (S.app
    --       t S.[ σ* ]t) .₂))
    --   ◾̃ ?)
