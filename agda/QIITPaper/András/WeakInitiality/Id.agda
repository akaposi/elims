
{-# OPTIONS --rewriting #-}

module WeakInitiality.Id where

open import StrictLib hiding (id; _∘_)
import Syntax as S
import AM as AM
open import AM using (Conʳ; Tyʳ; Tmʳ; Subʳ)

open import WeakInitiality.CwFOps
open import WeakInitiality.ElU

Id : ∀ {Γ}(a : Tm Γ U) → Tm Γ (El a) → Tm Γ (El a) → Ty Γ
Id {Γ , Γᶜ}(a , aᶜ)(t , tᶜ)(u , uᶜ) =
  (S.Id a t u) ,
  λ σ* e →
      (tᶜ σ* .₁ ⁻¹ ◾ (λ x → lift (coe (aᶜ σ* .₁) x)) & S.Reflect e
                   ◾ uᶜ σ* .₁)
    , tt

-- Id[] : Agda can't typecheck Id[] in time

-- (equality of semantic terms is propositional)
Reflect :
  ∀ {Γ}{a : Tm Γ (U {Γ})}{t u : Tm Γ (El a)} → Tm Γ (Id a t u) → t ≡ u
Reflect {Γ , Γᶜ}{a , aᶜ}{t , tᶜ}{u , uᶜ} (e , eᶜ) =
  ,≡ (S.Reflect e)
     (ext λ σ* →
       ,≡ (UIP _ _)
          (UIP _ _))
