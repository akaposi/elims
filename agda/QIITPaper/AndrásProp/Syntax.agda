{-# OPTIONS --rewriting #-}

module Syntax where

open import Lib hiding (id; _∘_)

infixl 7 _[_]T
infixl 5 _,s_
infix  6 _∘_
infixl 8 _[_]t

postulate
  Con : Set
  Ty  : Con → Set
  Tm  : ∀ Γ → Ty Γ → Set
  Tms : Con → Con → Set

  ∙     : Con
  _▶_   : (Γ : Con) → Ty Γ → Con

  _[_]T : ∀{Γ Δ} → Ty Δ → Tms Γ Δ → Ty Γ

  id    : ∀{Γ} → Tms Γ Γ
  _∘_   : ∀{Γ Δ Σ} → Tms Δ Σ → Tms Γ Δ → Tms Γ Σ
  ε     : ∀{Γ} → Tms Γ ∙
  _,s_  : ∀{Γ Δ}(σ : Tms Γ Δ){A : Ty Δ} → Tm Γ (A [ σ ]T) → Tms Γ (Δ ▶ A)
  π₁    : ∀{Γ Δ}{A : Ty Δ} → Tms Γ (Δ ▶ A) → Tms Γ Δ

  _[_]t : ∀{Γ Δ}{A : Ty Δ} → Tm Δ A → (σ : Tms Γ Δ) → Tm Γ (A [ σ ]T)
  π₂    : ∀{Γ Δ}{A : Ty Δ}(σ : Tms Γ (Δ ▶ A)) → Tm Γ (A [ π₁ σ ]T)

  [id]T : ∀{Γ}{A : Ty Γ} → A [ id ]T ≡ A
  [id]T' : ∀{Γ}{A : Ty Γ} → A [ id ]T ↦ A
  [][]T : ∀{Γ Δ Σ}{A : Ty Σ}{σ : Tms Γ Δ}{δ : Tms Δ Σ}
          → A [ δ ]T [ σ ]T ≡ A [ δ ∘ σ ]T
  [][]T' : ∀{Γ Δ Σ}{A : Ty Σ}{σ : Tms Γ Δ}{δ : Tms Δ Σ}
          → A [ δ ]T [ σ ]T ↦ A [ δ ∘ σ ]T

  idl   : ∀{Γ Δ}{σ : Tms Γ Δ} → (id ∘ σ) ≡ σ
  idl'  : ∀{Γ Δ}{σ : Tms Γ Δ} → (id ∘ σ) ↦ σ
  idr   : ∀{Γ Δ}{σ : Tms Γ Δ} → (σ ∘ id) ≡ σ
  idr'  : ∀{Γ Δ}{σ : Tms Γ Δ} → (σ ∘ id) ↦ σ
  ass   : ∀{Γ Δ Σ Ω}{σ : Tms Σ Ω}{δ : Tms Δ Σ}{ν : Tms Γ Δ}
        → (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν)
  ass'  : ∀{Γ Δ Σ Ω}{σ : Tms Σ Ω}{δ : Tms Δ Σ}{ν : Tms Γ Δ}
        → (σ ∘ δ) ∘ ν ↦ σ ∘ (δ ∘ ν)

  ,∘    : ∀{Γ Δ Σ}{δ : Tms Γ Δ}{σ : Tms Σ Γ}{A : Ty Δ}{t : Tm Γ (A [ δ ]T)}
        → ((δ ,s t) ∘ σ) ≡ ((δ ∘ σ) ,s tr (Tm Σ) [][]T (t [ σ ]t))
  ,∘'   : ∀{Γ Δ Σ}{δ : Tms Γ Δ}{σ : Tms Σ Γ}{A : Ty Δ}{t : Tm Γ (A [ δ ]T)}
        → ((δ ,s t) ∘ σ) ↦ ((δ ∘ σ) ,s tr (Tm Σ) [][]T (t [ σ ]t))
  π₁β   : ∀{Γ Δ}{A : Ty Δ}{σ : Tms Γ Δ}{t : Tm Γ (A [ σ ]T)}
        → (π₁ (σ ,s t)) ≡ σ
  π₁β'  : ∀{Γ Δ}{A : Ty Δ}{σ : Tms Γ Δ}{t : Tm Γ (A [ σ ]T)}
        → (π₁ (σ ,s t)) ↦ σ
  πη    : ∀{Γ Δ}{A : Ty Δ}{σ : Tms Γ (Δ ▶ A)}
        → (π₁ σ ,s π₂ σ) ≡ σ
  εη    : ∀{Γ}{σ : Tms Γ ∙}
        → σ ≡ ε
  π₂β   : ∀{Γ Δ}{A : Ty Δ}{σ : Tms Γ Δ}{t : Tm Γ (A [ σ ]T)}
        → π₂ (σ ,s t) ≡ tr (λ σ → Tm Γ (A [ σ ]T)) (π₁β ⁻¹) t
  π₂β'   : ∀{Γ Δ}{A : Ty Δ}{σ : Tms Γ Δ}{t : Tm Γ (A [ σ ]T)}
        → π₂ (σ ,s t) ↦ tr (λ σ → Tm Γ (A [ σ ]T)) (π₁β ⁻¹) t

wk : ∀{Γ}{A : Ty Γ} → Tms (Γ ▶ A) Γ
wk = π₁ id

vz : ∀{Γ}{A : Ty Γ} → Tm (Γ ▶ A) (A [ wk ]T)
vz = π₂ id

vs : ∀{Γ}{A B : Ty Γ} → Tm Γ A → Tm (Γ ▶ B) (A [ wk ]T)
vs x = x [ wk ]t

<_> : ∀{Γ}{A : Ty Γ} → Tm Γ A → Tms Γ (Γ ▶ A)
< t > = id ,s tr (Tm _) ([id]T ⁻¹) t

infix 4 <_>

_^_ : ∀ {Γ Δ : Con}(σ : Tms Γ Δ)(A : Ty Δ) → Tms (Γ ▶ (A [ σ ]T)) (Δ ▶ A)
_^_ {Γ} {Δ} σ A = σ ∘ wk ,s coe (Tm _ & [][]T) (vz {Γ}{A [ σ ]T})

infixl 5 _^_


-- Universe
--------------------------------------------------------------------------------

postulate
  U    : ∀{Γ} → Ty Γ
  U[]  : ∀{Γ Δ}{σ : Tms Γ Δ} → (U [ σ ]T) ≡ U
  U[]' : ∀{Γ Δ}{σ : Tms Γ Δ} → (U [ σ ]T) ↦ U
  El   : ∀{Γ}(a : Tm Γ U) → Ty Γ
  El[] : ∀{Γ Δ}{σ : Tms Γ Δ}{a : Tm Δ U}
       → (El a [ σ ]T) ≡ (El (coe (Tm Γ & U[]) (a [ σ ]t)))
  El[]' : ∀{Γ Δ}{σ : Tms Γ Δ}{a : Tm Δ U}
       → (El a [ σ ]T) ↦ (El (coe (Tm Γ & U[]) (a [ σ ]t)))

_^El_ :
  {Γ Δ : Con}(σ : Tms Γ Δ)(a : Tm Δ U)
  → Tms (Γ ▶ El (tr (Tm Γ) (U[] ) (a [ σ ]t))) (Δ ▶ El a)
_^El_ {Γ}{Δ} σ a = σ ∘ wk ,s tr (Tm (Γ ▶ El (tr (Tm Γ) (U[] {σ = σ}) (a [ σ ]t))))
                                  ((_[ wk ]T) & (El[] {Γ}{Δ}{σ}{a} ⁻¹) ◾
                                  ([][]T {A = El a}{wk}{σ})) vz

infixl 5 _^El_

_^U : ∀ {Γ Δ}(σ : Tms Γ Δ) → Tms (Γ ▶ U) (Δ ▶ U)
_^U {Γ}{Δ} σ =
  σ ∘ wk ,s coe (Tm (Γ ▶ U) & (U[] {σ = wk} ◾ U[] {σ = σ ∘ wk} ⁻¹)) vz

-- Identity
--------------------------------------------------------------------------------

postulate
  Id   : ∀ {Γ}(a : Tm Γ U) → Tm Γ (El a) → Tm Γ (El a) → Ty Γ
  Id[] : ∀ {Γ Δ}{σ : Tms Γ Δ}{a : Tm Δ U}{t u : Tm Δ (El a)}
        → Id a t u [ σ ]T ≡ Id (coe (Tm Γ & U[]) (a [ σ ]t))
                               (coe (Tm Γ & El[]) (t [ σ ]t))
                               (coe (Tm Γ & El[]) (u [ σ ]t))

  Id[]' : ∀ {Γ Δ}{σ : Tms Γ Δ}{a : Tm Δ U}{t u : Tm Δ (El a)}
        → Id a t u [ σ ]T ↦ Id (coe (Tm Γ & U[]) (a [ σ ]t))
                               (coe (Tm Γ & El[]) (t [ σ ]t))
                               (coe (Tm Γ & El[]) (u [ σ ]t))

  Reflect : ∀ {Γ a}{t u : Tm Γ (El a)} → Tm Γ (Id a t u) → t ≡ u

-- Inductive function
--------------------------------------------------------------------------------
postulate
  Π : ∀{Γ}(a : Tm Γ U)(B : Ty (Γ ▶ El a)) → Ty Γ

  Π[] : ∀{Γ Δ}{σ : Tms Γ Δ}{a : Tm Δ U}{B : Ty (Δ ▶ El a)}
      → (Π a B) [ σ ]T ≡ Π (tr (Tm Γ) U[] (a [ σ ]t))
                           (tr (λ x → Ty (Γ ▶ x)) El[] (B [ σ ^ El a ]T))
  Π[]' : ∀{Γ Δ}{σ : Tms Γ Δ}{a : Tm Δ U}{B : Ty (Δ ▶ El a)}
      → (Π a B) [ σ ]T ↦ Π (tr (Tm Γ) U[] (a [ σ ]t))
                           (tr (λ x → Ty (Γ ▶ x)) El[] (B [ σ ^ El a ]T))

  app : ∀{Γ}{a : Tm Γ U}{B : Ty (Γ ▶ El a)} → Tm Γ (Π a B) → Tm (Γ ▶ El a) B

  app[] : ∀{Γ Δ}{σ : Tms Γ Δ}{a : Tm Δ U}{B : Ty (Δ ▶ El a)}{t : Tm Δ (Π a B)}
          → tr2 (λ A → Tm (Γ ▶ A)) El[] refl (app t [ σ ^ El a ]t)
          ≡ app (tr (Tm _) Π[] (t [ σ ]t))

_$_ : ∀ {Γ}{a : Tm Γ U}{B : Ty (Γ ▶ El a)}(t : Tm Γ (Π a B))(u : Tm Γ (El a)) → Tm Γ (B [ < u > ]T)
t $ u = (app t) [ < u > ]t


-- Non-inductive function
--------------------------------------------------------------------------------
postulate
  ΠNI : ∀ {Γ}(A : Set) → (A → Ty Γ) → Ty Γ

  ΠNI[] : ∀ {Γ Δ}{σ : Tms Γ Δ}{A : Set}{B : A → Ty Δ}
          → ΠNI A B [ σ ]T ≡ ΠNI A (λ α → B α [ σ ]T)

  ΠNI[]' : ∀ {Γ Δ}{σ : Tms Γ Δ}{A : Set}{B : A → Ty Δ}
          → ΠNI A B [ σ ]T ↦ ΠNI A (λ α → B α [ σ ]T)

  appNI : ∀ {Γ}{A}{B : A → Ty Γ} → Tm Γ (ΠNI A B) → (∀ α → Tm Γ (B α))

  appNI[] :  ∀ {Γ Δ}{σ : Tms Γ Δ}{A}{B : A → Ty Δ}(t : Tm Δ (ΠNI A B)) α
             → appNI t α [ σ ]t ≡ appNI (tr (Tm Γ) ΠNI[] (t [ σ ]t)) α

  appNI[]' :  ∀ {Γ Δ}{σ : Tms Γ Δ}{A}{B : A → Ty Δ}(t : Tm Δ (ΠNI A B)) α
             → appNI t α [ σ ]t ↦ appNI (tr (Tm Γ) ΠNI[] (t [ σ ]t)) α


-- Small non-inductive function
--------------------------------------------------------------------------------
postulate
  Πₙᵢ : ∀ {Γ}(A : Set) → (A → Tm Γ U) → Tm Γ U

  Πₙᵢ[] : ∀ {Γ Δ}{σ : Tms Γ Δ}{A : Set}{b : A → Tm Δ U}
          → tr (Tm Γ) U[] (Πₙᵢ A b [ σ ]t) ≡ Πₙᵢ A (λ α → tr (Tm Γ) U[] (b α [ σ ]t))

  appₙᵢ : ∀ {Γ}{A}{b : A → Tm Γ U} → Tm Γ (El (Πₙᵢ A b)) → (∀ α → Tm Γ (El (b α)))

  appₙᵢ[] : ∀ {Γ Δ}{σ : Tms Γ Δ}{A}{b : A → Tm Δ U}(t : Tm Δ (El (Πₙᵢ A b))) α
          → tr (Tm Γ) El[] (appₙᵢ t α [ σ ]t) ≡ appₙᵢ {Γ}{A}{λ α → coe (Tm Γ & U[]) (b α [ σ ]t)} (coe (Tm Γ & (El[] ◾ El & Πₙᵢ[] {Γ}{Δ}{σ}{A})) (t [ σ ]t)) α

--------------------------------------------------------------------------------
-- Syntax rewrite rules & derived equalities
--------------------------------------------------------------------------------

{-# REWRITE
  [id]T' [][]T' idl' idr' ass' U[]' El[]' Π[]' Id[]' ΠNI[]' ,∘' π₁β' π₂β'
#-}
-- {-# REWRITE Πₙᵢ[] #-} -  - not accepted by Agda

-- postulate
--   [id]Trefl : ∀{Γ}{A : Ty Γ} → [id]T {Γ}{A} ≡ refl
--   [][]Trefl : ∀{Γ Δ Σ}{A : Ty Σ}{σ : Tms Γ Δ}{δ : Tms Δ Σ} → [][]T {Γ}{Δ}{Σ}{A}{σ}{δ} ≡ refl
--   idlrefl   : {Γ Δ : Con} {σ : Tms Γ Δ} → idl {σ = σ} ≡ refl
--   idrrefl   : {Γ Δ : Con} {σ : Tms Γ Δ} → idr {σ = σ} ≡ refl
--   assrefl   : {Γ Δ : Con} {Σ : Con} {Ω : Con} {σ : Tms Σ Ω} {δ : Tms Δ Σ}{ν : Tms Γ Δ} → ass {σ = σ}{δ}{ν} ≡ refl
--   U[]refl   : {Γ Δ : Con} {σ : Tms Γ Δ} → U[] {σ = σ} ≡ refl
--   El[]refl  : {Γ Δ : Con} {σ : Tms Γ Δ} {a : Tm Δ U} → El[] {σ = σ}{a} ≡ refl
--   Π[]refl   : {Γ Δ : Con} {σ : Tms Γ Δ} {a : Tm Δ U} {B : Ty (Δ ▶ El a)} → Π[] {σ = σ}{a}{B} ≡ refl
--   Id[]refl  : {Γ Δ : Con} {σ : Tms Γ Δ} {a : Tm Δ U} {t u : Tm Δ (El a)} → Id[] {σ = σ}{a}{t}{u} ≡ refl
--   ΠNI[]refl : {Γ Δ : Con} {σ : Tms Γ Δ} {A : Set} {B : A → Ty Δ} → ΠNI[] {σ = σ}{A}{B} ≡ refl
--   ,∘refl    : ∀{Γ Δ Σ}{δ : Tms Γ Δ}{σ : Tms Σ Γ}{A : Ty Δ}{t : Tm Γ (A [ δ ]T)}
--               → ,∘ {δ = δ}{σ}{A}{t} ≡ refl
--   π₁βrefl   : ∀{Γ Δ}{A : Ty Δ}{σ : Tms Γ Δ}{t : Tm Γ (A [ σ ]T)} → π₁β{A = A}{σ}{t} ≡ refl
--   π₂βrefl   : {Γ Δ : Con} {A : Ty Δ} {σ : Tms Γ Δ} {t : Tm Γ (A [ σ ]T)} → π₂β {A = A}{σ}{t} ≡ refl

-- {-# REWRITE
--   [id]Trefl [][]Trefl idlrefl idrrefl assrefl U[]refl El[]refl Π[]refl
--   Id[]refl ΠNI[]refl ,∘refl π₁βrefl π₂βrefl
-- #-}

-- Derived equalities (TODO: prove them).
-- We need a bunch of specializations for term subst laws to placate Agda's weak
-- REWRITE LHS matching.
postulate
  π₁∘   : ∀{Γ Δ Σ}{A : Ty Σ}{σ : Tms Δ (Σ ▶ A)}{δ : Tms Γ Δ} → π₁ σ ∘ δ ≡ π₁ (σ ∘ δ)
  π₁∘'  : ∀{Γ Δ Σ}{A : Ty Σ}{σ : Tms Δ (Σ ▶ A)}{δ : Tms Γ Δ} → π₁ σ ∘ δ ↦ π₁ (σ ∘ δ)
{-# REWRITE π₁∘' #-}

postulate
  π₂∘Ne   : ∀{Γ Δ Σ}{A : Ty Σ}{σ : Tms Δ (Σ ▶ A)}{δ : Tms Γ Δ}
        → π₂ σ [ δ ]t ≡ π₂ (σ ∘ δ)
  π₂∘Ne'   : ∀{Γ Δ Σ}{A : Ty Σ}{σ : Tms Δ (Σ ▶ A)}{δ : Tms Γ Δ}
        → π₂ σ [ δ ]t ↦ π₂ (σ ∘ δ)
{-# REWRITE π₂∘Ne' #-}

postulate
  [id]t : ∀ {Γ}{A}{t : Tm Γ A} → t [ id ]t ≡ t
  [id]t' : ∀ {Γ}{A}{t : Tm Γ A} → t [ id ]t ↦ t
{-# REWRITE [id]t' #-}

postulate
  [][]tU  : ∀ {Γ Δ Σ : Con}{σ : Tms Γ Δ} {δ : Tms Δ Σ}(t : Tm Σ U)
          → t [ δ ]t [ σ ]t ≡ t [ δ ∘ σ ]t
  [][]tU'  : ∀ {Γ Δ Σ : Con}{σ : Tms Γ Δ} {δ : Tms Δ Σ}(t : Tm Σ U)
          → t [ δ ]t [ σ ]t ↦ t [ δ ∘ σ ]t
{-# REWRITE [][]tU' #-}

postulate
  [][]tEl : ∀ {Γ Δ Σ : Con}{σ : Tms Γ Δ} {δ : Tms Δ Σ}{a : Tm Σ U}(t : Tm Σ (El a))
          → t [ δ ]t [ σ ]t ≡ t [ δ ∘ σ ]t
  [][]tNe : {Γ Δ : Con} {Σ₁ : Con} {A : Ty Σ₁} {σ : Tms Γ Δ} {δ : Tms Δ Σ₁}
           {t : Tm Σ₁ A} →
           t [ δ ]t [ σ ]t ≡ t [ δ ∘ σ ]t
  [][]tEl' : ∀ {Γ Δ Σ : Con}{σ : Tms Γ Δ} {δ : Tms Δ Σ}{a : Tm Σ U}(t : Tm Σ (El a))
          → t [ δ ]t [ σ ]t ↦ t [ δ ∘ σ ]t
  [][]tNe' : {Γ Δ : Con} {Σ₁ : Con} {A : Ty Σ₁} {σ : Tms Γ Δ} {δ : Tms Δ Σ₁}
           {t : Tm Σ₁ A} →
           t [ δ ]t [ σ ]t ↦ t [ δ ∘ σ ]t
{-# REWRITE [][]tEl' [][]tNe' #-}

postulate
  vz<>Elσ : ∀ {Γ Δ}{a}{t : Tm Γ (El a)}{σ : Tms Δ Γ}
        → vz [ < t [ σ ]t > ]t ≡ t [ σ ]t
  vz<>Elσ' : ∀ {Γ Δ}{a}{t : Tm Γ (El a)}{σ : Tms Δ Γ}
        → vz [ < t [ σ ]t > ]t ↦ t [ σ ]t
{-# REWRITE vz<>Elσ' #-}

postulate
  π₂∘U  : ∀{Γ Δ Σ}{σ : Tms Δ (Σ ▶ U)}{δ : Tms Γ Δ}
        → _[_]t {Γ}{Δ}{U}(π₂ σ) δ ≡ π₂ (σ ∘ δ)
  π₂∘U'  : ∀{Γ Δ Σ}{σ : Tms Δ (Σ ▶ U)}{δ : Tms Γ Δ}
        → _[_]t {Γ}{Δ}{U}(π₂ σ) δ ↦ π₂ (σ ∘ δ)
{-# REWRITE π₂∘U' #-}

postulate
  π₂∘El : ∀{Γ Δ Σ}{a}{σ : Tms Δ (Σ ▶ El a)}{δ : Tms Γ Δ}
        → _[_]t {Γ}{Δ}{El (a [ π₁ σ ]t)} (π₂ σ) δ ≡ π₂ (σ ∘ δ)
  π₂∘El' : ∀{Γ Δ Σ}{a}{σ : Tms Δ (Σ ▶ El a)}{δ : Tms Γ Δ}
        → _[_]t {Γ}{Δ}{El (a [ π₁ σ ]t)} (π₂ σ) δ ↦ π₂ (σ ∘ δ)
{-# REWRITE π₂∘El' #-}

{-# REWRITE appNI[]' #-}
