{-# OPTIONS --rewriting #-}

module WeakInitiality.ElU where

open import StrictLib hiding (id; _∘_)
import Syntax as S
import AM as AM
open import AM using (Conʳ; Tyʳ; Tmʳ; Tmsʳ)
open import WeakInitiality.CwFOps

U : ∀{Γ} → Ty Γ
U {Γ , Γᶜ} =
  S.U , (λ σ* a → S.Tm Γ* (S.El a) , λ t → lower (Tmʳ t .₁ γ*))

postulate -- for typechecking performance only. Remove the postulate and uncomment proof to check.
 U[]  : {Γ Δ : Con} {σ : Tms Γ Δ} → _≡_ {i} {Ty Γ} (_[_]T {Γ} {Δ} (U {Δ}) σ) (U {Γ})
 -- U[] {Γ , Γᶜ}{Δ , Δᶜ}{σ , σᶜ} =
 --  ,≡ refl
 --  (ext λ σ* → ext λ t →
 --    J (λ _ eq' → S.Tm Γ* (S.El t) ,
 --         coe (J (λ foo eq → (S.Tm Γ* (S.El t) → Tmʳ t .₁ γ*) ≡ (S.Tm Γ*
 --         (S.El t) → Tmʳ t .₁ γ*)) refl eq') (λ t₁ → lower (Tmʳ t₁
 --         .₁ γ*)) ≡ S.Tm Γ* (S.El t) , (λ t₁ → lower (Tmʳ t₁ .₁ γ*)))
 --       refl
 --       (σᶜ σ* .₁))

El : ∀{Γ}(a : Tm Γ U) → Ty Γ
El {Γ , Γᶜ}(a , aᶜ) =
  (S.El a) ,
  (λ σ* t →
    lift (coe (aᶜ σ* .₁ ) t) ,
    (  happly (aᶜ σ* .₂) (coe (aᶜ σ* .₁) t) ⁻¹
     ◾ J (λ _ eq → coe ((λ x → x → ₁ (Tmʳ a) (₁ (Tmsʳ σ*) γ*)) & eq)
            (λ t₁ → lower (Tmʳ t₁ .₁ γ*)) (coe eq t)
            ≡ lower (Tmʳ t .₁ γ*))
          refl
          (aᶜ σ* .₁)))

El[] :
  {Γ Δ : Con} {σ : Tms Γ Δ} {a : Tm Δ (U {Δ})} →
  _≡_ {_} {Ty Γ} (_[_]T {Γ} {Δ} (El {Δ} a) σ)
  (El {Γ}
   (coe {_} {Tm Γ (_[_]T {Γ} {Δ} (U {Δ}) σ)} {Tm Γ (U {Γ})}
    (_&_ {_} {suc _} {Ty Γ} {_} (Tm Γ)
     {_[_]T {Γ} {Δ} (U {Δ}) σ} {U {Γ}} (U[] {Γ} {Δ} {σ}))
    (_[_]t {Γ} {Δ} {U {Δ}} a σ)))
El[] {Γ , Γᶜ}{Δ , Δᶜ}{σ , σᶜ}{a , aᶜ} = cheat -- too slow to normalize goal
