{-# OPTIONS --rewriting #-}

module WeakInitiality.PiSmallNI where

open import StrictLib hiding (id; _∘_)
import Syntax as S
import AM as AM
open import AM using (Conʳ; Tyʳ; Tmʳ; Tmsʳ)

open import WeakInitiality.CwFOps
open import WeakInitiality.ElU

Πₙᵢ : ∀ {Γ}(A : Set) → (A → Tm Γ U) → Tm Γ U
Πₙᵢ {Γ , Γᶜ} A b =
  (S.Πₙᵢ A (λ α → b α .₁)) ,
  λ σ* →
    {!!} , -- ??
    {!!}
