{-# OPTIONS --rewriting #-}

module WeakInitiality.CwFLaws where

open import StrictLib hiding (id; _∘_)
import Syntax as S
import AM as AM
open import AM using (Conʳ; Tyʳ; Tmʳ; Tmsʳ)

open import WeakInitiality.CwFOps

-- Tm and Tms equalities are all trivial because of UIP + funext
-- So, we only need type equalities.
--------------------------------------------------------------------------------

[id]T : ∀{Γ}{A : Ty Γ} → A [ id ]T ≡ A
[id]T {Γ , Γᶜ}{A , Aᶜ} = refl

[][]T : {Γ Δ : Con} {Σ : Con} {A : Ty Σ} {σ : Tms Γ Δ}
    {δ : Tms Δ Σ} →
    _≡_ {_} {Ty Γ} (_[_]T {Γ} {Δ} (_[_]T {Δ} {Σ} A δ) σ)
    (_[_]T {Γ} {Σ} A (_∘_ {Γ} {Δ} {Σ} δ σ))
[][]T {Γ , Γᵀ}{Δ , Δᵀ}{Σ , Σᵀ}{A , Aᵀ}{σ , σᵀ}{δ , δᵀ} =
  ,≡ refl
  (ext λ σ* → ext λ t →
  ,≡ (coe∘ ((λ γ → ₁ (Tyʳ A) (₁ (Tmsʳ δ) γ)) & σᵀ σ* .₁)
           (Tyʳ A .₁ & δᵀ (σ S.∘ σ*) .₁)
           (Aᵀ (δ S.∘ (σ S.∘ σ*)) t .₁)
           ◾ coe-coe _ _ (Aᵀ (δ S.∘ (σ S.∘ σ*)) t .₁))
   (uñ (uncoe
      ((λ Aᶜ → ₂ (Tyʳ A) (₂ (Tmsʳ δ) (₂ (Tmsʳ σ) (Γᵀ σ* .₂))) Aᶜ (Tmʳ
          t .₁ γ*)) & (coe∘ ((λ γ → ₁ (Tyʳ A) (₁ (Tmsʳ δ) γ)) & σᵀ σ*
          .₁) (Tyʳ A .₁ & δᵀ (σ S.∘ σ*) .₁) (Aᵀ (δ S.∘ (σ S.∘ σ*)) t
          .₁) ◾ coe-coe (Tyʳ A .₁ & δᵀ (σ S.∘ σ*) .₁ ◾ (λ γ → ₁ (Tyʳ
          A) (₁ (Tmsʳ δ) γ)) & σᵀ σ* .₁) (Tyʳ A .₁ & (δᵀ (σ S.∘ σ*) .₁
          ◾ Tmsʳ δ .₁ & σᵀ σ* .₁)) (Aᵀ (δ S.∘ (σ S.∘ σ*)) t .₁)))
          _
      ◾̃ uncoe
        (J (λ foo eq → ₂ (Tyʳ A) (₂ (Tmsʳ δ) (Δᵀ (σ S.∘ σ*) .₂)) (₁ (₂
         ((A , Aᵀ) [ δ , δᵀ ]T) (σ S.∘ σ*) t)) (Tmʳ t .₁ γ*) ≡ ₂ (Tyʳ A)
         (₂ (Tmsʳ δ) (coe ((λ x → Conʳ Δ .₂ x (₁ (Tmsʳ σ) (₁ (Tmsʳ σ*)
         γ*))) & eq) (Δᵀ (σ S.∘ σ*) .₂))) (coe ((λ γ → ₁ (Tyʳ A) (₁
         (Tmsʳ δ) γ)) & eq) (₂ ((A , Aᵀ) [ δ , δᵀ ]T) (σ S.∘ σ*) t .₁))
         (Tmʳ t .₁ γ*)) refl (σᵀ σ* .₁) ◾ (λ x → ₂ (Tyʳ A) (₂ (Tmsʳ δ)
         x) (coe ((λ γ → ₁ (Tyʳ A) (₁ (Tmsʳ δ) γ)) & σᵀ σ* .₁) (₂ ((A ,
         Aᵀ) [ δ , δᵀ ]T) (σ S.∘ σ*) t .₁)) (Tmʳ t .₁ γ*)) & σᵀ σ* .₂)
         _
      ◾̃ uncoe
        (J (λ foo eq → Tyʳ A .₂ (Σᵀ (δ S.∘ (σ S.∘ σ*)) .₂) (₁ (Aᵀ (δ S.∘
         (σ S.∘ σ*)) t)) (Tmʳ t .₁ γ*) ≡ ₂ (Tyʳ A) (coe ((λ x → Conʳ Σ
         .₂ x (₁ (Tmsʳ δ) (₁ (Tmsʳ σ) (₁ (Tmsʳ σ*) γ*)))) & eq) (Σᵀ (δ
         S.∘ (σ S.∘ σ*)) .₂)) (coe (Tyʳ A .₁ & eq) (Aᵀ (δ S.∘ (σ S.∘
         σ*)) t .₁)) (Tmʳ t .₁ γ*)) refl (δᵀ (σ S.∘ σ*) .₁) ◾ (λ x → ₂
         (Tyʳ A) x (coe (Tyʳ A .₁ & δᵀ (σ S.∘ σ*) .₁) (Aᵀ (δ S.∘ (σ S.∘
         σ*)) t .₁)) (Tmʳ t .₁ γ*)) & δᵀ (σ S.∘ σ*) .₂)
         _
      ◾̃
        uncoe _ (Aᵀ (δ S.∘ (σ S.∘ σ*)) t .₂) ⁻¹̃
      )))
