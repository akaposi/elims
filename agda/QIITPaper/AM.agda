{-# OPTIONS --rewriting --without-K #-}

module AM where

open import Lib
open import JM

infixl 5 _▷_
infixl 7 _[_]T
infixl 5 _,_
infix  6 _∘_
infixl 8 _[_]t
infix 4 <_>
infixl 5 _^_

i : Level
i = suc (suc zero)

j : Level
j = (suc zero)

------------------------------------------------------------
-- substitution calculus
------------------------------------------------------------

Con : Set i
Con = Σ Set₁ λ Γᴬ → Γᴬ → Γᴬ → Set

Ty : Con → Set i
Ty (Γᴬ Σ, Γᴹ) = Σ (Γᴬ → Set₁) λ Aᴬ → {γ₀ γ₁ : Γᴬ} → Γᴹ γ₀ γ₁ → Aᴬ γ₀ → Aᴬ γ₁ → Set

Tms : Con → Con → Set j
Tms (Γᴬ Σ, Γᴹ) (Δᴬ Σ, Δᴹ) = Σ (Γᴬ → Δᴬ) λ σᴬ → {γ₀ γ₁ : Γᴬ}(γᴹ : Γᴹ γ₀ γ₁) → Δᴹ (σᴬ γ₀) (σᴬ γ₁)

Tm : (Γ : Con) → Ty Γ → Set j
Tm (Γᴬ Σ, Γᴹ) (Aᴬ Σ, Aᴹ) = Σ ((γ : Γᴬ) → Aᴬ γ) λ tᴬ → {γ₀ γ₁ : Γᴬ}(γᴹ : Γᴹ γ₀ γ₁) → Aᴹ γᴹ (tᴬ γ₀) (tᴬ γ₁)

∙ : Con
∙ = Lift ⊤ Σ, λ _ _ → ⊤

_▷_ : (Γ : Con) → Ty Γ → Con
(Γᴬ Σ, Γᴹ) ▷ (Aᴬ Σ, Aᴹ) = Σ Γᴬ Aᴬ Σ, λ { (γ₀ Σ, α₀) (γ₁ Σ, α₁) → Σ (Γᴹ γ₀ γ₁) λ γᴹ → Aᴹ γᴹ α₀ α₁}

_[_]T : ∀{Γ Δ} → Ty Δ → Tms Γ Δ → Ty Γ
(Aᴬ Σ, Aᴹ) [ σᴬ Σ, σᴹ ]T = (λ γ → Aᴬ (σᴬ γ)) Σ, λ γᴹ α₀ α₁ → Aᴹ (σᴹ γᴹ) α₀ α₁

id : ∀{Γ} → Tms Γ Γ
id = (λ γ → γ) Σ, λ γᴹ → γᴹ

_∘_ : ∀{Γ Θ Δ} → Tms Θ Δ → Tms Γ Θ → Tms Γ Δ
(σᴬ Σ, σᴹ) ∘ (δᴬ Σ, δᴹ) = (λ γ → σᴬ (δᴬ γ)) Σ, λ γᴹ → σᴹ (δᴹ γᴹ)

ε : ∀{Γ} → Tms Γ ∙
ε = (λ _ → lift tt) Σ, λ _ → tt

_,_  : ∀{Γ Δ A}(σ : Tms Γ Δ) → Tm Γ (A [ σ ]T) → Tms Γ (Δ ▷ A)
(σᴬ Σ, σᴹ) , (tᴬ Σ, tᴹ) = (λ γ → σᴬ γ Σ, tᴬ γ) Σ, λ γᴹ → σᴹ γᴹ Σ, tᴹ γᴹ

π₁ : ∀{Γ Δ}{A : Ty Δ} → Tms Γ (Δ ▷ A) → Tms Γ Δ
π₁ (σᴬ Σ, σᴹ) = (λ γ → proj₁ (σᴬ γ)) Σ, λ γᴹ → proj₁ (σᴹ γᴹ)

π₂ : ∀{Γ Δ}{A : Ty Δ}(σ : Tms Γ (Δ ▷ A)) → Tm Γ (A [ π₁ {A = A} σ ]T)
π₂ (σᴬ Σ, σᴹ) = (λ γ → proj₂ (σᴬ γ)) Σ, λ γᴹ → proj₂ (σᴹ γᴹ)

_[_]t : ∀{Γ Δ}{A : Ty Δ} → Tm Δ A → (σ : Tms Γ Δ) → Tm Γ (A [ σ ]T)
(tᴬ Σ, tᴹ) [ (σᴬ Σ, σᴹ) ]t = (λ γ → tᴬ (σᴬ γ)) Σ, λ γᴹ → tᴹ (σᴹ γᴹ)

[id]T : ∀{Γ}{A : Ty Γ} → A [ id ]T ≡ A
[id]T = refl

[][]T : ∀{Γ Θ Δ}{A : Ty Δ}{σ : Tms Θ Δ}{δ : Tms Γ Θ} → A [ σ ]T [ δ ]T ≡ A [ _∘_ {Γ}{Θ}{Δ} σ δ ]T
[][]T = refl

idl : ∀{Γ Δ}{δ : Tms Γ Δ} → (_∘_ {Γ}{Δ}{Δ} id δ) ≡ δ
idl = refl

idr : ∀{Γ Δ}{δ : Tms Γ Δ} → (_∘_ {Γ}{Γ}{Δ} δ id) ≡ δ
idr = refl

ass : ∀{Γ Δ Σ Ω}{σ : Tms Σ Ω}{δ : Tms Δ Σ}{ν : Tms Γ Δ}
      → _∘_ {Γ}{Δ}{Ω}(_∘_ {Δ}{Σ}{Ω} σ δ) ν ≡ _∘_ {Γ}{Σ}{Ω} σ (_∘_ {Γ}{Δ}{Σ} δ ν)
ass = refl

,∘ : ∀{Γ Θ Δ}{σ : Tms Θ Δ}{δ : Tms Γ Θ}{A : Ty Δ}{t : Tm Θ (A [ σ ]T)} →
  (_∘_ {Γ}{Θ}{Δ ▷ A} (_,_ {Θ}{Δ}{A} σ t) δ) ≡ (_,_ {Γ}{Δ}{A}(_∘_ {Γ}{Θ}{Δ} σ δ)(_[_]t {Γ}{Θ}{A [ σ ]T} t δ))
,∘ = refl

▷β₁ : ∀{Γ Δ}{A : Ty Δ}{δ : Tms Γ Δ}{a : Tm Γ (A [ δ ]T)}
      → (π₁ {Γ}{Δ}{A}(_,_ {Γ}{Δ}{A} δ a)) ≡ δ
▷β₁ = refl

πη : ∀{Γ Δ}{A : Ty Δ}{δ : Tms Γ (Δ ▷ A)}
      → _,_ {Γ}{Δ}{A}(π₁ {Γ}{Δ}{A} δ)(π₂ {Γ}{Δ}{A} δ) ≡ δ
πη = refl

εη : ∀{Γ}{σ : Tms Γ ∙}
      → σ ≡ ε
εη = refl

▷β₂ : ∀{Γ Δ}{A : Ty Δ}{δ : Tms Γ Δ}{a : Tm Γ (A [ δ ]T)}
      → tr (λ x → Tm Γ (A [ x ]T)) (▷β₁ {Γ}{Δ}{A}{δ}{a}) (π₂ {Γ}{Δ}{A}(_,_ {Γ}{Δ}{A} δ a)) ≡ a
▷β₂ = refl

wk : ∀{Γ}{A : Ty Γ} → Tms (Γ ▷ A) Γ
wk {Γ}{A} = π₁ {Γ ▷ A}{Γ}{A} id

vz : ∀{Γ}{A : Ty Γ} → Tm (Γ ▷ A) (A [ wk ]T)
vz {Γ}{A} = π₂ {Γ ▷ A}{Γ}{A} id

vs : ∀{Γ}{A B : Ty Γ} → Tm Γ A → Tm (Γ ▷ B) (A [ wk ]T)
vs {Γ}{A}{B} x = _[_]t {Γ ▷ B}{Γ}{A} x (wk {Γ}{B})

<_> : ∀{Γ}{A : Ty Γ} → Tm Γ A → Tms Γ (Γ ▷ A)
<_> {Γ}{A} t = _,_ {Γ}{Γ}{A} id (tr (Tm Γ) ([id]T {Γ}{A} ⁻¹) t)

_^_ : {Γ Δ : Con}(σ : Tms Γ Δ)(A : Ty Δ) → Tms (Γ ▷ A [ σ ]T) (Δ ▷ A)
_^_ {Γ}{Δ} σ A = _,_ {Γ ▷ A [ σ ]T}{Δ}{A} (_∘_ {Γ ▷ A [ σ ]T}{Γ}{Δ} σ wk) vz

------------------------------------------------------------
-- universe
------------------------------------------------------------

U : ∀{Γ} → Ty Γ
U {Γᴬ Σ, Γᴹ} = (λ _ → Set) Σ, λ _ a₀ a₁ → a₀ → a₁

U[] : ∀{Γ Δ}{σ : Tms Γ Δ} → _[_]T {Γ}{Δ} U σ ≡ U
U[] = refl

El : ∀{Γ}(a : Tm Γ U) → Ty Γ
El (aᴬ Σ, aᴹ) = (λ γ → Lift (aᴬ γ)) Σ, λ { γᴹ (lift x₀) (lift x₁) → aᴹ γᴹ x₀ ≡ x₁ }

El[] : ∀{Γ Δ}{σ : Tms Γ Δ}{a : Tm Δ U}
     → El a [ σ ]T ≡ El (tr (Tm Γ) (U[] {Γ}{Δ}{σ}) (_[_]t {Γ}{Δ}{U} a σ))
El[] = refl

------------------------------------------------------------
-- function space with small codomain
------------------------------------------------------------

Π : ∀{Γ}(a : Tm Γ U)(B : Ty (Γ ▷ El a)) → Ty Γ
Π (aᴬ Σ, aᴹ) (Bᴬ Σ, Bᴹ)
  = (λ γ → (α : aᴬ γ) → Bᴬ (γ Σ, lift α))
  Σ, λ {γ₀}{γ₁} γᴹ f₀ f₁ → (x₀ : aᴬ γ₀) → Bᴹ (γᴹ Σ, refl) (f₀ x₀) (f₁ (aᴹ γᴹ x₀))

Π[] : ∀{Γ Δ}{σ : Tms Γ Δ}{a : Tm Δ U}{B : Ty (Δ ▷ El a)}
    → (Π a B) [ σ ]T ≡ Π (tr (Tm Γ) (U[] {Γ}{Δ}{σ}) (_[_]t {Γ}{Δ}{U} a σ))
                         (tr (λ x → Ty (Γ ▷ x)) (El[] {σ = σ}{a}) (B [ σ ^ El a ]T))
Π[] = refl

app : ∀{Γ}{a : Tm Γ U}{B : Ty (Γ ▷ El a)} → Tm Γ (Π a B) → Tm (Γ ▷ El a) B
app {Γᴬ Σ, Γᴹ}{aᴬ Σ, aᴹ}{Bᴬ Σ, Bᴹ}(tᴬ Σ, tᴹ) =
  (λ { (γ Σ, lift α) → tᴬ γ α }) Σ,
  λ { {γ Σ, lift α}{γ' Σ, lift α'}(γᴹ Σ, αᴹ) → J (λ x z → Bᴹ (γᴹ Σ, z) (tᴬ γ α) (tᴬ γ' x)) (tᴹ γᴹ α) αᴹ }

app[] : ∀{Γ}{a : Tm Γ U}{B : Ty (Γ ▷ El a)}{t : Tm Γ (Π a B)}{Θ : Con}{σ : Tms Θ Γ} →
  _≡_ {A = Tm (Θ ▷ El a [ σ ]T) (B [ σ ^ El a ]T)}
      (_[_]t {A = B} (app {B = B} t) (σ ^ El a))
      (app {B = B [ σ ^ El a ]T}(_[_]t {A = Π a B} t σ))
app[] = refl

------------------------------------------------------------
-- identity type
------------------------------------------------------------

Id : ∀{Γ}(a : Tm Γ U)(t u : Tm Γ (El a)) → Ty Γ
Id {Γᴬ Σ, Γᴹ} (aᴬ Σ, aᴹ) (tᴬ Σ, tᴹ) (uᴬ Σ, uᴹ) =
  (λ γ → tᴬ γ ≡ uᴬ γ) Σ,
  (λ {γ}{γ'} γᴹ e e' → ⊤)

reflect : ∀{Γ a}{t u : Tm Γ (El a)} → Tm Γ (Id a t u) → t ≡ u
reflect {Γ}{a}{tᴬ Σ, tᴹ}{uᴬ Σ, uᴹ}(eᴬ Σ, eᴹ) = Σ,=
  (ext λ γ → eᴬ γ)
  (exti λ γ → exti λ γ' → ext λ γᴹ → UIP _ _)

Id[] : ∀{Γ}{a : Tm Γ U}{t u : Tm Γ (El a)}{Θ : Con}{σ : Tms Θ Γ} →
  (Id a t u) [ σ ]T ≡ Id (_[_]t {A = U} a σ) (_[_]t {A = El a} t σ) (_[_]t {A = El a} u σ)
Id[] = refl

------------------------------------------------------------
-- function space with metatheoretic domain
------------------------------------------------------------

Π' : ∀{Γ}(T : Set)(B : T → Ty Γ) → Ty Γ
Π' T B =
  (λ γ → (α : T) → proj₁ (B α) γ) Σ,
  (λ γᴹ f f' → (α : T) → proj₂ (B α) γᴹ (f α) (f' α))

Π'[] : ∀{Γ}{T : Set}{B : T → Ty Γ}{Θ : Con}{σ : Tms Θ Γ} → (Π' T B) [ σ ]T ≡ Π' T (λ α → B α [ σ ]T)
Π'[] = refl

app' : ∀{Γ}{T : Set}{B : T → Ty Γ}(t : Tm Γ (Π' T B))(α : T) → Tm Γ (B α)
app' (tᴬ Σ, tᴹ) α =
  (λ γ → tᴬ γ α) Σ,
  (λ γᴹ → tᴹ γᴹ α)

app'[] : ∀{Γ}{T : Set}{B : T → Ty Γ}{t : Tm Γ (Π' T B)}{α : T}{Θ : Con}{σ : Tms Θ Γ} →
  _[_]t {A = B α} (app' {B = B} t α) σ ≡ app' {B = λ α → B α [ σ ]T} (_[_]t {A = Π' T B} t σ) α
app'[] = refl

------------------------------------------------------------
-- function space with metatheoretic domain and small codomain
------------------------------------------------------------

Π'' : ∀{Γ}(T : Set)(b : T → Tm Γ U) → Tm Γ U
Π'' T b =
  (λ γ → (α : T) → proj₁ (b α) γ) Σ,
  (λ γᴹ f α → proj₂ (b α) γᴹ (f α))

Π''[] : ∀{Γ}{T : Set}{b : T → Tm Γ U}{Θ : Con}{σ : Tms Θ Γ} →
  _[_]t {A = U} (Π'' T b) σ ≡ Π'' T (λ α → _[_]t {A = U} (b α) σ)
Π''[] = refl

app'' : ∀{Γ}{T : Set}{b : T → Tm Γ U}(t : Tm Γ (El (Π'' T b)))(α : T) → Tm Γ (El (b α))
app'' (tᴬ Σ, tᴹ) α =
  (λ γ → lift (lower (tᴬ γ) α)) Σ,
  (λ γᴹ → ap (λ f → f α) (tᴹ γᴹ))

app''[] : ∀{Γ}{T : Set}{b : T → Tm Γ U}{t : Tm Γ (El (Π'' T b))}{α : T}{Θ : Con}{σ : Tms Θ Γ} →
  _[_]t {A = El (b α)} (app'' {b = b} t α) σ ≡ app'' {b = λ α → _[_]t {A = U} (b α) σ } (_[_]t {A = El (Π'' T b)} t σ) α
app''[] = refl

------------------------------------------------------------
-- old version of transport (not necessary because of reflect)
------------------------------------------------------------

transp : ∀{Γ}{a : Tm Γ U}(P : Ty (Γ ▷ El a)){t u : Tm Γ (El a)}(e : Tm Γ (Id a t u)) →
  Tm Γ (P [ <_> {A = El a} t ]T) → Tm Γ (P [ <_> {A = El a} u ]T)
transp {Γᴬ Σ, Γᴹ} {aᴬ Σ, aᴹ} (Pᴬ Σ, Pᴹ) {tᴬ Σ, tᴹ} {uᴬ Σ, uᴹ} (eᴬ Σ, eᴹ) (wᴬ Σ, wᴹ) =
  (λ γ → tr (λ x → Pᴬ (γ Σ, x)) (eᴬ γ) (wᴬ γ)) Σ,
  (λ {γ}{γ'} γᴹ →
    tr
      (λ z → Pᴹ (γᴹ Σ, z) (tr (λ x → Pᴬ (γ Σ, x)) (eᴬ γ) (wᴬ γ)) (tr (λ x → Pᴬ (γ' Σ, x)) (eᴬ γ') (wᴬ γ')))
      (UIP (ap (λ z → aᴹ γᴹ (lower z)) (eᴬ γ ⁻¹) ◾ tᴹ γᴹ ◾ ap lower (eᴬ γ')) (uᴹ γᴹ))
      (J (λ _ z → Pᴹ (γᴹ Σ, (ap (λ z → aᴹ γᴹ (lower z)) (eᴬ γ ⁻¹) ◾ tᴹ γᴹ ◾ ap lower z)) (tr (λ x → Pᴬ (γ Σ, x)) (eᴬ γ) (wᴬ γ)) (tr (λ x → Pᴬ (γ' Σ, x)) z (wᴬ γ')))
         (J (λ _ z → Pᴹ (γᴹ Σ, (ap (λ z → aᴹ γᴹ (lower z)) (z ⁻¹) ◾ tᴹ γᴹ ◾ refl)) (tr (λ x → Pᴬ (γ Σ, x)) z (wᴬ γ)) (wᴬ γ'))
            (tr (λ z → Pᴹ (γᴹ Σ, z) (wᴬ γ) (wᴬ γ')) (UIP _ _) (wᴹ γᴹ))
            (eᴬ γ))
         (eᴬ γ')))

transp[] : ∀{Γ}{a : Tm Γ U}{P : Ty (Γ ▷ El a)}{t u : Tm Γ (El a)}{e : Tm Γ (Id a t u)}
  {w : Tm Γ (P [ <_> {A = El a} t ]T)}{Θ : Con}{σ : Tms Θ Γ} →
  _[_]t {A = P [ <_> {A = El a} u ]T} (transp P e w) σ ≡
  transp (P [ σ ^ El a ]T) (_[_]t e σ) (_[_]t {A = P [ <_> {A = El a} t ]T} w σ)
transp[] = refl

------------------------------------------------------------
-- old identity type (in the universe)
------------------------------------------------------------

id' : ∀{Γ}(a : Tm Γ U)(t u : Tm Γ (El a)) → Tm Γ U
id' {Γᴬ Σ, Γᴹ} (aᴬ Σ, aᴹ) (tᴬ Σ, tᴹ) (uᴬ Σ, uᴹ) =
  (λ γ → lower (tᴬ γ) ≡ lower (uᴬ γ)) Σ,
  (λ {γ}{γ'} γᴹ e → tᴹ γᴹ ⁻¹ ◾ ap (aᴹ γᴹ) e ◾ uᴹ γᴹ)

refl' : ∀{Γ}{a : Tm Γ U}(t : Tm Γ (El a)) → Tm Γ (El (id' a t t))
refl' {Γᴬ Σ, Γᴹ} {aᴬ Σ, aᴹ} (tᴬ Σ, tᴹ) =
  (λ γ → lift refl) Σ,
  (λ {γ}{γ'} γᴹ → inv (tᴹ γᴹ))

transp' : ∀{Γ}{a : Tm Γ U}(p : Tm (Γ ▷ El a) U){t u : Tm Γ (El a)}(e : Tm Γ (El (id' a t u))) →
  Tm Γ (El p [ <_> {A = El a} t ]T) → Tm Γ (El p [ <_> {A = El a} u ]T)
transp' {Γᴬ Σ, Γᴹ} {aᴬ Σ, aᴹ} (pᴬ Σ, pᴹ) {tᴬ Σ, tᴹ} {uᴬ Σ, uᴹ} (eᴬ Σ, eᴹ) (vᴬ Σ, vᴹ) =
  (λ γ → lift (tr (λ x → pᴬ (γ Σ, x)) (ap lift (lower (eᴬ γ))) (lower (vᴬ γ)))) Σ,
  (λ {γ}{γ'} γᴹ →
    J (λ x z → pᴹ (γᴹ Σ, z) (tr (λ x → pᴬ (γ Σ, x)) (ap lift (lower (eᴬ γ))) (lower (vᴬ γ))) ≡ tr (λ x → pᴬ (γ' Σ, x)) (ap lift (tᴹ γᴹ ⁻¹ ◾ ap (aᴹ γᴹ) (lower (eᴬ γ)) ◾ z)) (pᴹ (γᴹ Σ, tᴹ γᴹ) (lower (vᴬ γ))))
      (J (λ x z → pᴹ (γᴹ Σ, refl) (tr (λ x → pᴬ (γ Σ, x)) (ap lift (lower (eᴬ γ))) (lower (vᴬ γ))) ≡ tr (λ x → pᴬ (γ' Σ, x)) (ap lift (z ⁻¹ ◾ ap (aᴹ γᴹ) (lower (eᴬ γ)) ◾ refl)) (pᴹ (γᴹ Σ, z) (lower (vᴬ γ))))
        (J (λ x z → pᴹ (γᴹ Σ, refl) (tr (λ x → pᴬ (γ Σ, x)) (ap lift z) (lower (vᴬ γ))) ≡ tr (λ x → pᴬ (γ' Σ, x)) (ap lift (ap (aᴹ γᴹ) z ◾ refl)) (pᴹ (γᴹ Σ, refl) (lower (vᴬ γ))))
          refl
          (lower (eᴬ γ)))
        (tᴹ γᴹ))
      (uᴹ γᴹ) ◾
    ap (λ z → tr (λ x → pᴬ (γ' Σ, x)) (ap lift z) (pᴹ (γᴹ Σ, tᴹ γᴹ) (lower (vᴬ γ)))) (eᴹ γᴹ) ◾
    ap (tr (λ x → pᴬ (γ' Σ, x)) (ap lift (lower (eᴬ γ')))) (vᴹ γᴹ))

id'[] : ∀{Γ}{a : Tm Γ U}{t u : Tm Γ (El a)}{Θ : Con}{σ : Tms Θ Γ} → _[_]t {A = U} (id' a t u) σ ≡ id' (_[_]t {A = U} a σ) (_[_]t {A = El a} t σ) (_[_]t {A = El a} u σ)
id'[] = refl

refl'[] : ∀{Γ}{a : Tm Γ U}{t : Tm Γ (El a)}{Θ : Con}{σ : Tms Θ Γ} → _[_]t {A = El (id' a t t)} (refl' {a = a} t) σ ≡ refl' {a = _[_]t {A = U} a σ} (_[_]t {A = El a} t σ)
refl'[] = refl

transp'[] : ∀{Γ}{a : Tm Γ U}{p : Tm (Γ ▷ El a) U}{t u : Tm Γ (El a)}{e : Tm Γ (El (id' a t u))}{v : Tm Γ (El p [ <_> {A = El a} t ]T)}{Θ : Con}{σ : Tms Θ Γ} →
  _[_]t {A = El p [ <_> {A = El a} u ]T} (transp' p e v) σ ≡
  transp' {Θ}{_[_]t {A = U} a σ}(_[_]t {A = U} p (σ ^ El a)) (_[_]t {A = El (id' a t u)} e σ) (_[_]t {A = El p [ <_> {A = El a} t ]T} v σ)
transp'[] = refl

id'β : ∀{Γ}{a : Tm Γ U}{p : Tm (Γ ▷ El a) U}{t : Tm Γ (El a)}{v : Tm Γ (El p [ <_> {A = El a} t ]T)} →
  transp' p (refl' {a = a} t) v ≡ v
id'β {Γᴬ Σ, Γᴹ}{aᴬ Σ, aᴹ}{pᴬ Σ, pᴹ}{tᴬ Σ, tᴹ}{vᴬ Σ, vᴹ} =
  Σ,= refl
    (exti λ γ → exti λ γ' → ext λ γᴹ →
      ap (λ z' → (J (λ x z → pᴹ (γᴹ Σ, z) (lower (vᴬ γ)) ≡ tr (λ x₁ → pᴬ (γ' Σ, x₁)) (ap lift (tᴹ γᴹ ⁻¹ ◾ z)) (pᴹ (γᴹ Σ, tᴹ γᴹ) (lower (vᴬ γ)))) (J (λ x z → pᴹ (γᴹ Σ, refl) (lower (vᴬ γ)) ≡ tr (λ x₁ → pᴬ (γ' Σ, x₁)) (ap lift (z ⁻¹ ◾ refl)) (pᴹ (γᴹ Σ, z) (lower (vᴬ γ)))) refl (tᴹ γᴹ)) (tᴹ γᴹ) ◾ ap (λ z → tr (λ x → pᴬ (γ' Σ, x)) (ap lift z) (pᴹ (γᴹ Σ, tᴹ γᴹ) (lower (vᴬ γ)))) (inv (tᴹ γᴹ)) ◾ z')) (ap-id (vᴹ γᴹ)) ◾
      ass◾ (J (λ x z → pᴹ (γᴹ Σ, z) (lower (vᴬ γ)) ≡ tr (λ x₁ → pᴬ (γ' Σ, x₁)) (ap lift (tᴹ γᴹ ⁻¹ ◾ z)) (pᴹ (γᴹ Σ, tᴹ γᴹ) (lower (vᴬ γ)))) (J (λ x z → pᴹ (γᴹ Σ, refl) (lower (vᴬ γ)) ≡ tr (λ x₁ → pᴬ (γ' Σ, x₁)) (ap lift (z ⁻¹ ◾ refl)) (pᴹ (γᴹ Σ, z) (lower (vᴬ γ)))) refl (tᴹ γᴹ)) (tᴹ γᴹ)) (ap (λ z → tr (λ x → pᴬ (γ' Σ, x)) (ap lift z) (pᴹ (γᴹ Σ, tᴹ γᴹ) (lower (vᴬ γ)))) (inv (tᴹ γᴹ))) (vᴹ γᴹ) ⁻¹ ◾
      ap (λ z → z ◾ vᴹ γᴹ) (J (λ x' z' → (J (λ x z → pᴹ (γᴹ Σ, z) (lower (vᴬ γ)) ≡ tr (λ x₁ → pᴬ (γ' Σ, x₁)) (ap lift (z' ⁻¹ ◾ z)) (pᴹ (γᴹ Σ, z') (lower (vᴬ γ)))) (J (λ x z → pᴹ (γᴹ Σ, refl) (lower (vᴬ γ)) ≡ tr (λ x₁ → pᴬ (γ' Σ, x₁)) (ap lift (z ⁻¹ ◾ refl)) (pᴹ (γᴹ Σ, z) (lower (vᴬ γ)))) refl z') z' ◾ ap (λ z → tr (λ x → pᴬ (γ' Σ, x)) (ap lift z) (pᴹ (γᴹ Σ, z') (lower (vᴬ γ)))) (inv z')) ≡ refl) refl (tᴹ γᴹ)))
