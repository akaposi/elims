{-# OPTIONS --rewriting --without-K #-}

module AC where

open import Lib
open import JM

infixl 5 _▷_
infixl 7 _[_]T
infixl 5 _,_
infix  6 _∘_
infixl 8 _[_]t
infix 4 <_>
infixl 5 _^_

i : Level
i = suc (suc zero)

j : Level
j = (suc zero)

import Syntax as S

postulate
  Ω : S.Con

------------------------------------------------------------
-- substitution calculus
------------------------------------------------------------

Con : Set i
Con = Σ S.Con λ Γ → Σ Set₁ λ Γᴬ → S.Tms Ω Γ → Γᴬ

Ty : Con → Set i
Ty (Γ Σ, Γᴬ Σ, Γᶜ) = Σ (S.Ty Γ) λ A → Σ (Γᴬ → Set₁) λ Aᴬ → (ν : S.Tms Ω Γ) → S.Tm Ω (A S.[ ν ]T) → Aᴬ (Γᶜ ν)

Tms : Con → Con → Set j
Tms (Γ Σ, Γᴬ Σ, Γᶜ) (Δ Σ, Δᴬ Σ, Δᶜ) =
  Σ (S.Tms Γ Δ) λ σ → Σ (Γᴬ → Δᴬ) λ σᴬ → (ν : S.Tms Ω Γ) → Δᶜ (σ S.∘ ν) ≡ σᴬ (Γᶜ ν)

Tm : (Γ : Con) → Ty Γ → Set j
Tm (Γ Σ, Γᴬ Σ, Γᶜ) (A Σ, Aᴬ Σ, Aᶜ) =
  Σ (S.Tm Γ A) λ t → Σ ((γ : Γᴬ) → Aᴬ γ) λ tᴬ → (ν : S.Tms Ω Γ) → Aᶜ ν (t S.[ ν ]t) ≡ tᴬ (Γᶜ ν)

∙ : Con
∙ = S.∙ Σ, Lift ⊤ Σ, λ _ → lift tt

_▷_ : (Γ : Con) → Ty Γ → Con
(Γ Σ, Γᴬ Σ, Γᶜ) ▷ (A Σ, Aᴬ Σ, Aᶜ) =
  (Γ S.▷ A) Σ, Σ Γᴬ Aᴬ Σ, λ ν → Γᶜ (S.π₁ ν) Σ, Aᶜ (S.π₁ ν) (S.π₂ ν)

_[_]T : ∀{Γ Δ} → Ty Δ → Tms Γ Δ → Ty Γ
(A Σ, Aᴬ Σ, Aᶜ) [ σ Σ, σᴬ Σ, σᶜ ]T = A S.[ σ ]T Σ, (λ γ → Aᴬ (σᴬ γ)) Σ, λ ν t → tr Aᴬ (σᶜ ν) (Aᶜ (σ S.∘ ν) t)

id : ∀{Γ} → Tms Γ Γ
id = S.id Σ, (λ γ → γ) Σ, λ ν → refl

_∘_ : ∀{Γ Θ Δ} → Tms Θ Δ → Tms Γ Θ → Tms Γ Δ
(σ Σ, σᴬ Σ, σᶜ) ∘ (δ Σ, δᴬ Σ, δᶜ) = (σ S.∘ δ) Σ, (λ γ → σᴬ (δᴬ γ)) Σ, λ ν → σᶜ (δ S.∘ ν) ◾ ap σᴬ (δᶜ ν)

ε : ∀{Γ} → Tms Γ ∙
ε = S.ε Σ, (λ _ → lift tt) Σ, λ _ → refl

_,_  : ∀{Γ Δ A}(σ : Tms Γ Δ) → Tm Γ (A [ σ ]T) → Tms Γ (Δ ▷ A)
(σ Σ, σᴬ Σ, σᶜ) , (t Σ, tᴬ Σ, tᶜ) = (σ S., t) Σ, (λ γ → σᴬ γ Σ, tᴬ γ) Σ, λ ν → Σ,= (σᶜ ν) (tᶜ ν)

π₁ : ∀{Γ Δ}{A : Ty Δ} → Tms Γ (Δ ▷ A) → Tms Γ Δ
π₁ (σ Σ, σᴬ Σ, σᶜ) = S.π₁ σ Σ, (λ γ → proj₁ (σᴬ γ)) Σ, λ ν → ap proj₁ (σᶜ ν)

π₂ : ∀{Γ Δ}{A : Ty Δ}(σ : Tms Γ (Δ ▷ A)) → Tm Γ (A [ π₁ {A = A} σ ]T)
π₂ {Γ Σ, Γᴬ Σ, Γᶜ}{A = A Σ, Aᴬ Σ, Aᶜ}(σ Σ, σᴬ Σ, σᶜ) =
  S.π₂ σ Σ,
  (λ γ → proj₂ (σᴬ γ)) Σ,
  λ ν → J (λ x z → coe (ap Aᴬ (ap proj₁ z)) (Aᶜ (S.π₁ (σ S.∘ ν)) (S.π₂ (σ S.∘ ν))) ≡ proj₂ x) refl (σᶜ ν)

_[_]t : ∀{Γ Δ}{A : Ty Δ} → Tm Δ A → (σ : Tms Γ Δ) → Tm Γ (A [ σ ]T)
_[_]t {A = A Σ, Aᴬ Σ, Aᶜ}(t Σ, tᴬ Σ, tᶜ) (σ Σ, σᴬ Σ, σᶜ) =
  (t S.[ σ ]t) Σ, (λ γ → tᴬ (σᴬ γ)) Σ, λ ν → ap (tr Aᴬ (σᶜ ν)) (tᶜ (σ S.∘ ν)) ◾ apd tᴬ (σᶜ ν)

[id]T : ∀{Γ}{A : Ty Γ} → A [ id ]T ≡ A
[id]T {Γ Σ, Γᴬ Σ, Γᶜ}{A Σ, Aᴬ Σ, Aᶜ} = refl

[][]T : ∀{Γ Θ Δ}{A : Ty Δ}{σ : Tms Θ Δ}{δ : Tms Γ Θ}
        → A [ σ ]T [ δ ]T ≡ A [ _∘_ {Γ}{Θ}{Δ} σ δ ]T
[][]T {Γ = Γ Σ, Γᴬ Σ, Γᶜ}{A = A Σ, Aᴬ Σ, Aᶜ}{σ Σ, σᴬ Σ, σᶜ}{δ Σ, δᴬ Σ, δᶜ} = Σ,= refl (Σ,= refl (ext λ ν → ext λ t →
  J (λ x z → coe (ap (λ γ → Aᴬ (σᴬ γ)) z) (coe (ap Aᴬ (σᶜ (δ S.∘ ν))) (Aᶜ (σ S.∘ (δ S.∘ ν)) t)) ≡
             coe (ap Aᴬ (σᶜ (δ S.∘ ν) ◾ ap σᴬ z)) (Aᶜ (σ S.∘ (δ S.∘ ν)) t))
    (J (λ x z → coe (ap Aᴬ z) (Aᶜ (σ S.∘ (δ S.∘ ν)) t) ≡
                coe (ap Aᴬ (z ◾ refl)) (Aᶜ (σ S.∘ (δ S.∘ ν)) t))
       refl
       (σᶜ (δ S.∘ ν)))
    (δᶜ ν)))

idl : ∀{Γ Δ}{δ : Tms Γ Δ} → (_∘_ {Γ}{Δ}{Δ} id δ) ≡ δ
idl = Σ,= refl (Σ,= refl (ext λ ν → UIP _ _))

idr : ∀{Γ Δ}{δ : Tms Γ Δ} → (_∘_ {Γ}{Γ}{Δ} δ id) ≡ δ
idr = Σ,= refl (Σ,= refl (ext λ ν → UIP _ _))

ass : ∀{Γ Δ Σ Ω}{σ : Tms Σ Ω}{δ : Tms Δ Σ}{ν : Tms Γ Δ}
      → _∘_ {Γ}{Δ}{Ω}(_∘_ {Δ}{Σ}{Ω} σ δ) ν ≡ _∘_ {Γ}{Σ}{Ω} σ (_∘_ {Γ}{Δ}{Σ} δ ν)
ass = Σ,= refl (Σ,= refl (ext λ ν → UIP _ _))

,∘ : ∀{Γ Θ Δ}{σ : Tms Θ Δ}{δ : Tms Γ Θ}{A : Ty Δ}{t : Tm Θ (A [ σ ]T)} →
  (_∘_ {Γ}{Θ}{Δ ▷ A} (_,_ {Θ}{Δ}{A} σ t) δ) ≡
  (_,_ {Γ}{Δ}{A}(_∘_ {Γ}{Θ}{Δ} σ δ) (tr (Tm Γ) ([][]T {Γ}{Θ}{Δ}{A}{σ}{δ}) (_[_]t {Γ}{Θ}{A [ σ ]T} t δ)))
,∘ = {!Σ,= !}

▷β₁ : ∀{Γ Δ}{A : Ty Δ}{δ : Tms Γ Δ}{a : Tm Γ (A [ δ ]T)}
      → (π₁ {Γ}{Δ}{A}(_,_ {Γ}{Δ}{A} δ a)) ≡ δ
▷β₁ = Σ,= refl (Σ,= refl (ext λ ν → UIP _ _))

πη : ∀{Γ Δ}{A : Ty Δ}{δ : Tms Γ (Δ ▷ A)}
      → _,_ {Γ}{Δ}{A}(π₁ {Γ}{Δ}{A} δ)(π₂ {Γ}{Δ}{A} δ) ≡ δ
πη = Σ,= refl (Σ,= refl (ext λ ν → UIP _ _))

εη : ∀{Γ}{σ : Tms Γ ∙}
      → σ ≡ ε {Γ}
εη = Σ,= S.∙η (Σ,= refl (ext λ ν → UIP _ _))

▷β₂ : ∀{Γ Δ}{A : Ty Δ}{δ : Tms Γ Δ}{a : Tm Γ (A [ δ ]T)}
      → tr (λ x → Tm Γ (A [ x ]T)) (▷β₁ {Γ}{Δ}{A}{δ}{a}) (π₂ {Γ}{Δ}{A}(_,_ {Γ}{Δ}{A} δ a)) ≡ a
▷β₂ = Σ,= {!!} {!!}

wk : ∀{Γ}{A : Ty Γ} → Tms (Γ ▷ A) Γ
wk {Γ}{A} = π₁ {Γ ▷ A}{Γ}{A} id

vz : ∀{Γ}{A : Ty Γ} → Tm (Γ ▷ A) (_[_]T {Γ ▷ A}{Γ} A (wk {Γ}{A}))
vz {Γ}{A} = π₂ {Γ ▷ A}{Γ}{A} id

vs : ∀{Γ}{A B : Ty Γ} → Tm Γ A → Tm (Γ ▷ B) (_[_]T {Γ ▷ B}{Γ} A (wk {Γ}{B}))
vs {Γ}{A}{B} x = _[_]t {Γ ▷ B}{Γ}{A} x (wk {Γ}{B})

<_> : ∀{Γ}{A : Ty Γ} → Tm Γ A → Tms Γ (Γ ▷ A)
<_> {Γ}{A} t = _,_ {Γ}{Γ}{A} id (tr (Tm Γ) ([id]T {Γ}{A} ⁻¹) t)

_^_ : {Γ Δ : Con}(σ : Tms Γ Δ)(A : Ty Δ) → Tms (Γ ▷ A [ σ ]T) (Δ ▷ A)
_^_ {Γ}{Δ} σ A =
  _,_ {Γ ▷ A [ σ ]T}{Δ}{A}
       (_∘_ {Γ ▷ A [ σ ]T}{Γ}{Δ} σ (wk {Γ}{A [ σ ]T}))
       (tr (Tm (Γ ▷ A [ σ ]T)) ([][]T {Γ ▷ A [ σ ]T}{Γ}{Δ}{A}{σ}{wk {Γ}{A [ σ ]T}}) (vz {Γ}{A [ σ ]T}))

------------------------------------------------------------
-- universe
------------------------------------------------------------

U : ∀{Γ} → Ty Γ
U {Γ Σ, Γᴬ Σ, Γᶜ} = S.U Σ, (λ _ → Set) Σ, λ ν a → S.Tm Ω (S.El a)

U[] : ∀{Γ Δ}{σ : Tms Γ Δ} → _[_]T {Γ}{Δ} U σ ≡ U
U[] {σ = σ Σ, σᴬ Σ, σᶜ} = Σ,= refl (Σ,= refl (ext λ ν → ext λ a → J (λ x z → coe (ap (λ _ → Set) z) (S.Tm Ω (S.El a)) ≡ S.Tm Ω (S.El a)) refl (σᶜ ν)))

El : ∀{Γ}(a : Tm Γ U) → Ty Γ
El (a Σ, aᴬ Σ, aᶜ) = S.El a Σ, (λ γ → Lift (aᴬ γ)) Σ, λ ν t → lift (coe (aᶜ ν) t)

El[] : ∀{Γ Δ}{σ : Tms Γ Δ}{a : Tm Δ U}
     → El a [ σ ]T ≡ El (tr (Tm Γ) (U[] {Γ}{Δ}{σ}) (_[_]t {Γ}{Δ}{U} a σ))
El[] {Γ Σ, Γᴬ Σ, Γᶜ}{Δ Σ, Δᴬ Σ, Δᶜ}{σ Σ, σᴬ Σ, σᶜ}{a Σ, aᴬ Σ, aᶜ} =
  {!Σ,= ? ?!}

------------------------------------------------------------
-- function space with small codomain
------------------------------------------------------------

Π : ∀{Γ}(a : Tm Γ U)(B : Ty (Γ ▷ El a)) → Ty Γ
Π (a Σ, aᴬ Σ, aᶜ) (B Σ, Bᴬ Σ, Bᶜ)
  = S.Π a B
  Σ, (λ γ → (α : aᴬ γ) → Bᴬ (γ Σ, lift α))
  Σ, λ ν t α → let u = coe (aᶜ ν ⁻¹) α in coe {!!} (Bᶜ (ν S., u) (coe {!!} (S.app t u)))
{-
Π[] : ∀{Γ Δ}{σ : Tms Γ Δ}{a : Tm Δ U}{B : Ty (Δ ▷ El a)}
    → (Π a B) [ σ ]T ≡ Π (tr (Tm Γ) (U[] {Γ}{Δ}{σ}) (_[_]t {Γ}{Δ}{U} a σ))
                         (tr (λ x → Ty (Γ ▷ x)) (El[] {σ = σ}{a}) (B [ σ ^ El a ]T))
Π[] = refl

app : ∀{Γ}{a : Tm Γ U}{B : Ty (Γ ▷ El a)} → Tm Γ (Π a B) → (u : Tm Γ (El a)) → Tm Γ (B [ <_> {Γ}{El a} u ]T)
app {Γᴬ Σ, Γᴹ}{aᴬ Σ, aᴹ}{Bᴬ Σ, Bᴹ}(tᴬ Σ, tᴹ)(uᴬ Σ, uᴹ) =
  (λ γ → tᴬ γ (lower (uᴬ γ))) Σ,
  (λ {γ}{γ'} γᴹ → J (λ x z → Bᴹ (γᴹ Σ, z) (tᴬ γ (lower (uᴬ γ))) (tᴬ γ' x) ) (tᴹ γᴹ (lower (uᴬ γ))) (uᴹ γᴹ))

app[] : ∀{Γ}{a : Tm Γ U}{B : Ty (Γ ▷ El a)}{t : Tm Γ (Π a B)}{u : Tm Γ (El a)}{Θ : Con}{σ : Tms Θ Γ} →
  _[_]t {A = B [ <_> {Γ}{El a} u ]T} (app {B = B} t u) σ ≡ app {B = B [ σ ^ El a ]T} (_[_]t {A = Π a B} t σ) (_[_]t {A = El a} u σ)
app[] = refl
-}
------------------------------------------------------------
-- identity type
------------------------------------------------------------

Id : ∀{Γ}(a : Tm Γ U)(t u : Tm Γ (El a)) → Ty Γ
Id {Γ Σ, Γᴬ Σ, Γᶜ} (a Σ, aᴬ Σ, aᶜ) (t Σ, tᴬ Σ, tᶜ) (u Σ, uᴬ Σ, uᶜ) =
  S.Id a t u Σ,
  (λ γ → tᴬ γ ≡ uᴬ γ) Σ,
  λ ν e → tᶜ ν ⁻¹ ◾ ap (λ z → lift (coe (aᶜ ν) z)) (S.reflect e) ◾ uᶜ ν

reflect : ∀{Γ a}{t u : Tm Γ (El a)} → Tm Γ (Id a t u) → t ≡ u
reflect {Γ}{a}{t Σ, tᴬ Σ, tᶜ}{u Σ, uᴬ Σ, uᶜ}(e Σ, eᴬ Σ, eᶜ) = Σ,= (S.reflect e) (Σ,= (ext λ γ → {!? ◾ eᴬ γ!}) (ext λ ν → UIP _ _))
{-
Id[] : ∀{Γ}{a : Tm Γ U}{t u : Tm Γ (El a)}{Θ : Con}{σ : Tms Θ Γ} →
  (Id a t u) [ σ ]T ≡ Id (_[_]t {A = U} a σ) (_[_]t {A = El a} t σ) (_[_]t {A = El a} u σ)
Id[] = Σ,= ? ?
-}
------------------------------------------------------------
-- function space with metatheoretic domain
------------------------------------------------------------
{-
Π' : ∀{Γ}(T : Set)(B : T → Ty Γ) → Ty Γ
Π' T B =
  (λ γ → (α : T) → proj₁ (B α) γ) Σ,
  (λ γᴹ f f' → (α : T) → proj₂ (B α) γᴹ (f α) (f' α))

Π'[] : ∀{Γ}{T : Set}{B : T → Ty Γ}{Θ : Con}{σ : Tms Θ Γ} → (Π' T B) [ σ ]T ≡ Π' T (λ α → B α [ σ ]T)
Π'[] = refl

app' : ∀{Γ}{T : Set}{B : T → Ty Γ}(t : Tm Γ (Π' T B))(α : T) → Tm Γ (B α)
app' (tᴬ Σ, tᴹ) α =
  (λ γ → tᴬ γ α) Σ,
  (λ γᴹ → tᴹ γᴹ α)

app'[] : ∀{Γ}{T : Set}{B : T → Ty Γ}{t : Tm Γ (Π' T B)}{α : T}{Θ : Con}{σ : Tms Θ Γ} →
  _[_]t {A = B α} (app' {B = B} t α) σ ≡ app' {B = λ α → B α [ σ ]T} (_[_]t {A = Π' T B} t σ) α
app'[] = refl

------------------------------------------------------------
-- function space with metatheoretic domain and small codomain
------------------------------------------------------------

Π'' : ∀{Γ}(T : Set)(b : T → Tm Γ U) → Tm Γ U
Π'' T b =
  (λ γ → (α : T) → proj₁ (b α) γ) Σ,
  (λ γᴹ f α → proj₂ (b α) γᴹ (f α))

Π''[] : ∀{Γ}{T : Set}{b : T → Tm Γ U}{Θ : Con}{σ : Tms Θ Γ} →
  _[_]t {A = U} (Π'' T b) σ ≡ Π'' T (λ α → _[_]t {A = U} (b α) σ)
Π''[] = refl

app'' : ∀{Γ}{T : Set}{b : T → Tm Γ U}(t : Tm Γ (El (Π'' T b)))(α : T) → Tm Γ (El (b α))
app'' (tᴬ Σ, tᴹ) α =
  (λ γ → lift (lower (tᴬ γ) α)) Σ,
  (λ γᴹ → ap (λ f → f α) (tᴹ γᴹ))

app''[] : ∀{Γ}{T : Set}{b : T → Tm Γ U}{t : Tm Γ (El (Π'' T b))}{α : T}{Θ : Con}{σ : Tms Θ Γ} →
  _[_]t {A = El (b α)} (app'' {b = b} t α) σ ≡ app'' {b = λ α → _[_]t {A = U} (b α) σ } (_[_]t {A = El (Π'' T b)} t σ) α
app''[] = refl
-}
