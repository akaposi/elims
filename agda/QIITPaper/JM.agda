module JM where

open import Lib

loopcoe : ∀{ℓ}{A : Set ℓ}(p : A ≡ A){a : A} → coe p a ≡ a
loopcoe refl = refl

UIP : ∀{ℓ}{A : Set ℓ}{a a' : A}(p q : a ≡ a') → p ≡ q
UIP refl refl = refl
