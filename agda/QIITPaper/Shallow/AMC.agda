
module AMC where
open import StrictLib

Ty : Set₂
Ty = Set₁

U : Ty
U = Set

El : U → Ty
El A = Lift A

Π : (a : U)(B : El a → Ty) → Ty
Π a B = (x : a) → B (lift x)

app : ∀ {a B} → Π a B → (t : El a) → B t
app f t = f (lower t)

ΠN : (A : Set)(B : A → Ty) → Ty
ΠN A B = (x : A) → B x

appN : ∀ {A B} → ΠN A B → (x : A) → B x
appN t α = t α

Πn : (A : Set)(b : A → U) → U
Πn A b = (x : A) → b x

appn : ∀ {A b} → El (Πn A b) → (x : A) → El (b x)
appn t α = lift (lower t α)

Id : ∀ {a} → El a → El a → Ty
Id t u = t ≡ u

Reflect : ∀ {a t u} → Id {a} t u → t ≡ u
Reflect e = e

------------------------------------------------------------

Tyᴹ : Ty → Set₁
Tyᴹ A = A → A → Set

Uᴹ : U → Tyᴹ


-- Setᴹ : Set → Set₁
-- Setᴹ A = A → Set

-- Uᴹ : Set → Set₁
-- Uᴹ A = A → Set

-- Elᴹ : ∀{a}(aᴹ : Uᴹ a) → Setᴹ a -- Elᴹ is identity, so it is left implicit from now on
-- Elᴹ aᴹ x = aᴹ x

-- -- inductive functions

-- Πᴹ : ∀ {a}(aᴹ : Uᴹ a){B : a → Set}(Bᴹ : ∀ {x} (xₘ : aᴹ x) → Setᴹ (B x)) → Setᴹ (Π a B)
-- Πᴹ {a} aᴹ {B} Bᴹ f = (x : a)(xₘ : aᴹ x) → Bᴹ xₘ (f x)

-- appᴹ :
--   ∀ {a}(aᴹ : Uᴹ a){B : a → Set}(Bᴹ : ∀ {x}(xₘ : aᴹ x) → Setᴹ (B x))
--     {t : Π a B}(tᴹ : Πᴹ aᴹ Bᴹ t){u : a} (uᴹ : aᴹ u)
--   → Bᴹ uᴹ (t u)
-- appᴹ {a} aᴹ {B} Bᴹ {t} tᴹ {u} uᴹ = tᴹ u uᴹ

-- -- non-inductive functions

-- Πₙᵢᴹ : (A : Set){B : A → Set}(Bᴹ : ∀ a → Setᴹ (B a)) → Setᴹ (Π A B)
-- Πₙᵢᴹ A {B} Bᴹ f = ∀ a → Bᴹ a (f a)

-- appₙᵢᴹ :
--   ∀ (A : Set)
--     {B : A → Set}  (Bᴹ : ∀ a → Setᴹ (B a))
--     {t : Πₙᵢ A B}  (tᴹ : Πₙᵢᴹ A Bᴹ t)
--     (u : A)
--   → Bᴹ u (t u)
-- appₙᵢᴹ A {B} Bᴹ {t} tᴹ u = tᴹ u

-- -- small non-inductive functions (infinitary parameters)

-- Πₙᵢₛᴹ : (A : Set){b : A → U}(Bᴹ : ∀ a → Uᴹ (b a)) → Uᴹ (Πₙᵢₛ A b)
-- Πₙᵢₛᴹ A {B} bᴹ = λ f → ∀ a → bᴹ a (f a)

-- appₙᵢₛᴹ :
--   ∀ (A : Set)
--     {b : A → Set}  (bᴹ : ∀ a → Uᴹ (b a))
--     {t : Πₙᵢₛ A b} (tᴹ : Πₙᵢₛᴹ A bᴹ t)
--     (u : A)
--   → bᴹ u (t u)
-- appₙᵢₛᴹ A {B} _ {t} tᴹ u = tᴹ u


-- -- Identity

-- ≡ᴹ :
--   {a : U} (aᴹ : Uᴹ a)
--   {t : a} (tᴹ : aᴹ t)
--   {u : a} (uᴹ : aᴹ u)
--   → Uᴹ (t ≡ u)
-- ≡ᴹ {a} aᴹ {t} tᴹ {u} uᴹ = λ z → tr aᴹ z tᴹ ≡ uᴹ

-- reflᴹ :
--   {a : U} (aᴹ : Uᴹ a)
--   {t : a} (tᴹ : aᴹ t)
--   → (≡ᴹ aᴹ tᴹ tᴹ) refl
-- reflᴹ {a} aᴹ {t} tᴹ = refl {x = tᴹ}

-- Jᴹ :
--   (a  : U)                  (aᴹ  : Uᴹ a)
--   (t  : a)                  (tᴹ  : aᴹ t)
--   (p  : ∀ x (z : t ≡ x) → U)(pᴹ  : ∀ x (xₘ : aᴹ x) z zₘ → Uᴹ (p x z))
--   (pr : p t refl)           (prᴹ : pᴹ _ tᴹ _ (reflᴹ aᴹ tᴹ) pr)
--   (u  : a)                  (uᴹ  : aᴹ u)
--   (eq : t ≡ u)              (eqᴹ : (≡ᴹ aᴹ tᴹ uᴹ) eq)
--   → pᴹ _ uᴹ _ eqᴹ (J p pr eq)
-- Jᴹ a aᴹ t tᴹ p pᴹ pr prᴹ u uᴹ eq eqᴹ =
--   J (λ xᴹ zᴹ → pᴹ u xᴹ eq zᴹ (J p pr eq))
--     (J (λ x z → pᴹ x (tr aᴹ z tᴹ) z refl (J p pr z))
--       prᴹ eq)
--     eqᴹ

-- Jβᴹ :
--   {a  : U}                   (aᴹ  : Uᴹ a)
--   {t  : a}                   (tᴹ  : aᴹ t)
--   {p  : ∀ x (z : t ≡ x) → U} (pᴹ  : ∀ x (xₘ :  aᴹ x) z zₘ → Uᴹ (p x z))
--   {pr : p t refl}            (prᴹ : pᴹ _ tᴹ _ (reflᴹ aᴹ tᴹ) pr)
--   → ≡ᴹ (pᴹ t tᴹ refl refl) (Jᴹ _ _ _ _ _ pᴹ _ prᴹ _ tᴹ refl refl) prᴹ refl -- Note: this last refl is Jβᶜ
-- Jβᴹ _ _ _ _ = refl

-- --------------------------------------------------------------------------------
-- -- Elimination
-- --------------------------------------------------------------------------------

-- Setᴱ : {A : Set}(Aᴹ : Setᴹ A) → Set₁
-- Setᴱ {A} Aᴹ = (x : A) → Aᴹ x → Set

-- Uᴱ : (A : Set) → Setᴹ A → Set
-- Uᴱ a aₘ = (x : a) → aₘ x

-- Elᴱ : (a : U)(aᴹ : Uᴹ a)(aᴱ : Uᴱ a aᴹ) → Setᴱ ( aᴹ)
-- Elᴱ a aᴹ aᴱ x xₘ = aᴱ x ≡ xₘ

-- -- inductive functions

-- Πᴱ : {a : U}      (aᴹ : Uᴹ a)                          (aᴱ : Uᴱ a aᴹ)
--      {B : a → Set}(Bᴹ : ∀ {x}(xₘ :  aᴹ x) → Setᴹ (B x))(Bᴱ : ∀ {x} xₘ (xₑ : Elᴱ _ aᴹ aᴱ x xₘ) → Setᴱ (Bᴹ xₘ))
--      → Setᴱ (Πᴹ aᴹ Bᴹ)
-- Πᴱ {a} aᴹ aᴱ {B} Bᴹ Bᴱ f fₘ = (x : a) → Bᴱ {x} (aᴱ x) (refl {x = aᴱ x}) (f x) (fₘ x (aᴱ x))


-- -- (t u)ᴱ

-- appᴱ :
--   {a : U}      (aᴹ : Uᴹ a)                          (aᴱ : Uᴱ a aᴹ)
--   {B : a → Set}(Bᴹ : ∀ {x}(xₘ :  aᴹ x) → Setᴹ (B x))(Bᴱ : ∀ {x} xₘ (xₑ : Elᴱ _ aᴹ aᴱ x xₘ) → Setᴱ (Bᴹ xₘ))
--   {t : Π a B}  (tᴹ : Πᴹ aᴹ Bᴹ t)                    (tᴱ : Πᴱ aᴹ aᴱ Bᴹ Bᴱ t tᴹ)
--   {u : a}      (uᴹ :  aᴹ u)                         (uᴱ : Elᴱ _ aᴹ aᴱ u uᴹ)
--   → Bᴱ uᴹ uᴱ (t u) (tᴹ u uᴹ)
-- appᴱ {a} aᴹ aᴱ {B} Bᴹ Bᴱ {t} tᴹ tᴱ {u} uᴹ uᴱ =
--   (J (λ uᴹ uᴱ → Bᴱ {u} uᴹ uᴱ (t u) (tᴹ u uᴹ)) (tᴱ u)) {uᴹ} uᴱ

-- -- non-inductive functions

-- Πₙᵢᴱ :
--   (A : Set)
--   {B : A → Set}   (Bᴹ : ∀ a → Setᴹ (B a))   (Bᴱ : ∀ a → Setᴱ (Bᴹ a))
--   → Setᴱ (Πₙᵢᴹ A Bᴹ)
-- Πₙᵢᴱ A {B} Bᴹ Bᴱ f fᴹ = (a : A) → Bᴱ a (f a) (fᴹ a)

-- appₙᵢᴱ :
--   ∀ (A : Set)
--     {B : A → Set}  (Bᴹ : ∀ a → Setᴹ (B a)) (Bᴱ : ∀ a → Setᴱ (Bᴹ a))
--     {t : Πₙᵢ A B}  (tᴹ : Πₙᵢᴹ A Bᴹ t)      (tᴱ : Πₙᵢᴱ A Bᴹ Bᴱ t tᴹ)
--     (a : A)
--   → Bᴱ a (t a) (tᴹ a)
-- appₙᵢᴱ A {B} Bᴹ Bᴱ {t} tᴹ tᴱ a = tᴱ a

-- -- small non-inductive functions
-- Πₙᵢₛᴱ :
--   (A : Set)
--   {b : A → U} (bᴹ : ∀ a → Uᴹ (b a)) (bᴱ : ∀ a → Uᴱ (b a) (bᴹ a))
--   → Uᴱ (Πₙᵢₛ A b) (Πₙᵢₛᴹ A bᴹ)
-- Πₙᵢₛᴱ A {b} bᴹ bᴱ = λ f a → bᴱ a (f a)

-- appₙᵢₛᴱ :
--   (A : Set)
--   {b : A → U}    (bᴹ : ∀ a → Uᴹ (b a)) (bᴱ : ∀ a → Uᴱ (b a) (bᴹ a))
--   {t : Πₙᵢₛ A b} (tᴹ : Πₙᵢₛᴹ A bᴹ t)   (tᴱ : Elᴱ _ (Πₙᵢₛᴹ A bᴹ) (Πₙᵢₛᴱ A bᴹ bᴱ) t tᴹ)
--   (a : A)
--   → Elᴱ _ (bᴹ a) (bᴱ a) (t a) (tᴹ a)
-- appₙᵢₛᴱ A {b} bᴹ bᴱ {t} tᴹ tᴱ a = ap (λ f → f a) {λ a → bᴱ a (t a)}{tᴹ} tᴱ

-- -- Identity

-- ≡ᴱ :
--   (a : U)(aᴹ : Uᴹ a)    (aᴱ : Uᴱ a aᴹ)
--   (t : a)(tᴹ : Elᴹ aᴹ t)(tᴱ : Elᴱ _ aᴹ aᴱ t tᴹ)
--   (u : a)(uᴹ : Elᴹ aᴹ u)(uᴱ : Elᴱ _ aᴹ aᴱ u uᴹ)
--   → Uᴱ (t ≡ u) (≡ᴹ aᴹ tᴹ uᴹ)
-- ≡ᴱ a aᴹ aᴱ t tᴹ tᴱ u uᴹ uᴱ =
--   λ e → tr (λ xᴹ → tr aᴹ e xᴹ ≡ uᴹ) tᴱ (tr (λ yᴹ → tr aᴹ e (aᴱ t) ≡ yᴹ) uᴱ (apd aᴹ aᴱ t u e))

-- reflᴱ :
--   {a : U} (aᴹ : Uᴹ a)    (aᴱ : Uᴱ a aᴹ)
--   {t : a} (tᴹ : Elᴹ aᴹ t)(tᴱ : Elᴱ _ aᴹ aᴱ t tᴹ)
--   → Elᴱ _ (≡ᴹ aᴹ tᴹ tᴹ) (≡ᴱ a aᴹ aᴱ t tᴹ tᴱ t tᴹ tᴱ) refl (reflᴹ aᴹ tᴹ)
-- reflᴱ {a} aᴹ aᴱ {t} tᴹ tᴱ =
--  J (λ xᴹ xᴱ →
--          tr (λ yᴹ → yᴹ ≡ xᴹ) xᴱ (tr (λ yᴹ → aᴱ t ≡ yᴹ) xᴱ refl) ≡ refl)
--        refl
--        tᴱ

-- -- Alternative, more structured implementation
-- -- ≡ᴱ :
-- --   (a : U)(aᴹ : Uᴹ a)    (aᴱ : Uᴱ a aᴹ)
-- --   (t : a)(tᴹ : Elᴹ aᴹ t)(tᴱ : Elᴱ _ aᴹ aᴱ t tᴹ)
-- --   (u : a)(uᴹ : Elᴹ aᴹ u)(uᴱ : Elᴱ _ aᴹ aᴱ u uᴹ)
-- --   → Uᴱ (t ≡ u) (≡ᴹ aᴹ tᴹ uᴹ)
-- -- ≡ᴱ a aᴹ aᴱ t tᴹ tᴱ u uᴹ uᴱ =
-- --   λ e → ap (tr aᴹ e) (tᴱ ⁻¹) ◾ apd aᴹ aᴱ t u e ◾ uᴱ

-- -- reflᴱ :
-- --   {a : U} (aᴹ : Uᴹ a)    (aᴱ : Uᴱ a aᴹ)
-- --   {t : a} (tᴹ : Elᴹ aᴹ t)(tᴱ : Elᴱ _ aᴹ aᴱ t tᴹ)
-- --   → Elᴱ _ (≡ᴹ aᴹ tᴹ tᴹ) (≡ᴱ a aᴹ aᴱ t tᴹ tᴱ t tᴹ tᴱ) refl (reflᴹ aᴹ tᴹ)
-- -- reflᴱ {a} aᴹ aᴱ {t} tᴹ tᴱ =
-- --   ap (_◾ tᴱ) (ap-id (tᴱ ⁻¹)) ◾ inv tᴱ
