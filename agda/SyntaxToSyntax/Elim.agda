{-# OPTIONS --without-K --rewriting #-}

postulate
  _↦_ : ∀{α}{A : Set α} → A → A → Set
{-# BUILTIN REWRITE _↦_ #-}

postulate
  I : Set
  El : I → Set

Π : (a : I) → (El a → Set) → Set
Π a B = (x : El a) → B x

syntax Π a (λ x → B) = x ∈ a ⇒ B

lam : {a : I}{B : El a → Set}(t : (x : El a) → B x) → Π a B
lam t = t

syntax lam (λ x → t) = Λ x ⇒ t
infix 4 _≡_

postulate
  _≡_  : {a : I}(t : El a)(u : El a) → I
  refl : {a : I}{t : El a} → El (t ≡ t)
  J    : ∀ (a : I)(t : El a)(P : (x : El a)(z : El (t ≡ x)) → Set)(pr : P t refl){u : El a}(eq : El (t ≡ u)) → P u eq
  βJ   : ∀ {a : I}{t : El a}{P : (x : El a)(z : El (t ≡ x)) → Set}{pr : P t refl}
       → J a t P pr refl ↦ pr
{-# REWRITE βJ #-}

-- Equality reflection allows us to prove that ᴱ respects ≡' (metatheoretical equality)
--------------------------------------------------------------------------------

open import Relation.Binary.PropositionalEquality renaming (_≡_ to _≡'_; refl to refl')

J' : ∀ {α β}{A : Set α}{a : A}(B : ∀ b → a ≡' b → Set β) → B a refl' → ∀ {b} p → B b p
J' B pr refl' = pr

transp' : ∀ {α β}{A : Set α}{a b : A}(B : A → Set β) → a ≡' b → B a → B b
transp' B refl' ba = ba

lower : ∀ {a : I}{t u : El a} → t ≡' u → El (t ≡ u)
lower refl' = refl

postulate
  lift : ∀ {a : I}{x y : El a} → El (x ≡ y) → x ≡' y -- reflect

  -- would be properly "trip: isEquiv lower"
  trip : ∀ {a : I}{t u : El a}(p : El (t ≡ u)) → lower (lift p) ≡' p



--------------------------------------------------------------------------------

transp : ∀ {a}(p : El a → I){t u : El a} → El (t ≡ u) → El (p t) → El (p u)
transp {a} p {t}{u} eq pt = J a t (λ u _ → El (p u)) pt eq

Transp : ∀ {a}(P : El a → Set){t u : El a} → El (t ≡ u) → P t → P u
Transp {a} P {t}{u} eq pt = J a t (λ u _ → P u) pt eq

ap : ∀ {a b}(f : x ∈ a ⇒ El b){t u : El a}(eq : El (t ≡ u)) → El (f t ≡ f u)
ap {a}{b} f {t}{u} eq = J a t (λ u eq → El (f t ≡ f u)) refl eq

-- apd : ∀ {a}{p : El a → I}(f : x ∈ a ⇒ El (p x)){t u : El a}(eq : El (t ≡ u)) → El (transp p eq (f t) ≡ f u)
-- apd {a}{p} f {t}{u} eq = J a t (λ y z → El (transp p z (f t) ≡ f y)) refl {u} eq

infixr 5 _◾_
_◾_ : ∀ {a}{t u v : El a} → El (t ≡ u) → El (u ≡ v) → El (t ≡ v)
_◾_ {a}{t}{u}{v} p q = transp (t ≡_) q p

_⁻¹ : ∀ {a}{t u : El a} → El (t ≡ u) → El (u ≡ t)
_⁻¹ {a}{t}{u} p = transp (_≡ t) p refl
infix 6 _⁻¹

inv : ∀ {a}{t u : El a}(eq : El (t ≡ u)) → El (eq ◾ eq ⁻¹ ≡ refl)
inv eq = J _ _ (λ u eq → El (eq ◾ eq ⁻¹ ≡ refl)) refl eq

inv⁻¹ : ∀ {a}{t u : El a}(eq : El (t ≡ u)) → El (eq ⁻¹ ◾ eq ≡ refl)
inv⁻¹ eq = J _ _ (λ u eq → El (eq ⁻¹ ◾ eq ≡ refl)) refl eq

-- Logical predicate transformation
--------------------------------------------------------------------------------

Setᴾ : Set → Set₁
Setᴾ A = A → Set

Iᴾ : Setᴾ I
Iᴾ a = x ∈ a ⇒ I

Elᴾ : {a : I}(aᴾ : Iᴾ a) → Setᴾ (El a)
Elᴾ aᴾ x = El (aᴾ x)

Πᴾ : ∀ {a}(aᴾ : Iᴾ a){B : El a → Set}(Bᴾ : ∀ {x} (xₚ : Elᴾ aᴾ x) → Setᴾ (B x)) → Setᴾ (Π a B)
Πᴾ {a} aᴾ {B} Bᴾ f = x ∈ a ⇒ xₚ ∈ aᴾ x ⇒ Bᴾ xₚ (f x)

appᴾ :
  ∀ {a}(aᴾ : Iᴾ a){B : El a → Set}(Bᴾ : ∀ {x}(xₚ : Elᴾ aᴾ x) → Setᴾ (B x))
    {t : Π a B}(tᴾ : Πᴾ aᴾ Bᴾ t)
    {u : El a} (uᴾ : Elᴾ aᴾ u)
  → Bᴾ uᴾ (t u)
appᴾ {a} aᴾ {B} Bᴾ {t} tᴾ {u} uᴾ = tᴾ u uᴾ

lamᴾ :
  ∀ {a}(aᴾ : Iᴾ a){B : El a → Set}(Bᴾ : ∀ {x}(xₚ : Elᴾ aᴾ x) → Setᴾ (B x))
    {t : (x : El a) → B x}(tᴾ : ∀ {x}(xₚ : Elᴾ aᴾ x) → Bᴾ xₚ (t x))
    → Πᴾ aᴾ Bᴾ (Λ x ⇒ t x)
lamᴾ {a} aᴾ {B} Bᴾ {t} tᴾ = Λ x ⇒ Λ xₚ ⇒ tᴾ xₚ

Πβᴾ :
  ∀{a}(aᴾ : Iᴾ a){B : El a → Set}(Bᴾ : ∀ {x}(xₚ : Elᴾ aᴾ x) → Setᴾ (B x))
  {t : (x : El a) → B x}(tᴾ : ∀ {x}(xₚ : Elᴾ aᴾ x) → Bᴾ xₚ (t x))
  {u : El a} (uᴾ : Elᴾ aᴾ u)
  → appᴾ aᴾ Bᴾ (lamᴾ aᴾ Bᴾ tᴾ) uᴾ ≡' tᴾ uᴾ
Πβᴾ _ _ _ _ = refl'

Πηᴾ :
  ∀ {a}(aᴾ : Iᴾ a){B : El a → Set}(Bᴾ : ∀ {x}(xₚ : Elᴾ aᴾ x) → Setᴾ (B x))
    {t : Π a B}(tᴾ : Πᴾ aᴾ Bᴾ t)
  → lamᴾ aᴾ Bᴾ (appᴾ aᴾ Bᴾ tᴾ) ≡' tᴾ
Πηᴾ _ _ _ = refl'

≡ᴾ :
  {a : I}    (aᴾ : Iᴾ a)
  {t : El a} (tᴾ : Elᴾ aᴾ t)
  {u : El a} (uᴾ : Elᴾ aᴾ u)
  → Iᴾ (t ≡ u)
≡ᴾ {a} aᴾ {t} tᴾ {u} uᴾ = Λ z ⇒ (transp aᴾ z tᴾ ≡ uᴾ)

reflᴾ :
  {a : I}    (aᴾ : Iᴾ a)
  {t : El a} (tᴾ : Elᴾ aᴾ t)
  → Elᴾ (≡ᴾ aᴾ tᴾ tᴾ) refl
reflᴾ {a} aᴾ {t} tᴾ = refl

Jᴾ :
  {a  : I}                          (aᴾ  : Iᴾ a)
  {t  : El a}                       (tᴾ  : Elᴾ aᴾ t)
  {P  : ∀ x (z : El (t ≡ x)) → Set} (Pᴾ  : ∀ {x} (xₚ : Elᴾ aᴾ x) {z} zₚ → Setᴾ (P x z))
  {pr : P t refl}                   (prᴾ : Pᴾ tᴾ (reflᴾ aᴾ tᴾ) pr)
  {u  : El a}                       (uᴾ  : Elᴾ aᴾ u)
  {eq : El (t ≡ u)}                 (eqᴾ : Elᴾ (≡ᴾ aᴾ tᴾ uᴾ) eq)
  → Pᴾ uᴾ eqᴾ (J a t P pr eq)
Jᴾ {a} aᴾ {t} tᴾ {P} Pᴾ {pr} prᴾ {u} uᴾ {eq} eqᴾ =
  J (aᴾ u) (transp aᴾ eq tᴾ)
     (λ xᴾ zᴾ → Pᴾ {u} xᴾ {eq} zᴾ (J a t P pr eq))
     (J a t
        (λ x z → Pᴾ {x} (transp aᴾ z tᴾ) {z} refl (J a t P pr z))
        prᴾ eq)
     eqᴾ

≡βᴾ :
  {a  : I}                          (aᴾ  : Iᴾ a)
  {t  : El a}                       (tᴾ  : Elᴾ aᴾ t)
  {P  : ∀ x (z : El (t ≡ x)) → Set} (Pᴾ  : ∀ {x} (xₚ : Elᴾ aᴾ x) {z} zₚ → Setᴾ (P x z))
  {pr : P t refl}                   (prᴾ : Pᴾ tᴾ (reflᴾ aᴾ tᴾ) pr)
  → Jᴾ aᴾ tᴾ Pᴾ prᴾ tᴾ (reflᴾ aᴾ tᴾ) ≡' prᴾ
≡βᴾ _ _ _ _ = refl'


-- Elimination transformation
--------------------------------------------------------------------------------

Setᴱ : {A : Set}(Aᴾ : Setᴾ A) → Set₁
Setᴱ {A} Aᴾ = (x : A) → Aᴾ x → Set

Iᴱ : Setᴱ Iᴾ
Iᴱ i iₚ = x ∈ i ⇒ El (iₚ x)

Elᴱ : {a : I}(aᴾ : Iᴾ a)(aᴱ : Iᴱ a aᴾ) → Setᴱ (Elᴾ aᴾ)
Elᴱ {a} aᴾ aᴱ x xₚ = El (aᴱ x ≡ xₚ)

Πᴱ : {a : I}         (aᴾ : Iᴾ a)                             (aᴱ : Iᴱ a aᴾ)
     {B : El a → Set}(Bᴾ : ∀ {x}(xₚ : Elᴾ aᴾ x) → Setᴾ (B x))(Bᴱ : ∀ {x} xₚ (xₑ : Elᴱ aᴾ aᴱ x xₚ) → Setᴱ (Bᴾ xₚ))
     → Setᴱ (Πᴾ aᴾ Bᴾ)
Πᴱ {a} aᴾ aᴱ {B} Bᴾ Bᴱ f fₚ = x ∈ a ⇒ Bᴱ {x} (aᴱ x) refl (f x) (fₚ x (aᴱ x))

appᴱ :
  {a : I}         (aᴾ : Iᴾ a)                             (aᴱ : Iᴱ a aᴾ)
  {B : El a → Set}(Bᴾ : ∀ {x}(xₚ : Elᴾ aᴾ x) → Setᴾ (B x))(Bᴱ : ∀ {x} xₚ (xₑ : Elᴱ aᴾ aᴱ x xₚ) → Setᴱ (Bᴾ xₚ))
  {t : Π a B}     (tᴾ : Πᴾ aᴾ Bᴾ t)                       (tᴱ : Πᴱ aᴾ aᴱ Bᴾ Bᴱ t tᴾ)
  {u : El a}      (uᴾ : Elᴾ aᴾ u)                         (uᴱ : Elᴱ aᴾ aᴱ u uᴾ)
  → Bᴱ uᴾ uᴱ (t u) (tᴾ u uᴾ)
appᴱ {a} aᴾ aᴱ {B} Bᴾ Bᴱ {t} tᴾ tᴱ {u} uᴾ uᴱ =
  J (aᴾ u) (aᴱ u) (λ xᴾ xᴱ → Bᴱ {u} xᴾ xᴱ (t u) (tᴾ u xᴾ)) (tᴱ u) {uᴾ} uᴱ

lamᴱ :
  {a : I}          (aᴾ : Iᴾ a)                              (aᴱ : Iᴱ a aᴾ)
  {B : El a → Set} (Bᴾ : ∀ {x}(xₚ : Elᴾ aᴾ x) → Setᴾ (B x)) (Bᴱ : ∀ {x} xₚ (xₑ : Elᴱ aᴾ aᴱ x xₚ) → Setᴱ (Bᴾ xₚ))
  {t : ∀ x → B x}  (tᴾ : ∀ {x}(xₚ : Elᴾ aᴾ x) → Bᴾ xₚ (t x))(tᴱ : ∀ {x} xₚ (xₑ : Elᴱ aᴾ aᴱ x xₚ) → Bᴱ xₚ xₑ (t x) (tᴾ xₚ))
  → Πᴱ aᴾ aᴱ Bᴾ Bᴱ (Λ x ⇒ t x) (Λ x ⇒ Λ xₚ ⇒ tᴾ xₚ)
lamᴱ {a} aᴾ aᴱ {B} Bᴾ Bᴱ {t} tᴾ tᴱ = Λ x ⇒ tᴱ (aᴱ x) refl

Πβᴱ :
  {a : I}         (aᴾ : Iᴾ a)                             (aᴱ : Iᴱ a aᴾ)
  {B : El a → Set}(Bᴾ : ∀ {x}(xₚ : Elᴾ aᴾ x) → Setᴾ (B x))(Bᴱ : ∀ {x} xₚ (xₑ : Elᴱ aᴾ aᴱ x xₚ) → Setᴱ (Bᴾ xₚ))
  {t : ∀ x → B x}  (tᴾ : ∀ {x}(xₚ : Elᴾ aᴾ x) → Bᴾ xₚ (t x))(tᴱ : ∀ {x} xₚ (xₑ : Elᴱ aᴾ aᴱ x xₚ) → Bᴱ xₚ xₑ (t x) (tᴾ xₚ))
  {u : El a}      (uᴾ : Elᴾ aᴾ u)                         (uᴱ : Elᴱ aᴾ aᴱ u uᴾ)
  → appᴱ aᴾ aᴱ Bᴾ Bᴱ (lamᴾ aᴾ Bᴾ tᴾ) (lamᴱ aᴾ aᴱ Bᴾ Bᴱ tᴾ tᴱ) uᴾ uᴱ ≡' tᴱ uᴾ uᴱ
Πβᴱ aᴾ aᴱ Bᴾ Bᴱ {t} tᴾ tᴱ {u} uᴾ uᴱ = 
  transp'
    (λ uᴱ → J (aᴾ u) (aᴱ u) (λ xᴾ xᴱ → Bᴱ xᴾ xᴱ (t u) (tᴾ xᴾ)) (tᴱ (aᴱ u) refl) uᴱ
            ≡' tᴱ uᴾ uᴱ) (trip uᴱ)
    (J'
      (λ uᴾ luᴱ → J (aᴾ u) (aᴱ u) (λ xᴾ xᴱ → Bᴱ xᴾ xᴱ (t u) (tᴾ xᴾ))
                  (tᴱ (aᴱ u) refl) (lower luᴱ)
                  ≡' tᴱ uᴾ (lower luᴱ))
      refl'
      (lift uᴱ))

Πηᴱ :
  {a : I}         (aᴾ : Iᴾ a)                             (aᴱ : Iᴱ a aᴾ)
  {B : El a → Set}(Bᴾ : ∀ {x}(xₚ : Elᴾ aᴾ x) → Setᴾ (B x))(Bᴱ : ∀ {x} xₚ (xₑ : Elᴱ aᴾ aᴱ x xₚ) → Setᴱ (Bᴾ xₚ))
  {t : Π a B}     (tᴾ : Πᴾ aᴾ Bᴾ t)                       (tᴱ : Πᴱ aᴾ aᴱ Bᴾ Bᴱ t tᴾ)
  → lamᴱ aᴾ aᴱ Bᴾ Bᴱ (appᴾ aᴾ Bᴾ tᴾ) (appᴱ aᴾ aᴱ Bᴾ Bᴱ tᴾ tᴱ) ≡' tᴱ
Πηᴱ _ _ _ _ _ _ = refl'

≡ᴱ :
  {a : I}    (aᴾ : Iᴾ a)    (aᴱ : Iᴱ a aᴾ)
  {t : El a} (tᴾ : Elᴾ aᴾ t)(tᴱ : Elᴱ aᴾ aᴱ t tᴾ)
  {u : El a} (uᴾ : Elᴾ aᴾ u)(uᴱ : Elᴱ aᴾ aᴱ u uᴾ)
  → Iᴱ (t ≡ u) (≡ᴾ aᴾ tᴾ uᴾ)
≡ᴱ {a} aᴾ aᴱ {t} tᴾ tᴱ {u} uᴾ uᴱ =
  Λ z ⇒ (J a t (λ u z → El (transp aᴾ z tᴾ ≡ aᴱ u)) (tᴱ ⁻¹) z ◾ uᴱ)
         -- reduces to (tᴱ ⁻¹ ◾ uᴱ) if (z = refl)

reflᴱ :
  {a : I}    (aᴾ : Iᴾ a)    (aᴱ : Iᴱ a aᴾ)
  {t : El a} (tᴾ : Elᴾ aᴾ t)(tᴱ : Elᴱ aᴾ aᴱ t tᴾ)
  → Elᴱ (≡ᴾ aᴾ tᴾ tᴾ) (≡ᴱ aᴾ aᴱ tᴾ tᴱ tᴾ tᴱ) refl (reflᴾ aᴾ tᴾ)
reflᴱ {a} aᴾ aᴱ {t} tᴾ tᴱ = inv⁻¹ tᴱ

Jᴱ :
  {a : I}    (aᴾ  : Iᴾ a)     (aᴱ  : Iᴱ a aᴾ)
  {t : El a} (tᴾ  : Elᴾ aᴾ t) (tᴱ  : Elᴱ aᴾ aᴱ t tᴾ)

  {P : (x : El a)(z : El (t ≡ x)) → Set}
    (Pᴾ : ∀{x}(xᴾ : Elᴾ aᴾ x){z}(zᴾ : Elᴾ (≡ᴾ aᴾ tᴾ xᴾ) z) → Setᴾ (P x z))
      (Pᴱ : ∀{x} xᴾ (xᴱ : Elᴱ aᴾ aᴱ x xᴾ){z} zᴾ (zᴱ : Elᴱ (≡ᴾ aᴾ tᴾ xᴾ) (≡ᴱ aᴾ aᴱ tᴾ tᴱ xᴾ xᴱ) z zᴾ)
            → Setᴱ {P x z} (Pᴾ xᴾ zᴾ))

  {pr : P t refl}
    (prᴾ : Pᴾ tᴾ (reflᴾ aᴾ tᴾ) pr)
      (prᴱ : Pᴱ tᴾ tᴱ (reflᴾ aᴾ tᴾ) (reflᴱ aᴾ aᴱ tᴾ tᴱ) pr prᴾ)

  {u  : El a}(uᴾ  : Elᴾ aᴾ u)(uᴱ  : Elᴱ aᴾ aᴱ u uᴾ)

  {eq : El (t ≡ u)}
    (eqᴾ : Elᴾ (≡ᴾ aᴾ tᴾ uᴾ) eq)
      (eqᴱ : Elᴱ (≡ᴾ aᴾ tᴾ uᴾ) (≡ᴱ aᴾ aᴱ tᴾ tᴱ uᴾ uᴱ) eq eqᴾ)

  → Pᴱ uᴾ uᴱ eqᴾ eqᴱ (J a t P pr eq) (Jᴾ aᴾ tᴾ Pᴾ prᴾ uᴾ eqᴾ)

Jᴱ {a} aᴾ aᴱ {t} tᴾ tᴱ {P} Pᴾ Pᴱ {pr} prᴾ prᴱ {u} uᴾ uᴱ {eq} eqᴾ eqᴱ =
  J (transp aᴾ eq tᴾ ≡ uᴾ)
    (≡ᴱ aᴾ aᴱ tᴾ tᴱ uᴾ uᴱ eq)
    (λ eqᴾ eqᴱ → Pᴱ uᴾ uᴱ eqᴾ eqᴱ (J a t P pr eq) (Jᴾ aᴾ tᴾ Pᴾ prᴾ uᴾ eqᴾ))
    (J (aᴾ u) (aᴱ u)
       (λ uᴾ uᴱ → Pᴱ uᴾ uᴱ (≡ᴱ aᴾ aᴱ tᴾ tᴱ uᴾ uᴱ eq) refl (J a t P pr eq)
                     (Jᴾ aᴾ tᴾ Pᴾ prᴾ uᴾ (≡ᴱ aᴾ aᴱ tᴾ tᴱ uᴾ uᴱ eq)))
       (J a t
          (λ u eq → Pᴱ (aᴱ u) refl (≡ᴱ aᴾ aᴱ tᴾ tᴱ (aᴱ u) refl eq) refl (J a t P pr eq)
                       (Jᴾ aᴾ tᴾ Pᴾ prᴾ (aᴱ u) (≡ᴱ aᴾ aᴱ tᴾ tᴱ (aᴱ u) refl eq)))
          (Transp
            (λ refl' → Pᴱ (aᴱ t) refl (tᴱ ⁻¹) refl' pr (Jᴾ aᴾ tᴾ Pᴾ prᴾ (aᴱ t) (tᴱ ⁻¹)))
            (inv (ap (Λ y ⇒ (tᴱ ⁻¹ ◾ y)) (inv tᴱ ⁻¹)))
            (J (aᴱ t ≡ aᴱ t) _
               (λ x invtᴱ →
                 Pᴱ (aᴱ t) x (tᴱ ⁻¹ ◾ x)
                    (ap (Λ y ⇒ (tᴱ ⁻¹ ◾ y)) (invtᴱ ⁻¹) ◾ ap (Λ y ⇒ (tᴱ ⁻¹ ◾ y)) (invtᴱ ⁻¹) ⁻¹)
                    pr (Jᴾ aᴾ tᴾ Pᴾ prᴾ (aᴱ t) (tᴱ ⁻¹ ◾ x)))
               (J _ _
                  (λ aᴱt tᴱ⁻¹ →
                    Pᴱ aᴱt (tᴱ ◾ tᴱ⁻¹) (tᴱ ⁻¹ ◾ tᴱ ◾ tᴱ⁻¹) refl
                    pr (Jᴾ aᴾ tᴾ Pᴾ prᴾ aᴱt (tᴱ ⁻¹ ◾ tᴱ ◾ tᴱ⁻¹)))
                  (Transp
                     (λ refl' → Pᴱ tᴾ tᴱ (tᴱ ⁻¹ ◾ tᴱ) refl' pr (Jᴾ aᴾ tᴾ Pᴾ prᴾ tᴾ (tᴱ ⁻¹ ◾ tᴱ)))
                     (inv (inv⁻¹ tᴱ))
                     (J _ _
                        (λ x z → Pᴱ tᴾ tᴱ x (inv⁻¹ tᴱ ◾ z) pr (Jᴾ aᴾ tᴾ Pᴾ prᴾ tᴾ x))
                        prᴱ
                        (inv⁻¹ tᴱ ⁻¹)))
                  (tᴱ ⁻¹))
              (inv tᴱ)))
          eq)
       uᴱ)
    eqᴱ

≡βᴱ :
  {a : I}    (aᴾ  : Iᴾ a)     (aᴱ  : Iᴱ a aᴾ)
  {t : El a} (tᴾ  : Elᴾ aᴾ t) (tᴱ  : Elᴱ aᴾ aᴱ t tᴾ)

  {P : (x : El a)(z : El (t ≡ x)) → Set}
    (Pᴾ : ∀{x}(xᴾ : Elᴾ aᴾ x){z}(zᴾ : Elᴾ (≡ᴾ aᴾ tᴾ xᴾ) z) → Setᴾ (P x z))
      (Pᴱ : ∀{x} xᴾ (xᴱ : Elᴱ aᴾ aᴱ x xᴾ){z} zᴾ (zᴱ : Elᴱ (≡ᴾ aᴾ tᴾ xᴾ) (≡ᴱ aᴾ aᴱ tᴾ tᴱ xᴾ xᴱ) z zᴾ)
            → Setᴱ {P x z} (Pᴾ xᴾ zᴾ))

  {pr : P t refl}
    (prᴾ : Pᴾ tᴾ (reflᴾ aᴾ tᴾ) pr)
      (prᴱ : Pᴱ tᴾ tᴱ (reflᴾ aᴾ tᴾ) (reflᴱ aᴾ aᴱ tᴾ tᴱ) pr prᴾ)
  → Jᴱ {a} aᴾ aᴱ {t} tᴾ tᴱ {P} Pᴾ Pᴱ {pr} prᴾ prᴱ {t} tᴾ tᴱ {refl} (reflᴾ aᴾ tᴾ) (reflᴱ aᴾ aᴱ tᴾ tᴱ) ≡' prᴱ
≡βᴱ {a} aᴾ aᴱ {t} tᴾ tᴱ {P} Pᴾ Pᴱ {pr} prᴾ prᴱ =
  transp'
    (λ tᴱ →
      (Pᴱ  : {x : El a} (xᴾ : El (aᴾ x)) (xᴱ : Elᴱ aᴾ aᴱ x xᴾ)
        {z : El (t ≡ x)} (zᴾ : El (≡ᴾ aᴾ tᴾ xᴾ z)) →
        Elᴱ (≡ᴾ aᴾ tᴾ xᴾ) (≡ᴱ aᴾ aᴱ tᴾ tᴱ xᴾ xᴱ) z zᴾ → Setᴱ (Pᴾ xᴾ zᴾ))
      (prᴱ : Pᴱ tᴾ tᴱ (reflᴾ aᴾ tᴾ) (reflᴱ aᴾ aᴱ tᴾ tᴱ) pr prᴾ)
      → Jᴱ aᴾ aᴱ tᴾ tᴱ Pᴾ Pᴱ prᴾ prᴱ tᴾ tᴱ (reflᴾ aᴾ tᴾ)
        (reflᴱ aᴾ aᴱ tᴾ tᴱ)
        ≡' prᴱ)
    (trip tᴱ)
    (λ Pᴱ prᴱ →
      J'
        (λ tᴾ lifttᴱ →
          (Pᴾ  : {x : El a} (xᴾ : Elᴾ aᴾ x) {z : El (t ≡ x)} → Elᴾ (≡ᴾ aᴾ tᴾ xᴾ) z → Setᴾ (P x z))
          (prᴾ : Pᴾ tᴾ (reflᴾ aᴾ tᴾ) pr)
          (Pᴱ  : {x : El a} (xᴾ : El (aᴾ x)) (xᴱ : Elᴱ aᴾ aᴱ x xᴾ)
                 {z : El (t ≡ x)} (zᴾ : El (≡ᴾ aᴾ tᴾ xᴾ z)) →
                 Elᴱ (≡ᴾ aᴾ tᴾ xᴾ) (≡ᴱ aᴾ aᴱ tᴾ (lower lifttᴱ) xᴾ xᴱ) z zᴾ →
                 Setᴱ (Pᴾ xᴾ zᴾ))
          (prᴱ : Pᴱ tᴾ (lower lifttᴱ) (reflᴾ aᴾ tᴾ)
                (reflᴱ aᴾ aᴱ tᴾ (lower lifttᴱ)) pr prᴾ)
        → Jᴱ aᴾ aᴱ tᴾ (lower lifttᴱ) Pᴾ Pᴱ prᴾ prᴱ tᴾ
          (lower lifttᴱ) (reflᴾ aᴾ tᴾ) (reflᴱ aᴾ aᴱ tᴾ (lower lifttᴱ))
          ≡' prᴱ)
        (λ Pᴾ prᴾ Pᴱ prᴱ → refl')
        (lift tᴱ) Pᴾ prᴾ Pᴱ prᴱ)
    Pᴱ prᴱ


{-
-- an alternative version for equality, I wasn't able to figure it out

≡ᴱ :
  {a : I}    (aᴾ : Iᴾ a)    (aᴱ : Iᴱ a aᴾ)
  {t : El a} (tᴾ : Elᴾ aᴾ t)(tᴱ : Elᴱ aᴾ aᴱ t tᴾ)
  {u : El a} (uᴾ : Elᴾ aᴾ u)(uᴱ : Elᴱ aᴾ aᴱ u uᴾ)
  → Iᴱ (t ≡ u) (≡ᴾ aᴾ tᴾ uᴾ)
≡ᴱ {a} aᴾ aᴱ {t} tᴾ tᴱ {u} uᴾ uᴱ =
  Λ z ⇒ transp (Λ xᴾ ⇒ ≡ᴾ aᴾ xᴾ uᴾ z) tᴱ (transp (Λ yᴾ ⇒ ≡ᴾ aᴾ (aᴱ t) yᴾ z) uᴱ (apd aᴱ z))

reflᴱ :
  {a : I}    (aᴾ : Iᴾ a)    (aᴱ : Iᴱ a aᴾ)
  {t : El a} (tᴾ : Elᴾ aᴾ t)(tᴱ : Elᴱ aᴾ aᴱ t tᴾ)
  → Elᴱ (≡ᴾ aᴾ tᴾ tᴾ) (≡ᴱ aᴾ aᴱ tᴾ tᴱ tᴾ tᴱ) refl (reflᴾ aᴾ tᴾ)
reflᴱ {a} aᴾ aᴱ {t} tᴾ tᴱ
  = J (aᴾ t)
      (aᴱ t)
      (λ xᴾ xᴱ → Elᴱ (≡ᴾ aᴾ xᴾ xᴾ) (≡ᴱ aᴾ aᴱ xᴾ xᴱ xᴾ xᴱ) refl (reflᴾ aᴾ xᴾ))
      refl
      {tᴾ} tᴱ

Jᴱ :
  {a : I}    (aᴾ  : Iᴾ a)     (aᴱ  : Iᴱ a aᴾ)
  {t : El a} (tᴾ  : Elᴾ aᴾ t) (tᴱ  : Elᴱ aᴾ aᴱ t tᴾ)

  {P : (x : El a)(z : El (t ≡ x)) → Set}
    (Pᴾ : ∀{x}(xᴾ : Elᴾ aᴾ x){z}(zᴾ : Elᴾ (≡ᴾ aᴾ tᴾ xᴾ) z) → Setᴾ (P x z))
      (Pᴱ : ∀{x} xᴾ (xᴱ : Elᴱ aᴾ aᴱ x xᴾ){z} zᴾ (zᴱ : Elᴱ (≡ᴾ aᴾ tᴾ xᴾ) (≡ᴱ aᴾ aᴱ tᴾ tᴱ xᴾ xᴱ) z zᴾ)
            → Setᴱ {P x z} (Pᴾ xᴾ zᴾ))

  {pr : P t refl}
    (prᴾ : Pᴾ tᴾ (reflᴾ aᴾ tᴾ) pr)
      (prᴱ : Pᴱ tᴾ tᴱ (reflᴾ aᴾ tᴾ) (reflᴱ aᴾ aᴱ tᴾ tᴱ) pr prᴾ)

  {u  : El a}(uᴾ  : Elᴾ aᴾ u)(uᴱ  : Elᴱ aᴾ aᴱ u uᴾ)

  {eq : El (t ≡ u)}
    (eqᴾ : Elᴾ (≡ᴾ aᴾ tᴾ uᴾ) eq)
      (eqᴱ : Elᴱ (≡ᴾ aᴾ tᴾ uᴾ) (≡ᴱ aᴾ aᴱ tᴾ tᴱ uᴾ uᴱ) eq eqᴾ)

  → Pᴱ uᴾ uᴱ eqᴾ eqᴱ (J a t P pr eq) (Jᴾ aᴾ tᴾ Pᴾ prᴾ uᴾ eqᴾ)

Jᴱ {a} aᴾ aᴱ {t} tᴾ tᴱ {P} Pᴾ Pᴱ {pr} prᴾ prᴱ {u} uᴾ uᴱ {eq} eqᴾ eqᴱ
  = J (≡ᴾ aᴾ tᴾ uᴾ eq)
      (≡ᴱ aᴾ aᴱ tᴾ tᴱ uᴾ uᴱ eq)
      (λ eqᴾ eqᴱ → Pᴱ uᴾ uᴱ eqᴾ eqᴱ (J a t P pr eq) (Jᴾ aᴾ tᴾ Pᴾ prᴾ uᴾ eqᴾ))
      (J (aᴾ u)
         (aᴱ u)
         (λ uᴾ uᴱ → Pᴱ uᴾ uᴱ (≡ᴱ aᴾ aᴱ tᴾ tᴱ uᴾ uᴱ eq) refl (J a t P pr eq) (Jᴾ aᴾ tᴾ Pᴾ prᴾ uᴾ (≡ᴱ aᴾ aᴱ tᴾ tᴱ uᴾ uᴱ eq)))
         (J a t
            (λ u eq → Pᴱ (aᴱ u) refl (≡ᴱ aᴾ aᴱ tᴾ tᴱ (aᴱ u) refl eq) refl (J a t P pr eq) (Jᴾ aᴾ tᴾ Pᴾ prᴾ (aᴱ u) (≡ᴱ aᴾ aᴱ tᴾ tᴱ (aᴱ u) refl eq)))
            {!rr!}
            {u}
            eq)
         {uᴾ}
         uᴱ)
      {eqᴾ}
      eqᴱ
  where
    rr = J (aᴾ t) tᴾ
           (λ zᴾ zᴱ → Pᴱ zᴾ (Transp (Elᴱ aᴾ aᴱ t) zᴱ tᴱ)
                         {!!}
                         {!refl!}
                         pr
                         {!!})
           {!!}   -- prᴱ
           {aᴱ t}(tᴱ ⁻¹)

    --  (J (aᴾ t) tᴾ (λ zᴾ zᴱ → Pᴱ zᴾ (Transp (Elᴱ aᴾ aᴱ t) zᴱ tᴱ) (≡ᴱ aᴾ aᴱ tᴾ tᴱ zᴾ (Transp (Elᴱ aᴾ aᴱ t) zᴱ tᴱ) refl) refl pr (Jᴾ aᴾ tᴾ Pᴾ prᴾ zᴾ (≡ᴱ aᴾ aᴱ tᴾ tᴱ zᴾ (Transp (Elᴱ aᴾ aᴱ t) zᴱ tᴱ) refl)) → Pᴱ (aᴱ t) refl (≡ᴱ aᴾ aᴱ tᴾ tᴱ (aᴱ t) refl refl) refl pr (Jᴾ aᴾ tᴾ Pᴾ prᴾ (aᴱ t) (≡ᴱ aᴾ aᴱ tᴾ tᴱ (aᴱ t) refl refl))) {!λ x → x!} (tᴱ ⁻¹) qq)
    qq = J (aᴾ t)
           tᴾ
           (λ zᴾ zᴱ → Pᴱ zᴾ (Transp (Elᴱ aᴾ aᴱ t) zᴱ tᴱ) (≡ᴱ aᴾ aᴱ tᴾ tᴱ zᴾ (Transp (Elᴱ aᴾ aᴱ t) zᴱ tᴱ) refl) refl pr (Jᴾ aᴾ tᴾ Pᴾ prᴾ zᴾ (≡ᴱ aᴾ aᴱ tᴾ tᴱ zᴾ (Transp (Elᴱ aᴾ aᴱ t) zᴱ tᴱ) refl)))
           {!prᴱ!}
           {aᴱ t}
           (tᴱ ⁻¹)



    Pᴱ= : {x : El a}
          {xᴾ₀ : El (aᴾ x)}{xᴾ₁ : El (aᴾ x)}(xᴾ₂ : El (xᴾ₀ ≡ xᴾ₁))
          {xᴱ₀ : Elᴱ aᴾ aᴱ x xᴾ₀}{xᴱ₁ : Elᴱ aᴾ aᴱ x xᴾ₁}(xᴱ₂ : El (Transp (Elᴱ aᴾ aᴱ x) xᴾ₂ xᴱ₀ ≡ xᴱ₁))
          {z : El (t ≡ x)}
          {zᴾ₀ : El (≡ᴾ aᴾ tᴾ xᴾ₀ z)}{zᴾ₁ : El (≡ᴾ aᴾ tᴾ xᴾ₁ z)}(zᴾ₂ : El (transp (λ xᴾ → ≡ᴾ aᴾ tᴾ xᴾ z) xᴾ₂ zᴾ₀ ≡ zᴾ₁))
          {zᴱ₀ : Elᴱ (≡ᴾ aᴾ tᴾ xᴾ₀) (≡ᴱ aᴾ aᴱ tᴾ tᴱ xᴾ₀ xᴱ₀) z zᴾ₀}{zᴱ₁ : Elᴱ (≡ᴾ aᴾ tᴾ xᴾ₁) (≡ᴱ aᴾ aᴱ tᴾ tᴱ xᴾ₁ xᴱ₁) z zᴾ₁}
--            (zᴱ₂ : El (zᴱ₀ ≡ zᴱ₁))
          {y : P x z}
          {yᴾ₀ : Pᴾ xᴾ₀ zᴾ₀ y}{yᴾ₁ : Pᴾ xᴾ₁ zᴾ₁ y}
        → Pᴱ {x} xᴾ₀ xᴱ₀ {z} zᴾ₀ zᴱ₀ y yᴾ₀
        → Pᴱ {x} xᴾ₁ xᴱ₁ {z} zᴾ₁ zᴱ₁ y yᴾ₁
    Pᴱ= = {!!}


{-
    Pᴱ= : {x₀ : El a}{x₁ : El a}(x₂ : El (x₀ ≡ x₁))
          {xᴾ₀ : El (aᴾ x₀)}{xᴾ₁ : El (aᴾ x₁)}(xᴾ₂ : El (transp xᴾ₀ ≡ xᴾ₁))
          {xᴱ₀ : Elᴱ aᴾ aᴱ x₀ xᴾ₀}{xᴱ₁ : Elᴱ aᴾ aᴱ x₁ xᴾ₁}(xᴱ₂ : xᴱ₀ ≡ xᴱ₁)
          {z₀ : El (t ≡ x₀)}{z₁ : El (t ≡ x₁)}(z₂ : z₀ ≡ z₁)
          {zᴾ₀ : El (≡ᴾ aᴾ tᴾ xᴾ₀ z₀)}{zᴾ₁ : El (≡ᴾ aᴾ tᴾ xᴾ₁ z₁)}(zᴾ₂ : zᴾ₀ ≡ zᴾ₁)
          {zᴱ₀ : Elᴱ (≡ᴾ aᴾ tᴾ xᴾ₀) (≡ᴱ aᴾ aᴱ tᴾ tᴱ xᴾ₀ xᴱ₀) z₀ zᴾ₀}{zᴱ₁ : Elᴱ (≡ᴾ aᴾ tᴾ xᴾ₁) (≡ᴱ aᴾ aᴱ tᴾ tᴱ xᴾ₁ xᴱ₁) z₁ zᴾ₁}(zᴱ₂ : zᴱ₀ ≡ zᴱ₁)
          {y₀ : P x₀ z₀}{y₁ : P x₁ z₁}(y₂ : y₀ ≡ y₁)
          {yᴾ₀ : Pᴾ xᴾ₀ zᴾ₀}{yᴾ₁ : Pᴾ xᴾ₁ zᴾ₁}(yᴾ₂ : yᴾ₀ ≡ yᴾ₁)
        → Pᴱ {x₀} xᴾ₀ xᴱ₀ {z₀} zᴾ₀ zᴱ₀ y₀ yᴾ₀
        → Pᴱ {x₁} xᴾ₁ xᴱ₁ {z₁} zᴾ₁ zᴱ₁ y₁ yᴾ₁
    Pᴱ= = ?
-}
{-
(J (aᴾ u)
         (aᴱ u)
         (λ uᴾ uᴱ → Pᴱ uᴾ uᴱ (≡ᴱ aᴾ aᴱ tᴾ tᴱ uᴾ uᴱ eq) refl (J a t P pr eq) (Jᴾ aᴾ tᴾ Pᴾ prᴾ uᴾ (≡ᴱ aᴾ aᴱ tᴾ tᴱ uᴾ uᴱ eq)))
         (J a t
            (λ u eq → Pᴱ (aᴱ u) refl (≡ᴱ aᴾ aᴱ tᴾ tᴱ (aᴱ u) refl eq) refl (J a t P pr eq) (Jᴾ aᴾ tᴾ Pᴾ prᴾ (aᴱ u) (≡ᴱ aᴾ aᴱ tᴾ tᴱ (aᴱ u) refl eq)))
            {!!}
            {u}
            eq)
         {uᴾ}
         uᴱ)


J (aᴾ t)
               (aᴱ t)
               (λ zᴾ zᴱ → Pᴱ (aᴱ t) refl (≡ᴱ aᴾ aᴱ zᴾ zᴱ (aᴱ t) refl refl) refl pr (Jᴾ aᴾ tᴾ Pᴾ prᴾ (aᴱ t) (≡ᴱ aᴾ aᴱ zᴾ zᴱ (aᴱ t) refl refl)))
               ?
               {tᴾ}
               tᴱ

J (aᴾ t)
            (aᴱ t)
            (λ zᴾ zᴱ → Pᴱ (aᴱ u) refl (≡ᴱ aᴾ aᴱ zᴾ zᴱ (aᴱ u) refl eq) refl (J a t P pr eq) (Jᴾ aᴾ zᴾ Pᴾ prᴾ (aᴱ u) (≡ᴱ aᴾ aᴱ zᴾ zᴱ (aᴱ u) refl eq)))
            ?
            {tᴾ}
            tᴱ


  = J (≡ᴾ aᴾ tᴾ uᴾ eq)
      (≡ᴱ aᴾ aᴱ tᴾ tᴱ uᴾ uᴱ eq)
      (λ eqᴾ eqᴱ → Pᴱ uᴾ uᴱ eqᴾ eqᴱ (J a t P pr eq) (Jᴾ aᴾ tᴾ Pᴾ prᴾ uᴾ eqᴾ))
      (J (aᴾ u)
         (aᴱ u)
         (λ uᴾ uᴱ → Pᴱ uᴾ uᴱ (≡ᴱ aᴾ aᴱ tᴾ tᴱ uᴾ uᴱ eq) refl (J a t P pr eq) (Jᴾ aᴾ tᴾ Pᴾ prᴾ uᴾ (≡ᴱ aᴾ aᴱ tᴾ tᴱ uᴾ uᴱ eq)))
         {!!}
         {uᴾ}
         uᴱ)
      {eqᴾ}
      eqᴱ
-}
-}
