
{-# OPTIONS --without-K --rewriting #-}

open import Function 

postulate
  _~_ : ∀{α}{A : Set α} → A → A → Set
{-# BUILTIN REWRITE _~_ #-}

postulate
  I : Set
  El : I → Set

Π : (a : I) → (El a → Set) → Set
Π a B = (x : El a) → B x

syntax Π a (λ x → B) = x ∈ a ⇒ B

lam : {a : I}{B : El a → Set}(t : (x : El a) → B x) → Π a B
lam t = t

syntax lam (λ x → t) = Λ x ⇒ t

postulate
  _≡_  : {a : I}(t : El a)(u : El a) → I
  refl : {a : I}{t : El a} → El (t ≡ t)
  J    : ∀ (a : I)(t : El a)(P : (x : El a)(z : El (t ≡ x)) → Set)(pr : P t refl){u : El a}(eq : El (t ≡ u)) → P u eq
  βJ   : ∀ {a : I}{t : El a}{P : (x : El a)(z : El (t ≡ x)) → Set}{pr : P t refl}
       → J a t P pr refl ~ pr
{-# REWRITE βJ #-}

infix 4 _≡_

--------------------------------------------------------------------------------

transp : ∀ {a}(p : El a → I){t u : El a} → El (t ≡ u) → El (p t) → El (p u)
transp {a} p {t}{u} eq pt = J a t (λ u _ → El (p u)) pt eq

Transp : ∀ {a}(P : El a → Set){t u : El a} → El (t ≡ u) → P t → P u
Transp {a} P {t}{u} eq pt = J a t (λ u _ → P u) pt eq

ap : ∀ {a b}(f : x ∈ a ⇒ El b){t u : El a}(eq : El (t ≡ u)) → El (f t ≡ f u)
ap {a}{b} f {t}{u} eq = J a t (λ u eq → El (f t ≡ f u)) refl eq

infixr 5 _◾_
_◾_ : ∀ {a}{t u v : El a} → El (t ≡ u) → El (u ≡ v) → El (t ≡ v)
_◾_ {a}{t}{u}{v} p q = transp (t ≡_) q p

_⁻¹ : ∀ {a}{t u : El a} → El (t ≡ u) → El (u ≡ t)
_⁻¹ {a}{t}{u} p = transp (_≡ t) p refl
infix 6 _⁻¹

inv : ∀ {a}{t u : El a}(eq : El (t ≡ u)) → El (eq ◾ eq ⁻¹ ≡ refl)
inv eq = J _ _ (λ u eq → El (eq ◾ eq ⁻¹ ≡ refl)) refl eq

inv⁻¹ : ∀ {a}{t u : El a}(eq : El (t ≡ u)) → El (eq ⁻¹ ◾ eq ≡ refl)
inv⁻¹ eq = J _ _ (λ u eq → El (eq ⁻¹ ◾ eq ≡ refl)) refl eq

-- Recursor transformation
--------------------------------------------------------------------------------

Setᴿ : (A⁰ A¹ : Set) → Set₁
Setᴿ A⁰ A¹ = A⁰ → A¹ → Set

Iᴿ : Setᴿ I I
Iᴿ i₀ i₁ = _ ∈ i₀ ⇒ El i₁

Elᴿ : {a⁰ a¹ : I}(aᴿ : Iᴿ a⁰ a¹) → Setᴿ (El a⁰) (El a¹)
Elᴿ {a⁰}{a¹} aᴿ x₀ x₁ = El (aᴿ x₀ ≡ x₁)

Πᴿ :
  {a⁰ a¹ : I}(aᴿ : Iᴿ a⁰ a¹)
  {B⁰ : El a⁰ → Set}{B¹ : El a¹ → Set}(Bᴿ : ∀ {x₀} {x₁} (xᵣ : Elᴿ aᴿ x₀ x₁) → Setᴿ (B⁰ x₀) (B¹ x₁))
  → Setᴿ (Π a⁰ B⁰) (Π a¹ B¹)
Πᴿ {a⁰}{a¹} aᴿ {B⁰}{B¹} Bᴿ f₀ f₁ = x₀ ∈ a⁰ ⇒ Bᴿ {x₀}{aᴿ x₀} refl (f₀ x₀) (f₁ (aᴿ x₀))

appᴿ :
  {a⁰ : I}          {a¹ : I}          (aᴿ : Iᴿ a⁰ a¹)
  {B⁰ : El a⁰ → Set}{B¹ : El a¹ → Set}(Bᴿ : ∀ {x₀}{x₁}(xᵣ : Elᴿ aᴿ x₀ x₁) → Setᴿ (B⁰ x₀) (B¹ x₁))
  {t⁰ : Π a⁰ B⁰}    {t¹ : Π a¹ B¹}    (tᴿ : Πᴿ aᴿ Bᴿ t⁰ t¹)
  {u⁰ : El a⁰}      {u¹ : El a¹}      (uᴿ : Elᴿ aᴿ u⁰ u¹)
  → Bᴿ uᴿ (t⁰ u⁰) (t¹ u¹)
appᴿ {a⁰}{a¹} aᴿ {B⁰}{B¹} Bᴿ {t⁰}{t¹} tᴿ {u⁰}{u¹} uᴿ =
  J a¹ (aᴿ u⁰) (λ u¹ uᴿ → Bᴿ {u⁰}{u¹} uᴿ (t⁰ u⁰) (t¹ u¹)) (tᴿ u⁰) uᴿ

-- appᴿ' :
--   {a⁰ : I}          {a¹ : I}          (aᴿ : Iᴿ a⁰ a¹)
--   {t⁰ : _ ∈ a⁰ ⇒ I }{t¹ : _ ∈ a¹ ⇒ I} (tᴿ : Πᴿ aᴿ (λ _ → Iᴿ) t⁰ t¹)
--   {u⁰ : El a⁰}      {u¹ : El a¹}      (uᴿ : Elᴿ aᴿ u⁰ u¹)
--   → Iᴿ (t⁰ u⁰) (t¹ u¹)
-- appᴿ' {a⁰}{a¹} aᴿ {t⁰}{t¹} tᴿ {u⁰}{u¹} uᴿ =
--   J a¹ (aᴿ u⁰) (λ u¹ uᴿ → Iᴿ (t⁰ u⁰) (t¹ u¹)) (tᴿ u⁰) uᴿ


lamᴿ :
  {a⁰ : I}           {a¹ : I}            (aᴿ : Iᴿ a⁰ a¹)
  {B⁰ : El a⁰ → Set} {B¹ : El a¹ → Set}  (Bᴿ : ∀ {x₀}{x₁}(xᵣ : Elᴿ aᴿ x₀ x₁) → Setᴿ (B⁰ x₀) (B¹ x₁))
  {t⁰ : ∀ x₀ → B⁰ x₀}{t¹ : ∀ x₁ → B¹ x₁} (tᴿ : ∀ {x₀}{x₁}(xᵣ : Elᴿ aᴿ x₀ x₁) → Bᴿ xᵣ (t⁰ x₀) (t¹ x₁))
  → Πᴿ aᴿ Bᴿ (Λ x₀ ⇒ t⁰ x₀) (Λ x₁ ⇒ t¹ x₁)
lamᴿ {a⁰}{a¹} aᴿ {B⁰}{B¹} Bᴿ {t⁰}{t¹} tᴿ = Λ x₀ ⇒ tᴿ {x₀}{aᴿ x₀} refl

≡ᴿ :
  {a⁰ : I}     {a¹ : I}     (aᴿ : Iᴿ a⁰ a¹)
  {t⁰ : El a⁰} {t¹ : El a¹} (tᴿ : Elᴿ aᴿ t⁰ t¹)
  {u⁰ : El a⁰} {u¹ : El a¹} (uᴿ : Elᴿ aᴿ u⁰ u¹)
  → Iᴿ (t⁰ ≡ u⁰) (t¹ ≡ u¹)
≡ᴿ {a⁰}{a¹} aᴿ {t⁰}{t¹} tᴿ {u⁰}{u¹} uᴿ = Λ eq ⇒ ((tᴿ ⁻¹ ◾ ap aᴿ eq) ◾ uᴿ)

reflᴿ :
  {a⁰ : I}     {a¹ : I}     (aᴿ : Iᴿ a⁰ a¹)
  {t⁰ : El a⁰} {t¹ : El a¹} (tᴿ : Elᴿ aᴿ t⁰ t¹)
  → Elᴿ (≡ᴿ aᴿ tᴿ tᴿ) refl refl
reflᴿ {a⁰}{a¹} aᴿ {t⁰}{t¹} tᴿ = inv⁻¹ tᴿ

Jᴿ :
  {a⁰ : I}     {a¹ : I}     (aᴿ : Iᴿ a⁰ a¹)
  {t⁰ : El a⁰} {t¹ : El a¹} (tᴿ : Elᴿ aᴿ t⁰ t¹)

  {P⁰ : ∀ x₀ → El (t⁰ ≡ x₀) → Set}
    {P¹ : ∀ x₁ → El (t¹ ≡ x₁) → Set}
      (Pᴿ : ∀ {x₀}{x₁}(xᵣ : Elᴿ aᴿ x₀ x₁){eq₀}{eq₁}(eqᵣ : Elᴿ (≡ᴿ aᴿ tᴿ xᵣ) eq₀ eq₁) → Setᴿ (P⁰ x₀ eq₀) (P¹ x₁ eq₁))

  {pr⁰ : P⁰ t⁰ refl}{pr¹ : P¹ t¹ refl}(prᴿ : Pᴿ tᴿ (reflᴿ aᴿ tᴿ) pr⁰ pr¹ )

  {u⁰ : El a⁰} {u¹ : El a¹} (uᴿ : Elᴿ aᴿ u⁰ u¹)
  {eq⁰ : El (t⁰ ≡ u⁰)}{eq¹ : El (t¹ ≡ u¹)}(eqᴿ : Elᴿ (≡ᴿ aᴿ tᴿ uᴿ) eq⁰ eq¹)

  → Pᴿ uᴿ eqᴿ (J a⁰ t⁰ P⁰ pr⁰ eq⁰) (J a¹ t¹ P¹ pr¹ eq¹)

Jᴿ {a⁰}{a¹} aᴿ {t⁰}{t¹} tᴿ {P⁰}{P¹} Pᴿ {pr⁰}{pr¹} prᴿ {u⁰}{u¹} uᴿ {eq⁰}{eq¹} eqᴿ  =
  J _ _
    (λ eq¹ eqᴿ → Pᴿ uᴿ eqᴿ (J a⁰ t⁰ P⁰ pr⁰ eq⁰) (J a¹ t¹ P¹ pr¹ eq¹))
    (J _ _
       (λ u¹ uᴿ → Pᴿ uᴿ refl (J a⁰ t⁰ P⁰ pr⁰ eq⁰) (J a¹ t¹ P¹ pr¹ (≡ᴿ aᴿ tᴿ uᴿ eq⁰)))
       (J _ _
          (λ u⁰ eq⁰ → Pᴿ refl refl (J a⁰ t⁰ P⁰ pr⁰ eq⁰)(J a¹ t¹ P¹ pr¹ (≡ᴿ aᴿ tᴿ refl eq⁰)))
          (Transp
            (λ x → Pᴿ refl x pr⁰ (J a¹ t¹ P¹ pr¹ (tᴿ ⁻¹))) (inv (ap (Λ y ⇒ (tᴿ ⁻¹ ◾ y)) (inv tᴿ ⁻¹)))
            (J (aᴿ t⁰ ≡ aᴿ t⁰) (tᴿ ◾ tᴿ ⁻¹)
              (λ x invtᴿ →
                 Pᴿ x 
                   (ap (Λ y ⇒ (tᴿ ⁻¹ ◾ y)) (invtᴿ ⁻¹) ◾ ap (Λ y ⇒ (tᴿ ⁻¹ ◾ y)) (invtᴿ ⁻¹) ⁻¹)
                   pr⁰ (J a¹ t¹ P¹ pr¹ (tᴿ ⁻¹ ◾ x)))
               (J _ t¹
                 (λ aᴿt⁰ tᴿ⁻¹ →
                     Pᴿ (tᴿ ◾ tᴿ⁻¹) refl pr⁰ (J a¹ t¹ P¹ pr¹ (tᴿ ⁻¹ ◾ tᴿ ◾ tᴿ⁻¹)))
                 (Transp (λ x → Pᴿ tᴿ x pr⁰ (J a¹ t¹ P¹ pr¹ (tᴿ ⁻¹ ◾ tᴿ))) (inv (inv⁻¹ tᴿ))
                   (J _ _
                     (λ x z → Pᴿ tᴿ (inv⁻¹ tᴿ ◾ z) pr⁰ (J a¹ t¹ P¹ pr¹ x))
                     prᴿ
                     (inv⁻¹ tᴿ ⁻¹)))
                 (tᴿ ⁻¹))
              (inv tᴿ)))
           eq⁰)
       uᴿ)
    eqᴿ
