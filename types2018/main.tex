\documentclass{easychair}

\usepackage{doc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage[T1]{fontenc}
\usepackage[utf8x]{inputenc}
\usepackage{amssymb,color,relsize}
\definecolor{keywordcolor}{rgb}{0.7, 0.1, 0.1}   % red
\definecolor{tacticcolor}{rgb}{0.1, 0.2, 0.6}    % blue
\definecolor{commentcolor}{rgb}{0.4, 0.4, 0.4}   % grey
\definecolor{symbolcolor}{rgb}{0.0, 0.1, 0.6}    % blue
\definecolor{sortcolor}{rgb}{0.1, 0.5, 0.1}      % green
\definecolor{attributecolor}{rgb}{0.7, 0.1, 0.1} % red


\newcommand{\Con}{\mathsf{Con}}
\newcommand{\Ty}{\mathsf{Ty}}

\usepackage{listings}
\def\lstlanguagefiles{lstlean.tex}
%% \lstset{language=lean}

\lstset{language=lean,breakatwhitespace,xleftmargin=\parindent}
\lstMakeShortInline"

\authorrunning{Altenkirch, Kaposi, Kovács, and von Raumer}
\titlerunning{Reducing Inductive-Inductive Types}

\title{Reducing Inductive-Inductive Types\\ to Indexed Inductive Types}

\author{
   Thorsten Altenkirch\inst{1}
\and
   Ambrus Kaposi\inst{2}
\and
   Andr\'as Kov\'acs\inst{2}
\and
   Jakob von Raumer\inst{1}
}

\institute{
   University of Nottingham, United Kingdom\\
   \email{thorsten.altenkirch@nott.ac.uk}, \email{jakob@von-raumer.de}
\and
   E\"otv\"os Lor\'and University, Budapest, Hungary\\
   \email{\{akaposi, kovacsandras\}@inf.elte.hu}
}

\begin{document}
\maketitle

\section{Motivation}

Many dependently typed languages which are built on foundations like the calculus of constructions (CoC) provide support for indexed inductive types. These are type families which are inductively defined using constructors that create an
instance over arbitrary elements of the base type (the type of $\mathbb{N}$-indexed vectors being a prominent example).
Most of these languages, e.\,g. Coq~\cite{coq} and Lean~\cite{lean}, don't allow for so called inductive-inductive types in which, for example, the user mutually defines a type $A$ and a family $B$ in which $A$ may appear in the index, and where constructors of $A$ and $B$ may refer to the constructors of each other.

Use cases for inductive-inductive types encompass important applications like the internalization of the syntax of dependent type theory itself (``type theory in type theory''~\cite{ttintt}) and the definition of the Cauchy construction of the real numbers ~\cite{hottbook}.
We thus ask the question whether inductive-inductive types can be emulated in a language that only provides indexed inductive types, that is, most importantly, how to construct an appropriate eliminator for these types.
The quesion whether this is possible has been brought up in previous studies on inductive-inductive types~\cite{nordvallinductive, gabephd}.

\section{Approach}

We are given a list of \emph{sorts} which we want to define and a list of \emph{constructors}, each with potential references to others.
First we define the category of \emph{typed algebras}, of which we aim to construct the initial element.
Next, we also define \emph{untyped algebras} which we obtain by erasing all the indices from sorts and constructors.
The initial object can be constructed using indexed induction only.
To make up for the missing indexing in the untyped algebras, we introduce an inductively defined \emph{well-typedness predicate} which contains the information that a given element of an untyped sort is really indexed by a given index element.
The desired initial object of the category of typed algebras is then given by using this predicate to filter for well-typed elements.
To prove initiality, we construct an inductive relation between elements of the initial untyped algebra and elements of an arbitrary typed algebra.
We then show that this relation is functional.

\section{Results \& Future Work}

The approach has been formalized for the example of a fragment of an
inductive-inductive syntax of type theory in Agda, and ported to
Lean. We aim to use Lean's meta-language~\cite{lean-meta} to automate the
construction and provide a user-defined command to create
inductive-inductive types.

\vspace{-0.6em}

\section{Example}

As a first example, we looked at the type $\Con$ of \emph{contexts} and the type $\Ty$ of \emph{types} in a formalized syntax of a type theory, with a constructor $\mathsf{nil} : \Con$ for an empty context, $\mathsf{ext} : \prod_{\Gamma : \Con} \Ty(\Gamma) \to \Con$ for context extension, $\mathsf{unit} : \prod_{\Gamma : \Con} \Ty(\Gamma)$ for an atomic unit type and $\mathsf{pi} :
\prod_{\Gamma : \Con, A : \Ty(\Gamma)} \Ty(\mathsf{ext}(\Gamma, A)) \to \Ty(\Gamma)$ for a $\Pi$-type.
For this example the typed and untyped algebras are represented by the following Lean code:

\noindent\begin{minipage}{.25\textwidth}
\begin{lstlisting}
structure CT :=
  (C : Type u)
  (T : C → Type u)
  (nil : C)
  (ext : Π Γ, T Γ → C)
  (unit : Π (Γ : C), T Γ)
  (pi : Π Γ A, 
    T (ext Γ A) → T Γ)

structure CT' :=
  (C : Type u)
  (T : Type u)
  (nil : C)
  (ext : C → T → C)
  (unit : C → T)
  (pi : C → T → T → T)
\end{lstlisting}
\end{minipage}\hfill
\begin{minipage}{.65\textwidth}
\begin{lstlisting}
inductive S'0 : bool → Type u
| nil : S'0 ff
| ext : S'0 ff → S'0 tt → S'0 ff
| unit : S'0 ff → S'0 tt
| pi : S'0 ff → S'0 tt → S'0 tt → S'0 tt

parameters (M : CT)
def rel_arg : bool → Type u
| ff := M.C
| tt := Σ γ, M.T γ
inductive rel : Π b, S'0 b → rel_arg b → Prop
| nil : rel ff S'0.nil M.nil
| ext : Π Γ A γ a, rel ff Γ γ → rel tt A ⟨γ, a⟩
    → rel ff (S'0.ext Γ A) (M.ext γ a)
| unit : Π Γ γ, rel ff Γ γ → rel tt (S'0.unit Γ) ⟨γ, M.unit γ⟩
| pi : Π Γ A B γ a b, rel ff Γ γ → rel tt A ⟨γ, a⟩ → 
    rel tt B ⟨M.ext γ a, b⟩ → rel tt (S'0.pi Γ A B) ⟨γ, M.pi γ a b⟩
\end{lstlisting}
\end{minipage}

\vspace{-0.6em}

\bibliographystyle{plain}
\bibliography{references}

\end{document}
