\documentclass{easychair}

\usepackage{doc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage[T1]{fontenc}
\usepackage[utf8x]{inputenc}
\usepackage{proof}
\usepackage{amssymb,color,relsize}

\input{abbrevs.tex}

\title{Constructing inductive-inductive types using a domain-specific
  type theory\thanks{This work was supported by the European Union, co-financed by
  the European Social Fund (EFOP-3.6.3-VEKOP-16-2017-00002)}}
\titlerunning{Constructing inductive-inductive types using a DSTT}

\author{
   Thorsten Altenkirch\inst{1}
\and
   Péter Diviánszky\inst{2}
\and
   Ambrus Kaposi\inst{2}
\and
   Andr\'as Kov\'acs\inst{2}
}

\authorrunning{Altenkirch, Diviánszky, Kaposi, Kovács}

\institute{
   University of Nottingham, United Kingdom\\
   \email{thorsten.altenkirch@nott.ac.uk}
\and
   E\"otv\"os Lor\'and University, Budapest, Hungary\\
   \email{\{divip|akaposi|kovacsandras\}@inf.elte.hu}
}

\begin{document}
\maketitle

{\em Inductive-inductive types} (IITs, \cite{nordvallinductive}) allow
the mutual definition of a type and a family over the type, thus
generalising mutual inductive types and indexed families of
types. Examples of IITs are the well-typed syntax of type theory
\cite{chapman09eatitself} or the dense completion of an ordered set
\cite[Example 3.3]{nordvallinductive}. In this talk we {\em define}
signatures for IITs using a {\em domain-specific type theory} (DSTT)
where a context is a signature of an IIT. By induction on the syntax
of the DSTT, we {\em describe} what it means to have constructors and
dependent eliminator for each signature (following \cite{hiit}). Then
we show that the initial algebras and the dependent eliminator {\em
  exist}: the initial algebras will be given by terms of the DSTT and
the eliminator will be given by the logical predicate interpretation
\cite{bernardy12parametricity}. Apart from having an embedded syntax
of the DSTT, the metatheory is usual intensional type theory.

\paragraph{Signatures of inductive-inductive types.} Our DSTT has an
empty universe (we write underline for $\El$) and a function space
where the domain is small (that is, the domain can only be a neutral
term) and there is no $\lambda$. We write $a\Ra B$ when $B$ does not
mention the input. We write $\Gamma\vdash\sigma:\Delta$ for a context
morphism from $\Gamma$ to $\Delta$ (a list of terms which provide all
types in $\Delta$) and $A[\sigma]$, $t[\sigma]$ for substituted types
and terms.
\[
\text{Contexts and variables:}
\hspace{2em}
\begin{gathered}
  \infer{\vdash \cdot}{\vspace{1em}}
\end{gathered}
\hspace{2em}
\begin{gathered}
  \infer{\vdash \Gamma,x:A}{\Gamma\vdash A}
\end{gathered}
\hspace{2em}
\begin{gathered}
  \infer{\Gamma,x:A\vdash x : A}{\Gamma\vdash A}
\end{gathered}
\hspace{2em}
\begin{gathered}
  \infer{\Gamma,y:B\vdash x : A}{\Gamma\vdash x : A && \Gamma\vdash B}
\end{gathered}
\hspace{10em}
\]
\[
\text{Universe:}
\hspace{2em}
\begin{gathered}
  \infer{\Gamma \vdash \U}{\vspace{1em}}
\end{gathered}
\hspace{2em}
\begin{gathered}
  \infer{\Gamma \vdash \underline{a}}{\Gamma \vdash a : \U}
\end{gathered}
\hspace{50em}
\]
\[
\text{Functions with small domain:}
\hspace{1em}
\begin{gathered}
  \infer{\Gamma \vdash (x:a)\Ra B}{\Gamma \vdash a : \U && \Gamma,x:\underline{a} \vdash B}
\end{gathered}
\hspace{2em}
\begin{gathered}
  \infer{\Gamma \vdash t\, u : B[x \mapsto u]}{\Gamma \vdash t : (x:a)\Ra B && \Gamma \vdash u : \underline{a}}
\end{gathered}
\hspace{50em}
\]
A context $\Gamma$ in this DSTT is a signature for an IIT. For
example, a subset of the intrinsic syntax of type theory (contexts and
types) is given by the signature
$\cdot,\,Con:\U,\,Ty:Con\Ra\U,\,\bullet:\underline{Con},\,
\rhd:(\mathit{\Gamma}:Con)\Ra Ty\,\mathit{\Gamma}\Ra\underline{Con},\,
U:(\mathit{\Gamma}:Con)\Ra
\underline{Ty\,\mathit{\Gamma}},\,\Pi:(\mathit{\Gamma}:Con)\Ra(A:Ty\,\mathit{\Gamma})\Ra
Ty\,(\mathit{\Gamma}\rhd A)\Ra \underline{Ty\,\mathit{\Gamma}}$. Our
simpler running example is $\Theta := (\cdot,\,
N:\U,z:\underline{N},\,s:N\Ra\underline{N})$.

\paragraph{Notion of algebra ($\blank^\C$).}

By induction on the syntax of the DSTT we define the operation
$\blank^\C$. Given a signature $\Gamma$ (a context), $\Gamma^\C$ is
the notion of algebra corresponding to it. The action of the operation
on contexts, types, context morphisms and terms is specified as
follows.
\[
\begin{gathered}
  \infer{\Gamma^\C\in\Set}{\vdash\Gamma}
\end{gathered}
\hspace{2em}
\begin{gathered}
  \infer{A^\C\in\Gamma^\C\ra\Set}{\Gamma\vdash A}
\end{gathered}
\hspace{2em}
\begin{gathered}
  \infer{\sigma^\C\in\Gamma^\C\ra \Delta^\C}{\Gamma\vdash \sigma:\Delta}
\end{gathered}
\hspace{2em}
\begin{gathered}
  \infer{t^\C\in(\gamma\in\Gamma^\C)\ra A^\C(\gamma)}{\Gamma\vdash t:A}
\end{gathered}
\]
$\blank^\C$ is the standard (set-theoretic) interpretation of the
DSTT. For example, $(\Gamma,x:A)^\C := (\gamma:\Gamma^\C)\times
A^\C(\gamma)$, $((x:a)\ra B)^\C(\gamma) := (\alpha\in a^\C(\gamma))\ra
B^\C(\gamma,\alpha)$, $\U^\C(\gamma):=\Set$ and
${\underline{a}}^\C(\gamma) := a^\C(\gamma)$. Thus for our running
example we get natural number algebras: $\Theta^\C = (n\in \Set)\times
n \times (n \ra n)$.

\paragraph{Notion of family over an algebra ($\blank^\F$).}

For a signature $\Gamma$, $\Gamma^\F$ is the logical predicate
\cite{bernardy12parametricity} over $\Gamma^\C$. If
$\gamma\in\Gamma^\C$ is the initial algebra, $\Gamma^\C(\gamma)$ gives
the parameters of the eliminator (motives and methods). We have
$\Theta^\F(n,z,s) = (n_F\in n\ra\Set)\times n_F(z)\times\big((x:n)\ra
n_F(x)\ra n_F(s(x))\big)$ for any $(n,z,s)\in\Theta^\C$. For a context
$\Gamma$ we have $\Gamma^\F\in\Gamma^\C\ra\Set$ and for a context
morphism $\Gamma\vdash\sigma:\Delta$ we get
$\sigma^\F\in\Gamma^\F(\gamma)\ra\Delta^\F(\sigma^\C(\gamma))$. We
omit the specification for types and terms for reasons of space and we
will do similarly for the operations below.

\paragraph{Notion of section ($\blank^\E$).}

For a signature $\Gamma$, an algebra $\gamma\in\Gamma^\C$ and a family
$\gamma_F\in\Gamma^\F(\gamma)$, $\Gamma^\E(\gamma,\gamma_F)$ is the
type of sections from $\gamma$ to $\gamma_F$. For $\Theta$ this says
that given an algebra $(n,z,s)$ and a family $(n_F, z_F, s_F)$ over
it, we get a function $n_F$ which maps $z$ to $z_F$ and $s$ to $s_F$:
$\Theta^\E((n,z,s),(n_F,z_F,s_F))= \big(n_S\in (\alpha\in n)\ra
n_F(\alpha)\big)\times\big(n_S(z)=z_F\big)\times\big(n_S(s(x))=s_F(n_S(x))\big)$.

\paragraph{Existence of constructors ($\blank^{\CC_{\blank}}$).} For every
signature $\Gamma$, we would like to have an element of $\Gamma^\C$,
the initial algebra or the type and term constructors of the IIT. We
define these as terms of the DSTT. E.g.\ for $\Theta$, we would like
to generate $(\{t\,|\,\Theta\vdash t:\underline{n}\}, z, \lambda
x.s\,x) \in\Theta^\C$. We fix a signature $\Omega$ and define the
operator $\blank^{\CC_\Omega}$ by induction on the syntax of the
DSTT. For a context $\Gamma$, we have $\Gamma^{\CC_\Omega}\in
(\Omega\vdash\sigma:\Gamma)\ra\Gamma^\C$ and for a type $\Gamma\vdash
A$, we have
$A^{\CC_\Omega}\in\mbox{$(\Omega\vdash\sigma:\Gamma)$}\times(\Gamma\vdash
t:A[\sigma])\ra A^\C(\Gamma^{\CC_\Omega}(\sigma))$. On the universe,
$\blank^{\CC_\Omega}$ returns terms of the given type:
$\U^{\CC_\Omega}(\sigma,a):= \{t\,|\,\Omega\vdash t:\underline{a}\}$,
and for ${\underline{a}}$ it simply returns the term:
${\underline{a}}^{\CC_\Omega}(\sigma,t):= t$. The initial
$\Omega$-algebra is given by
$\Omega^{\CC_\Omega}(\id_\Omega)\in\Omega^\C$ where $\id_\Omega$ is
the identity context morphism on $\Omega$.

\paragraph{Existence of dependent eliminator ($\blank^{\EE_{\blank}}$).}

After fixing a signature $\Omega$ and given motives and methods
$\omega\in\Omega^\F(\Omega^\CC(\id_\Omega))$, we define the
operation $\blank^{\EE_{\omega}}$ by induction on the syntax of the
DSTT. For a context $\Gamma$ it gives
$\Gamma^{\EE_{\omega}}\in(\Omega\vdash\sigma:\Gamma)\ra\Gamma^\E(\Gamma^\CC(\sigma),
\sigma^\F(\omega))$. $\U^{\EE_{\omega}}$ works by calling the
$\blank^\F$ operation on its input. Again, the identity substitution
is used when deriving the eliminator itself (note that
${\id_\Omega}^\F(\omega)=\omega$):
$\Omega^{\EE_\omega}(\id_\Omega)\in\Omega^\E(\Omega^\CC(\id_\Omega),
\omega)$. For our example, given
$\theta\in\Theta^\F(\Theta^\CC(\id_\Theta))$, we get
$\Theta^{\EE_{\theta}}(\id_\Theta)=\big(\lambda
t.\mathsf{transport}(t^\CC(\id_\Theta), t^\F(\theta)), \refl,
\lambda t\,t_F.\mathsf{J}(\refl, t^\CC(\id_\Theta))\big)$.

\paragraph{Further work.}

An Agda formalisation of the operations $\blank^{\CC_{\blank}}$ and
$\blank^{\EE_{\blank}}$ is on the way (the previous operations are
formalised). We would also like to extend them to a larger DSTT
describing infinitary constructors and equality constructors as well
(quotient inductive-inductive types). A Haskell implementation of a
type theory with IITs using this approach would be also interesting to
experiment with.
\vspace{-0.7em}
\bibliographystyle{plain}
\bibliography{references}

\end{document}
